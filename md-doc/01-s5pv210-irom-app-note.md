## S5PV210 | 微处理器启动流程

------

[TOC]



- ### S5PV210启动概述

S5PV210支持从多种设备启动，如OneNAND、NAND、MMC等。S5PV210的启动框图如图所示，BL0是指S5PV210微处理器的IROM中固化的启动代码，BL1是指在IRAM自动从外存储器（NAND、SD、USB）中复制的uboot.bin二进制文件的头16KB代码，BL2是指在代码重定向后在内存中执行的UBOOT的完整代码。

  三者之间的关系是：BL0将BL1加载到IRAM，然后BL1在IRAM中运行并将BL2加载到SDRAM，BL2加载嵌入式操作系统。BL是BootLoader的简称。

  S5PV210上电将从IROM处执行固化的启动代码BL0，它对时钟等初始化、对启动设置进行判断，并从启动设备中复制BL1（最大16KB）到IRAM（地址0xD0020000处，其中0xD002 0000之前的16B存储BL1的校验信息和BL1中的尺寸）中，并对BL1进行校验，校验成功后转入BL1进行执行。BL1执行完成后，开始执行BL2，BL2加载内核，把OS在SDRAM中运行起来。

<img src="https://img-blog.csdnimg.cn/20200313215338178.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L0lfZmVpZ2U=,size_16,color_FFFFFF,t_70" alt="img" style="zoom: 80%;" />



- ### S5PV210的启动顺序

```bash
①iROM可以进行初始引导：初始化系统时钟，设备专用控制器和引导设备。

②iROM引导代码可以将引导加载程序加载到SRAM。引导加载程序称为BL1。
然后，iROM在安全启动模式下验证BL1的完整性。

③将执行BL1：BL1将在SRAM上加载剩余的引导加载程序，称为BL2。
然后在安全启动模式下，BL1验证BL2的完整性。

④将执行BL2：BL2初始化DRAM控制器，然后将OS数据加载到SDRAM。

⑤最后，跳转到OS的起始地址。那将为使用系统创造良好的环境。
```



- ### iROM（BL0）的启动顺序

```bash
1.禁用看门狗定时器

2.初始化指令缓存

3.初始化堆栈区域（请参见“内存映射”）

4.初始化堆区域。（请参见“内存图”）

5.初始化块设备复制功能。（请参见“设备复制功能”）

6.初始化PLL并设置系统时钟。（请参阅“时钟配置”）

7.将BL1复制到内部SRAM区域（请参见“设备复制功能”）

8.验证BL1的校验和。
如果校验和失败，则iROM将尝试第二次启动。（SD / MMC通道2）

9.检查是否为安全启动模式。
如果安全密钥值是在S5PV210中写入的，则为安全启动模式。
如果是安全启动模式，请验证BL1的完整性。

10.跳转到BL1的起始地址。
```



- ### V210启动流程图

<img src="https://img-blog.csdnimg.cn/20200313220300400.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L0lfZmVpZ2U=,size_16,color_FFFFFF,t_70" alt="img" style="zoom:80%;" />



- ### 第一次启动失败时的iROM第二次启动顺序

<img src="https://img-blog.csdnimg.cn/20200313221145938.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L0lfZmVpZ2U=,size_16,color_FFFFFF,t_70" alt="img" style="zoom:80%;" />



- ### 用于引导代码描述的标题信息数据

<img src="https://img-blog.csdnimg.cn/20200313230941895.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L0lfZmVpZ2U=,size_16,color_FFFFFF,t_70" style="zoom:80%;" />



- ### 编写校验和示例代码

```c
for(count=0;count< dataLength;count+=1)
{
    buffer = (*(volatile u8*)(uBlAddr+count));
    checkSum = checkSum + buffer;
}
//-count 变量是unsigned int类型。
//-dataLength 变量是无符号的int类型。 它包含BL1的大小（字节）。
//-buffer 变量是无符号的短类型。 用于从BL1读取1字节数据。
//-checkSum 变量是unsigned int类型。 它包含BL1的总和。
```

------

具体的实例代码(为BIN文件添加校验和文件头)：

```c
/*******************************************************************
 *   > File Name: mkv210_image.c
 *   > Author: fly
 *   > Mail: xxxxxx@icode.net
 *   > Create Time: 2021-06-17  4/24  12:03:22 +0800
 *   > Note: 将USB启动时使用的BIN文件制作得到SD启动的Image
 *          计算校验和，添加16字节文件头，校验和写入第8字节处
 *================================================================*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define ERR_STR                 strerror(errno)
#define SPL_HEADER_SIZE         (16)
#define SPL_HEADER              "@S5PV210$$$$****"
#define IMG_SIZE                (16*1204)

char *mk_getCheckSumFile(char *binName)
{
    static char checkSumFileName[128] = {0};
    snprintf(checkSumFileName, 128, "%s%s", binName, ".sd");
    return (char*)checkSumFileName;
}

long mk_getFileLen(FILE* fp)
{
    static long fileLen = 0;
    fseek(fp, 0L, SEEK_END);
    fileLen = ftell(fp);
    fseek(fp, 0L, SEEK_SET);
    return fileLen;
}

int main(int argc, char* argv[])
{
    FILE* fps, *fpd;
    long nbytes, fileLen;
    unsigned int checksum, count;
    char *BUF = NULL, *pBUF = NULL;
    int i;

    if(argc != 2){
        printf("Usage: %s <bin-file>\n", argv[0]);exit(EXIT_FAILURE);
    }

    /* 打开源BIN文件 */
    fps = fopen(argv[1], "rb");
    if (fps == NULL){
        printf("fopen %s err: %s\n", argv[1], ERR_STR);
        exit(EXIT_FAILURE);
    }

    /* 创建目标BIN文件 */
    fpd = fopen(mk_getCheckSumFile(argv[1]), "w+b");
    if (fpd == NULL){
        printf("fopen %s err: %s\n", mk_getCheckSumFile(argv[1]), ERR_STR);
        fclose(fps);exit(EXIT_FAILURE);
    }

    /* 获取源文件大小 */
    fileLen = mk_getFileLen(fps);
    if(fileLen < (IMG_SIZE - SPL_HEADER_SIZE)){
        count = fileLen;
    }else{
        count = IMG_SIZE - SPL_HEADER_SIZE;
    }

    BUF = (char *)malloc(IMG_SIZE);/* malloc 16KB BUF */
    if (BUF == NULL){
        printf("malloc err: %s\n", ERR_STR);
        fclose(fps);fclose(fpd);
        exit(EXIT_FAILURE);
    }
    memcpy(&BUF[0], SPL_HEADER, SPL_HEADER_SIZE);
    nbytes = fread(BUF+SPL_HEADER_SIZE, 1, count, fps);

    /* 计算文件检验和 */
    pBUF = BUF + SPL_HEADER_SIZE;
    for(i = 0, checksum = 0; i< IMG_SIZE - SPL_HEADER_SIZE; i++)
    {
        checksum += (0x000000FF) & *pBUF++;
    }
    pBUF = BUF + 8;
    *((unsigned int *)pBUF) = checksum;

    /* 将校验和源文件写入目标文件 */
    fwrite(BUF, 1, IMG_SIZE, fpd);

    printf("the checksum 0x%08X for %ldbytes, output: %s\n", \
            checksum, fileLen, mk_getCheckSumFile(argv[1]));

    free(BUF);
    fclose(fps);
    fclose(fpd);

    return 0;
}
```

编译Makefile文件：

```makefile
.PHONY: all clean

CC              = gcc
SRC             = ${wildcard *.c}
BIN             = ${patsubst %.c, %, $(SRC)}
CFLAGS  	    = -g -Wall
RM              = rm -rf

all:$(BIN)

$(BIN):%:%.c
        $(CC) -o $@ $^ $(CFALGS)

clean:
        $(RM) a.out $(BIN) .*.*.sw? *.sd
```

