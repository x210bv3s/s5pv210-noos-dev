## S5PV210 | IIC总线应用实例（裸机G-Sensor使用）

- ### 电路原理

![HW-IIC](..\res\IIC-BUS_Interface\HW-IIC.png)

------

![HW-GSensor](..\res\IIC-BUS_Interface\HW-GSensor.PNG)

图中G-Sensor（KXTF9-2050）通过第一组IIC总线（I2C_SDA0、I2C_SCL0）与主机进行通信。PWMTOUT3拉高开启G-Sensor的电源。

- ### 资料查阅

Datasheet关于串口部分资料详见芯片手册如下章节：   
<u>Section 8 CONNECTIVITY/STORAGE-》2 IIC-BUS INTERFACE  </u>

