# s5pv210-LinuxKernel编译log-20221110

## 1.代码及工作路径

### 1-1.代码仓库

> [https://gitee.com/x210bv3s/qt_x210v3s_160307/tree/master/kernel](https://gitee.com/x210bv3s/qt_x210v3s_160307/tree/master/kernel)

### 1-2.工作目录

```bash
fly@fly-vm:kernel$ pwd
/home/fly/project/x210bv3s/qt_x210v3s_160307/kernel
```

## 2.编译

> `fly@fly-vm:kernel$ make distclean`
>
> `fly@fly-vm:kernel$ make v210_defconfig`
>
> `fly@fly-vm:kernel$ make -j4`

```bash
fly@fly-vm:kernel$ ls arch/arm/configs/
v210_defconfig  x210ii_initrd_defconfig  x210ii_qt_defconfig
fly@fly-vm:kernel$ make distclean
  CLEAN   arch/arm/boot/compressed
  CLEAN   arch/arm/boot
  CLEAN   /home/fly/project/x210bv3s/qt_x210v3s_160307/kernel
  CLEAN   arch/arm/kernel
  CLEAN   drivers/char
  CLEAN   drivers/video/logo
  CLEAN   firmware
  CLEAN   kernel
  CLEAN   lib
  CLEAN   .tmp_versions
  CLEAN   vmlinux System.map .tmp_kallsyms2.S .tmp_kallsyms1.o .tmp_kallsyms2.o .tmp_kallsyms1.S .tmp_vmlinux1 .tmp_vmlinux2 .tmp_System.map
  CLEAN   scripts/basic
  CLEAN   scripts/kconfig
  CLEAN   scripts/mod
  CLEAN   scripts
  CLEAN   include/config include/generated
  CLEAN   .config .version include/linux/version.h Module.symvers
fly@fly-vm:kernel$ make v210_defconfig
#
# configuration written to .config
#
fly@fly-vm:kernel$ make -j4
```

## 3.编译LOG

```bash
fly@fly-vm:kernel$ make -j4
scripts/kconfig/conf -s arch/arm/Kconfig
  CHK     include/linux/version.h
  UPD     include/linux/version.h
  CHK     include/generated/utsrelease.h
  UPD     include/generated/utsrelease.h
  HOSTCC  scripts/kallsyms
  HOSTCC  scripts/pnmtologo
  CC      scripts/mod/empty.o
  HOSTCC  scripts/mod/mk_elfconfig
  MKELF   scripts/mod/elfconfig.h
  HOSTCC  scripts/mod/file2alias.o
  Generating include/generated/mach-types.h
  HOSTCC  scripts/conmakehash
  CC      kernel/bounds.s
  HOSTCC  scripts/mod/modpost.o
  GEN     include/generated/bounds.h
  CC      arch/arm/kernel/asm-offsets.s
  HOSTCC  scripts/mod/sumversion.o
  HOSTLD  scripts/mod/modpost
  GEN     include/generated/asm-offsets.h
  CALL    scripts/checksyscalls.sh
  CHK     include/generated/compile.h
  CC      init/do_mounts.o
  CC      init/main.o
  LD      usr/built-in.o
  CC      arch/arm/kernel/compat.o
  UPD     include/generated/compile.h
  CC      init/do_mounts_rd.o
  CC      arch/arm/kernel/elf.o
  AS      arch/arm/kernel/entry-armv.o
  CC      init/noinitramfs.o
  AS      arch/arm/kernel/entry-common.o
  CC      init/calibrate.o
  CC      arch/arm/kernel/irq.o
  CC      init/version.o
  CC      arch/arm/kernel/process.o
  CC      arch/arm/kernel/ptrace.o
  CC      arch/arm/mm/dma-mapping.o
  LD      init/mounts.o
  LD      init/built-in.o
  CC      arch/arm/common/vic.o
  CC      arch/arm/kernel/return_address.o
  CC      arch/arm/common/pl330.o
  CC      arch/arm/kernel/setup.o
  CC      arch/arm/mm/extable.o
  CC      arch/arm/kernel/signal.o
  CC      arch/arm/mm/fault.o
arch/arm/kernel/setup.c: In function 'setup_arch':
arch/arm/kernel/setup.c:692: warning: too few arguments for format
  CC      arch/arm/mm/init.o
  CC      arch/arm/kernel/sys_arm.o
  CC      arch/arm/kernel/stacktrace.o
  LD      arch/arm/common/built-in.o
  CC      arch/arm/mach-s5pv210/cpu.o
arch/arm/mm/init.c: In function 'mem_init':
arch/arm/mm/init.c:644: warning: format '%08lx' expects type 'long unsigned int', but argument 12 has type 'unsigned int'
  CC      arch/arm/kernel/time.o
  CC      arch/arm/kernel/traps.o
  CC      arch/arm/mach-s5pv210/init.o
  CC      arch/arm/kernel/armksyms.o
  CC      arch/arm/mm/iomap.o
  CC      arch/arm/mach-s5pv210/clock.o
  CC      arch/arm/kernel/module.o
  CC      arch/arm/kernel/sys_oabi-compat.o
  CC      arch/arm/mm/fault-armv.o
  CC      arch/arm/mach-s5pv210/dma.o
  CC      arch/arm/kernel/thumbee.o
  CC      arch/arm/kernel/pmu.o
  CC      arch/arm/mm/flush.o
  CC      arch/arm/kernel/io.o
  CC      arch/arm/mach-s5pv210/gpiolib.o
  AS      arch/arm/kernel/head.o
  CC      arch/arm/kernel/init_task.o
  LDS     arch/arm/kernel/vmlinux.lds
  LD      arch/arm/kernel/built-in.o
  CC      arch/arm/mm/ioremap.o
  CC      arch/arm/mach-s5pv210/pmu.o
  CC      arch/arm/mm/mmap.o
  CC      arch/arm/mach-s5pv210/setup-i2c0.o
  CC      arch/arm/mach-s5pv210/setup-i2c1.o
  CC      arch/arm/mm/pgd.o
  CC      arch/arm/mach-s5pv210/setup-i2c2.o
  CC      arch/arm/plat-s5p/dev-uart.o
  CC      arch/arm/mm/mmu.o
  CC      arch/arm/mach-s5pv210/pm.o
  CC      arch/arm/plat-s5p/cpu.o
  CC      arch/arm/mm/vmregion.o
  AS      arch/arm/mach-s5pv210/sleep.o
  CC      arch/arm/plat-s5p/clock.o
  CC      arch/arm/mach-s5pv210/power-domain.o
  CC      arch/arm/mm/proc-syms.o
  CC      arch/arm/plat-s5p/irq.o
  CC      arch/arm/mm/alignment.o
  CC      arch/arm/mach-s5pv210/mach-x210.o
  AS      arch/arm/mm/abort-ev7.o
  CC      arch/arm/plat-s5p/devs.o
  AS      arch/arm/mm/pabort-v7.o
  AS      arch/arm/mm/cache-v7.o
  CC      arch/arm/mm/copypage-v6.o
  CC      arch/arm/mm/context.o
  AS      arch/arm/mm/tlb-v7.o
  AS      arch/arm/mm/proc-v7.o
  LD      arch/arm/mm/built-in.o
  CC      arch/arm/plat-s5p/bootmem.o
  CC      arch/arm/plat-s5p/irq-eint.o
arch/arm/mach-s5pv210/include/mach/system.h:19: warning: 'arch_idle' defined but not used
arch/arm/mach-s5pv210/include/mach/system.h:24: warning: 'arch_reset' defined but not used
arch/arm/mach-s5pv210/mach-x210.c:1338: warning: 's5k5ba_power_en' defined but not used
  CC      arch/arm/plat-samsung/init.o
  CC      arch/arm/plat-s5p/irq-eint-group.o
  CC      arch/arm/plat-s5p/pm.o
  CC      arch/arm/plat-samsung/clock.o
  CC      arch/arm/mach-s5pv210/pm-wifi.o
  CC      arch/arm/plat-s5p/irq-pm.o
  CC      arch/arm/plat-s5p/hr-time-rtc.o
  CC      arch/arm/plat-samsung/pwm-clock.o
  CC      arch/arm/plat-s5p/dev-mfc.o
  CC      arch/arm/plat-s5p/setup-mfc.o
  CC      arch/arm/plat-samsung/gpio.o
  CC      arch/arm/mach-s5pv210/smdkc110-rtc.o
  CC      arch/arm/vfp/vfpmodule.o
  CC      arch/arm/plat-samsung/gpio-config.o
  LD      arch/arm/plat-s5p/built-in.o
  CC      kernel/sched.o
arch/arm/mach-s5pv210/smdkc110-rtc.c:437: warning: 'ticnt_save' defined but not used
  CC      arch/arm/plat-samsung/gpiolib.o
  AS      arch/arm/vfp/entry.o
  AS      arch/arm/vfp/vfphw.o
  CC      arch/arm/mach-s5pv210/setup-sdhci.o
  CC      arch/arm/vfp/vfpsingle.o
  CC      arch/arm/plat-samsung/clock-clksrc.o
  CC      arch/arm/plat-samsung/irq-uart.o
  CC      arch/arm/vfp/vfpdouble.o
  CC      arch/arm/plat-samsung/irq-vic-timer.o
  CC      arch/arm/mach-s5pv210/dev-audio.o
  CC      arch/arm/plat-samsung/dev-hsmmc.o
  CC      arch/arm/mach-s5pv210/dev-fiqdbg.o
  LD      arch/arm/vfp/vfp.o
  LD      arch/arm/vfp/built-in.o
  CC      arch/arm/plat-samsung/dev-hsmmc1.o
  CC      mm/bootmem.o
  CC      arch/arm/plat-samsung/dev-hsmmc2.o
  CC      arch/arm/mach-s5pv210/dev-onenand.o
  CC      arch/arm/plat-samsung/dev-hsmmc3.o
  CC      arch/arm/mach-s5pv210/adc.o
  CC      mm/filemap.o
  CC      arch/arm/plat-samsung/dev-i2c0.o
  CC      arch/arm/plat-samsung/dev-i2c1.o
  CC      kernel/fork.o
  CC      arch/arm/mach-s5pv210/setup-fb.o
  CC      arch/arm/plat-samsung/dev-i2c2.o
  CC      arch/arm/mach-s5pv210/setup-fimc0.o
  CC      arch/arm/plat-samsung/dev-uart.o
  CC      arch/arm/mach-s5pv210/setup-fimc1.o
  CC      arch/arm/mach-s5pv210/setup-fimc2.o
  CC      arch/arm/plat-samsung/dev-wdt.o
  CC      arch/arm/mach-s5pv210/setup-csis.o
  CC      arch/arm/plat-samsung/s3c-pl330.o
  CC      arch/arm/mach-s5pv210/cpuidle.o
  CC      mm/mempool.o
  AS      arch/arm/mach-s5pv210/didle.o
  CC      arch/arm/mach-s5pv210/button-x210.o
  CC      kernel/exec_domain.o
  CC      mm/oom_kill.o
  CC      arch/arm/plat-samsung/pm.o
  LD      arch/arm/mach-s5pv210/built-in.o
  CC      kernel/panic.o
  CC      fs/open.o
arch/arm/plat-samsung/pm.c: In function '__DMC_PhyDriving':
arch/arm/plat-samsung/pm.c:125: warning: unused variable 'map_reg'
arch/arm/plat-samsung/pm.c: In function '__DMC_SetDQSn':
arch/arm/plat-samsung/pm.c:170: warning: unused variable 'map_reg'
arch/arm/plat-samsung/pm.c: At top level:
arch/arm/plat-samsung/pm.c:188: warning: function declaration isn't a prototype
  CC      kernel/printk.o
  CC      mm/fadvise.o
  CC      arch/arm/plat-samsung/pm-gpio.o
  CC      arch/arm/plat-samsung/pwm.o
  CC      fs/read_write.o
  CC      mm/maccess.o
  LD      arch/arm/plat-samsung/built-in.o
  CC      mm/page_alloc.o
  CC      ipc/util.o
  CC      kernel/cpu.o
  CC      kernel/exit.o
  CC      fs/file_table.o
  CC      ipc/msgutil.o
  CC      ipc/msg.o
  CC      fs/super.o
  CC      kernel/itimer.o
  CC      mm/page-writeback.o
  CC      ipc/sem.o
  CC      fs/char_dev.o
  TIMEC   kernel/timeconst.h
  CC      kernel/softirq.o
  CC      fs/stat.o
  CC      mm/readahead.o
  CC      ipc/shm.o
  CC      fs/exec.o
  CC      kernel/resource.o
  CC      mm/swap.o
  CC      ipc/ipcns_notifier.o
  CC      kernel/sysctl.o
  CC      ipc/syscall.o
  CC      mm/truncate.o
  CC      ipc/ipc_sysctl.o
  CC      fs/pipe.o
  LD      ipc/built-in.o
  CC      mm/vmscan.o
  CC      security/commoncap.o
  CC      kernel/sysctl_binary.o
  CC      fs/namei.o
  CC      security/min_addr.o
  LD      security/built-in.o
  CC      crypto/api.o
  CC      kernel/capability.o
  CC      mm/shmem.o
  CC      crypto/cipher.o
  CC      kernel/ptrace.o
  CC      crypto/compress.o
  CC      fs/fcntl.o
  CC      kernel/timer.o
  CC      crypto/algapi.o
  CC      mm/prio_tree.o
  CC      fs/ioctl.o
  CC      mm/util.o
  CC      kernel/user.o
  CC      crypto/scatterwalk.o
  CC      kernel/signal.o
  CC      fs/readdir.o
  CC      mm/mmzone.o
  CC      crypto/proc.o
  CC      mm/vmstat.o
  CC      fs/select.o
  CC      crypto/ablkcipher.o
  CC      mm/backing-dev.o
  CC      fs/fifo.o
  CC      crypto/blkcipher.o
  CC      mm/page_isolation.o
  CC      kernel/sys.o
  CC      fs/dcache.o
  CC      mm/mm_init.o
  CC      mm/mmu_context.o
  CC      crypto/ahash.o
  CC      mm/fremap.o
  CC      crypto/shash.o
  CC      mm/highmem.o
  CC      kernel/kmod.o
  CC      fs/inode.o
  CC      mm/madvise.o
  CC      kernel/workqueue.o
  CC      crypto/algboss.o
  CC      mm/memory.o
  CC      crypto/testmgr.o
  CC      kernel/pid.o
  CC      fs/attr.o
  CC      fs/bad_inode.o
  CC      kernel/rcupdate.o
  CC      fs/file.o
  CC      kernel/extable.o
  CC      mm/mincore.o
  CC      crypto/crypto_wq.o
  CC      kernel/params.o
  CC      fs/filesystems.o
  LD      crypto/crypto_algapi.o
  CC      crypto/aead.o
  CC      mm/mlock.o
  CC      kernel/posix-timers.o
  CC      fs/namespace.o
  LD      crypto/crypto_blkcipher.o
  CC      crypto/chainiv.o
  CC      mm/mmap.o
  CC      crypto/eseqiv.o
  CC      kernel/kthread.o
  CC      kernel/wait.o
  LD      crypto/crypto_hash.o
  CC      crypto/pcompress.o
  CC      fs/seq_file.o
  LD      crypto/cryptomgr.o
  CC      crypto/hmac.o
  CC      kernel/kfifo.o
  CC      mm/mprotect.o
  CC      kernel/sys_ni.o
  CC      crypto/md5.o
  CC      kernel/posix-cpu-timers.o
  CC      fs/xattr.o
  CC      mm/mremap.o
  CC      crypto/sha1_generic.o
  CC      crypto/ecb.o
  CC      fs/libfs.o
  CC      mm/msync.o
  CC      kernel/mutex.o
  CC      crypto/cbc.o
  CC      mm/rmap.o
  CC      crypto/des_generic.o
  CC      kernel/hrtimer.o
  CC      fs/fs-writeback.o
  CC      mm/vmalloc.o
  CC      crypto/twofish.o
  CC      kernel/rwsem.o
  CC      fs/pnode.o
  CC      kernel/nsproxy.o
  CC      crypto/twofish_common.o
  CC      fs/drop_caches.o
  CC      kernel/srcu.o
  CC      fs/splice.o
  CC      kernel/semaphore.o
  CC      crypto/aes_generic.o
  CC      mm/pagewalk.o
  CC      mm/init-mm.o
  CC      kernel/notifier.o
  CC      mm/dmapool.o
  CC      kernel/ksysfs.o
  CC      fs/sync.o
  CC      mm/sparse.o
  CC      crypto/arc4.o
  CC      kernel/pm_qos_params.o
  CC      crypto/deflate.o
  CC      fs/utimes.o
  CC      kernel/sched_clock.o
  CC      mm/ashmem.o
  CC      crypto/michael_mic.o
  CC      kernel/cred.o
  CC      fs/stack.o
  CC      crypto/crc32c.o
  CC      mm/slub.o
  CC      fs/fs_struct.o
  CC      crypto/authenc.o
  CC      kernel/async.o
  CC      fs/statfs.o
  CC      kernel/range.o
  CC      kernel/groups.o
  CC      crypto/rng.o
  CC      fs/buffer.o
  CC      crypto/krng.o
  CC      mm/percpu_up.o
  CC      kernel/freezer.o
  CC [M]  crypto/ansi_cprng.o
  LD      mm/built-in.o
  CC      block/elevator.o
  CC      kernel/sysctl_check.o
  LD      crypto/crypto.o
  LD      crypto/built-in.o
  LD      drivers/auxdisplay/built-in.o
  CC      drivers/base/core.o
  CC      kernel/stacktrace.o
  CC      block/blk-core.o
  CC      kernel/irq/handle.o
  CC      drivers/base/sys.o
  CC      fs/bio.o
  CC      kernel/irq/manage.o
  CC      drivers/base/bus.o
  CC      kernel/irq/spurious.o
  CC      block/blk-tag.o
  CC      kernel/irq/resend.o
  CC      drivers/base/dd.o
  CC      fs/block_dev.o
  CC      kernel/irq/chip.o
  CC      drivers/base/driver.o
  CC      block/blk-sysfs.o
  CC      drivers/base/class.o
  CC      kernel/irq/devres.o
  CC      block/blk-barrier.o
  CC      kernel/irq/autoprobe.o
  CC      drivers/base/platform.o
  CC      fs/direct-io.o
  CC      kernel/irq/proc.o
  CC      block/blk-settings.o
  CC      kernel/irq/pm.o
  CC      drivers/base/cpu.o
  LD      kernel/irq/built-in.o
  CC      kernel/power/main.o
  CC      fs/mpage.o
  CC      drivers/base/firmware.o
  CC      block/blk-ioc.o
  CC      drivers/base/init.o
  CC      kernel/power/console.o
  CC      drivers/base/map.o
  CC      block/blk-map.o
  CC      drivers/base/devres.o
  CC      kernel/power/process.o
  CC      fs/ioprio.o
  CC      block/blk-exec.o
  CC      drivers/base/attribute_container.o
  CC      kernel/power/suspend.o
  CC      fs/cramfs/inode.o
  CC      drivers/base/transport_class.o
  CC      block/blk-merge.o
  CC      kernel/power/nvs.o
  CC      drivers/base/power/sysfs.o
  CC      drivers/base/power/main.o
  CC      fs/cramfs/uncompress.o
  CC      kernel/power/wakelock.o
  CC      block/blk-softirq.o
  LD      fs/cramfs/cramfs.o
  LD      fs/cramfs/built-in.o
  CC      fs/debugfs/inode.o
  CC      block/blk-timeout.o
  CC      fs/debugfs/file.o
  CC      drivers/base/power/generic_ops.o
  CC      kernel/power/userwakelock.o
  LD      drivers/base/power/built-in.o
  CC      drivers/base/dma-mapping.o
  CC      block/blk-iopoll.o
  CC      kernel/power/earlysuspend.o
  LD      fs/debugfs/debugfs.o
  LD      fs/debugfs/built-in.o
  CC      fs/devpts/inode.o
  CC      drivers/base/dma-coherent.o
  CC      block/blk-lib.o
  CC      drivers/base/firmware_class.o
  CC      kernel/power/fbearlysuspend.o
  LD      fs/devpts/devpts.o
  LD      fs/devpts/built-in.o
  CC      fs/ext2/balloc.o
  CC      block/ioctl.o
  CC      kernel/power/poweroff.o
  LD      kernel/power/built-in.o
  CC      kernel/time/timekeeping.o
  CC      drivers/base/module.o
  LD      drivers/base/built-in.o
  CC      drivers/block/brd.o
  CC      fs/ext2/dir.o
  CC      block/genhd.o
  CC      kernel/time/ntp.o
  CC      drivers/block/loop.o
  CC      fs/ext2/file.o
  CC      kernel/time/clocksource.o
  CC      block/scsi_ioctl.o
  CC      fs/ext2/ialloc.o
  CC      kernel/time/jiffies.o
  CC      kernel/time/timer_list.o
  CC      block/noop-iosched.o
  CC      fs/ext2/inode.o
  LD      drivers/block/built-in.o
  LD      drivers/cdrom/built-in.o
  CC      drivers/char/mem.o
  CC      kernel/time/timecompare.o
  CC      block/deadline-iosched.o
  CC      kernel/time/timeconv.o
  CC      kernel/time/clockevents.o
  CC      drivers/char/random.o
  CC      block/cfq-iosched.o
  CC      fs/ext2/ioctl.o
  CC      kernel/time/tick-common.o
  CC      fs/ext2/namei.o
  CC      kernel/time/tick-oneshot.o
  CC      drivers/char/tty_io.o
  CC      kernel/time/tick-sched.o
  CC      fs/ext2/super.o
  LD      kernel/time/built-in.o
  CC      kernel/mutex-debug.o
  CC      kernel/futex.o
  LD      block/built-in.o
  CC      sound/sound_core.o
  CC      fs/ext2/symlink.o
  CC      fs/ext2/xattr.o
  CC      drivers/char/n_tty.o
  LD      sound/arm/built-in.o
  LD      sound/atmel/built-in.o
  CC      sound/core/memalloc.o
  CC      fs/ext2/xattr_user.o
  CC      sound/core/pcm.o
  CC      kernel/rtmutex.o
  CC      fs/ext2/xattr_trusted.o
  CC      drivers/char/tty_ioctl.o
  LD      fs/ext2/ext2.o
  LD      fs/ext2/built-in.o
  CC      fs/ext4/balloc.o
  CC      kernel/rtmutex-debug.o
  CC      sound/core/pcm_native.o
  CC      drivers/char/tty_ldisc.o
  CC      kernel/up.o
  CC      fs/ext4/bitmap.o
  CC      kernel/spinlock.o
  CC      drivers/char/tty_buffer.o
  CC      fs/ext4/dir.o
  CC      kernel/uid16.o
  CC      drivers/char/tty_port.o
  CC      fs/ext4/file.o
  CC      kernel/module.o
  LD      drivers/char/buzzer/built-in.o
  CC      drivers/char/hw_random/core.o
  CC      sound/core/pcm_lib.o
  CC      fs/ext4/fsync.o
  LD      drivers/char/hw_random/rng-core.o
  LD      drivers/char/hw_random/built-in.o
  CC      drivers/char/led/x210-led.o
  CC      fs/ext4/ialloc.o
drivers/char/led/x210-led.c: In function 'x210_led_read':
drivers/char/led/x210-led.c:124: warning: control reaches end of non-void function
  LD      drivers/char/led/built-in.o
  CC      drivers/char/pty.o
  CC      kernel/kallsyms.o
  CC      sound/core/pcm_timer.o
  CC      fs/ext4/inode.o
  CC      drivers/char/misc.o
  CC      sound/core/pcm_misc.o
  CC      kernel/cgroup.o
  CC      drivers/char/vt_ioctl.o
  CC      sound/core/pcm_memory.o
  CC      sound/core/timer.o
  CC      drivers/char/vc_screen.o
  CC      drivers/char/selection.o
  CC      sound/core/sound.o
  CC      drivers/char/keyboard.o
  CC      kernel/cgroup_freezer.o
  CC      sound/core/init.o
  CC      fs/ext4/ioctl.o
  CC      kernel/res_counter.o
  CC      kernel/softlockup.o
  CC      fs/ext4/namei.o
  CC      drivers/char/consolemap.o
  CC      sound/core/memory.o
  CC      kernel/hung_task.o
  CC      sound/core/info.o
  CC      kernel/rcutree.o
  CONMK   drivers/char/consolemap_deftbl.c
  CC      drivers/char/vt.o
  CC      sound/core/control.o
  CC      kernel/utsname_sysctl.o
  CC      fs/ext4/super.o
  CC      kernel/elfcore.o
  CC      kernel/time.o
  CC      sound/core/misc.o
  LD      kernel/built-in.o
  CC      sound/core/device.o
  CC      sound/core/sound_oss.o
  MK_FW   firmware/samsung_mfc_fw.bin.gen.S
  IHEX    firmware/samsung_mfc_fw.bin
  AS      firmware/samsung_mfc_fw.bin.gen.o
  SHIPPED drivers/char/defkeymap.c
  CC      drivers/char/sysrq.o
  LD      firmware/built-in.o
  CC      net/socket.o
  CC      sound/core/info_oss.o
  CC      fs/ext4/symlink.o
  CC      drivers/char/apm-emulation.o
  CC      sound/core/jack.o
  CC      fs/ext4/hash.o
  CC      sound/core/oss/mixer_oss.o
  CC      drivers/char/s3c_mem.o
  CC      fs/ext4/resize.o
drivers/char/s3c_mem.c: In function 's3c_mem_ioctl':
drivers/char/s3c_mem.c:291: warning: passing argument 1 of 'cpu_cache.dma_map_area' makes pointer from integer without a cast
drivers/char/s3c_mem.c:291: note: expected 'const void *' but argument is of type 'long unsigned int'
  LD      net/802/built-in.o
  CC      net/core/sock.o
  CC      drivers/char/consolemap_deftbl.o
  CC      drivers/char/defkeymap.o
  LD      drivers/char/built-in.o
  LD      drivers/clocksource/built-in.o
  CC      sound/core/oss/pcm_oss.o
  CC      drivers/cpuidle/cpuidle.o
  CC      drivers/cpuidle/driver.o
  CC      fs/ext4/extents.o
  CC      drivers/cpuidle/governor.o
  CC      drivers/cpuidle/sysfs.o
  CC      drivers/cpuidle/governors/ladder.o
  CC      drivers/cpuidle/governors/menu.o
  CC      net/core/request_sock.o
  LD      drivers/cpuidle/governors/built-in.o
  LD      drivers/cpuidle/built-in.o
  LD      drivers/crypto/built-in.o
  CC      sound/core/oss/pcm_plugin.o
  LD      drivers/firmware/built-in.o
  CC      drivers/gpio/gpiolib.o
  CC      net/core/skbuff.o
  CC      fs/ext4/ext4_jbd2.o
  CC      sound/core/oss/io.o
  LD      drivers/gpio/built-in.o
  LD      drivers/gpu/drm/i2c/built-in.o
  LD      drivers/gpu/drm/built-in.o
  CC      drivers/gpu/pvr/osfunc.o
  CC      sound/core/oss/copy.o
  CC      fs/ext4/migrate.o
  CC      sound/core/oss/linear.o
  CC      fs/ext4/mballoc.o
  CC      sound/core/oss/mulaw.o
  CC      drivers/gpu/pvr/mutils.o
  CC      sound/core/oss/route.o
  CC      net/core/iovec.o
  CC      drivers/gpu/pvr/mmap.o
  CC      sound/core/oss/rate.o
  CC      drivers/gpu/pvr/module.o
  CC      net/core/datagram.o
  LD      sound/core/oss/snd-mixer-oss.o
  LD      sound/core/oss/snd-pcm-oss.o
  LD      sound/core/oss/built-in.o
  CC      sound/core/seq/seq_device.o
  CC      drivers/gpu/pvr/pdump.o
  CC      drivers/gpu/pvr/proc.o
  CC      sound/core/seq/seq_dummy.o
  CC      net/core/stream.o
  CC      drivers/gpu/pvr/pvr_bridge_k.o
  CC      sound/core/seq/seq_midi_event.o
  CC      fs/ext4/block_validity.o
  CC      drivers/gpu/pvr/pvr_debug.o
  CC      net/core/scm.o
  CC      sound/core/seq/seq.o
  CC      drivers/gpu/pvr/mm.o
  CC      fs/ext4/move_extent.o
  CC      sound/core/seq/seq_lock.o
  CC      sound/core/seq/seq_clientmgr.o
  CC      net/core/gen_stats.o
  CC      drivers/gpu/pvr/mutex.o
  CC      net/core/gen_estimator.o
  CC      drivers/gpu/pvr/event.o
  CC      fs/ext4/xattr.o
  CC      drivers/gpu/pvr/osperproc.o
  CC      net/core/net_namespace.o
  CC      drivers/gpu/pvr/buffer_manager.o
  CC      sound/core/seq/seq_memory.o
  CC      net/core/sysctl_net_core.o
  CC      fs/ext4/xattr_user.o
  CC      sound/core/seq/seq_queue.o
  CC      drivers/gpu/pvr/devicemem.o
  CC      net/core/dev.o
  CC      fs/ext4/xattr_trusted.o
  CC      sound/core/seq/seq_fifo.o
  LD      fs/ext4/ext4.o
  LD      fs/ext4/built-in.o
  CC      drivers/gpu/pvr/deviceclass.o
  CC      fs/fat/cache.o
  CC      sound/core/seq/seq_prioq.o
  CC      fs/fat/dir.o
  CC      sound/core/seq/seq_timer.o
  CC      drivers/gpu/pvr/handle.o
  CC      sound/core/seq/seq_system.o
  CC      sound/core/seq/seq_ports.o
  CC      drivers/gpu/pvr/hash.o
  CC      fs/fat/fatent.o
  CC      drivers/gpu/pvr/metrics.o
  CC      sound/core/seq/seq_info.o
  CC      drivers/gpu/pvr/pvrsrv.o
  CC      net/core/ethtool.o
  CC      sound/core/seq/oss/seq_oss.o
  CC      fs/fat/file.o
  CC      sound/core/seq/oss/seq_oss_init.o
  CC      drivers/gpu/pvr/queue.o
  CC      fs/fat/inode.o
  CC      sound/core/seq/oss/seq_oss_timer.o
  CC      net/core/dev_addr_lists.o
  CC      drivers/gpu/pvr/ra.o
  CC      sound/core/seq/oss/seq_oss_ioctl.o
  CC      fs/fat/misc.o
  CC      sound/core/seq/oss/seq_oss_event.o
  CC      drivers/gpu/pvr/resman.o
  CC      net/core/dst.o
  CC      fs/fat/namei_msdos.o
  CC      sound/core/seq/oss/seq_oss_rw.o
  CC      drivers/gpu/pvr/power.o
  CC      net/core/netevent.o
  CC      net/core/neighbour.o
  CC      sound/core/seq/oss/seq_oss_synth.o
  CC      drivers/gpu/pvr/mem.o
  CC      fs/fat/namei_vfat.o
  CC      drivers/gpu/pvr/pdump_common.o
  CC      drivers/gpu/pvr/bridged_support.o
  CC      drivers/gpu/pvr/bridged_pvr_bridge.o
  CC      sound/core/seq/oss/seq_oss_midi.o
  LD      fs/fat/fat.o
  LD      fs/fat/vfat.o
  LD      fs/fat/msdos.o
  LD      fs/fat/built-in.o
  CC      fs/isofs/namei.o
  CC      sound/core/seq/oss/seq_oss_readq.o
  CC      fs/isofs/inode.o
  CC      sound/core/seq/oss/seq_oss_writeq.o
  CC      net/core/rtnetlink.o
  CC      drivers/gpu/pvr/perproc.o
  LD      sound/core/seq/oss/snd-seq-oss.o
  LD      sound/core/seq/oss/built-in.o
  LD      sound/core/seq/snd-seq-dummy.o
  LD      sound/core/seq/snd-seq.o
  LD      sound/core/seq/snd-seq-device.o
  LD      sound/core/seq/snd-seq-midi-event.o
  LD      sound/core/seq/built-in.o
  LD      sound/core/snd.o
  LD      sound/core/snd-timer.o
  LD      sound/core/snd-pcm.o
  LD      sound/core/snd-page-alloc.o
  LD      sound/core/built-in.o
  CC      drivers/gpu/pvr/lists.o
  LD      sound/drivers/mpu401/built-in.o
  LD      sound/drivers/opl3/built-in.o
  LD      sound/drivers/opl4/built-in.o
  LD      sound/drivers/pcsp/built-in.o
  LD      sound/drivers/vx/built-in.o
  LD      sound/drivers/built-in.o
  CC      fs/isofs/dir.o
  LD      sound/i2c/other/built-in.o
  LD      sound/i2c/built-in.o
  LD      sound/isa/ad1816a/built-in.o
  LD      sound/isa/ad1848/built-in.o
  LD      sound/isa/cs423x/built-in.o
  LD      sound/isa/es1688/built-in.o
  LD      sound/isa/gus/built-in.o
  CC      drivers/gpu/pvr/sgx/bridged_sgx_bridge.o
  LD      sound/isa/msnd/built-in.o
  LD      sound/isa/opti9xx/built-in.o
  LD      sound/isa/sb/built-in.o
  LD      sound/isa/wavefront/built-in.o
  LD      sound/isa/wss/built-in.o
  LD      sound/isa/built-in.o
  LD      sound/mips/built-in.o
  LD      sound/parisc/built-in.o
  LD      sound/pci/ac97/built-in.o
  CC      fs/isofs/util.o
  LD      sound/pci/ali5451/built-in.o
  LD      sound/pci/asihpi/built-in.o
  LD      sound/pci/au88x0/built-in.o
  LD      sound/pci/aw2/built-in.o
  LD      sound/pci/ca0106/built-in.o
  LD      sound/pci/cs46xx/built-in.o
  LD      sound/pci/cs5535audio/built-in.o
  LD      sound/pci/ctxfi/built-in.o
  LD      sound/pci/echoaudio/built-in.o
  LD      sound/pci/emu10k1/built-in.o
  LD      sound/pci/hda/built-in.o
  LD      sound/pci/ice1712/built-in.o
  LD      sound/pci/korg1212/built-in.o
  LD      sound/pci/lx6464es/built-in.o
  CC      fs/isofs/rock.o
  LD      sound/pci/mixart/built-in.o
  CC      net/core/utils.o
  LD      sound/pci/nm256/built-in.o
  LD      sound/pci/oxygen/built-in.o
  LD      sound/pci/pcxhr/built-in.o
  CC      drivers/gpu/pvr/sgx/sgxinit.o
  LD      sound/pci/riptide/built-in.o
  LD      sound/pci/rme9652/built-in.o
  LD      sound/pci/trident/built-in.o
  LD      sound/pci/vx222/built-in.o
  LD      sound/pci/ymfpci/built-in.o
  LD      sound/pci/built-in.o
  LD      sound/pcmcia/pdaudiocf/built-in.o
  LD      sound/pcmcia/vx/built-in.o
  LD      sound/pcmcia/built-in.o
  LD      sound/ppc/built-in.o
  LD      sound/sh/built-in.o
  CC      sound/soc/soc-core.o
  CC      net/core/link_watch.o
  CC      fs/isofs/export.o
  CC      drivers/gpu/pvr/sgx/sgxpower.o
  CC      fs/isofs/joliet.o
  CC      drivers/gpu/pvr/sgx/sgxreset.o
  CC      net/core/filter.o
  CC      fs/isofs/compress.o
  CC      drivers/gpu/pvr/sgx/sgxutils.o
  CC      sound/soc/soc-dapm.o
  CC      drivers/gpu/pvr/sgx/sgxkick.o
  LD      fs/isofs/isofs.o
  LD      fs/isofs/built-in.o
  CC      fs/jbd2/transaction.o
  CC      net/core/flow.o
  CC      drivers/gpu/pvr/sgx/sgxtransfer.o
  CC      drivers/gpu/pvr/sgx/mmu.o
  CC      net/core/net-sysfs.o
  CC      sound/soc/soc-jack.o
  CC      drivers/gpu/pvr/sgx/pb.o
  CC      fs/jbd2/commit.o
  CC      net/core/fib_rules.o
  CC      sound/soc/soc-cache.o
  CC      drivers/gpu/pvr/s5pc110/sysconfig.o
  CC      drivers/gpu/pvr/s5pc110/sysutils.o
  CC      sound/soc/soc-utils.o
  CC      fs/jbd2/recovery.o
  CC      drivers/gpu/pvr/s3c_bc/s3c_bc.o
  LD      net/core/built-in.o
  CC      net/ethernet/eth.o
  LD      sound/soc/atmel/built-in.o
  LD      sound/soc/au1x/built-in.o
  LD      sound/soc/blackfin/built-in.o
  CC      drivers/gpu/pvr/s3c_bc/s3c_bc_linux.o
  CC      sound/soc/codecs/wm8976.o
  CC      fs/jbd2/checkpoint.o
sound/soc/codecs/wm8976.c:669: warning: initialization from incompatible pointer type
sound/soc/codecs/wm8976.c: In function 'wm8976_init':
sound/soc/codecs/wm8976.c:815: warning: label 'card_err' defined but not used
sound/soc/codecs/wm8976.c: At top level:
sound/soc/codecs/wm8976.c:447: warning: 'wm8976_set_dai_sysclk' defined but not used
sound/soc/codecs/wm8976.c:613: warning: 'wm8976_mute' defined but not used
  CC      drivers/gpu/pvr/s3c_lcd/s3c_displayclass.o
  LD      net/ethernet/built-in.o
  LD      net/ieee802154/built-in.o
  CC      net/ipv4/route.o
  LD      sound/soc/codecs/snd-soc-wm8976.o
  LD      sound/soc/codecs/built-in.o
  LD      sound/soc/davinci/built-in.o
  LD      sound/soc/fsl/built-in.o
  LD      sound/soc/imx/built-in.o
  LD      sound/soc/omap/built-in.o
  LD      sound/soc/pxa/built-in.o
  CC      sound/soc/s3c24xx/s3c-dma-wrapper.o
  CC      fs/jbd2/revoke.o
  CC      drivers/gpu/pvr/s3c_lcd/s3c_lcd.o
sound/soc/s3c24xx/s3c-dma-wrapper.c:27: warning: "pr_debug" redefined
include/linux/kernel.h:420: note: this is the location of the previous definition
  LD      drivers/gpu/pvr/pvrsrvkm.o
  CC      sound/soc/s3c24xx/s3c-i2s-v2.o
  LD      drivers/gpu/pvr/s3c_lcd.o
  LD      drivers/gpu/pvr/s3c_bc.o
  CC      fs/jbd2/journal.o
  LD      drivers/gpu/pvr/built-in.o
  LD      drivers/gpu/vga/built-in.o
  LD      drivers/gpu/built-in.o
  CC      drivers/hid/hid-core.o
sound/soc/s3c24xx/s3c-i2s-v2.c:45: warning: "pr_debug" redefined
include/linux/kernel.h:420: note: this is the location of the previous definition
  CC      sound/soc/s3c24xx/s3c-idma.o
In file included from sound/soc/s3c24xx/s3c-idma.c:24:
sound/soc/s3c24xx/s3c-idma.h:22: warning: "LP_DMA_PERIOD" redefined
arch/arm/plat-s5p/include/plat/regs-iis.h:320: note: this is the location of the previous definition
  CC      net/ipv4/inetpeer.o
  CC      sound/soc/s3c24xx/s3c-dma.o
  CC      drivers/hid/hid-input.o
sound/soc/s3c24xx/s3c-dma.c:40: warning: "pr_debug" redefined
include/linux/kernel.h:420: note: this is the location of the previous definition
sound/soc/s3c24xx/s3c-dma.c: In function 's3c_dma_free_dma_buffers':
sound/soc/s3c24xx/s3c-dma.c:482: warning: statement with no effect
sound/soc/s3c24xx/s3c-dma.c:471: warning: 'stream' may be used uninitialized in this function
  LD      fs/jbd2/jbd2.o
  LD      fs/jbd2/built-in.o
  CC      fs/jffs2/compr.o
  CC      sound/soc/s3c24xx/s3c64xx-i2s-v4.o
  CC      net/ipv4/protocol.o
  CC      fs/jffs2/dir.o
  CC      net/ipv4/ip_input.o
  CC      sound/soc/s3c24xx/s5p-i2s_sec.o
  CC      drivers/hid/hid-debug.o
  CC      fs/jffs2/file.o
In file included from sound/soc/s3c24xx/s5p-i2s_sec.c:27:
sound/soc/s3c24xx/s3c-idma.h:22: warning: "LP_DMA_PERIOD" redefined
arch/arm/plat-s5p/include/plat/regs-iis.h:320: note: this is the location of the previous definition
  CC      sound/soc/s3c24xx/smdkv2xx_wm8976slv.o
  CC      fs/jffs2/ioctl.o
  CC      net/ipv4/ip_fragment.o
  CC      drivers/hid/hidraw.o
  CC      fs/jffs2/nodelist.o
  LD      sound/soc/s3c24xx/snd-soc-s3c24xx.o
  LD      sound/soc/s3c24xx/snd-soc-s3c64xx-i2s-v4.o
  LD      sound/soc/s3c24xx/snd-soc-s3c-i2s-v2.o
  LD      sound/soc/s3c24xx/snd-soc-s3c-idma.o
  LD      sound/soc/s3c24xx/snd-soc-s3c-dma-wrapper.o
  LD      sound/soc/s3c24xx/snd-soc-s5p-i2s_sec.o
  LD      sound/soc/s3c24xx/snd-soc-smdkv2xx-wm8976.o
  LD      sound/soc/s3c24xx/built-in.o
  LD      sound/soc/s6000/built-in.o
  LD      sound/soc/sh/built-in.o
  CC      drivers/hid/usbhid/hid-core.o
  LD      sound/soc/txx9/built-in.o
  LD      sound/soc/snd-soc-core.o
  LD      sound/soc/built-in.o
  LD      sound/sparc/built-in.o
  LD      sound/spi/built-in.o
  CC      net/ipv4/ip_forward.o
  LD      sound/synth/emux/built-in.o
  LD      sound/synth/built-in.o
  CC      fs/jffs2/malloc.o
  LD      sound/usb/caiaq/built-in.o
  LD      sound/usb/misc/built-in.o
  LD      sound/usb/usx2y/built-in.o
  LD      sound/usb/built-in.o
  CC      sound/last.o
  CC      fs/jffs2/read.o
  LD      sound/soundcore.o
  LD      sound/built-in.o
  LD      arch/arm/lib/built-in.o
  AS      arch/arm/lib/ashldi3.o
  AS      arch/arm/lib/ashrdi3.o
  AS      arch/arm/lib/backtrace.o
  CC      net/ipv4/ip_options.o
  AS      arch/arm/lib/changebit.o
  CC      fs/jffs2/nodemgmt.o
  AS      arch/arm/lib/clear_user.o
  CC      drivers/hid/usbhid/hid-quirks.o
  AS      arch/arm/lib/clearbit.o
  AS      arch/arm/lib/copy_from_user.o
  AS      arch/arm/lib/copy_page.o
  AS      arch/arm/lib/copy_to_user.o
  AS      arch/arm/lib/csumipv6.o
  AS      arch/arm/lib/csumpartial.o
  AS      arch/arm/lib/csumpartialcopy.o
  CC      drivers/hid/usbhid/hiddev.o
  AS      arch/arm/lib/csumpartialcopyuser.o
  AS      arch/arm/lib/delay.o
  AS      arch/arm/lib/div64.o
  CC      fs/jffs2/readinode.o
  AS      arch/arm/lib/findbit.o
  AS      arch/arm/lib/getuser.o
  AS      arch/arm/lib/io-readsb.o
  AS      arch/arm/lib/io-readsl.o
  AS      arch/arm/lib/io-readsw-armv4.o
  AS      arch/arm/lib/io-writesb.o
  CC      net/ipv4/ip_output.o
  AS      arch/arm/lib/io-writesl.o
  AS      arch/arm/lib/io-writesw-armv4.o
  AS      arch/arm/lib/lib1funcs.o
  AS      arch/arm/lib/lshrdi3.o
  AS      arch/arm/lib/memchr.o
  AS      arch/arm/lib/memcpy.o
  AS      arch/arm/lib/memmove.o
  AS      arch/arm/lib/memset.o
  LD      drivers/hid/usbhid/usbhid.o
  LD      drivers/hid/usbhid/built-in.o
  LD      drivers/hid/hid.o
  AS      arch/arm/lib/memzero.o
  LD      drivers/hid/built-in.o
  AS      arch/arm/lib/muldi3.o
  CC      drivers/i2c/i2c-boardinfo.o
  AS      arch/arm/lib/putuser.o
  AS      arch/arm/lib/setbit.o
  AS      arch/arm/lib/sha1.o
  AS      arch/arm/lib/strchr.o
  AS      arch/arm/lib/strncpy_from_user.o
  AS      arch/arm/lib/strnlen_user.o
  CC      drivers/i2c/i2c-core.o
  AS      arch/arm/lib/strrchr.o
  CC      fs/jffs2/write.o
  AS      arch/arm/lib/testchangebit.o
  AS      arch/arm/lib/testclearbit.o
  AS      arch/arm/lib/testsetbit.o
  AS      arch/arm/lib/ucmpdi2.o
  AR      arch/arm/lib/lib.a
  CC      lib/bcd.o
  CC      lib/div64.o
  CC      lib/sort.o
  CC      fs/jffs2/scan.o
  CC      net/ipv4/ip_sockglue.o
  CC      drivers/i2c/i2c-dev.o
  CC      lib/parser.o
  CC      lib/halfmd4.o
  CC      drivers/i2c/algos/i2c-algo-bit.o
  CC      lib/debug_locks.o
  CC      lib/random32.o
  CC      fs/jffs2/gc.o
  CC      lib/bust_spinlocks.o
  CC      net/ipv4/inet_hashtables.o
  LD      drivers/i2c/algos/built-in.o
  CC      drivers/i2c/busses/i2c-gpio.o
  CC      lib/hexdump.o
  CC      drivers/i2c/busses/i2c-s3c2410.o
  CC      lib/kasprintf.o
  CC      lib/bitmap.o
  CC      fs/jffs2/symlink.o
  CC      net/ipv4/inet_timewait_sock.o
  LD      drivers/i2c/busses/built-in.o
  LD      drivers/i2c/built-in.o
  LD      drivers/idle/built-in.o
  CC      fs/jffs2/build.o
  LD      drivers/ieee1394/built-in.o
  LD      drivers/ieee802154/built-in.o
  CC      drivers/input/input.o
  CC      lib/scatterlist.o
  CC      fs/jffs2/erase.o
  CC      net/ipv4/inet_connection_sock.o
  CC      lib/string_helpers.o
  CC      lib/gcd.o
  CC      fs/jffs2/background.o
  CC      lib/lcm.o
  CC      drivers/input/input-compat.o
  CC      lib/list_sort.o
  CC      fs/jffs2/fs.o
  CC      drivers/input/ff-core.o
  CC      lib/uuid.o
  CC      net/ipv4/tcp.o
  CC      lib/iomap_copy.o
  CC      lib/devres.o
  CC      drivers/input/mousedev.o
  CC      fs/jffs2/writev.o
  CC      lib/spinlock_debug.o
  CC      fs/jffs2/super.o
  CC      lib/find_last_bit.o
  CC      drivers/input/evdev.o
  CC      lib/hweight.o
  CC      fs/jffs2/debug.o
  CC      lib/kernel_lock.o
  CC      lib/bitrev.o
  CC      fs/jffs2/wbuf.o
  CC      drivers/input/keyboard/atkbd.o
  CC      lib/crc-ccitt.o
  CC      lib/crc16.o
  CC      net/ipv4/tcp_input.o
  CC      lib/crc-itu-t.o
  HOSTCC  lib/gen_crc32table
  CC      lib/libcrc32c.o
  CC      fs/jffs2/xattr.o
  LD      drivers/input/keyboard/built-in.o
  CC      drivers/input/mouse/psmouse-base.o
  CC      lib/reed_solomon/reed_solomon.o
  LD      lib/reed_solomon/built-in.o
  CC      lib/zlib_deflate/deflate.o
  CC      fs/jffs2/xattr_trusted.o
  CC      drivers/input/mouse/synaptics.o
  CC      lib/zlib_deflate/deftree.o
  CC      fs/jffs2/xattr_user.o
  CC      drivers/input/mouse/alps.o
  CC      fs/jffs2/security.o
  CC      lib/zlib_deflate/deflate_syms.o
  CC      fs/jffs2/acl.o
  CC      net/ipv4/tcp_output.o
  LD      lib/zlib_deflate/zlib_deflate.o
  LD      lib/zlib_deflate/built-in.o
  CC      lib/zlib_inflate/inffast.o
  CC      drivers/input/mouse/logips2pp.o
  CC      lib/zlib_inflate/inflate.o
  CC      drivers/input/mouse/trackpoint.o
  CC      fs/jffs2/compr_rtime.o
  CC      lib/zlib_inflate/infutil.o
  CC      fs/jffs2/compr_zlib.o
  CC      lib/zlib_inflate/inftrees.o
  LD      drivers/input/mouse/psmouse.o
  LD      drivers/input/mouse/built-in.o
  CC      drivers/input/touchscreen/ts-s3c.o
  CC      lib/zlib_inflate/inflate_syms.o
  CC      fs/jffs2/summary.o
  LD      lib/zlib_inflate/zlib_inflate.o
  LD      lib/zlib_inflate/built-in.o
drivers/input/touchscreen/ts-s3c.c:158: warning: 'ts_filter_fixed' defined but not used
  CC      lib/textsearch.o
  CC      lib/ts_kmp.o
  CC      drivers/input/touchscreen/ts_filter_chain.o
  CC      net/ipv4/tcp_timer.o
  CC      lib/ts_bm.o
  CC      drivers/input/touchscreen/ts_filter_group.o
  LD      fs/jffs2/jffs2.o
  LD      fs/jffs2/built-in.o
  CC      fs/lockd/clntlock.o
  CC      lib/ts_fsm.o
  CC      drivers/input/touchscreen/ts_filter_median.o
  CC      lib/nlattr.o
  CC      drivers/input/touchscreen/ts_filter_mean.o
  CC      net/ipv4/tcp_ipv4.o
  CC      drivers/input/touchscreen/ts_filter_linear.o
  CC      fs/lockd/clntproc.o
  CC      drivers/input/touchscreen/ts_filter_zoom.o
  CC      lib/argv_split.o
  CC      drivers/input/touchscreen/gslX680.o
  CC      lib/cmdline.o
  CC      lib/ctype.o
drivers/input/touchscreen/gslX680.c: In function 'gsl_load_fw':
drivers/input/touchscreen/gslX680.c:268: warning: assignment discards qualifiers from pointer target type
drivers/input/touchscreen/gslX680.c: In function 'startup_chip':
drivers/input/touchscreen/gslX680.c:345: warning: ISO C90 forbids mixed declarations and code
drivers/input/touchscreen/gslX680.c: In function 'gslX680_ts_worker':
drivers/input/touchscreen/gslX680.c:595: warning: ISO C90 forbids mixed declarations and code
drivers/input/touchscreen/gslX680.c:597: warning: missing braces around initializer
drivers/input/touchscreen/gslX680.c:597: warning: (near initialization for 'cinfo.x')
  CC      fs/lockd/host.o
  CC      lib/dec_and_lock.o
  CC      lib/decompress.o
  CC      lib/dump_stack.o
  LD      drivers/input/touchscreen/built-in.o
  LD      drivers/input/input-core.o
  LD      drivers/input/built-in.o
  CC      drivers/input/serio/serio.o
  CC      lib/extable.o
  CC      net/ipv4/tcp_minisocks.o
  CC      lib/flex_array.o
  CC      fs/lockd/svc.o
  CC      lib/idr.o
  CC      drivers/input/serio/serport.o
  CC      net/ipv4/tcp_cong.o
  CC      lib/int_sqrt.o
  CC      drivers/input/serio/libps2.o
  CC      fs/lockd/svclock.o
  CC      lib/ioremap.o
  CC      lib/irq_regs.o
  LD      drivers/input/serio/built-in.o
  LD      drivers/lguest/built-in.o
  LD      drivers/macintosh/built-in.o
  CC      drivers/md/dm-uevent.o
  CC      lib/is_single_threaded.o
  CC      net/ipv4/datagram.o
  CC      lib/klist.o
  CC      fs/lockd/svcshare.o
  CC      drivers/md/dm.o
  CC      lib/kobject.o
  CC      net/ipv4/raw.o
  CC      fs/lockd/svcproc.o
  CC      lib/kobject_uevent.o
  CC      lib/kref.o
  CC      fs/lockd/svcsubs.o
  CC      net/ipv4/udp.o
  CC      drivers/md/dm-table.o
  CC      lib/plist.o
  CC      lib/prio_heap.o
  CC      lib/prio_tree.o
  CC      fs/lockd/mon.o
  CC      lib/proportions.o
  CC      drivers/md/dm-target.o
  CC      lib/radix-tree.o
  CC      drivers/md/dm-linear.o
  CC      lib/ratelimit.o
  CC      fs/lockd/xdr.o
  CC      lib/rbtree.o
  CC      net/ipv4/udplite.o
  CC      drivers/md/dm-stripe.o
  CC      lib/reciprocal_div.o
  CC      lib/rwsem-spinlock.o
  CC      drivers/md/dm-ioctl.o
  CC      net/ipv4/arp.o
  CC      lib/sha1.o
  CC      fs/lockd/grace.o
  CC      lib/show_mem.o
  CC      fs/lockd/xdr4.o
  CC      lib/string.o
  CC      lib/vsprintf.o
  CC      drivers/md/dm-io.o
  CC      net/ipv4/icmp.o
  CC      fs/lockd/svc4proc.o
  CC      drivers/md/dm-kcopyd.o
  GEN     lib/crc32table.h
  LD      fs/lockd/lockd.o
  AR      lib/lib.a
  CC      lib/crc32.o
  LD      fs/lockd/built-in.o
  CC      drivers/md/dm-sysfs.o
  CC      net/ipv4/devinet.o
  CC      fs/nfs/client.o
  LD      lib/built-in.o
  CC      fs/nfs/dir.o
  CC      drivers/md/dm-crypt.o
  CC      net/ipv4/af_inet.o
  CC      fs/nfs/file.o
  LD      drivers/md/dm-mod.o
  LD      drivers/md/built-in.o
  LD      drivers/media/IR/keymaps/built-in.o
  LD      drivers/media/IR/built-in.o
  CC      drivers/media/common/tuners/tuner-xc2028.o
  CC      fs/nfs/getroot.o
  CC      fs/nfs/inode.o
  CC      fs/nfs/super.o
  CC      net/ipv4/igmp.o
  CC      drivers/media/common/tuners/tuner-simple.o
  CC      drivers/media/common/tuners/tuner-types.o
  CC      fs/nfs/nfs2xdr.o
  CC      drivers/media/common/tuners/mt20xx.o
  CC      net/ipv4/fib_frontend.o
  CC      fs/nfs/direct.o
  CC      drivers/media/common/tuners/tda8290.o
  CC      fs/nfs/pagelist.o
  CC      net/ipv4/fib_semantics.o
  CC      fs/nfs/proc.o
  CC      drivers/media/common/tuners/tea5767.o
  CC      fs/nfs/read.o
  CC      net/ipv4/inet_fragment.o
  CC      drivers/media/common/tuners/tea5761.o
  CC      fs/nfs/symlink.o
  CC      net/ipv4/sysctl_net_ipv4.o
  CC      drivers/media/common/tuners/tda9887.o
  CC      fs/nfs/unlink.o
  CC      fs/nfs/write.o
  CC      drivers/media/common/tuners/xc5000.o
  CC      net/ipv4/sysfs_net_ipv4.o
  CC      fs/nfs/namespace.o
  CC      net/ipv4/fib_hash.o
  CC      drivers/media/common/tuners/mc44s803.o
  CC      fs/nfs/mount_clnt.o
  LD      drivers/media/common/tuners/built-in.o
  LD      drivers/media/common/built-in.o
  LD      drivers/media/radio/built-in.o
  CC      drivers/media/video/v4l2-dev.o
  CC      fs/nfs/dns_resolve.o
  CC      net/ipv4/proc.o
  CC      drivers/media/video/v4l2-ioctl.o
  CC      fs/nfs/cache_lib.o
  CC      fs/nfs/nfsroot.o
  CC      fs/nfs/nfs3proc.o
  CC      net/ipv4/esp4.o
  CC      fs/nfs/nfs3xdr.o
  CC      fs/nfs/nfs3acl.o
  CC      net/ipv4/tunnel4.o
  CC      drivers/media/video/v4l2-device.o
  CC      drivers/media/video/v4l2-fh.o
  CC      net/ipv4/xfrm4_mode_transport.o
  CC      fs/nfs/nfs4proc.o
  CC      drivers/media/video/v4l2-event.o
  CC      fs/nfs/nfs4xdr.o
  CC      drivers/media/video/v4l2-int-device.o
  CC      net/ipv4/ipconfig.o
  CC      drivers/media/video/v4l2-common.o
  CC      drivers/media/video/v4l1-compat.o
  CC      net/ipv4/netfilter.o
  CC      net/ipv4/netfilter/nf_nat_rule.o
  CC      drivers/media/video/ov2655_2m.o
drivers/media/video/ov2655_2m.c: In function '__OV2655_init_2bytes':
drivers/media/video/ov2655_2m.c:176: warning: unused variable 'bytes'
drivers/media/video/ov2655_2m.c:173: warning: unused variable 'client'
drivers/media/video/ov2655_2m.c: In function 'ov2655_write_regs':
drivers/media/video/ov2655_2m.c:222: warning: unused variable 'client'
drivers/media/video/ov2655_2m.c: In function 'ov2655_init':
drivers/media/video/ov2655_2m.c:643: warning: unused variable 'i'
  CC      fs/nfs/nfs4state.o
  CC      drivers/media/video/gspca/gspca.o
  CC      net/ipv4/netfilter/nf_nat_standalone.o
  CC      fs/nfs/nfs4renewd.o
  CC      net/ipv4/netfilter/nf_conntrack_l3proto_ipv4.o
  CC      fs/nfs/delegation.o
  CC      fs/nfs/idmap.o
  CC      net/ipv4/netfilter/nf_conntrack_proto_icmp.o
  LD      drivers/media/video/gspca/gspca_main.o
  LD      drivers/media/video/gspca/built-in.o
  CC      drivers/media/video/samsung/fimc/fimc_dev.o
  CC      fs/nfs/callback.o
  CC      fs/nfs/callback_xdr.o
  CC      net/ipv4/netfilter/nf_conntrack_l3proto_ipv4_compat.o
  CC      drivers/media/video/samsung/fimc/fimc_v4l2.o
  CC      fs/nfs/callback_proc.o
  CC      net/ipv4/netfilter/nf_nat_core.o
  CC      fs/nfs/nfs4namespace.o
  CC      drivers/media/video/samsung/fimc/fimc_capture.o
  CC      fs/nfs/sysctl.o
  CC      net/ipv4/netfilter/nf_nat_helper.o
  CC      drivers/media/video/samsung/fimc/fimc_output.o
  LD      fs/nfs/nfs.o
  LD      fs/nfs/built-in.o
  CC      fs/nfs_common/nfsacl.o
  CC      drivers/media/video/samsung/fimc/fimc_overlay.o
  LD      fs/nfs_common/nfs_acl.o
  LD      fs/nfs_common/built-in.o
drivers/media/video/samsung/fimc/fimc_overlay.c:101: warning: 'fimc_change_fifo_position' defined but not used
  CC      fs/nls/nls_base.o
  CC      net/ipv4/netfilter/nf_nat_proto_unknown.o
  CC      drivers/media/video/samsung/fimc/fimc_regs.o
  CC      fs/nls/nls_cp437.o
  CC      fs/nls/nls_ascii.o
  CC      net/ipv4/netfilter/nf_nat_proto_common.o
  CC      fs/nls/nls_iso8859-1.o
  CC      drivers/media/video/samsung/fimc/csis.o
  LD      fs/nls/built-in.o
  CC      fs/notify/fsnotify.o
  CC      net/ipv4/netfilter/nf_nat_proto_tcp.o
  CC      drivers/mfd/mfd-core.o
  LD      drivers/media/video/samsung/fimc/built-in.o
  CC      drivers/media/video/samsung/g2d/fimg2d_3x.o
  CC      fs/notify/notification.o
  CC      drivers/mfd/max8698.o
  CC      fs/notify/group.o
  CC      net/ipv4/netfilter/nf_nat_proto_udp.o
  LD      drivers/mfd/built-in.o
  CC      drivers/misc/pmem.o
  LD      drivers/media/video/samsung/g2d/built-in.o
  CC      drivers/media/video/samsung/jpeg_v2/jpg_mem.o
  CC      fs/notify/inode_mark.o
  CC      drivers/media/video/samsung/jpeg_v2/jpg_misc.o
  CC      net/ipv4/netfilter/nf_nat_proto_icmp.o
  CC      drivers/media/video/samsung/jpeg_v2/jpg_opr.o
  LD      fs/notify/dnotify/built-in.o
  CC      fs/notify/inotify/inotify.o
  LD      drivers/misc/cb710/built-in.o
  LD      drivers/misc/eeprom/built-in.o
  CC      drivers/misc/apanic.o
  CC      net/ipv4/netfilter/nf_defrag_ipv4.o
  CC      drivers/media/video/samsung/jpeg_v2/s3c-jpeg.o
drivers/misc/apanic.c: In function 'apanic_proc_read':
drivers/misc/apanic.c:200: warning: assignment makes pointer from integer without a cast
  CC      fs/notify/inotify/inotify_fsnotify.o
  LD      drivers/media/video/samsung/jpeg_v2/built-in.o
  CC      drivers/media/video/samsung/mfc50/mfc.o
  LD      drivers/misc/built-in.o
  CC      fs/notify/inotify/inotify_user.o
  CC      drivers/mmc/card/block.o
  CC      net/ipv4/netfilter/nf_nat_amanda.o
  CC      net/ipv4/netfilter/nf_nat_ftp.o
  LD      fs/notify/inotify/built-in.o
  LD      fs/notify/built-in.o
  CC      drivers/mmc/card/queue.o
  CC      fs/ntfs/aops.o
  CC      drivers/media/video/samsung/mfc50/mfc_buffer_manager.o
  CC      drivers/media/video/samsung/mfc50/mfc_intr.o
  CC      net/ipv4/netfilter/nf_nat_h323.o
  LD      drivers/mmc/card/mmc_block.o
  LD      drivers/mmc/card/built-in.o
  CC      drivers/mmc/core/core.o
  CC      drivers/media/video/samsung/mfc50/mfc_memory.o
  CC      drivers/media/video/samsung/mfc50/mfc_opr.o
  CC      fs/ntfs/attrib.o
  CC      net/ipv4/netfilter/nf_nat_irc.o
  CC      drivers/mmc/core/bus.o
  CC      drivers/mmc/core/host.o
  CC      drivers/media/video/samsung/mfc50/mfc_shared_mem.o
  CC      net/ipv4/netfilter/nf_nat_pptp.o
  LD      drivers/media/video/samsung/mfc50/built-in.o
  CC      drivers/media/video/samsung/tv20/ddc.o
  CC      drivers/mmc/core/mmc.o
  CC      net/ipv4/netfilter/nf_nat_sip.o
  CC      drivers/media/video/samsung/tv20/hpd.o
  CC      fs/ntfs/collate.o
  CC      drivers/mmc/core/mmc_ops.o
drivers/media/video/samsung/tv20/hpd.c: In function 's5p_hpd_probe':
drivers/media/video/samsung/tv20/hpd.c:299: warning: passing argument 2 of 'request_irq' from incompatible pointer type
include/linux/interrupt.h:135: note: expected 'irq_handler_t' but argument is of type 'enum irqreturn_t (*)(int)'
drivers/media/video/samsung/tv20/hpd.c:305: warning: passing argument 1 of 's5p_hdmi_register_isr' from incompatible pointer type
drivers/media/video/samsung/tv20/s5p_tv.h:781: note: expected 'hdmi_isr' but argument is of type 'enum irqreturn_t (*)(int)'
drivers/media/video/samsung/tv20/hpd.c:308: warning: passing argument 1 of 's5p_hdmi_register_isr' from incompatible pointer type
drivers/media/video/samsung/tv20/s5p_tv.h:781: note: expected 'hdmi_isr' but argument is of type 'enum irqreturn_t (*)(int)'
  CC      drivers/media/video/samsung/tv20/cec.o
  CC      fs/ntfs/compress.o
  CC      drivers/mmc/core/sd.o
  CC      net/ipv4/netfilter/nf_nat_tftp.o
  CC      drivers/media/video/samsung/tv20/s5pv210/cec_s5pv210.o
  CC      drivers/mmc/core/sd_ops.o
  CC      fs/ntfs/debug.o
  CC      net/ipv4/netfilter/nf_nat_proto_dccp.o
  CC      drivers/media/video/samsung/tv20/s5pv210/hdcp_s5pv210.o
  CC      fs/ntfs/dir.o
  CC      drivers/mmc/core/sdio.o
drivers/media/video/samsung/tv20/s5pv210/hdcp_s5pv210.c:193: warning: 'write_ainfo' defined but not used
drivers/media/video/samsung/tv20/s5pv210/hdcp_s5pv210.c:626: warning: 'efuse_ceil' defined but not used
drivers/media/video/samsung/tv20/s5pv210/hdcp_s5pv210.c:980: warning: 's5p_hdcp_reset' defined but not used
drivers/media/video/samsung/tv20/s5pv210/hdcp_s5pv210.c:1365: warning: 's5p_hdcp_is_reset' defined but not used
drivers/media/video/samsung/tv20/s5pv210/hdcp_s5pv210.c:1375: warning: 's5p_set_hpd_detection' defined but not used
  CC      net/ipv4/netfilter/nf_nat_proto_gre.o
  CC      drivers/mmc/core/sdio_ops.o
  CC      drivers/media/video/samsung/tv20/s5pv210/hdmi_s5pv210.o
  CC      fs/ntfs/file.o
  CC      drivers/mmc/core/sdio_bus.o
  CC      net/ipv4/netfilter/nf_nat_proto_udplite.o
  CC      drivers/mmc/core/sdio_cis.o
  CC      net/ipv4/netfilter/nf_nat_proto_sctp.o
  CC      drivers/media/video/samsung/tv20/s5pv210/sdout_s5pv210.o
  CC      drivers/mmc/core/sdio_io.o
  CC      drivers/mmc/core/sdio_irq.o
  CC      net/ipv4/netfilter/ip_tables.o
  CC      drivers/media/video/samsung/tv20/s5pv210/tv_power_s5pv210.o
  CC      drivers/mmc/core/debugfs.o
  CC      drivers/media/video/samsung/tv20/s5pv210/vmixer_s5pv210.o
  CC      fs/ntfs/index.o
  LD      drivers/mmc/core/mmc_core.o
  LD      drivers/mmc/core/built-in.o
  CC      drivers/mmc/host/sdhci.o
  CC      drivers/media/video/samsung/tv20/s5pv210/vprocessor_s5pv210.o
  CC      net/ipv4/netfilter/iptable_filter.o
  CC      fs/ntfs/inode.o
  CC      drivers/media/video/samsung/tv20/s5p_stda_tvout_if.o
  LD      net/ipv4/netfilter/iptable_nat.o
  CC      net/ipv4/netfilter/ipt_addrtype.o
  CC      drivers/mmc/host/sdhci-s3c.o
  CC      drivers/media/video/samsung/tv20/s5p_stda_grp.o
  CC      net/ipv4/netfilter/ipt_ah.o
  LD      drivers/mmc/host/built-in.o
  LD      drivers/mmc/built-in.o
  CC      drivers/mtd/mtdcore.o
  CC      net/ipv4/netfilter/ipt_ecn.o
  CC      fs/ntfs/mft.o
  CC      drivers/media/video/samsung/tv20/s5p_stda_video_layer.o
  CC      drivers/mtd/mtdsuper.o
  CC      net/ipv4/netfilter/ipt_LOG.o
  CC      drivers/mtd/mtdpart.o
  CC      drivers/media/video/samsung/tv20/s5p_tv_v4l2.o
  CC      drivers/mtd/mtdconcat.o
  CC      net/ipv4/netfilter/ipt_MASQUERADE.o
  CC      drivers/media/video/samsung/tv20/s5p_tv_base.o
  CC      fs/ntfs/mst.o
  CC      fs/ntfs/namei.o
  CC      net/ipv4/netfilter/ipt_NETMAP.o
  CC      drivers/mtd/mtdchar.o
  LD      drivers/media/video/samsung/tv20/s5p_tvout.o
  LD      drivers/media/video/samsung/tv20/built-in.o
  LD      drivers/media/video/samsung/built-in.o
  CC      drivers/media/video/uvc/uvc_driver.o
  CC      fs/ntfs/runlist.o
  CC      net/ipv4/netfilter/ipt_REDIRECT.o
  CC      drivers/mtd/mtd_blkdevs.o
  CC      net/ipv4/netfilter/ipt_REJECT.o
  CC      drivers/mtd/mtdblock.o
  CC      drivers/media/video/uvc/uvc_queue.o
  CC      net/ipv4/netfilter/arp_tables.o
  CC      fs/ntfs/super.o
  CC      drivers/mtd/chips/chipreg.o
  CC      drivers/media/video/uvc/uvc_v4l2.o
  LD      drivers/mtd/chips/built-in.o
  LD      drivers/mtd/devices/built-in.o
  LD      drivers/mtd/lpddr/built-in.o
  LD      drivers/mtd/maps/built-in.o
  CC      drivers/mtd/nand/nand_base.o
  CC      net/ipv4/netfilter/arpt_mangle.o
  CC      drivers/media/video/uvc/uvc_video.o
  CC      net/ipv4/netfilter/arptable_filter.o
  CC      fs/ntfs/sysctl.o
  CC      drivers/media/video/uvc/uvc_ctrl.o
  CC      fs/ntfs/unistr.o
  CC      drivers/mtd/nand/nand_bbt.o
  LD      net/ipv4/netfilter/nf_conntrack_ipv4.o
  LD      net/ipv4/netfilter/nf_nat.o
  LD      net/ipv4/netfilter/built-in.o
  CC      net/ipv4/inet_diag.o
  CC      fs/ntfs/upcase.o
  CC      drivers/mtd/nand/nand_ecc.o
  CC      drivers/media/video/uvc/uvc_status.o
  CC      fs/ntfs/bitmap.o
  CC      drivers/mtd/nand/nand_ids.o
  CC      drivers/media/video/uvc/uvc_isight.o
  CC      net/ipv4/tcp_diag.o
  CC      fs/ntfs/lcnalloc.o
  CC      drivers/mtd/nand/s3c_nand.o
drivers/mtd/nand/s3c_nand.c:185: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:185: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:185: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:185: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:185: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:185: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:185: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:185: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:185: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:185: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:185: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:185: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:185: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:185: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:185: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:185: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:186: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:186: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:186: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:186: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:186: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:186: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:186: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:186: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:186: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:186: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:186: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:186: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:186: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:186: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:186: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:186: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:187: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:187: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:187: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:187: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:187: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:187: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:187: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:187: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:187: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:187: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:187: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:187: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:187: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:187: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:187: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:187: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:188: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:188: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:188: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:188: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:188: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:188: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:188: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:188: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:188: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:188: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:188: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:188: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:188: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:188: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:188: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:188: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:189: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:189: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:189: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:189: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:189: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:189: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:189: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:189: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:189: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:189: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:189: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:189: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:189: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:189: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c:189: warning: excess elements in array initializer
drivers/mtd/nand/s3c_nand.c:189: warning: (near initialization for 's3c_nand_oob_128.eccpos')
drivers/mtd/nand/s3c_nand.c: In function 's3c_nand_probe':
drivers/mtd/nand/s3c_nand.c:1073: warning: assignment from incompatible pointer type
drivers/mtd/nand/s3c_nand.c:1086: warning: assignment from incompatible pointer type
drivers/mtd/nand/s3c_nand.c:1101: warning: assignment from incompatible pointer type
drivers/mtd/nand/s3c_nand.c:1113: warning: assignment from incompatible pointer type
  LD      drivers/media/video/uvc/uvcvideo.o
  LD      drivers/media/video/uvc/built-in.o
  LD      drivers/media/video/videodev.o
  LD      drivers/media/video/built-in.o
  LD      drivers/media/built-in.o
  CC      drivers/net/mii.o
  CC      net/ipv4/tcp_cubic.o
  CC      fs/ntfs/logfile.o
  LD      drivers/mtd/nand/nand.o
  LD      drivers/mtd/nand/built-in.o
  LD      drivers/mtd/onenand/built-in.o
  LD      drivers/mtd/tests/built-in.o
  LD      drivers/mtd/mtd.o
  LD      drivers/mtd/built-in.o
  LD      drivers/platform/built-in.o
  CC      drivers/power/power_supply_core.o
  CC      drivers/net/Space.o
  CC      drivers/power/power_supply_sysfs.o
  CC      net/ipv4/xfrm4_policy.o
  CC      fs/ntfs/quota.o
  CC      drivers/power/s3c_fake_battery.o
  CC      drivers/net/loopback.o
  CC      fs/ntfs/usnjrnl.o
  CC      net/ipv4/xfrm4_state.o
  LD      fs/ntfs/ntfs.o
  CC      drivers/net/ppp_generic.o
  LD      fs/ntfs/built-in.o
  CC      fs/partitions/check.o
  LD      drivers/power/power_supply.o
  LD      drivers/power/built-in.o
  CC      drivers/regulator/core.o
  CC      net/ipv4/xfrm4_input.o
  CC      fs/partitions/msdos.o
  CC      net/ipv4/xfrm4_output.o
  CC      drivers/regulator/max8698.o
  CC      fs/partitions/efi.o
  LD      drivers/regulator/built-in.o
  LD      net/ipv4/built-in.o
  CC      drivers/rtc/rtc-lib.o
  CC      drivers/net/ppp_async.o
  CC      net/ipv6/af_inet6.o
  CC      drivers/rtc/hctosys.o
  LD      fs/partitions/built-in.o
  CC      fs/proc/mmu.o
  CC      drivers/rtc/class.o
  CC      fs/proc/task_mmu.o
  CC      drivers/net/ppp_synctty.o
  CC      drivers/rtc/interface.o
  CC      fs/proc/inode.o
  CC      net/ipv6/anycast.o
  CC      drivers/rtc/rtc-dev.o
  CC      drivers/net/ppp_deflate.o
  CC      fs/proc/root.o
  CC      drivers/net/bsd_comp.o
  CC      drivers/rtc/rtc-proc.o
  CC      net/ipv6/ip6_output.o
  CC      fs/proc/base.o
  CC      drivers/net/ppp_mppe.o
  CC      drivers/rtc/rtc-sysfs.o
  CC      drivers/rtc/alarm.o
  CC      drivers/net/pppox.o
  CC      drivers/rtc/alarm-dev.o
  CC      drivers/net/pppoe.o
  CC      fs/proc/generic.o
  LD      drivers/rtc/rtc-core.o
  LD      drivers/rtc/built-in.o
  CC      drivers/scsi/scsi.o
  CC      net/ipv6/ip6_input.o
  CC      fs/proc/array.o
  CC      drivers/net/pppolac.o
  CC      drivers/scsi/hosts.o
  CC      net/ipv6/addrconf.o
  CC      fs/proc/proc_tty.o
  CC      drivers/net/pppopns.o
  CC      drivers/scsi/scsi_ioctl.o
  CC      fs/proc/cmdline.o
  CC      fs/proc/cpuinfo.o
  CC      drivers/scsi/constants.o
  CC      fs/proc/devices.o
  CC      drivers/net/slhc.o
  CC      fs/proc/interrupts.o
  CC      drivers/scsi/scsicam.o
  CC      fs/proc/loadavg.o
  CC      drivers/net/ifb.o
  CC      drivers/scsi/scsi_error.o
  CC      fs/proc/meminfo.o
  CC      fs/proc/stat.o
  CC      drivers/net/dm9000.o
  CC      net/ipv6/addrlabel.o
  CC      fs/proc/uptime.o
  CC      drivers/scsi/scsi_lib.o
  CC      fs/proc/version.o
  CC      fs/proc/softirqs.o
  CC      net/ipv6/route.o
  LD      drivers/net/arm/built-in.o
  CC      fs/proc/proc_sysctl.o
  LD      drivers/net/wan/built-in.o
  LD      drivers/net/wimax/built-in.o
  CC      drivers/net/wireless/hostap/hostap_80211_rx.o
  CC      fs/proc/proc_net.o
  CC      drivers/scsi/scsi_lib_dma.o
  CC      fs/proc/kmsg.o
  CC      drivers/scsi/scsi_scan.o
  CC      drivers/net/wireless/hostap/hostap_80211_tx.o
  CC      fs/proc/page.o
  LD      fs/proc/proc.o
  LD      fs/proc/built-in.o
  CC      net/ipv6/ip6_fib.o
  LD      fs/quota/built-in.o
  CC      fs/ramfs/inode.o
  CC      drivers/net/wireless/hostap/hostap_ap.o
  CC      drivers/scsi/scsi_sysfs.o
  CC      fs/ramfs/file-mmu.o
  LD      fs/ramfs/ramfs.o
  LD      fs/ramfs/built-in.o
  CC      fs/romfs/storage.o
  CC      net/ipv6/ipv6_sockglue.o
  CC      drivers/scsi/scsi_devinfo.o
  CC      fs/romfs/super.o
  CC      drivers/scsi/scsi_sysctl.o
  CC      drivers/net/wireless/hostap/hostap_info.o
  CC      drivers/scsi/scsi_proc.o
  LD      fs/romfs/romfs.o
  LD      fs/romfs/built-in.o
  CC      fs/sysfs/inode.o
  CC      net/ipv6/ndisc.o
  CC      drivers/scsi/scsi_trace.o
  CC      fs/sysfs/file.o
  CC      drivers/net/wireless/hostap/hostap_ioctl.o
  CC      drivers/scsi/sd.o
  CC      fs/sysfs/dir.o
  CC      net/ipv6/udp.o
  CC      fs/sysfs/symlink.o
  CC      fs/sysfs/mount.o
  CC      drivers/net/wireless/hostap/hostap_main.o
  CC      fs/sysfs/bin.o
  LD      drivers/scsi/arm/built-in.o
  LD      drivers/scsi/sd_mod.o
  CC      drivers/scsi/sg.o
  CC      fs/sysfs/group.o
  CC      net/ipv6/udplite.o
  CC      drivers/net/wireless/hostap/hostap_proc.o
  CC      net/ipv6/raw.o
  LD      fs/sysfs/built-in.o
  CC      fs/sysv/ialloc.o
  CC      fs/sysv/balloc.o
  LD      drivers/net/wireless/hostap/hostap.o
  LD      drivers/net/wireless/hostap/built-in.o
  LD      drivers/net/wireless/built-in.o
  LD      drivers/net/built-in.o
  CC      drivers/serial/serial_core.o
  CC      fs/sysv/inode.o
  CC [M]  drivers/scsi/scsi_wait_scan.o
  CC      net/ipv6/protocol.o
  LD      drivers/scsi/scsi_mod.o
  LD      drivers/scsi/built-in.o
  CC      drivers/spi/spi.o
  CC      fs/sysv/itree.o
  CC      net/ipv6/icmp.o
  LD      drivers/spi/built-in.o
  CC      drivers/staging/staging.o
  CC      drivers/serial/samsung.o
  CC      drivers/staging/android/binder.o
  CC      fs/sysv/file.o
  CC      fs/sysv/dir.o
  CC      net/ipv6/mcast.o
  CC      drivers/serial/s5pv210.o
  CC      fs/sysv/namei.o
  LD      drivers/serial/built-in.o
  CC      drivers/switch/switch_class.o
  CC      drivers/switch/switch_gpio.o
  CC      fs/sysv/super.o
drivers/switch/switch_gpio.c: In function 'gpio_switch_work':
drivers/switch/switch_gpio.c:50: warning: unused variable 'data'
drivers/switch/switch_gpio.c:49: warning: unused variable 'state'
drivers/switch/switch_gpio.c: In function 'gpio_switch_probe':
drivers/switch/switch_gpio.c:151: warning: label 'err_request_gpio' defined but not used
drivers/switch/switch_gpio.c:149: warning: label 'err_set_gpio_input' defined but not used
drivers/switch/switch_gpio.c:148: warning: label 'err_detect_irq_num_failed' defined but not used
  LD      drivers/switch/built-in.o
  CC      drivers/usb/core/usb.o
  CC      fs/sysv/symlink.o
  CC      drivers/staging/android/logger.o
  CC      drivers/usb/core/hub.o
  LD      fs/sysv/sysv.o
  LD      fs/sysv/built-in.o
  CC      fs/udf/balloc.o
  CC      drivers/staging/android/ram_console.o
fs/udf/balloc.c: In function 'udf_bitmap_new_block':
fs/udf/balloc.c:263: warning: passing argument 1 of '_find_next_bit_le' from incompatible pointer type
/home/fly/project/x210bv3s/qt_x210v3s_160307/kernel/arch/arm/include/asm/bitops.h:163: note: expected 'const long unsigned int *' but argument is of type 'char *'
fs/udf/balloc.c:275: warning: passing argument 1 of '_find_next_bit_le' from incompatible pointer type
/home/fly/project/x210bv3s/qt_x210v3s_160307/kernel/arch/arm/include/asm/bitops.h:163: note: expected 'const long unsigned int *' but argument is of type 'char *'
fs/udf/balloc.c:301: warning: passing argument 1 of '_find_next_bit_le' from incompatible pointer type
/home/fly/project/x210bv3s/qt_x210v3s_160307/kernel/arch/arm/include/asm/bitops.h:163: note: expected 'const long unsigned int *' but argument is of type 'char *'
fs/udf/balloc.c:315: warning: passing argument 1 of '_find_next_bit_le' from incompatible pointer type
/home/fly/project/x210bv3s/qt_x210v3s_160307/kernel/arch/arm/include/asm/bitops.h:163: note: expected 'const long unsigned int *' but argument is of type 'char *'
  CC      net/ipv6/reassembly.o
  CC      drivers/staging/android/timed_output.o
  CC      fs/udf/dir.o
  CC      drivers/staging/android/timed_gpio.o
  CC      drivers/staging/android/lowmemorykiller.o
  CC      fs/udf/file.o
  CC      net/ipv6/tcp_ipv6.o
  LD      drivers/staging/android/built-in.o
  LD      drivers/staging/built-in.o
  CC      drivers/usb/core/hcd.o
  CC      drivers/video/fb_notify.o
  CC      fs/udf/ialloc.o
  CC      drivers/video/fbmem.o
  CC      fs/udf/inode.o
  CC      drivers/usb/core/urb.o
  CC      net/ipv6/exthdrs.o
  CC      drivers/video/fbmon.o
  CC      drivers/usb/core/message.o
  CC      drivers/video/fbcmap.o
  CC      fs/udf/lowlevel.o
  CC      net/ipv6/datagram.o
  CC      drivers/video/fbsysfs.o
  CC      fs/udf/namei.o
  CC      drivers/usb/core/driver.o
  CC      drivers/video/modedb.o
  CC      net/ipv6/ip6_flowlabel.o
  CC      drivers/video/fbcvt.o
  CC      drivers/usb/core/config.o
  CC      fs/udf/partition.o
  CC      drivers/video/backlight/lcd.o
  CC      fs/udf/super.o
  CC      drivers/usb/core/file.o
  CC      net/ipv6/inet6_connection_sock.o
  CC      drivers/video/backlight/backlight.o
  CC      drivers/usb/core/buffer.o
  CC      drivers/video/backlight/pwm_bl.o
  CC      net/ipv6/sysctl_net_ipv6.o
  CC      drivers/usb/core/sysfs.o
  LD      drivers/video/backlight/built-in.o
  CC      drivers/video/console/dummycon.o
  LD      drivers/video/console/built-in.o
  LD      drivers/video/display/built-in.o
  CC      net/ipv6/xfrm6_policy.o
  CC      drivers/video/logo/logo.o
  CC      fs/udf/truncate.o
  CC      drivers/usb/core/endpoint.o
  LOGO    drivers/video/logo/logo_linux_mono.c
  LOGO    drivers/video/logo/logo_linux_vga16.c
  LOGO    drivers/video/logo/logo_linux_clut224.c
  LOGO    drivers/video/logo/logo_x210_clut224.c
  LOGO    drivers/video/logo/logo_superh_mono.c
  LOGO    drivers/video/logo/logo_superh_vga16.c
  LOGO    drivers/video/logo/clut_vga16.c
  LOGO    drivers/video/logo/logo_blackfin_vga16.c
  LOGO    drivers/video/logo/logo_spe_clut224.c
  LOGO    drivers/video/logo/logo_mac_clut224.c
  LOGO    drivers/video/logo/logo_superh_clut224.c
  LOGO    drivers/video/logo/logo_sun_clut224.c
  LOGO    drivers/video/logo/logo_linux_full_clut224.c
  CC      fs/udf/symlink.o
  CC      drivers/usb/core/devio.o
  LOGO    drivers/video/logo/logo_parisc_clut224.c
  LOGO    drivers/video/logo/logo_blackfin_clut224.c
  LOGO    drivers/video/logo/logo_dec_clut224.c
  LOGO    drivers/video/logo/logo_m32r_clut224.c
  LOGO    drivers/video/logo/logo_sgi_clut224.c
  CC      drivers/video/logo/logo_linux_mono.o
  CC      drivers/video/logo/logo_linux_vga16.o
  CC      net/ipv6/xfrm6_state.o
  CC      drivers/video/logo/logo_linux_clut224.o
  CC      drivers/video/logo/logo_x210_clut224.o
  LD      drivers/video/logo/built-in.o
  CC      fs/udf/directory.o
  LD      drivers/video/omap2/displays/built-in.o
  LD      drivers/video/omap2/dss/built-in.o
  LD      drivers/video/omap2/omapfb/built-in.o
  LD      drivers/video/omap2/built-in.o
  CC      drivers/video/samsung/s3cfb.o
  CC      fs/udf/misc.o
  CC      net/ipv6/xfrm6_input.o
  CC      fs/udf/udftime.o
  CC      drivers/video/samsung/s3cfb_fimd6x.o
  CC      drivers/usb/core/notify.o
  CC      net/ipv6/xfrm6_output.o
  CC      fs/udf/unicode.o
  CC      drivers/usb/core/generic.o
  LD      drivers/video/samsung/built-in.o
  CC      drivers/video/cfbfillrect.o
  CC      drivers/usb/core/quirks.o
  CC      net/ipv6/netfilter.o
  LD      fs/udf/udf.o
  LD      fs/udf/built-in.o
  CC      fs/yaffs2/yaffs_ecc.o
  CC      drivers/video/cfbcopyarea.o
  CC      drivers/usb/core/devices.o
  CC      fs/yaffs2/yaffs_vfs_glue.o
  CC      drivers/video/cfbimgblt.o
  CC      net/ipv6/fib6_rules.o
  LD      drivers/usb/core/usbcore.o
  LD      drivers/usb/core/built-in.o
  LD      drivers/usb/early/built-in.o
  CC      drivers/usb/host/ehci-hcd.o
  LD      drivers/video/fb.o
  LD      drivers/video/built-in.o
  CC      net/key/af_key.o
drivers/usb/host/ehci-hub.c:109: warning: 'ehci_adjust_port_wakeup_flags' defined but not used
  CC      net/ipv6/proc.o
  CC      fs/yaffs2/yaffs_guts.o
  CC      net/ipv6/ah6.o
  LD      drivers/usb/host/built-in.o
  LD      net/key/built-in.o
  LD      drivers/usb/misc/built-in.o
  CC      net/ipv6/esp6.o
  CC      net/mac80211/main.o
  CC      drivers/usb/storage/scsiglue.o
  CC      drivers/usb/storage/protocol.o
  CC      net/ipv6/ipcomp6.o
  CC      net/mac80211/status.o
  CC      drivers/usb/storage/transport.o
  CC      fs/yaffs2/yaffs_checkptrw.o
  CC      net/ipv6/xfrm6_tunnel.o
  CC      net/mac80211/sta_info.o
  CC      fs/yaffs2/yaffs_packedtags1.o
  CC      drivers/usb/storage/usb.o
  CC      fs/yaffs2/yaffs_packedtags2.o
  CC      net/ipv6/tunnel6.o
  CC      fs/yaffs2/yaffs_nand.o
  CC      net/mac80211/wep.o
  CC      drivers/usb/storage/initializers.o
  CC      fs/yaffs2/yaffs_tagscompat.o
  CC      net/ipv6/xfrm6_mode_transport.o
  CC      drivers/usb/storage/sierra_ms.o
  CC      net/mac80211/wpa.o
  CC      fs/yaffs2/yaffs_tagsvalidity.o
  CC      net/ipv6/xfrm6_mode_tunnel.o
  CC      drivers/usb/storage/option_ms.o
  CC      fs/yaffs2/yaffs_mtdif.o
  CC      fs/yaffs2/yaffs_mtdif1.o
  CC      drivers/usb/storage/usual-tables.o
  CC      net/mac80211/scan.o
  CC      net/ipv6/xfrm6_mode_beet.o
  LD      drivers/usb/storage/usb-storage.o
  CC      fs/yaffs2/yaffs_mtdif2.o
  LD      drivers/usb/storage/built-in.o
  LD      drivers/usb/built-in.o
  LD      drivers/built-in.o
  CC      fs/yaffs2/yaffs_nameval.o
  CC      net/ipv6/mip6.o
  CC      net/mac80211/offchannel.o
  CC      net/mac80211/ht.o
  CC      fs/yaffs2/yaffs_allocator.o
  CC      net/mac80211/agg-tx.o
  CC      fs/yaffs2/yaffs_yaffs1.o
  LD      net/ipv6/netfilter/built-in.o
  CC      net/ipv6/sit.o
  CC      net/mac80211/agg-rx.o
  CC      fs/yaffs2/yaffs_yaffs2.o
  CC      net/mac80211/mlme.o
  CC      net/mac80211/ibss.o
  CC      net/ipv6/ip6_tunnel.o
  CC      fs/yaffs2/yaffs_bitmap.o
  CC      fs/yaffs2/yaffs_verify.o
  CC      net/mac80211/work.o
  LD      fs/yaffs2/yaffs.o
  LD      fs/yaffs2/built-in.o
  CC      fs/eventpoll.o
  CC      net/mac80211/iface.o
  CC      net/ipv6/addrconf_core.o
  CC      net/mac80211/rate.o
  CC      fs/anon_inodes.o
  CC      net/ipv6/exthdrs_core.o
  CC      net/mac80211/michael.o
  CC      fs/signalfd.o
  CC      net/mac80211/tkip.o
  CC      net/mac80211/aes_ccm.o
  CC      net/ipv6/inet6_hashtables.o
  CC      fs/timerfd.o
  CC      net/mac80211/aes_cmac.o
  CC      net/mac80211/cfg.o
  CC      fs/eventfd.o
  LD      net/ipv6/ipv6.o
  LD      net/ipv6/built-in.o
  CC      net/mac80211/rx.o
  CC      net/netfilter/core.o
  CC      fs/locks.o
  CC      net/mac80211/spectmgmt.o
  CC      net/netfilter/nf_log.o
  CC      net/mac80211/tx.o
  CC      net/netfilter/nf_queue.o
  CC      fs/binfmt_misc.o
  CC      net/mac80211/key.o
  CC      net/netfilter/nf_sockopt.o
  CC      fs/binfmt_script.o
  CC      fs/binfmt_elf.o
  CC      net/mac80211/util.o
  CC      net/mac80211/wme.o
  CC      net/netfilter/nf_conntrack_core.o
  CC      net/mac80211/event.o
  CC      fs/mbcache.o
  CC      net/mac80211/chan.o
  CC      net/mac80211/debugfs.o
  CC      net/netfilter/nf_conntrack_standalone.o
  CC      fs/posix_acl.o
  CC      net/mac80211/debugfs_sta.o
  CC      fs/xattr_acl.o
  CC      net/netfilter/nf_conntrack_expect.o
  CC      net/mac80211/debugfs_netdev.o
  CC      fs/generic_acl.o
  CC      net/mac80211/debugfs_key.o
  LD      fs/built-in.o
  CC      net/netfilter/nf_conntrack_helper.o
  CC      net/mac80211/mesh.o
  CC      net/mac80211/mesh_pathtbl.o
  CC      net/netfilter/nf_conntrack_proto.o
  CC      net/netfilter/nf_conntrack_l3proto_generic.o
  CC      net/mac80211/mesh_plink.o
  CC      net/netfilter/nf_conntrack_proto_generic.o
  CC      net/mac80211/mesh_hwmp.o
  CC      net/netfilter/nf_conntrack_proto_tcp.o
  CC      net/netfilter/nf_conntrack_proto_udp.o
  CC      net/mac80211/pm.o
  CC      net/mac80211/rc80211_minstrel.o
  CC      net/netfilter/nf_conntrack_extend.o
  CC      net/mac80211/rc80211_minstrel_debugfs.o
  CC      net/netfilter/nf_conntrack_acct.o
  CC      net/netfilter/nf_conntrack_ecache.o
  CC      net/netfilter/nf_conntrack_h323_main.o
  LD      net/mac80211/mac80211.o
  LD      net/mac80211/built-in.o
  CC      net/netlink/af_netlink.o
  CC      net/netfilter/nf_conntrack_h323_asn1.o
  CC      net/netfilter/nfnetlink.o
  CC      net/netfilter/nfnetlink_queue.o
  CC      net/netfilter/nfnetlink_log.o
  CC      net/netlink/genetlink.o
  LD      net/netfilter/nf_conntrack.o
  CC      net/netfilter/nf_conntrack_proto_dccp.o
  CC      net/netfilter/nf_conntrack_proto_gre.o
  CC      net/netfilter/nf_conntrack_proto_sctp.o
  LD      net/netlink/built-in.o
  CC      net/packet/af_packet.o
  CC      net/netfilter/nf_conntrack_proto_udplite.o
  CC      net/netfilter/nf_conntrack_netlink.o
  CC      net/netfilter/nf_conntrack_amanda.o
  CC      net/netfilter/nf_conntrack_ftp.o
  LD      net/netfilter/nf_conntrack_h323.o
  CC      net/netfilter/nf_conntrack_irc.o
  LD      net/packet/built-in.o
  CC      net/rfkill/core.o
  CC      net/netfilter/nf_conntrack_netbios_ns.o
  CC      net/sched/sch_generic.o
  CC      net/netfilter/nf_conntrack_pptp.o
  LD      net/rfkill/rfkill.o
  LD      net/rfkill/built-in.o
  CC      net/netfilter/nf_conntrack_sane.o
  CC      net/sunrpc/clnt.o
  CC      net/sched/sch_mq.o
  CC      net/netfilter/nf_conntrack_sip.o
  CC      net/netfilter/nf_conntrack_tftp.o
  CC      net/sched/sch_api.o
  CC      net/netfilter/x_tables.o
  CC      net/sunrpc/xprt.o
  CC      net/netfilter/xt_tcpudp.o
  CC      net/sched/sch_blackhole.o
  CC      net/netfilter/xt_mark.o
  CC      net/netfilter/xt_connmark.o
  CC      net/sunrpc/socklib.o
  CC      net/sched/cls_api.o
  CC      net/netfilter/xt_CLASSIFY.o
  CC      net/netfilter/xt_NFQUEUE.o
  CC      net/sunrpc/xprtsock.o
  CC      net/netfilter/xt_comment.o
  CC      net/netfilter/xt_connbytes.o
  CC      net/sched/act_api.o
  CC      net/netfilter/xt_connlimit.o
  CC      net/netfilter/xt_conntrack.o
  CC      net/netfilter/xt_hashlimit.o
  CC      net/sched/act_police.o
  CC      net/sunrpc/sched.o
  CC      net/netfilter/xt_helper.o
  CC      net/netfilter/xt_hl.o
  CC      net/sched/act_gact.o
  CC      net/netfilter/xt_iprange.o
  CC      net/sunrpc/auth.o
  CC      net/netfilter/xt_length.o
  CC      net/sched/act_mirred.o
  CC      net/netfilter/xt_limit.o
  CC      net/netfilter/xt_mac.o
  CC      net/sched/sch_fifo.o
  CC      net/netfilter/xt_owner.o
  CC      net/sunrpc/auth_null.o
  CC      net/netfilter/xt_pkttype.o
  CC      net/netfilter/xt_policy.o
  CC      net/sched/sch_htb.o
  CC      net/sunrpc/auth_unix.o
  CC      net/netfilter/xt_quota.o
  CC      net/netfilter/xt_state.o
  CC      net/netfilter/xt_statistic.o
  CC      net/sunrpc/auth_generic.o
  CC      net/netfilter/xt_string.o
  CC      net/netfilter/xt_time.o
  CC      net/sched/sch_ingress.o
  CC      net/sunrpc/svc.o
  CC      net/netfilter/xt_u32.o
  LD      net/netfilter/netfilter.o
  CC      net/sched/cls_u32.o
  CC      net/unix/af_unix.o
  LD      net/netfilter/built-in.o
  CC      net/wanrouter/wanproc.o
  CC      net/sunrpc/svcsock.o
  CC      net/sched/ematch.o
  CC      net/wanrouter/wanmain.o
  CC      net/sched/em_u32.o
  CC      net/unix/garbage.o
  LD      net/wanrouter/wanrouter.o
  LD      net/wanrouter/built-in.o
  CC      net/wimax/id-table.o
  CC      net/sunrpc/svcauth.o
  LD      net/sched/built-in.o
  CC      net/wireless/core.o
  CC      net/wimax/op-msg.o
  CC      net/unix/sysctl_net_unix.o
  CC      net/sunrpc/svcauth_unix.o
  LD      net/unix/unix.o
  LD      net/unix/built-in.o
  CC      net/sunrpc/addr.o
  CC      net/wimax/op-reset.o
  CC      net/wireless/sysfs.o
  CC      net/sunrpc/rpcb_clnt.o
  CC      net/wimax/op-rfkill.o
  CC      net/sunrpc/timer.o
  CC      net/wireless/radiotap.o
  CC      net/wireless/util.o
  CC      net/sunrpc/xdr.o
  CC      net/wimax/op-state-get.o
  CC      net/sunrpc/sunrpc_syms.o
  CC      net/wimax/stack.o
  CC      net/sunrpc/cache.o
  CC      net/sunrpc/rpc_pipe.o
  CC      net/wireless/reg.o
  CC      net/wimax/debugfs.o
  CC      net/sunrpc/svc_xprt.o
  LD      net/wimax/wimax.o
  CC      net/sunrpc/backchannel_rqst.o
  LD      net/wimax/built-in.o
  CC      net/xfrm/xfrm_policy.o
  CC      net/wireless/scan.o
  CC      net/sunrpc/bc_svc.o
  CC      net/sunrpc/stats.o
  CC      net/sunrpc/sysctl.o
  CC      net/sunrpc/auth_gss/auth_gss.o
  CC      net/wireless/nl80211.o
  LD      net/sunrpc/sunrpc.o
  CC      net/sysctl_net.o
  CC      net/xfrm/xfrm_state.o
  CC      net/xfrm/xfrm_hash.o
  CC      net/xfrm/xfrm_input.o
  CC      net/sunrpc/auth_gss/gss_generic_token.o
  CC      net/sunrpc/auth_gss/gss_mech_switch.o
  CC      net/xfrm/xfrm_output.o
  CC      net/xfrm/xfrm_algo.o
  CC      net/sunrpc/auth_gss/svcauth_gss.o
  CC      net/xfrm/xfrm_sysctl.o
  CC      net/xfrm/xfrm_ipcomp.o
  CC      net/wireless/mlme.o
  CC      net/sunrpc/auth_gss/gss_krb5_mech.o
  CC      net/wireless/ibss.o
  LD      net/xfrm/built-in.o
  CC      net/sunrpc/auth_gss/gss_krb5_seal.o
  CC      net/sunrpc/auth_gss/gss_krb5_unseal.o
  CC      net/wireless/sme.o
  CC      net/sunrpc/auth_gss/gss_krb5_seqnum.o
  CC      net/wireless/chan.o
  CC      net/sunrpc/auth_gss/gss_krb5_wrap.o
  CC      net/sunrpc/auth_gss/gss_krb5_crypto.o
  CC      net/wireless/ethtool.o
  CC      net/sunrpc/auth_gss/gss_krb5_keys.o
  CC      net/wireless/wext-compat.o
  CC      net/wireless/wext-sme.o
  CC      net/wireless/lib80211.o
  LD      net/sunrpc/auth_gss/auth_rpcgss.o
  LD      net/sunrpc/auth_gss/rpcsec_gss_krb5.o
  LD      net/sunrpc/auth_gss/built-in.o
  LD      net/sunrpc/built-in.o
  CC      net/wireless/lib80211_crypt_wep.o
  CC      net/wireless/lib80211_crypt_ccmp.o
  CC      net/wireless/lib80211_crypt_tkip.o
  CC      net/wireless/wext-core.o
  CC      net/wireless/wext-proc.o
  CC      net/wireless/wext-spy.o
  CC      net/wireless/wext-priv.o
  LD      net/wireless/cfg80211.o
  LD      net/wireless/built-in.o
  LD      net/built-in.o
  LD      vmlinux.o
  MODPOST vmlinux.o
WARNING: modpost: Found 5 section mismatch(es).
To see full details build your kernel with:
'make CONFIG_DEBUG_SECTION_MISMATCH=y'
  GEN     .version
  CHK     include/generated/compile.h
  UPD     include/generated/compile.h
  CC      init/version.o
  LD      init/built-in.o
  LD      .tmp_vmlinux1
  KSYM    .tmp_kallsyms1.S
  AS      .tmp_kallsyms1.o
  LD      .tmp_vmlinux2
  KSYM    .tmp_kallsyms2.S
  AS      .tmp_kallsyms2.o
  LD      vmlinux
  SYSMAP  System.map
  SYSMAP  .tmp_System.map
  OBJCOPY arch/arm/boot/Image
  Kernel: arch/arm/boot/Image is ready
  GZIP    arch/arm/boot/compressed/piggy.gzip
  AS      arch/arm/boot/compressed/head.o
  CC      arch/arm/boot/compressed/misc.o
  CC      arch/arm/boot/compressed/decompress.o
  Building modules, stage 2.
  MODPOST 2 modules
  SHIPPED arch/arm/boot/compressed/lib1funcs.S
  AS      arch/arm/boot/compressed/lib1funcs.o
  CC      crypto/ansi_cprng.mod.o
  CC      drivers/scsi/scsi_wait_scan.mod.o
  LD [M]  drivers/scsi/scsi_wait_scan.ko
  LD [M]  crypto/ansi_cprng.ko
  AS      arch/arm/boot/compressed/piggy.gzip.o
  LD      arch/arm/boot/compressed/vmlinux
  OBJCOPY arch/arm/boot/zImage
  Kernel: arch/arm/boot/zImage is ready
fly@fly-vm:kernel$

```

### 3-1.编译生成的内核

```bash
fly@fly-vm:kernel$ cd arch/arm/boot/
fly@fly-vm:boot$ ls
bootp  compressed  Image  install.sh  Makefile  test  test2  zImage
fly@fly-vm:boot$ ls -l zImage
-rwxrwxr-x 1 fly fly 3668864 11月 12 00:48 zImage
```

## 4.下载内核

---

### 4-1.uboot启动环境

```bash
SD checksum Error
OK

U-Boot 1.3.4-dirty (Nov 13 2022 - 15:19:51) for x210@Flyer


CPU:  S5PV210@1000MHz(OK)
        APLL = 1000MHz, HclkMsys = 200MHz, PclkMsys = 100MHz
        MPLL = 667MHz, EPLL = 96MHz
                       HclkDsys = 166MHz, PclkDsys = 83MHz
                       HclkPsys = 133MHz, PclkPsys = 66MHz
                       SCLKA2M  = 200MHz
Serial = CLKUART
Board:   X210BV3S
DRAM:    512 MB
Flash:   8 MB
SD/MMC:  3728MB
In:      serial
Out:     serial
Err:     serial
[LEFT UP] boot mode
checking mode for fastboot ...
Hit any key to stop autoboot:  0
x210 #
x210 #
x210 # printenv
baudrate=115200
ethact=dm9000
ethaddr=00:40:5c:26:0a:5b
mtdpart=80000 400000 3000000
bootdelay=2
filesize=60000
fileaddr=C0008000
netmask=255.255.255.0
gatewayip=192.168.0.1
ipaddr=192.168.0.188
bootcmd=tftp 30008000 zImage;bootm 30008000
serverip=192.168.0.105
bootargs=root=/dev/nfs nfsroot=192.168.0.105:/opt/v210_rootfs ip=192.168.0.188:192.168.0.105:192.168.0.1:255.255.255.0::eth0:off init=/linuxrc console=ttySAC2,115200

Environment size: 428/16380 bytes
x210 #
```

---------------

#### 4-1-1.调整启动环境

```bash
x210 # setenv serverip '192.168.0.104'
x210 # saveenv
Saving Environment to SMDK bootable device...
done
x210 # setenv bootargs 'root=/dev/nfs nfsroot 192.168.0.104:/opt/v210_rootfs ip=192.168.0.188:192.168.0.104:192.168.0.1:255.255.255.0::eth0:off init=/linuxrc console=ttySAC2,115200'
x210 # saveenv
Saving Environment to SMDK bootable device...
done
x210 # printenv
baudrate=115200
ethact=dm9000
ethaddr=00:40:5c:26:0a:5b
mtdpart=80000 400000 3000000
bootdelay=2
filesize=60000
fileaddr=C0008000
netmask=255.255.255.0
gatewayip=192.168.0.1
ipaddr=192.168.0.188
bootcmd=tftp 30008000 zImage;bootm 30008000
serverip=192.168.0.104
bootargs=root=/dev/nfs nfsroot 192.168.0.104:/opt/v210_rootfs ip=192.168.0.188:192.168.0.104:192.168.0.1:255.255.255.0::eth0:off init=/linuxrc console=ttySAC2,115200

Environment size: 428/16380 bytes
```



### 4-2.开发机网络环境

```bash
[$(USER)@fly-vm kernel]$ ifconfig
docker0   Link encap:Ethernet  HWaddr 02:42:ef:70:c0:5e
          inet addr:172.17.0.1  Bcast:172.17.255.255  Mask:255.255.0.0
          UP BROADCAST MULTICAST  MTU:1500  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)

ens33     Link encap:Ethernet  HWaddr 00:0c:29:0e:2f:a0
          inet addr:192.168.0.104  Bcast:192.168.0.255  Mask:255.255.255.0
          inet6 addr: fe80::61ee:c87d:4b00:9673/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:255087 errors:0 dropped:0 overruns:0 frame:0
          TX packets:66806 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:376999320 (376.9 MB)  TX bytes:4758279 (4.7 MB)

ens36     Link encap:Ethernet  HWaddr 00:0c:29:0e:2f:aa
          inet addr:192.168.236.128  Bcast:192.168.236.255  Mask:255.255.255.0
          inet6 addr: fe80::9de9:2a10:831:fc9c/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:10746397 errors:0 dropped:0 overruns:0 frame:0
          TX packets:10905192 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:2515081571 (2.5 GB)  TX bytes:3442298000 (3.4 GB)

lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:106 errors:0 dropped:0 overruns:0 frame:0
          TX packets:106 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:8315 (8.3 KB)  TX bytes:8315 (8.3 KB)
```

---------

### 4-3.为内核准备文件系统

```bash
[$(USER)@fly-vm opt]$ sudo ln -s /home/$(USER)/project/x210bv3s/busybox-1.35.0/x210_rootfs v210_rootfs
[$(USER)@fly-vm opt]$ ls -l v210_rootfs
lrwxrwxrwx 1 root root 58 11月 30 22:27 v210_rootfs -> /home/$(USER)/project/x210bv3s/busybox-1.35.0/x210_rootfs
[$(USER)@fly-vm opt]$ cd v210_rootfs
[$(USER)@fly-vm v210_rootfs]$ ls -a
.  ..  bin  dev  etc  lib  linuxrc  proc  sbin  sys  tmp  usr  var
```

---------

#### 4-3-1.测试文件系统（挂载）

```bash
[$(USER)@fly-vm ~]$ mkdir nfs_test
[$(USER)@fly-vm ~]$ sudo mount 192.168.0.104:/opt/v210_rootfs  nfs_test/
mount.nfs: mount system call failed
[$(USER)@fly-vm ~]$ sudo /etc/init.d/nfs-kernel-server restart
[ ok ] Restarting nfs-kernel-server (via systemctl): nfs-kernel-server.service.
[$(USER)@fly-vm ~]$ sudo exportfs -a
[$(USER)@fly-vm ~]$ sudo mount 192.168.0.104:/opt/v210_rootfs  nfs_test/
[$(USER)@fly-vm ~]$ cd nfs_test/
[$(USER)@fly-vm nfs_test]$ ls -a
.  ..  bin  dev  etc  lib  linuxrc  proc  sbin  sys  tmp  usr  var
[$(USER)@fly-vm nfs_test]$ cd ..
[$(USER)@fly-vm ~]$ sudo umount nfs_test/
```



### 4-4.复制内核到TFTP目录

```bash
[$(USER)@fly-vm tftpboot]$ cd -
/home/$(USER)/project/x210bv3s/qt_x210v3s_160307/kernel
[$(USER)@fly-vm kernel]$ ls arch/arm/boot/zImage
arch/arm/boot/zImage
[$(USER)@fly-vm kernel]$ sudo cp arch/arm/boot/zImage /tftpboot/
[$(USER)@fly-vm kernel]$ cd /tftpboot/
[$(USER)@fly-vm tftpboot]$ ls -l
total 3584
-rwxr-xr-x 1 root root 3668860 11月 30 22:22 zImage
```

----

### 4-5.`TFTP`下载内核、`NFS`挂载`rootfs`

> 通过上面的一些准备，可以开始`TFTP`下载内核、`NFS`挂载`rootfs`；

-----------

#### 4-5-1.挂载rootfs失败1

```bash
[    8.251563] Root-NFS: Server returned error -13 while mounting /tftpboot/192.168.0.188
[    8.258055] VFS: Unable to mount root fs via NFS, trying floppy.
[    8.264269] VFS: Cannot open root device "nfs" or unknown-block(2,0)
[    8.270315] Please append a correct "root=" boot option; here are the available partitions:
[    8.278786] b300         3817472 mmcblk0 driver: mmcblk
[    8.283837]   b301          264759 mmcblk0p1
[    8.288063]   b302          264759 mmcblk0p2
[    8.292309]   b303          104412 mmcblk0p3
[    8.296555]   b304         3158463 mmcblk0p4
[    8.300803] b308         7761920 mmcblk1 driver: mmcblk
[    8.306001]   b309         7757824 mmcblk1p1
[    8.310247] Kernel panic - not syncing: VFS: Unable to mount root fs on unknown-block(2,0)
[    8.318498] Backtrace:
[    8.320926] [<c0037fb8>] (dump_backtrace+0x0/0x110) from [<c0509828>] (dump_stack+0x18/0x1c)
[    8.329329]  r6:00008000 r5:dfecc000 r4:c0701b6c r3:00000002
[    8.334949] [<c0509810>] (dump_stack+0x0/0x1c) from [<c05098a4>] (panic+0x78/0xf8)
[    8.343557] [<c050982c>] (panic+0x0/0xf8) from [<c0008fa8>] (mount_block_root+0x1d8/0x218)
[    8.350739]  r3:00000002 r2:00000001 r1:dfc37f60 r0:c064f8aa
[    8.356351] [<c0008dd0>] (mount_block_root+0x0/0x218) from [<c00090ac>] (mount_root+0xc4/0xf8)
[    8.364953] [<c0008fe8>] (mount_root+0x0/0xf8) from [<c0009244>] (prepare_namespace+0x164/0x1bc)
[    8.373807]  r5:c002b331 r4:c073d900
[    8.377235] [<c00090e0>] (prepare_namespace+0x0/0x1bc) from [<c00084fc>] (kernel_init+0x128/0x170)
[    8.386174]  r5:c00083d4 r4:c073d6c0
[    8.389719] [<c00083d4>] (kernel_init+0x0/0x170) from [<c005b894>] (do_exit+0x0/0x5f0)
[    8.397616]  r4:00000000 r3:00000000
[    8.401152] Rebooting in 5 seconds..
[   13.440230] Restarting Linux version 2.6.35.7+ ($(USER)@fly-vm) (gcc version 4.4.1 (Sourcery G++ Lite 2009q3-67) ) #1 PREEMPT Wed Nov 30 21:54:05 CST 2022
[   13.440241]
[   13.455998] arch_reset: attempting watchdog re

```

-------

##### 4-5-1-1.尝试重启nfs无效

```bash
[$(USER)@fly-vm ~]$ sudo /etc/init.d/nfs-kernel-server restart
[ ok ] Restarting nfs-kernel-server (via systemctl): nfs-kernel-server.service.
```

------

##### 4-5-1-2.最终解决办法

> 最后修改启动参数如下，成功进入系统；

---

```bash
setenv bootargs 'root=/dev/nfs nfsroot=192.168.0.104:/opt/rootfs ip=192.168.0.188:192.168.0.104:192.168.0.1:255.255.255.0::eth0:off init=/linuxrc console=ttySAC2,115200'
```

> 或者挂载另外一个文件系统。

--------

```bash
setenv bootargs 'root=/dev/nfs nfsroot=192.168.0.104:/opt/v210_rootfs ip=192.168.0.188:192.168.0.104:192.168.0.1:255.255.255.0::eth0:off init=/linuxrc console=ttySAC2,115200'
```

