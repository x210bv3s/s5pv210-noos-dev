

# [`S5PV210`|`S5PV210` 概述](https://www.cnblogs.com/feige1314/p/17392046.html)

------------------------------------------

[TOC]



## 1.`S5PV210` 概述

- ### 1.1 架构概述

> `S5PV210` 是一款 **32** 位 **RISC** 高性价比、低功耗、高性能的微处理器解决方案，适用于手机和一般应用。它集成了`ARM Cortex-A8`**内核**，实现了**ARM**架构**V7-A**，支持外设。

> 为`3G`和`3.5G`通信业务提供优化的硬件（`H/W`）性能，`S5PV210`采用64位内部总线架构。这包括许多用于运动视频处理、显示控制和缩放等任务的强大硬件加速器。集成多格式编解码器 (`MFC`) 支持 `MPEG-1/2/4`、`H.263` 和 `H.264` 的编码和解码，以及 `VC1` 的解码。
> 此硬件加速器 (**MFC**) 支持实时视频会议和模拟电视输出、用于 **NTSC** 的 **HDMI** 和 **PAL** 模式。

> `S5PV210` 具有与外部存储器的接口，能够维持高端通信服务所需的大量存储器带宽。存储器系统具有用于并行访问的 **Flash/ROM** 外部存储器端口和 DRAM 端口，以满足高带宽。 **DRAM 控制器**支持 **LPDDR1**（移动 DDR）、**DDR2** 或 **LPDDR2**。

> **Flash/ROM** 端口支持 **NAND Flash**、**NOR-Flash**、**OneNAND**、**SRAM** 和 **ROM** 类型的外部存储器。

> 为了降低系统总成本并增强整体功能，`S5PV210` 包括许多硬件外设，例如 **TFT 24** 位真彩色 **LCD 控制器**、**摄像头接口**、**MIPI DSI**、**CSI-2**、**电源管理系统管理器**、**ATA 接口**、四个 **UART**、 24 通道 **DMA**、五个**定时器**、**通用 I/O 端口**、三个 **I2S**、**S/PDIF**、三个（通用）**IIC-BUS** 接口、两个 **HS-SPI**、**USB Host 2.0**、高速运行的 **USB 2.0 OTG** (480Mbps) 、四个 SD 主机和高速多媒体卡接口，以及四个用于时钟生成的 **PLL**。

> 带有 **MCP** 的包上包 (**POP**) 选项可用于小尺寸应用。

- ### 1.2 `S5PV210` 框图

> `S5PV210` 的完整框图如图 1-1 所示。

<img src="https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/S5PV210-Block-Diagram.png" alt="S5PV210-Block-Diagram" style="zoom:60%;" />

- ### 1.3 `S5PV210`的主要特性

  `S5PV210` 的主要特性包括：  
  	• 带有 NEON 的基于 ARM CortexTM-A8 的 CPU 子系统    
  		− 32/ 32 KB I/D 缓存，512 KB L2 缓存    
  		− 工作频率高达 800 MHz 或 1 GHz  
  	• 64 位多层总线架构   
  		− ARM CortexTM-A8 的 MSYS 域、3D 引擎、多格式编解码器和中断控制器   
  	• 工作频率高达 200 MHz   
  		− DSYS 域主要用于 Display IP（如 LCD 控制器、Camera interface 和 TVout）和 MDMA   
  	• 工作频率高达 166 MHz   
  		− PSYS 域主要用于其他系统组件，例如系统外设、外部存储器接口、外围 DMA、连接 IP 和音频接口。   
  	• 工作频率高达 133 MHz   
  		− 用于低功耗音频播放的音频域   
  	• 移动应用的高级电源管理   
  	• 64 KB ROM 用于安全启动和 96 KB RAM 用于安全功能   
  	• 8 位 ITU 601/656 相机接口支持水平尺寸高达 4224 像素的缩放和 8192 像素的未缩放分辨率  
  	• 多格式编解码器提供高达 1080p@30fps 的 MPEG-4/H.263/H.264 编码和解码以及高达 1080p@30 fps 的 MPEG-2/VC1 视频解码   
  	• 具有可编程着色器的 3D 图形加速高达 2000 万个三角形/秒和 1000 个百万像素/秒   
  	• 2D 图形加速高达 160Mpixels/s   
  	• 1/ 2/ 4/ 8 bpp Palletized 或 8/ 16/ 24 bpp Non-Palletized 彩色 TFT 建议高达 XGA 分辨率   
  	• 电视输出和 HDMI 接口支持 NTSC 和 PAL 模式，带有图像增强器   
  	• MIPI-DSI 和 MIPI-CSI 接口支持   
  	• 1 个 AC-97 音频编解码器接口和 3 通道 PCM 串行音频接口   
  	• 三个 24 位 I2S 接口支持   
  	• 1 个 TX only S/PDIF 接口支持数字音频   
  	• 三个 I2C 接口支持   
  	• 两个 SPI 支持   
  	• 四个 UART 支持三个用于蓝牙 2.0 的 Mbps 端口   
  	• 片上 USB 2.0 OTG 支持高速（480 Mbps，片上收发器）  
  	• 片上 USB 2.0 主机支持   
  	• 异步调制解调器接口支持   
  	• 四个 SD/ SDIO/ HS-MMC 接口支持   
  	• ATA/ATAPI-6 标准接口支持   
  	• 24 通道 DMA 控制器（8 通道用于内存到内存 DMA，16 通道用于外设 DMA）   
  	• 支持 14x8 键矩阵   
  	• 10 通道 12 位多路复用 ADC  
  	• 可配置的 GPIO   
  	• 实时时钟、PLL、PWM 定时器和看门狗定时器   
  	• 系统定时器支持在掉电模式（睡眠模式除外）下提供准确的滴答时间   
  	• 内存子系统   
  		− 带有 x8 或 x16 数据总线的异步 SRAM/ ROM/ NOR 接口   
  		− NAND 接口与 x8 数据总线   
  		− 带 x16 数据总线的复用/解复用 OneNAND 接口   
  		− LPDDR1 接口与 x16 或 x32 数据总线（高达 400 Mbps/pin DDR）   
  		− DDR2 接口与 x16 或 x32 数据总线（高达 400 Mbps/pin DDR）   
  		− LPDDR2 接口（高达 400 Mbps/pin DDR）  

- #### 1.3.1 微处理器

  - 该微处理器的主要特性包括：  
    • ARM CortexTM-A8 处理器是第一款基于ARMv7 架构的应用处理器。  
    • ARM CortexTM-A8 处理器能够将速度扩展到 1 GHz，满足功耗优化的移动设备的要求，这些设备要求以低于 300mW 的速度运行；性能优化的消费类应用程序需要 2000 Dhrystone MIPS。  
    • 支持首个采用 ARM 技术的超标量处理器，用于增强代码密度和性能，NEONTM 技术用于多媒体和信号处理，以及 Jazelle® RCT 技术用于增强代码密度和性能。  
    Java 和其他字节码语言的提前和即时编译。  
    • ARM CortexTM-A8 的其他特性包括：  
    	− Thumb-2 技术可实现更高的性能、能效和代码密度  
    	− NEOTM 信号处理扩展  
    	− Jazelle RCT Java 加速技术  
    	− 用于安全交易和 DRM 的 TrustZone 技术  
    	− 13 级主整数流水线  
    	− 10 级 NEOTM 媒体管道   
    	− 使用标准编译 RAM 的集成 L2 缓存   
    	− 针对性能和功耗优化的 L1 缓存      

- #### 1.3.2 内存子系统

  内存子系统的主要特性包括：  
  • 高带宽内存矩阵子系统  
  • 两个独立的外部存储器端口（1 x16 静态混合存储器端口和 2 x32 DRAM 端口）  
  • 矩阵架构通过同时访问能力增加了整体带宽   
  − SRAM/ ROM/ NOR 接口  
  	o x8 或 x16 数据总线   
  	o 地址范围支持：23 位    
  	o 支持异步接口    
  	o 支持字节和半字访问     
  − OneNAND 接口  
  	o x16 数据总线    
  	o 地址范围支持：16 位       
  	o 支持字节和半字访问    
  	o 支持 OneNAND 和 Flex OneNAND   
  	o 支持专用 DMA   
  − NAND 接口  
  	o 支持行业标准 NAND 接口   
  	o x8 数据总线   
  − LPDDR1 接口  
  	o x32 数据总线，400 Mbps/pin 双倍数据速率 (DDR)   
  	o 1.8V 接口电压   
  	o DMC0 端口支持高达 4 Gb，DMC1 端口支持高达 8 Gb   
  − DDR2 接口  
  	o x32 数据总线，具有 400 Mbps/引脚双倍数据速率 (DDR)   
  	o 1.8V 接口电压   
  	o DMC0 端口支持高达 4 Gb，DMC1 端口支持高达 8 Gb   
  	o 在 8 bank 的情况下，DMC 0&1 中只有 1 个 CS 可用   
  − LPDDR2 接口  
  	o x32 数据总线，速率高达 400 Mbps/pin   
  	o 1.2V/1.35V 接口电压   
  	o DMC0 端口支持高达 4 Gb，DMC1 端口支持高达 8 Gb   

- #### 1.3.3 多媒体

  多媒体的主要特点包括：  

  ##### • 相机接口  

  ​	− 多输入支持   
  ​		o ITU-R BT 601/656 模式   
  ​		o DMA（AXI 64 位接口）模式   
  ​		o MIPI (CSI) 模式   
  ​	− 多输出支持   
  ​		o DMA（AXI 64 位接口）模式  
  ​		o 直接 FIFO 模式  
  ​	− 数字放大 (DZI) 功能  
  ​	− 多相机输入支持  
  ​	− 视频同步信号的可编程极性  
  ​	−  输入水平尺寸支持高达 4224 像素的缩放和 8192 像素的非缩放分辨率  
  ​	− 图像镜像和旋转（X轴镜像、Y轴镜像、90°、180°和270°旋转）  
  ​	− 各种图像格式生成  
  ​	− 捕捉帧控制支持  
  ​	− 图像效果支持  

  ##### • 多格式视频编解码器 (MFC)  

  ​	− ITU-T H.264、ISO/IEC 14496-10  
  ​		o 解码支持 Baseline/Main/High Profile Level 4.0（灵活宏块排序除外）  
  ​	(FMO)、任意切片排序 (ASO) 和冗余切片 (RS))   
  ​		o 编码支持 Baseline/Main/High Profile（FMO、ASO 和 RS 除外）  
  ​	− ITU-T H.263 Profile level 3  
  ​		o 解码支持 Profile3，最高可达标清分辨率 30 fps（支持 H.263 附件）  
  ​	- 附件 I：高级帧内编码  
  ​	- 附件 J：去块（环路）滤波器     
  ​	- 附件 K：无 FMO 和 ASO 的切片结构模式      
  ​	- 附件 T：修改后的量化     
  ​	- 附件 D：无限制运动矢量模式    
  ​	- 附件 F：高级预测模式，除了亮度的重叠运动补偿  
  ​		o 编码支持 Baseline Profile（支持客户尺寸高达 1920x1088）  
  ​		− ISO/IEC 14496-2 MPEG-4  
  ​		o 解码支持 MPEG-4 Simple/Advanced Simple Profile Level5  
  ​		− 编码支持MPEG-4 Simple/Advanced Simple Profile  
  ​		− ISO/IEC 13818-2 MPEG-2  
  ​		o 解码支持 Main Profile High level  
  ​		o 解码支持除 D-picture 之外的 MPEG-1  
  ​		− SMPTE 421M VC-1  
  ​		o 解码支持 Simple Profile Medium Level/Main Profile High Level/Advanced Profile Level4  
  ​		• JPEG 编解码器  
  ​		− 支持高达 8192x8192 的压缩/解压  
  ​		− 支持的压缩格式  
  ​		o 输入原始图像：YCbCr4:2:2 或 RGB565  
  ​		o 输出 JPEG 文件：YCbCr4:2:2 或 YCbCr4:2:0 的基线 JPEG  
  ​		− 支持的解压格式（参见第 9.13 章 JPEG）  
  ​		o 输入 JPEG 文件：YCbCr4:4:4、YCbCr4:2:2、YCbCr4:2:0 的基线 JPEG 或灰色  
  ​		o 输出原始图像：YCbCr4:2:2 或 YCbCr4:2:0  
  ​		− 支持通用色彩空间转换器  
  ​		• 3D 图形引擎 (SGX540)  
  ​		− 支持通用硬件上的3D图形、矢量图和视频编解码  
  ​		− 基于瓦片的架构  
  ​		− Universal Scalable Shader Engine – 包含像素和顶点着色器的多线程引擎功能  
  ​		− 行业标准 API 支持 –OGL-ES 1.1 和 2.0 以及 OpenVG 1.0  
  ​		− 细粒度的任务切换、负载均衡和电源管理  
  ​		− 先进的几何 DMA 驱动操作，可最大限度地减少 CPU 交互  
  ​		− 可编程高质量图像抗锯齿  
  ​		− 完全虚拟化的内存寻址，用于操作系统在统一内存架构中的功能  
  ​		• 2D 图形引擎  
  ​		− 比特BLT  
  ​		− 支持最大 8000x8000 图像尺寸  
  ​		− 窗口剪裁、90°/180°/270° 旋转、X 翻转/Y 翻转  
  ​		− 反向寻址（X 正/负，Y 正/负）  
  ​		− 全 4 操作数光栅操作 (ROP4)  
  ​		− Alpha 混合（固定 Alpha 值/每像素 Alpha 值）  
  ​		− 任意大小的像素图案绘制，图案缓存  
  ​		− 16/24/32-bpp。打包的 24-bpp 颜色格式  
  ​		• 模拟电视接口  
  ​		− 输出视频格式：NTSC-M/NTSC-J/NTSC4.43/PAL-B、D、G、H、I/PAL-M/PAL-N/PAL-Nc/PAL-60
  ​		合规  
  ​		− 支持的输入格式：ITU-R BT.601 (YCbCr 4 :4 :4)  
  ​		− 支持 480i/p 和 576i 分辨率  
  ​		− 支持复合  
  ​		• 数字电视接口  
  ​		− 高清多媒体接口（HDMI）1.3  
  ​		− 支持高达 1080p 30Hz 和 8 声道/112 kHz/24 位音频  
  ​		− 支持 480p、576p、720p、1080i、1080p（不支持 480i）  
  ​		− 支持 HDCP v1.1  
  ​		• 旋转器  
  ​		− 支持的图像格式：YCbCr422（交错）、YCbCr420（非交错）、RGB565 和 RGB888  
  ​		（未包装）  
  ​		− 支持旋转角度：90、180、270、垂直翻转、水平翻转  
  ​		• 视频处理器：视频处理器支持：  
  ​		− BOB/ 2D-IPC 模式  
  ​		− 产生 YCbCr 4:4:4 输出以帮助混音器混合视频和图形  
  ​		− 1/4X 至 16X 垂直缩放，带有 4 抽头/16 相多相滤波器  
  ​		− 1/4X 至 16X 水平缩放，带有 8 抽头/16 相多相滤波器  
  ​		− 平移和扫描、信箱和使用缩放的 NTSC/PAL 转换  
  ​		− 显示区域内灵活缩放的视频定位  
  ​		− 1/16 像素分辨率平移和扫描模式  
  ​		− 灵活的后期视频处理  
  ​		o 色彩饱和度、亮度/对比度增强、边缘增强  
  ​		o BT.601 和 BT.709 之间的色彩空间转换  
  ​		− 视频输入源尺寸高达 1920x1080  
  ​		• 视频混合器  
  ​		视频混合器支持：  
  ​		− 重叠和混合输入视频和图形层  
  ​		− 480i/p、576i/p、720p 和 1080i/p 显示尺寸  
  ​		− 四层（1 个视频层、2 个图形层和 1 个背景层）  
  ​		• TFT-LCD 接口  
  ​		TFT-LCD 接口支持：  
  ​		− 24/ 18/ 16-bpp 并行 RGB 接口 LCD  
  ​		− 8/ 6 bpp 串行 RGB 接口  
  ​		− 双 i80 接口 LCD  
  ​		− 1/ 2/ 4/ 8 bpp 托盘化或 8/16/24-bpp 非托盘化彩色 TFT  
  ​		− 典型实际屏幕尺寸：1024x768、800x480、640x480、320x240、160x160等  
  ​		− 虚拟图像高达 16M 像素（4K 像素 x4K 像素）  
  ​		− 五个用于 PIP 或 OSD 的窗口层  
  ​		− 实时叠加平面复用  
  ​		− 可编程 OSD 窗口定位  
  ​		− 8 位 Alpha 混合（平面/像素）  
  ​		− ITU-BT601/656 格式输出	  

- #### 1.3.4 音频子系统

  音频子系统的主要特性包括：  

  ##### • 音频处理由可重构处理器 (RP) 推进  

  ##### • 低功耗音频子系统  

  ​	− 5.1 通道 I2S，具有 32 位宽 64 深度 FIFO  
  ​	− 128 KB 音频播放输出缓冲区  
  ​	−  硬件混音器混合主要和次要声音   

- #### 1.3.5 安全子系统

  安全子系统的主要功能包括：  

  ##### • 片上安全启动 ROM  

  ​	− 64 KB 安全启动 ROM 用于安全启动  

  ##### • 片上安全 RAM  

  ​	− 96 KB 安全 RAM 用于安全功能  

  ##### • 硬件加密加速器  

  ​	− 安全集成的 DES/TDES、AES、SHA-1、PRNG 和 PKA  
  ​	− 访问控制（带有 ARM TrustZone 硬件的安全域管理器）  
  ​	− 为安全的单独（安全/非安全）执行环境启用增强的安全平台敏感应用  

  ##### • 安全 JTAG  

  ​	− JTAG 用户认证  
  ​	− JTAG 模式下的访问控制  

- #### 1.3.6 连通性

  连通性的主要特点包括：  
  
  ##### • PCM 音频接口  
  
  ​	− 16 位单声道音频接口  
  ​	− 仅主模式  
  ​	− 支持三口PCM接口  
  
  ##### • AC97 音频接口  
  
  ​	− 立体声 PCM 输入、立体声 PCM 输出和单声道 MIC 输入的独立通道  
  ​	− 16 位立体声（2 声道）音频  
  ​	− 可变采样率 AC97 编解码器接口（48 kHz 及以下）  
  ​	− 支持 AC97 全规格  
  
  ##### • SPDIF 接口（仅限 TX）  
  
  ​	− 每个样本支持高达 24 位的线性 PCM  
  ​	− 非线性 PCM 格式，如 AC3、MPEG1 和 MPEG2 支持  
  ​	− 2x24 位缓冲区，交替填充数据  
  
  ##### • I2S 总线接口  
  
  ​	− 三个 I2S 总线，用于基于 DMA 操作的音频编解码器接口  
  ​	− 串行，每通道 8/16/24 位数据传输  
  ​	− 支持I2S、MSB-justified、LSB-justified数据格式  
  ​	− 支持 PCM 5.1 声道  
  ​	− 各种位时钟频率和编解码器时钟频率支持  
  ​		o 16、24、32、48 fs 的位时钟频率  
  ​		o 256、384、512、768 fs 的编解码器时钟  
  ​	− 支持 1 个 5.1 通道 I2S 端口（在音频子系统中）和 2 个 2 通道 I2S 端口  
  
  ##### • 调制解调器接口  
  
  ​	− 异步直接/间接 16 位 SRAM 式接口  
  ​	− 用于直接接口的片上 16 KB 双端口 SRAM 缓冲器  
  
  ##### • I2C 总线接口  
  
  ​	− 三个多主 IIC 总线  
  ​	− 串行、面向 8 位和双向数据传输可在标准中以高达 100 Kbit/s 的速度进行模式  
  ​	− 快速模式下高达 400 Kbit/s  
  
  ##### • ATA 控制器  
  
  ​	− 兼容 ATA/ATAPI-6 标准  
  
  ##### • 串口  
  
  ​	− 四个 UART，具有基于 DMA 或基于中断的操作  
  ​	− 支持 5 位、6 位、7 位或 8 位串行数据发送/接收  
  ​	− UART0 的 Rx/Tx 独立 256 字节 FIFO、UART1 的 64 字节 FIFO 和 UART2/3 的 16 字节 FIFO  
  ​	− 可编程波特率  
  ​	− 支持 IrDA 1.0 SIR (115.2 Kbps) 模式  
  ​	− 用于测试的环回模式   
  ​	− 波特时钟生成中的非整数时钟分频   
  
  ##### • USB 2.0 OTG  
  
  ​	− 符合 USB 2.0 的 OTG Revision 1.0a 补充  
  ​	− 支持高达 480 Mbps 的高速  
  ​	− 片上 USB 收发器  
  
  ##### • USB 主机 2.0  
  
  ​	− 符合 USB Host 2.0  
  ​	− 支持高达 480 Mbps 的高速  
  ​	− 片上 USB 收发器  
  
  ##### • HS-MMC/SDIO 接口  
  
  ​	− 多媒体卡协议版本 4.3 兼容 (HS-MMC)  
  ​	− SD 存储卡协议版本 2.0 兼容   
  ​	− 基于 DMA 或基于中断的操作   
  ​	− 128 字 FIFO 用于 Tx/Rx  
  ​	− 四口HS-MMC或四口SDIO   
  
  ##### • SPI 接口  
  
  ​	− 符合三个串行外设接口协议 2.11 版   
  ​	− 用于 SPI0 的 Rx/Tx 独立 64 字 FIFO 和用于 SPI1 的 16 字 FIFO  
  ​	− 基于 DMA 或基于中断的操作            	 
  
  ##### • 通用输入输出口  
  
  ​	− 237 个多功能输入/输出端口  
  ​	− 控制 178 个外部中断  
  ​	− GPA0：8 个输入/输出端口 – 2xUART 带流量控制  
  ​	− GPA1：4 个输入/输出端口 – 2xUART 不带流量控制或 1xUART 带流量控制  
  ​	− GPB：8 个输入/输出端口 – 2x SPI  
  ​	− GPC0：5 个输入/输出端口 – I2S、PCM、AC97  
  ​	− GPC1：5 个输入/输出端口 – I2S、SPDIF、LCD_FRM  
  ​	− GPD0：4 个输入/输出端口 – PWM   
  ​	− GPD1：6 个输入/输出端口 – 3xI2C、PWM、IEM   
  ​	− GPE0,1：13 进/出端口 – 摄像头接口  
  ​	− GPF0,1,2,3: 30 输入/输出端口 – LCD 接口   
  ​	− GPG0,1,2,3: 28 in/out port – 4xMMC channel (Channel 0 and 2 support 4-bit and 8-bit mode, but channel 1 and 3 only support 4-bit mode)  
  ​	− GPH0、1、2、3：32 输入/输出端口 – 键盘、外部唤醒（高达 32 位）、HDMI  
  ​	− GPI：低功耗 I2S、PCM  
  ​	− GPJ0、1、2、3、4：35 个输入/输出端口——调制解调器 IF、CAMIF、CFCON、键盘、SROM ADDR[22:16]   
  ​	− MP0_1,2,3: 20 in/out port – EBI (SROM, NF, CF, and OneNAND) 的控制信号  
  ​	− MP0_4,5,6,7: 32 in/out memory port – EBI（有关EBI配置的更多信息，请参阅第5.6章EBI）     		
  
- #### 1.3.7 系统外设

  系统外设的主要特性包括：  
  
  ##### • 实时时钟  
  
  ​	− 完整的时钟功能：秒、分钟、小时、日期、日、月和年  
  ​	− 32.768kHz 操作  
  ​	− 报警中断  
  ​	− 计时中断   
  
  ##### • 锁相环  
  
  ​	− 4 个片上 PLL，APLL/MPLL/EPLL/VPLL   
  ​	− APLL 生成 ARM 内核和 MSYS 时钟   
  ​	− MPLL 生成系统总线时钟和特殊时钟   
  ​	− EPLL 生成特殊时钟   
  ​	− VPLL 为视频接口生成时钟   
  
  ##### • 键盘   
  
  ​	− 14x8 键矩阵支持  
  ​	− 提供内部去抖动过滤器   
  
  ##### • 带脉宽调制的定时器   
  
  ​	− 具有基于中断操作的五通道 32 位内部定时器   
  ​	− 带 PWM 的四通道 32 位定时器   
  ​	− 可编程占空比、频率和极性   
  ​	− 死区生成   
  ​	− 支持外部时钟源   
  
  ##### • 系统定时器   
  
  ​	− 准确的计时器在除睡眠以外的任何电源模式下提供精确的 1ms 滴答  
  ​	− 可以在不停止参考滴答定时器的情况下更改中断间隔  
  
  ##### • DMA   
  
  ​	− 基于微码编程的DMA  
  ​	− 特定指令集为编程 DMA 传输提供了灵活性  
  ​	− 支持链表DMA功能  
  ​	− 支持三个增强型内置 DMA，每个 DMA 有 8 个通道，因此通道总数 支持的是 24  
  ​	− 支持一种 Memory-to-memory 类型优化的 DMA 和两种 Peripheral-to-memory 类型优化 DMA  
  ​	− M2M DMA 最多支持 16 个突发，P2M DMA 最多支持 8 个突发  
  
  ##### • A/D 转换器和触摸屏界面  
  
  ​	− 10 通道多路复用 ADC  
  ​	− 最大 500Ksamples/sec 和 12 位分辨率  
  
  ##### • 看门狗定时器  
  
  ​	− 16 位看门狗定时器  
  
  ##### • 向量中断控制器  
  
  ​	− 中断设备驱动程序等软件可以屏蔽特定的中断请求  
  ​	− 中断嵌套的中断源优先级  
  
  ##### • 能源管理  
  
  ​	− 组件的时钟门控控制  
  ​	− 提供多种低功耗模式，例如空闲、停止、深度停止、深度空闲和睡眠模式  
  ​	− 休眠模式下的唤醒源为外部中断、RTC 闹钟、Tick 定时器和按键接口。  
  ​	− 停止和深度停止模式的唤醒源是 MMC、触摸屏界面、系统定时器和睡眠模式的整个唤醒源。  
  ​	− 深度空闲模式的唤醒源为 5.1ch I2S 和停止模式的唤醒源。	  
  
- ### 1.4 惯例

- #### 1.4.1 寄存器 R/W 约定

| 符号 | 定义           | 描述                                                         |
| ---- | -------------- | ------------------------------------------------------------ |
| R    | 只读           | 应用程序有权读取注册字段。 写入只读字段无效。                |
| W    | 只写           | 应用程序有权在注册字段中写入。                               |
| R/W  | 可读写         | 应用程序有权在“注册”字段中读取和写入。 应用程序通过写入 1’b1 来设置该字段，并通过写入 1’b0 清除它。 |
| R/WC | 读写清除       | 应用程序有权在“注册”字段中读取和写入。 应用程序通过写入 1’b1 清除该字段。 寄存器写入 1'b0 对该字段没有影响。 |
| R/WS | 读取和写入设置 | 应用程序有权在“注册”字段中读取和写入。 应用程序通过写入 1’b1 来设置该字段。 寄存器写入 1'b0 对该字段没有影响。 |

- #### 1.4.2 寄存器值约定

| 表达式           | 描述                           |
| ---------------- | ------------------------------ |
| x                | 未定义位                       |
| X                | 未定义的多个位                 |
| ?                | 未定义，但取决于设备或引脚状态 |
| Device dependent | 该值取决于设备                 |
| Pin value        | 该值取决于引脚状态             |

------



## 2.内存映射

> 本章介绍 **S5PV210** 处理器中可用的内存映射。

- ### 2.1 内存地址映射

<img src="https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/S5PV210-MEM-ADDR-MAP.png" alt="S5PV210-MEM-ADDR-MAP" style="zoom:60%;" />

- ### 2.1.1 设备特定地址空间

| 起始地址    | 结束地址    | 大小   | 描述（备注）                        |
| ----------- | ----------- | ------ | ----------------------------------- |
| 0x0000_0000 | 0x1FFF_FFFF | 512MB  | Boot area(镜像区域取决于引导模式。) |
| 0x2000_0000 | 0x3FFF_FFFF | 512MB  | DRAM 0                              |
| 0x4000_0000 | 0x7FFF_FFFF | 1024MB | DRAM 1                              |
| 0x8000_0000 | 0x87FF_FFFF | 128MB  | SROM Bank 0                         |
| 0x8800_0000 | 0x8FFF_FFFF | 128MB  | SROM Bank 1                         |
| 0x9000_0000 | 0x97FF_FFFF | 128MB  | SROM Bank 2                         |
| 0x9800_0000 | 0x9FFF_FFFF | 128MB  | SROM Bank 3                         |
| 0xA000_0000 | 0xA7FF_FFFF | 128MB  | SROM Bank 4                         |
| 0xA800_0000 | 0xAFFF_FFFF | 128MB  | SROM Bank 5                         |
| 0xB000_0000 | 0xBFFF_FFFF | 256MB  | OneNAND/NAND Controller and SFR     |
| 0xC000_0000 | 0xCFFF_FFFF | 256MB  | MP3_SRAM output buffer              |
| 0xD000_0000 | 0xD000_FFFF | 64KB   | IROM                                |
| 0xD001_0000 | 0xD001_FFFF | 64KB   | Reserved                            |
| 0xD002_0000 | 0xD003_7FFF | 96KB   | IRAM                                |
| 0xD800_0000 | 0xDFFF_FFFF | 128MB  | DMZ ROM                             |
| 0xE000_0000 | 0xFFFF_FFFF | 512MB  | SFR region                          |

<img src="https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/S5PV210-INTERNAL_MEM_ADDR_MAP.PNG" alt="S5PV210-INTERNAL_MEM_ADDR_MAP" style="zoom:60%;" />

```shell
NOTE: TZPCR0SIZE[5:0](TZPC0); (in TZPC SFR)
- 4KByte chunks
- Recommended value: 6'b00_0000 ~ 6'b10_0000
* if (TZPCR0SIZE[5](TZPC0) == 1'b1), the full address range in iSRAM is configured as secure.
* if (TZPCR0SIZE(TZPC0) == 6'b00_0000), there is non-secure region in iSRAM (0kB).
* if (TZPCR0SIZE(TZPC0) == 6'b00_0001), the minimum secure region size is 4kB.
* if (TZPCR0SIZE(TZPC0) == 6'b01_0000), the 64KB from iSRAM start address specifies the secure region.
- iROM is always secure area
```

- ### 2.1.2 特殊功能寄存器映射

| 起始地址    | 结束地址    | 描述             |
| ----------- | ----------- | ---------------- |
| 0xE000_0000 | 0xE00F_FFFF | CHIPID           |
| 0xE010_0000 | 0xE01F_FFFF | SYSCON           |
| 0xE020_0000 | 0xE02F_FFFF | GPIO             |
| 0xE030_0000 | 0xE03F_FFFF | AXI_DMA          |
| 0xE040_0000 | 0xE04F_FFFF | AXI_PSYS         |
| 0xE050_0000 | 0xE05F_FFFF | AXI_PSFR         |
| 0xE060_0000 | 0xE06F_FFFF | TZPC2            |
| 0xE070_0000 | 0xE07F_FFFF | IEM_APC          |
| 0xE080_0000 | 0xE08F_FFFF | IEM_IEC          |
| 0xE090_0000 | 0xE09F_FFFF | PDMA0            |
| 0xE0A0_0000 | 0xE0AF_FFFF | PDMA1            |
| 0xE0D0_0000 | 0xE0DF_FFFF | CORESIGHT        |
| 0xE0E0_0000 | 0xE0EF_FFFF | SECKEY           |
| 0xE0F0_0000 | 0xE0FF_FFFF | ASYNC_AUDIO_PSYS |
| 0xE110_0000 | 0xE11F_FFFF | SPDIF            |
| 0xE120_0000 | 0xE12F_FFFF | PCM1             |
| 0xE130_0000 | 0xE13F_FFFF | SPI0             |
| 0xE140_0000 | 0xE14F_FFFF | SPI1             |
| 0xE160_0000 | 0xE16F_FFFF | KEYIF            |
| 0xE170_0000 | 0xE17F_FFFF | TSADC            |
| 0xE180_0000 | 0xE18F_FFFF | I2C0 (general)   |
| 0xE1A0_0000 | 0xE1AF_FFFF | I2C2 (PMIC)      |
| 0xE1B0_0000 | 0xE1BF_FFFF | HDMI_CEC         |
| 0xE1C0_0000 | 0xE1CF_FFFF | TZPC3            |
| 0xE1D0_0000 | 0xE1DF_FFFF | AXI_GSYS         |
| 0xE1F0_0000 | 0xE1FF_FFFF | ASYNC_PSFR_AUDIO |
| 0xE210_0000 | 0xE21F_FFFF | I2S1             |
| 0xE220_0000 | 0xE22F_FFFF | AC97             |
| 0xE230_0000 | 0xE23F_FFFF | PCM0             |
| 0xE250_0000 | 0xE25F_FFFF | PWM              |
| 0xE260_0000 | 0xE26F_FFFF | ST               |
| 0xE270_0000 | 0xE27F_FFFF | WDT              |
| 0xE280_0000 | 0xE28F_FFFF | RTC_APBIF        |
| 0xE290_0000 | 0xE29F_FFFF | UART             |
| 0xE800_0000 | 0xE80F_FFFF | SROMC            |

------

| 起始地址    | 结束地址    | 描述               |
| ----------- | ----------- | ------------------ |
| 0xE820_0000 | 0xE82F_FFFF | CFCON              |
| 0xEA00_0000 | 0xEA0F_FFFF | SECSS              |
| 0xEB00_0000 | 0xEB0F_FFFF | SDMMC0             |
| 0xEB10_0000 | 0xEB1F_FFFF | SDMMC1             |
| 0xEB20_0000 | 0xEB2F_FFFF | SDMMC2             |
| 0xEB30_0000 | 0xEB3F_FFFF | SDMMC3             |
| 0xEB40_0000 | 0xEB4F_FFFF | TSI                |
| 0xEC00_0000 | 0xEC0F_FFFF | USBOTG             |
| 0xEC10_0000 | 0xEC1F_FFFF | USBOTG_PHY_CON     |
| 0xEC20_0000 | 0xEC2F_FFFF | USBHOST_EHCI       |
| 0xEC30_0000 | 0xEC3F_FFFF | USBHOST_OHCI       |
| 0xED00_0000 | 0xED0F_FFFF | MODEM              |
| 0xED10_0000 | 0xED1F_FFFF | HOST               |
| 0xEE00_0000 | 0xEE8F_FFFF | AUDIO_SS           |
| 0xEE90_0000 | 0xEE9F_FFFF | AUDIO_SS/ASS_DMA   |
| 0xEEA0_0000 | 0xEEAF_FFFF | AUDIO_SS/ASS_IBUF0 |
| 0xEEB0_0000 | 0xEEBF_FFFF | AUDIO_SS/ASS_IBUF1 |
| 0xEEC0_0000 | 0xEECF_FFFF | AUDIO_SS/ASS_OBUF0 |
| 0xEED0_0000 | 0xEEDF_FFFF | AUDIO_SS/ASS_OBUF1 |
| 0xEEE0_0000 | 0xEEEF_FFFF | AUDIO_SS/ASS_APB   |
| 0xEEF0_0000 | 0xEEFF_FFFF | AUDIO_SS/ASS_ODO   |
| 0xF000_0000 | 0xF00F_FFFF | DMC0_SFR           |
| 0xF100_0000 | 0xF10F_FFFF | AXI_MSYS           |
| 0xF110_0000 | 0xF11F_FFFF | AXI_MSFR           |
| 0xF120_0000 | 0xF12F_FFFF | AXI_VSYS           |
| 0xF140_0000 | 0xF14F_FFFF | DMC1_SFR           |
| 0xF150_0000 | 0xF15F_FFFF | TZPC0              |
| 0xF160_0000 | 0xF16F_FFFF | SDM                |
| 0xF170_0000 | 0xF17F_FFFF | MFC                |
| 0xF180_0000 | 0xF18F_FFFF | ASYNC_MFC_VSYS0    |
| 0xF190_0000 | 0xF19F_FFFF | ASYNC_MFC_VSYS1    |
| 0xF1A0_0000 | 0xF1AF_FFFF | ASYNC_DSYS_MSYS0   |
| 0xF1B0_0000 | 0xF1BF_FFFF | ASYNC_DSYS_MSYS1   |
| 0xF1C0_0000 | 0xF1CF_FFFF | ASYNC_MSFR_DSFR    |
| 0xF1D0_0000 | 0xF1DF_FFFF | ASYNC_MSFR_PSFR    |
| 0xF1E0_0000 | 0xF1EF_FFFF | ASYNC_MSYS_DMC0    |

------

| 起始地址    | 结束地址    | 描述               |
| ----------- | ----------- | ------------------ |
| 0xF1F0_0000 | 0xF1FF_FFFF | ASYNC_MSFR_MPERI   |
| 0xF200_0000 | 0xF20F_FFFF | VIC0               |
| 0xF210_0000 | 0xF21F_FFFF | VIC1               |
| 0xF220_0000 | 0xF22F_FFFF | VIC2               |
| 0xF230_0000 | 0xF23F_FFFF | VIC3               |
| 0xF280_0000 | 0xF28F_FFFF | TZIC0              |
| 0xF290_0000 | 0xF29F_FFFF | TZIC1              |
| 0xF2A0_0000 | 0xF2AF_FFFF | TZIC2              |
| 0xF2B0_0000 | 0xF2BF_FFFF | TZIC3              |
| 0xF300_0000 | 0xF3FF_FFFF | G3D                |
| 0xF800_0000 | 0xF80F_FFFF | FIMD               |
| 0xF900_0000 | 0xF90F_FFFF | TVENC              |
| 0xF910_0000 | 0xF91F_FFFF | VP                 |
| 0xF920_0000 | 0xF92F_FFFF | MIXER              |
| 0xFA00_0000 | 0xFA0F_FFFF | G2D                |
| 0xFA10_0000 | 0xFA1F_FFFF | HDMI_LINK          |
| 0xFA20_0000 | 0xFA2F_FFFF | SMDMA              |
| 0xFA30_0000 | 0xFA3F_FFFF | ROT                |
| 0xFA40_0000 | 0xFA4F_FFFF | AXI_LSYS           |
| 0xFA50_0000 | 0xFA5F_FFFF | DSIM               |
| 0xFA60_0000 | 0xFA6F_FFFF | CSIS               |
| 0xFA70_0000 | 0xFA7F_FFFF | AXI_DSYS           |
| 0xFA80_0000 | 0xFA8F_FFFF | AXI_DSFR           |
| 0xFA90_0000 | 0xFA9F_FFFF | I2C_HDMI_PHY       |
| 0xFAA0_0000 | 0xFAAF_FFFF | AXI_TSYS           |
| 0xFAB0_0000 | 0xFABF_FFFF | I2C_HDMI_DDC       |
| 0xFAC0_0000 | 0xFACF_FFFF | AXI_XSYS           |
| 0xFAD0_0000 | 0xFADF_FFFF | TZPC1              |
| 0xFAF0_0000 | 0xFAFF_FFFF | ASYNC_PSYS_DSYS_u0 |
| 0xFB20_0000 | 0xFB2F_FFFF | FIMC0              |
| 0xFB30_0000 | 0xFB3F_FFFF | FIMC1              |
| 0xFB40_0000 | 0xFB4F_FFFF | FIMC2              |
| 0xFB60_0000 | 0xFB6F_FFFF | JPEG               |