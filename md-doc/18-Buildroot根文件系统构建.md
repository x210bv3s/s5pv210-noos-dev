## `S5PV210` | `Buildroot`根文件系统构建

---

**时间：**`2023年11月13日12:29:17`

[TOC]



## 1.参考

> 1.[如何使用buildroot - 搜索 (bing.com)](https://cn.bing.com/search?q=如何使用buildroot&form=ANNTH1&refig=655191f2e30c43f7b2318ab99e61e602)
>
> 2.[正点原子嵌入式linux驱动开发——Buildroot根文件系统构建 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/661007035#:~:text=buildroot构建根文件系统 1 配置buildroot 将buildroot源码buildroot-2020.02.6.tar.bz2拷贝到ubuntu中，拷贝完成以后对其进行解压，命令如下： tar -vxjf buildroot-2020.02.6.tar.bz2 ...,配置完成以后就可以编译buildroot了，编译完成以后buildroot就会生成编译出来的根文件系统压缩包，可以直接使用。 输入如下命令开始编译： ... 3 buildroot根文件系统测试 buildroot制作出来的根文件系统已经准备好了，接下来就是对其进行测试。 测试方法也是通过nfs挂载的方式，启动uboot，修改bootargs环境变量，设置nfsroot目录为Ubuntu中的rootfs目录，命令如下： )
>
> 3.[小白专用，Buildroot 超简单入门指南 | 开源软件 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/508579943)
>
> 4.[Buildroot - 让嵌入式 Linux 变得简单](https://buildroot.org/download.html)

## 2.`Buildroot`根文件系统构建

### 2-1.获取代码

```bash
[fly@752fac4b02e9 x210bv3s]$ wget https://buildroot.org/downloads/buildroot-2023.02.6.tar.gz
--2023-11-13 12:27:01--  https://buildroot.org/downloads/buildroot-2023.02.6.tar.gz
Resolving buildroot.org (buildroot.org)... 140.211.167.122
Connecting to buildroot.org (buildroot.org)|140.211.167.122|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 7249412 (6.9M) [application/x-gzip]
Saving to: ‘buildroot-2023.02.6.tar.gz’

buildroot-2023.02.6.tar.gz          100%[===================================================================>]   6.91M  1.34MB/s    in 5.1s

2023-11-13 12:27:08 (1.34 MB/s) - ‘buildroot-2023.02.6.tar.gz’ saved [7249412/7249412]
[fly@752fac4b02e9 x210bv3s]$ tar -zxvf buildroot-2023.02.6.tar.gz
[fly@752fac4b02e9 x210bv3s]$ cd buildroot-2023.02.6/
[fly@752fac4b02e9 buildroot-2023.02.6]$ ls
arch   boot     Config.in         configs  DEVELOPERS  fs     Makefile         package  support  toolchain
board  CHANGES  Config.in.legacy  COPYING  docs        linux  Makefile.legacy  README   system   utils
[fly@752fac4b02e9 buildroot-2023.02.6]$ git init
Initialized empty Git repository in /home/fly/project/x210bv3s/buildroot-2023.02.6/.git/
[fly@752fac4b02e9 buildroot-2023.02.6]$ git log
commit c6b138bd9bb56507a7f9778683de41edccf44c2c (HEAD -> master, origin/master)
Author: fly <lenovo-thinkpad@docker.com>
Date:   Mon Nov 13 12:40:23 2023 +0800

    #20231113-01# Repo Init
```

> [buildroot-2023.02.6: S5PV210 | Buildroot根文件系统构建 (gitee.com)](https://gitee.com/x210bv3s/buildroot-2023.02.6)