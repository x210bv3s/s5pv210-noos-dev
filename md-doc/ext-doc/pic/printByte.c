/*******************************************************************
 *   > File Name: printByte.c
 *   > Author: fly
 *   > Mail: 1358326274@qq.com
 *   > Create Time: 2022年11月13日 星期日 01时54分16秒
 ******************************************************************/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
    FILE *fp;

    if(argc != 2){
        printf("Usage: %s file\n", argv[0]);exit(-1);
    }

    if((fp = fopen(argv[1], "r")) == NULL){
        printf("fail to fopen: %s\n", strerror(errno));
        return (-1);
    }

    int c;
    int i = 1;
    printf("\"");
    while((c = fgetc(fp)) != EOF)
    {
        //printf("%02X ", c);
        printf("\\%o", c);
        if((i != 0)&&(i % (0xf + 1 )== 0)){
            printf("\"\n\"");
        }
        i ++;

    }
    printf("\"\n");

    fclose(fp);

    return 0;
}
