# GIT双仓库操作

--------------------

## 参考：

> ### 1.[使用Gitee](https://www.liaoxuefeng.com/wiki/896043488029600/1163625339727712)

-------------------

## 1.查看原仓库

```bash
fly@fly-vm:busybox-1.35.0$ git log -2
commit 600faaa590a482c447ca650fec9a42519a321729
Author: fly <lf.ye@samoon.net>
Date:   Sat Nov 19 15:00:28 2022 +0800

    #20221119-09# 一个简单的驱动模块实验

commit 528b81ea8f2faa2419330dc3436558b00089763d
Author: fly <lf.ye@samoon.net>
Date:   Fri Nov 18 00:09:35 2022 +0800

    #20221117-08# 简单的应用程序
fly@fly-vm:busybox-1.35.0$ git remote -v
origin  git@gitee.com:x210bv3s/busybox-1.35.0.git (fetch)
origin  git@gitee.com:x210bv3s/busybox-1.35.0.git (push)
```

----------------

## 2.删除已有的远程仓库

```bash
fly@fly-vm:busybox-1.35.0$ git remote rm origin
fly@fly-vm:busybox-1.35.0$ git remote -v
fly@fly-vm:busybox-1.35.0$
```

------------------------------

## 3.先关联`gitee`远程仓库

```bash
fly@fly-vm:busybox-1.35.0$ git remote add gitee  git@gitee.com:x210bv3s/busybox-1.35.0.git
fly@fly-vm:busybox-1.35.0$ git remote -v
gitee   git@gitee.com:x210bv3s/busybox-1.35.0.git (fetch)
gitee   git@gitee.com:x210bv3s/busybox-1.35.0.git (push)
```

## 4.再关联gitcode远程仓库

### 4-1.先在gitcode上新建一个仓库

![gitcode-01](.\pic\gitcode-01.PNG)

### 4-2.关联`gitcode`远程仓库

```bash
fly@fly-vm:busybox-1.35.0$ git remote add gitcode git@gitcode.net:x210bv3s/busybox-1.35.0.git
fly@fly-vm:busybox-1.35.0$ git remote -v
gitcode git@gitcode.net:x210bv3s/busybox-1.35.0.git (fetch)
gitcode git@gitcode.net:x210bv3s/busybox-1.35.0.git (push)
gitee   git@gitee.com:x210bv3s/busybox-1.35.0.git (fetch)
gitee   git@gitee.com:x210bv3s/busybox-1.35.0.git (push)
```

---------------

## 5.推送

-----------

```bash
fly@fly-vm:busybox-1.35.0$ git push gitee master
Everything up-to-date
fly@fly-vm:busybox-1.35.0$ git push gitcode master
The authenticity of host 'gitcode.net (119.3.229.170)' can't be established.
RSA key fingerprint is SHA256:pyrMa3p0o90Qsuz2+kMX3CIBl+S1cZsdRlCoaosSgQs.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'gitcode.net,119.3.229.170' (RSA) to the list of known hosts.
Permission denied (publickey).
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
```

### 5-1.添加ssh密匙

----------------

```bash
fly@fly-vm:.ssh$ pwd
/home/fly/.ssh
fly@fly-vm:.ssh$ ls
id_ed25519  id_ed25519.pub  known_hosts
fly@fly-vm:.ssh$ cat id_ed25519.pub
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAeccxhWF1+Ltf0/ZCG3ZfywuWBORFltV8jf+4KibB99 dell-ubuntu-vm@code.com
```

> 将目录`.ssh`下`*.pub`中的内容粘贴到`gitcode`中设置的ssh密匙中

### 5-2.重新推送

```bash
fly@fly-vm:busybox-1.35.0$ git push gitcode master
Counting objects: 2398, done.
Compressing objects: 100% (2036/2036), done.
Writing objects: 100% (2398/2398), 5.94 MiB | 3.99 MiB/s, done.
Total 2398 (delta 143), reused 0 (delta 0)
remote: Resolving deltas: 100% (143/143), done.
To git@gitcode.net:x210bv3s/busybox-1.35.0.git
 * [new branch]      master -> master
```

---------------

> 这样一来，我们的本地库就可以同时与多个远程库互相同步。