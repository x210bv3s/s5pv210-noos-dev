# BMP文件解析实践

-----------------------

## 1.新建BMP图片

> 使用画图工具新建只有一个像素的图片

![BMP-T02](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/ext-doc/pic/BMP-T01.png)

--------------

> 画板放大到最最大。

![BMP-T02](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/ext-doc/pic/BMP-T02.png)

-----------------

> 编辑画笔将图片涂成纯红色。

![BMP-T03](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/ext-doc/pic/BMP-T03.png)

--------------

> 保存图像。

![BMP-T04](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/ext-doc/pic/BMP-T04.png)

-------------

![BMP-T05](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/ext-doc/pic/BMP-T05.png)

------------

## 2.分析BMP文件

> 查看文件属性：
>
> 大小：58字节（58字节）；占用空间：1.00KB（1024字节）
>
> 尺寸：1x1；宽度：1像素；高度：1像素；位深度：24
>
> 使用uedit32.exe 打开图片；

-------------

![BMP-TEST01](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/ext-doc/pic/BMP-TEST01.png)

---------------

```c
42 4D 3A 00 00 00 00 00 00 00 36 00 00 00 28 00
00 00 01 00 00 00 01 00 00 00 01 00 18 00 00 00
00 00 04 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 FF 00
```

----------

![BMP-T06](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/ext-doc/pic/BMP-T06.png)