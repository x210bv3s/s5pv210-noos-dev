## S5PV210 | 裸机汇编LED闪烁实验

------

[TOC]



- ### 原理图

<img src="https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/HW-LED.PNG" alt="HW-LED" style="zoom: 67%;" />

<img src="https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/HW-LED1.PNG" style="zoom: 80%;" />

- ### DATASHEET

#### 2.2.7 PORT GROUP GPD0 CONTROL REGISTER

> There are six control registers, namely, GPD0CON, GPD0DAT, GPD0PUD, GPD0DRV, GPD0CONPDN and
> GPD0PUDPDN in the Port Group GPD0 Control Registers.

#####  2.2.7.1 Port Group GPD0 Control Register (GPD0CON, R/W, Address = 0xE020_00A0)  

![GPD0CON](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/GPD0CON.PNG)

##### 2.2.7.2 Port Group GPD0 Control Register (GPD0DAT, R/W, Address = 0xE020_00A4)  

![GPD0DAT](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/GPD0DAT.PNG)

##### 2.2.7.3 Port Group GPD0 Control Register (GPD0PUD, R/W, Address = 0xE020_00A8)  

![GPD0PUD](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/GPD0PUD.PNG)

##### 2.2.7.4 Port Group GPD0 Control Register (GPD0DRV, R/W, Address = 0xE020_00AC)  

![GPD0DRV](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/GPD0DRV.PNG)

##### 2.2.7.5 Port Group GPD0 Control Register (GPD0CONPDN, R/W, Address = 0xE020_00B0)  

![GPD0PUDPDN](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/GPD0CONPDN.PNG)

##### 2.2.7.6 Port Group GPD0 Control Register (GPD0PUDPDN, R/W, Address = 0xE020_00B4)  

![GPD0PUDPDN](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/GPD0PUDPDN.PNG)

- ### 代码实现

```assembly
/***********************************************************
 *   > File Name: simple_led.S
 *   > Author: fly
 *   > Create Time: 2021-06-27  0/25  16:51:13 +0800
 *========================================================*/
#define GPD0CON         0xE02000A0
#define GPD0DAT         0xE02000A4

#define GPJ0CON         0xE0200240
#define GPJ0DAT         0xE0200244

#define PS_HOLD_CONTORL 0xE010E81C
#define WTCON           0xE2700000
#define SVC_STACK       0xD0037D80

//#define CONFIG_SYS_ICACHE_OFF   1

.global _start
_start:
    //给5v电源置锁
    //LDR指令：从内存中将1个32位的字读取到目标寄存器中
    //STR指令：将1个32位的字数据写入到指令中指定的内存单元中
    //BIC指令：位清除指令
    //ORR指令：逻辑或操作指令
    ldr r0,=PS_HOLD_CONTORL     //r0=0xE010E81C
    ldr r1,[r0]                 //将r0地址处的数据读出，保存到r1中（零偏移）
    orr r1,r1,#0x300            //设置r1的第8、9位，其他位保持不变
    orr r1,r1,#0x1              //设置r1的第1位，其他位保持不变
    str r1,[r0]                 //将r1中的内容传输到r0中数指定的地址内存中去

    //关看门狗
    ldr r0, =WTCON
    mov r1, #0                  //将立即数0传输到r1处
    str r1, [r0]

    //开/关iCache
    // MRC指令:从协处理器寄存器传数据到ARM寄存器
    // MCR指令:从ARM寄存器传数据到协处理器寄存器
    mrc p15, 0, r0, c1, c0, 0
    #ifdef CONFIG_SYS_ICACHE_OFF
    bic r0, r0, #0x00001000     @ clear bit 12 (I) I-Cache
    #else
    orr r0, r0, #0x00001000     @ set bit 12 (I) I-Cache
    #endif
    mcr p15, 0, r0, c1, c0, 0

    //设置栈，以便调用c函数
    ldr sp, =SVC_STACK

led_init:

    /* 初始化GPIO口(配置为输出模式)，下面是比较规范的一种写法 */    
    ldr r0,=GPD0CON             //r0=0xE02000A0
    ldr r1,[r0]                 //将r0地址处的数据读出，保存到r1中（零偏移）
    orr r1,r1,#0x00000010       //设置r1的第4位(置1)，其他位保持不变[7:4]->0001=Output
    str r1,[r0]                 //将r1中的内容传输到r0中数指定的地址内存中去

    ldr r0,=0x11111111
    ldr r1,=GPJ0CON
    str r0, [r1]                //把GPJ0所有的IO设置为输出模式

    /* 熄灭LED1/2/3 */
    ldr r0, = ((1<<3)|(1<<4)|(1<<5))
    ldr r1, =GPJ0DAT
    str r0, [r1]
 
led_run:   
    /* 点亮LED4，GPIO口输出低电平 */
    ldr r0,=GPD0DAT             //r0=0xE02000A4
    ldr r1,[r0]                 //将r0地址处的数据读出，保存到r1中（零偏移）
    bic r1,r1,#0x0002           //清除r1的第1位(置0)，其他位保持不变[1]
    str r1,[r0]                 //将r1中的内容传输到r0中数指定的地址内存中去
    bl delay

    /* 熄灭LED4，GPIO口输出高电平 */
    ldr r0,=GPD0DAT             //r0=0xE02000A4
    ldr r1,[r0]                 //将r0地址处的数据读出，保存到r1中（零偏移）
    orr r1,r1,#0x0002           //设置r1的第1位(置1)，其他位保持不变[1]
    str r1,[r0]                 //将r1中的内容传输到r0中数指定的地址内存中去
    bl delay

    bl led_run

half:
    b half

    /* 延时函数：delay*/
delay:
    ldr r2,=9000000
    ldr r3,=0x0
delay_loop:
    //SUB指令：从寄存器Rn中减去shifter_operand表示的数值，
    //并将结果保存在目标寄存器Rd中，并根据指令的执行结果
    //设置CPSR中的相应标志位
    //SUB {<cond>} {s} <Rd>,<Rn>,<shifter_operand>
    sub r2,r2,#1                //r2 = r2 - 1
    //CMP指令:使用寄存器Rn的值减去shifter_operand的值，
    //根据操作的结果更新CPSR中相应的条件标志位，以便后面
    //的指令根据相应的条件标志位来判断是否执行
    //CMP {<cond>} <Rn>,<shifter_operand>
    cmp r2, r3
    bne delay_loop
    mov pc,lr
```

编译用Makefile:

```makefile
# 将所有的.o文件链接成.elf文件，“-Ttext 0x0”
# 表示程序的运行地址是0x0，由于目前编写的是位置
# 无关码，可以在任一地址运行
# 将elf文件抽取为可在开发板上运行的bin文件
# 将elf文件反汇编保存在dis文件中，调试程序会用
# 添加文件头
.PHONY: all clean tools

CROSS		?= arm-linux-
NAME		:= LED

LD			:= $(CROSS)ld
OC			:= $(CROSS)objcopy
OD			:= $(CROSS)objdump
CC			:= $(CROSS)gcc
MK			:= ../../tools/mk_image/mkv210_image

all:$(NAME).bin

$(NAME).bin : simple_led.o
	$(LD) -Ttext 0x0 -o $(NAME).elf $^
	$(OC) -O binary $(NAME).elf $(NAME).bin
	$(OD) -D $(NAME).elf > $(NAME)_elf.dis
	$(MK) $(NAME).bin

# 将当前目录下存在的汇编文件及C文件编译成.o文件
%.o : %.S
	$(CC) -o $@ $< -c
%.o : %.c
	$(CC) -o $@ $< -c

clean:
	$(RM) *.o *.elf *.bin *.dis *.sd

tools:
	make -C ../../tools/mk_image/
```

