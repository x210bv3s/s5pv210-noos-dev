## `S5PV210` | `S5PV210`上进行`Linux`开发

------------------

[TOC]



## 准备工作

### 开发板：

![](https://img-blog.csdnimg.cn/20190414172040395.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L0lfZmVpZ2U=,size_16,color_FFFFFF,t_70)

### 交叉编译工具链：

>  [`S5PV210` | 安装交叉编译工具链（`toolchain`）](https://gitee.com/x210bv3s/s5pv210-noos-dev/blob/master/md-doc/09-s5pv210-%E5%AE%89%E8%A3%85%E4%BA%A4%E5%8F%89%E7%BC%96%E8%AF%91%E5%B7%A5%E5%85%B7%E9%93%BE.md)

### 项目代码：

> [`https://gitee.com/x210bv3s/qt_x210v3s_160307`](https://gitee.com/x210bv3s/qt_x210v3s_160307)

### 编译rootfs需要的dl文件：

> [`https://download.csdn.net/download/I_feige/75540238`](https://download.csdn.net/download/I_feige/75540238)

dl目录是buildroot的默认下载目录，buildroot会根据你的配置从网上自动下载你需要的工具包，而工具包保存的地方就是buildroot顶目录下的dl目录。由于网络问题，可能导致下载失败。所以我将所需的工具包，打包上传到csdn以供下载。

需要将压缩包`dl.20220111.tar.bz2`解压到以下目录：

```bash
fly@fly-vm:~/workSpace/x210bv3s$ pwd
/home/fly/workSpace/x210bv3s
fly@fly-vm:~/workSpace/x210bv3s$ tar -jxvf dl.20220111.tar.bz2 -C qt_x210v3s_160307/buildroot/
```

----------------------------------------------------------------------------------------

```bash
[$(USER)@fly-vm dl]$ pwd
/home/$(USER)/x210bv3s/qt_x210v3s_160307/buildroot/dl
[$(USER)@fly-vm dl]$ ls
alsa-lib-1.0.26.tar.bz2
alsa-utils-1.0.26.tar.bz2
arm-2012.03-57-arm-none-linux-gnueabi-i686-pc-linux-gnu.tar.bz2
autoconf-2.68.tar.gz
automake-1.11.6.tar.gz
bash-4.2.tar.gz
busybox-1.21.0.tar.bz2
bzip2-1.0.6.tar.gz
ccache-3.1.8.tar.bz2
cjson-42.tar.gz
dbus-1.4.24.tar.gz
expat-2.1.0.tar.gz
fakeroot_1.18.2.orig.tar.bz2
ffmpeg-0.8.12.tar.bz2
freetype-2.4.11.tar.bz2
glib-2.30.3.tar.xz
gmp-5.1.0.tar.bz2
gnutls-3.1.8.tar.xz
googlewebfonts.tgz
gperf-3.0.4.tar.gz
gst-ffmpeg-0.10.13.tar.bz2
gst-plugins-base-0.10.36.tar.bz2
gstreamer-0.10.36.tar.bz2
gzip-1.5.tar.gz
kmod-12.tar.xz
libffi-3.0.12.tar.gz
libgcrypt-1.5.0.tar.bz2
libgpg-error-1.10.tar.gz
libid3tag-0.15.1b.tar.gz
libjpeg-turbo-1.2.1.tar.gz
libmad-0.15.1b.tar.gz
libpng-1.4.12.tar.bz2
libtool-2.2.10.tar.gz
libxml2-2.9.0.tar.gz
lrzsz-0.12.20.tar.gz
lua-5.1.5.tar.gz
m4-1.4.16.tar.bz2
madplay-0.15.2b.tar.gz
memstat_0.8.tar.gz
minicom-2.6.1.tar.gz
MPlayer-1.1.tar.xz
ncurses-5.9.tar.gz
nettle-2.6.tar.gz
ntfs-3g_ntfsprogs-2013.1.13.tgz
ocf-linux-20120127.tar.gz
openssh-6.1p1.tar.gz
openssl-1.0.1e.tar.gz
opkg-635.tar.gz
pkgconf-0.8.9.tar.bz2
qextserialport-6c47244de4ce6db.tar.gz
qt-everywhere-opensource-src-4.8.4.tar.gz
readline-6.2.tar.gz
SDL-1.2.15.tar.gz
SDL_gfx-2.0.23.tar.gz
SDL_image-1.2.6.tar.gz
SDL_mixer-1.2.12.tar.gz
SDL_net-1.2.8.tar.gz
SDL_sound-1.0.3.tar.gz
SDL_ttf-2.0.11.tar.gz
sqlite-autoconf-3071401.tar.gz
strace-4.7.tar.xz
tiff-4.0.3.tar.gz
tslib-412d99d.tar.gz
udev-182.tar.bz2
util-linux-2.20.1.tar.bz2
vim-699f8d8f096d.tar.gz
wireless_tools.29.tar.gz
wpa_supplicant-2.0.tar.gz
xinetd-2.3.15.tar.gz
zlib-1.2.7.tar.bz2
```

## 开发环境示意图

![pic_00](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/pic/pic_00.png)

## 软件编写流程

![pic_01](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/pic/pic_01.png)

## 1.uboot

### 编译uboot：

最终生成的`uboot.bin`文件位于目录`/home/$(USER)/x210bv3s/qt_x210v3s_160307/uboot`下；

```c
[$(USER)@fly-vm uboot]$ make distclean
[$(USER)@fly-vm uboot]$ pwd
/home/$(USER)/x210bv3s/qt_x210v3s_160307/uboot
[$(USER)@fly-vm uboot]$ make distclean
[$(USER)@fly-vm uboot]$ make x210_sd_config
Configuring for x210_sd board...
[$(USER)@fly-vm uboot]$ make -j4
```

--------------------------------

### 为uboot添加校验和：

```
[$(USER)@fly-vm uboot]$ ../../s5pv210-noos-dev/tools/mk_image/mkv210 u-boot.bin
fileLen:393216Bytes
checkSum:0x000ed555 for 8176 bytes.
```
<img src="https://img-blog.csdnimg.cn/20200313230941895.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L0lfZmVpZ2U=,size_16,color_FFFFFF,t_70" style="zoom:80%;" />

-----------------------

### nand启动内核的uboot环境变量：

```bash
baudrate=115200
ethact=dm9000
ethaddr=00:40:5c:26:0a:5b
mtdpart=80000 400000 3000000
bootdelay=2
filesize=60000
fileaddr=C0008000
netmask=255.255.255.0
gatewayip=192.168.199.1
serverip=192.168.199.179
ipaddr=192.168.199.188
bootargs=console=ttySAC2,115200 root=/dev/mmcblk0p2 rw init=/linuxrc rootfstype=ext3
bootcmd=movi read kernel 30008000; movi read rootfs 30B00000 300000; bootm 30008000 30B00000

Environment size: 402/16380 bytes
```



### 以从SD2启动开发板:

#### 破坏iNand中的BootLoader

**在`linux`和`android`系统下，擦除`uboot`的方法：**

```bash
busybox dd if=/dev/zero of=/dev/block/mmcblk0 bs=512 seek=1 count=1 conv=sync
sync
```

**在`uboot`底下如何擦除`uboot`:**

```bash
# 将DDR内存地址0x30000000处的内容写入到uboot分区
movi write u-boot 0x30000000
```

> 说明：把板载**iNand**的第一个扇区用全`0`来填充，其实就是擦除它，这样板载的**iNand**的**bootloader**的开始的第1个扇区就被破坏了，将来启动时**iROM**还是会从**iNand**中读取前**`16KB`**，然后计算校验和。因为有一个扇区被擦除，所以校验和不通过，会启动失败（会从**SD2**去执行2nd启动）；

> 破坏板载`iNand`的`bootloader`后，不插外部SD卡，启动会打印：SD checksum error

#### 制作SD卡启动盘：

> `朱老师物联网大讲堂-全部视频\朱有鹏老师嵌入式linux核心课程\开发版光盘资料\X210V3S_A\tools`

以管理员模式打开工具[`x210_Fusing_Tool.exe`](https://download.csdn.net/download/i_feige/11937635)，

![tool-fusing](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/tool-fusing.PNG)

> 清除工具`PATH`路径，进入`C:\Users\fly\AppData\Roaming\SDFusing`目录，删除`ini`文件即可；

擦除uboot分区后，在SD2通道插入sd卡启动盘，上电即可启动开发板；

通过U-Boot版本和编译时间戳可以确认，成功加载并执行了新的U-Boot;

```bash
U-Boot 1.3.4 (Jan 15 2022 - 21:23:53) for x210@Flyer


CPU:  S5PV210@1000MHz(OK)
        APLL = 1000MHz, HclkMsys = 200MHz, PclkMsys = 100MHz
        MPLL = 667MHz, EPLL = 96MHz
                       HclkDsys = 166MHz, PclkDsys = 83MHz
                       HclkPsys = 133MHz, PclkPsys = 66MHz
                       SCLKA2M  = 200MHz
Serial = CLKUART
Board:   X210BV3S
DRAM:    512 MB
Flash:   8 MB
SD/MMC:  3728MB
In:      serial
Out:     serial
Err:     serial
[LEFT UP] boot mode
checking mode for fastboot ...
Hit any key to stop autoboot:  0
```

### 通过uboot执行裸机程序：

使用tftp下载然后并执行程序；

```
tftp 0x40008000 arm.bin    (从主机tftpboot目录下载arm.bin程序到0x40008000)
go 0x40008000     		(将pc指针指向地址0x40008000执行)
```



## 2.linux-kernel

### 配置文件路径：

```bash
[$(USER)@fly-vm configs]$ pwd
/home/$(USER)/x210bv3s/qt_x210v3s_160307/kernel/arch/arm/configs
[$(USER)@fly-vm configs]$ ls
v210_defconfig  x210ii_initrd_defconfig  x210ii_qt_defconfig
```

### 编译kernel：

```
[$(USER)@fly-vm kernel]$ pwd
/home/$(USER)/x210bv3s/qt_x210v3s_160307/kernel
[$(USER)@fly-vm kernel]$ make distclean
  CLEAN   arch/arm/boot/compressed
  CLEAN   arch/arm/boot
  CLEAN   /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/kernel
  CLEAN   arch/arm/kernel
  CLEAN   drivers/char
  CLEAN   drivers/video/logo
  CLEAN   firmware
  CLEAN   kernel
  CLEAN   lib
  CLEAN   .tmp_versions
  CLEAN   vmlinux System.map .tmp_kallsyms2.S .tmp_kallsyms1.o .tmp_kallsyms1.S .tmp_kallsyms2.o .tmp_vmlinux1 .tmp_vmlinux2 .tmp_System.map
  CLEAN   scripts/basic
  CLEAN   scripts/kconfig
  CLEAN   scripts/mod
  CLEAN   scripts
  CLEAN   include/config include/generated
  CLEAN   .config .config.old .version include/linux/version.h Module.symvers
[$(USER)@fly-vm kernel]$ make v210_defconfig
#
# configuration written to .config
#
[$(USER)@fly-vm kernel]$ make -j4
```
------------------------------

编译的`zImage`,位于如下目录：

```
qt_x210v3s_160307/kernel/arch/arm/boot
```

------------------------
使用`fastboot`将`zImage`刷入`kernel`分区，重启开发板，通过`kernel`版本和编译时间戳可确认内核烧录成功。

```bash
Starting kernel ...



Hello, world 0.@Flyer

Uncompressing Linux... done, booting the kernel.
[    0.000000] Initializing cgroup subsys cpu
[    0.000000] Linux version 2.6.35.7+ ($(USER)@fly-vm) (gcc version 4.4.1 (Sourcery G++ Lite 2009q3-67) ) #1 PREEMPT Wed Jan 12 22:12:04 CST 2022
[    0.000000] CPU: ARMv7 Processor [412fc082] revision 2 (ARMv7), cr=10c53c7f
[    0.000000] CPU: VIPT nonaliasing data cache, VIPT nonaliasing instruction cache
[    0.000000] Machine: SMDKV210
```

------------------------



## 3.rootfs

### mk编译脚本：

```bash
#!/bin/sh
#
# Description   : Build Qt Script.
# Authors               : jianjun jiang - jerryjianjun@gmail.com
# Version               : 0.01
# Notes                 : None
#

CPU_NUM=$(cat /proc/cpuinfo |grep processor|wc -l)
CPU_NUM=$((CPU_NUM+1))

SOURCE_DIR=$(cd `dirname $0` ; pwd)

RELEASE_DIR=${SOURCE_DIR}/release/
BOOTLOADER_XBOOT_CONFIG=arm32-x210ii
QT_KERNEL_CONFIG=x210ii_qt_defconfig
INITRD_KERNEL_CONFIG=x210ii_initrd_defconfig
BUILDROOT_CONFIG=x210_defconfig

setup_environment()
{
        cd ${SOURCE_DIR};
        mkdir -p ${RELEASE_DIR} || return 1;
}

build_bootloader_xboot()
{
        if [ ! -f ${RELEASE_DIR}/zImage-initrd ]; then
                echo "not found kernel zImage-initrd, please build kernel first" >&2
                return 1
        fi

        if [ ! -f ${RELEASE_DIR}/zImage-qt ]; then
                echo "not found kernel zImage-qt, please build kernel first" >&2
                return 1
        fi

        # copy zImage-initrd and zImage-qt to xboot's romdisk directory
        cp -v ${RELEASE_DIR}/zImage-initrd ${SOURCE_DIR}/xboot/src/arch/arm32/mach-x210ii/romdisk/boot || return 1;
        cp -v ${RELEASE_DIR}/zImage-qt ${SOURCE_DIR}/xboot/src/arch/arm32/mach-x210ii/romdisk/boot || return 1;

        # compiler xboot
        cd ${SOURCE_DIR}/xboot || return 1
        make TARGET=${BOOTLOADER_XBOOT_CONFIG} CROSS=/usr/local/arm/arm-2012.09/bin/arm-none-eabi- clean || return 1;
        make TARGET=${BOOTLOADER_XBOOT_CONFIG} CROSS=/usr/local/arm/arm-2012.09/bin/arm-none-eabi- || return 1;

        # rm zImage-initrd and zImage-qt
        rm -fr ${SOURCE_DIR}/xboot/src/arch/arm32/mach-x210ii/romdisk/boot/zImage-initrd
        rm -fr ${SOURCE_DIR}/xboot/src/arch/arm32/mach-x210ii/romdisk/boot/zImage-qt

        # copy xboot.bin to release directory
        cp -v ${SOURCE_DIR}/xboot/output/xboot.bin ${RELEASE_DIR}

        echo "" >&2
        echo "^_^ xboot path: ${RELEASE_DIR}/xboot.bin" >&2
        return 0
}

build_bootloader_uboot_nand()
{
        cd ${SOURCE_DIR}/uboot || return 1

        make distclean
        make x210_nand_config
        make -j${CPU_NUM}
        mv u-boot.bin uboot_nand.bin
        if [ -f uboot_nand.bin ]; then
                cp uboot_nand.bin ${RELEASE_DIR}/uboot.bin
                cd ${RELEASE_DIR}
                ${SOURCE_DIR}/tools/mkheader uboot.bin
                echo "^_^ uboot_nand.bin is finished successful!"
                exit
        else
                echo "make error,cann't compile u-boot.bin!"
                exit
        fi
}

build_bootloader_uboot_inand()
{
        cd ${SOURCE_DIR}/uboot || return 1

        make distclean
        make x210_sd_config
        make -j${CPU_NUM}
        mv u-boot.bin uboot_inand.bin
        if [ -f uboot_inand.bin ]; then
                cp uboot_inand.bin ${RELEASE_DIR}/uboot.bin
                cd ${RELEASE_DIR}
                ${SOURCE_DIR}/tools/mkheader uboot.bin
                echo "^_^ uboot_inand.bin is finished successful!"
                exit
        else
                echo "make error,cann't compile u-boot.bin!"
                exit
        fi
}


build_kernel()
{
        cd ${SOURCE_DIR}/kernel || return 1

        # make ${INITRD_KERNEL_CONFIG} || return 1
        # make -j${threads} || return 1
        # dd if=${SOURCE_DIR}/kernel/arch/arm/boot/zImage of=${RELEASE_DIR}/zImage-initrd bs=2048 count=8192 conv=sync;

        make ${QT_KERNEL_CONFIG} || return 1
        make -j${threads} || return 1
        dd if=${SOURCE_DIR}/kernel/arch/arm/boot/zImage of=${RELEASE_DIR}/zImage-qt bs=2048 count=8192 conv=sync;

        echo "" >&2
        # echo "^_^ initrd kernel path: ${RELEASE_DIR}/zImage-initrd" >&2
        echo "^_^ qt kernel path: ${RELEASE_DIR}/zImage-qt" >&2

        return 0
}

build_rootfs()
{
        cd ${SOURCE_DIR}/buildroot || return 1

        make ${BUILDROOT_CONFIG} || return 1
        make || return 1

        # copy rootfs.tar to release directory
        cp -v ${SOURCE_DIR}/buildroot/output/images/rootfs.tar ${RELEASE_DIR} || { return 1; }
}

# must root user
gen_qt_rootfs_ext3()
{
        if [ ! -f ${RELEASE_DIR}/rootfs.tar ]; then
                echo "not found rootfs.tar, please build rootfs" >&2
                return 1
        fi

        echo "making ext3 qt4.8 rootfs now,wait a moment..."
        cd ${RELEASE_DIR} 	# 进入工作目录
        rm -rf rootfs 		# 移除rootfs目录
        mkdir -p rootfs		# 创建rootfs目录
        tar xf rootfs.tar -C rootfs # 解压文件到rootfs目录

        rm rootfs_qt4.ext3 # 移除rootfs镜像文件
        rm -rf rootfs_img  # 移除rootfs_img目录
        mkdir -p rootfs_img # 创建rootfs_img目录

        dd if=/dev/zero of=rootfs_qt4.ext3 bs=1024 count=262144 # 创建一个大的空文件
        mkfs.ext3 rootfs_qt4.ext3 # 格式化文件成ext3文件系统
        # 挂载rootfs_qt4.ext3到rootfs_img目录下，通过该目录访问文件系统
        mount -o loop rootfs_qt4.ext3 ./rootfs_img
		# 复制rootfs文件到rootfs_img
        cp ./rootfs/* ./rootfs_img -ar
        umount ./rootfs_img			#卸载目录

        echo "^_^ make rootfs_qt4.ext3 successful!"
}

# must root user
gen_qt_rootfs_jffs2()
{
        if [ ! -f ${RELEASE_DIR}/rootfs.tar ]; then
                echo "not found rootfs.tar, please build rootfs" >&2
                return 1
        fi

        echo "making jffs2 qt4.8 rootfs now,wait a moment..."
        cd ${RELEASE_DIR}
        rm -rf rootfs
        mkdir -p rootfs
        tar xf rootfs.tar -C rootfs

        [ -e "rootfs" ] ||{ echo "error!can't find rootfs dir"; exit;}

        mkfs.jffs2 -r rootfs -o rootfs_qt4.jffs2 -e 0x20000 -s 0x800 --pad=0x5000000 -n
        echo "^_^ make rootfs_qt4.jffs2 successful!"
}

gen_qt_update_bin()
{
        # check image files
        if [ ! -f ${RELEASE_DIR}/xboot.bin ]; then
                echo "not found bootloader xboot.bin, please build bootloader" >&2
                return 1
        fi

        if [ ! -f ${RELEASE_DIR}/zImage-initrd ]; then
                echo "not found kernel zImage-initrd, please build kernel first" >&2
                return 1
        fi

        if [ ! -f ${RELEASE_DIR}/zImage-qt ]; then
                echo "not found kernel zImage-qt, please build kernel first" >&2
                return 1
        fi

        if [ ! -f ${RELEASE_DIR}/rootfs.tar ]; then
                echo "not found rootfs.tar, please build rootfs" >&2
                return 1
        fi

        rm -fr ${RELEASE_DIR}/tmp || return 1;
        rm -fr ${RELEASE_DIR}/qt-update.bin || return 1;
        mkdir -p ${RELEASE_DIR}/tmp || return 1;

        # copy image files
        cp ${RELEASE_DIR}/xboot.bin ${RELEASE_DIR}/tmp/;
        cp ${RELEASE_DIR}/zImage-initrd ${RELEASE_DIR}/tmp/;
        cp ${RELEASE_DIR}/zImage-qt ${RELEASE_DIR}/tmp/;
        cp ${RELEASE_DIR}/rootfs.tar ${RELEASE_DIR}/tmp/;

        # create md5sum.txt
        cd ${RELEASE_DIR}/tmp/;
        find . -type f -print | while read line; do
                if [ $line != 0 ]; then
                        md5sum ${line} >> md5sum.txt
                fi
        done

        # mkisofs
        mkisofs -l -r -o ${RELEASE_DIR}/qt-update.bin ${RELEASE_DIR}/tmp/ || return 1;

        cd ${SOURCE_DIR} || return 1
        rm -fr ${RELEASE_DIR}/tmp || return 1;
        return 0;
}

threads=4;
xboot=no;
uboot_inand=no;
uboot_nand=no;
kernel=no;
rootfs=no;
rootfs_ext3=no;
rootfs_jffs2=no;
update=no;

if [ -z $1 ]; then
        xboot=yes
        uboot_inand=no;
        uboot_nand=no;
        kernel=yes
        rootfs=yes
        rootfs_ext3=no;
        rootfs_jffs2=no;
        update=yes
fi

while [ "$1" ]; do
    case "$1" in
        -j=*)
                x=$1
                threads=${x#-j=}
                ;;
        -x|--xboot)
                xboot=yes
            ;;
        -ui|--uboot_inand)
                uboot_inand=yes
            ;;
        -un|--uboot_nand)
                uboot_nand=yes
            ;;
        -k|--kernel)
            kernel=yes
            ;;
        -r|--rootfs)
                rootfs=yes
            ;;
        -re|--rootfs_ext3)
                rootfs_ext3=yes
            ;;
        -rj|--rootfs_jffs2)
                rootfs_jffs2=yes
            ;;
        -U|--update)
                update=yes
            ;;
        -a|--all)
                xboot=yes
                kernel=yes
                rootfs=yes
                update=yes
            ;;
        -h|--help)
            cat >&2 <<EOF
Usage: mk [OPTION]
Build script for compile the source of telechips project.

  -j=n                 using n threads when building source project (example: -j=16)
  -x, --xboot          build bootloader xboot from source file
  -ui,--uboot_inand    build uboot for emmc
  -un,--uboot_nand     build uboot for nand flash
  -k, --kernel         build kernel from source file and using default config file
  -r, --rootfs         build root file system
  -re,--rootfs_ext3    build rootfs for emmc,used with uboot
  -rj,--rootfs_jffs2   build rootfs for nand,used with uboot
  -U, --update         gen update package update.bin,used with xboot
  -a, --all            build all, include anything
  -h, --help           display this help and exit
EOF
            exit 0
            ;;
        *)
            echo "build.sh: Unrecognised option $1" >&2
            exit 1
            ;;
    esac
    shift
done

setup_environment || exit 1

if [ "${kernel}" = yes ]; then
        build_kernel || exit 1
fi

if [ "${xboot}" = yes ]; then
        build_bootloader_xboot || exit 1
fi

if [ "${uboot_inand}" = yes ]; then
        build_bootloader_uboot_inand || exit 1
fi

if [ "${uboot_nand}" = yes ]; then
        build_bootloader_uboot_nand || exit 1
fi

if [ "${rootfs}" = yes ]; then
        build_rootfs || exit 1
fi

if [ "${rootfs_ext3}" = yes ]; then
        gen_qt_rootfs_ext3 || exit 1
fi

if [ "${rootfs_jffs2}" = yes ]; then
        gen_qt_rootfs_jffs2 || exit 1
fi

if [ "${update}" = yes ]; then
        gen_qt_update_bin || exit 1
fi

exit 0
```


### mk脚本文件的用法：

```bash
[$(USER)@fly-vm qt_x210v3s_160307]$ ./mk -h
Usage: mk [OPTION]
Build script for compile the source of telechips project.

  -j=n                 using n threads when building source project (example: -j=16)
  -x, --xboot          build bootloader xboot from source file
  -ui,--uboot_inand    build uboot for emmc
  -un,--uboot_nand     build uboot for nand flash
  -k, --kernel         build kernel from source file and using default config file
  -r, --rootfs         build root file system
  -re,--rootfs_ext3    build rootfs for emmc,used with uboot
  -rj,--rootfs_jffs2   build rootfs for nand,used with uboot
  -U, --update         gen update package update.bin,used with xboot
  -a, --all            build all, include anything
  -h, --help           display this help and exit
```

----------------------------

### 编译rootfs:

#### 方法1：

```bash
[$(USER)@fly-vm qt_x210v3s_160307]$ ./mk -r
```

#### 方法2：

```cpp
[$(USER)@fly-vm qt_x210v3s_160307]$ cd buildroot/
[$(USER)@fly-vm buildroot]$ make x210_defconfig
#
# configuration written to /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/.config
#
[$(USER)@fly-vm buildroot]$ make
```



--------------------------------

```bash
[$(USER)@fly-vm buildroot]$ make
/usr/bin/make -j2  HOSTCC="/usr/bin/gcc" HOSTCXX="/usr/bin/g++" silentoldconfig
make[1]: Entering directory '/home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot'
BR2_DEFCONFIG='' KCONFIG_AUTOCONFIG=/home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/build/buildroot-config/auto.conf KCONFIG_AUTOHEADER=/home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/build/buildroot-config/autoconf.h KCONFIG_TRISTATE=/home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/build/buildroot-config/tristate.config BUILDROOT_CONFIG=/home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/.config /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/build/buildroot-config/conf --silentoldconfig Config.in
make[1]: Leaving directory '/home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot'
mkdir -p /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/etc
echo "x210v3" > /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/etc/hostname
/bin/sed -i -e '$a \127.0.1.1\tx210v3' \
        -e '/^127.0.1.1/d' /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/etc/hosts
mkdir -p /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/etc
echo "Welcome to Buildroot" > /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/etc/issue
/bin/sed -i -e 's,^root:[^:]*:,root:$1$UHvUh8C7$KlWEcEbho0KQU/t6eubJt0:,' /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/etc/shadow
/bin/sed -i -e '/# GENERIC_SERIAL$/s~^.*#~ttySAC2::respawn:/sbin/getty -L ttySAC2 115200 vt100 #~' \
        /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/etc/inittab
/bin/sed -i -e '/^#.*# REMOUNT_ROOTFS_RW$/s~^#\+~~' /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/etc/inittab
rm -rf /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/usr/include /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/usr/share/aclocal \
        /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/usr/lib/pkgconfig /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/usr/share/pkgconfig
find /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/lib \( -name '*.a' -o -name '*.la' \) -print0 | xargs -0 rm -f
find /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/usr/lib \( -name '*.a' -o -name '*.la' \) -print0 | xargs -0 rm -f
rm -rf /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/usr/share/gdb
rm -rf /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/usr/man /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/usr/share/man
rm -rf /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/usr/info /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/usr/share/info
rm -rf /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/usr/doc /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/usr/share/doc
rm -rf /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/usr/share/gtk-doc
rmdir /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/usr/share 2>/dev/null
Makefile:454: recipe for target 'target-finalize' failed
make: [target-finalize] Error 1 (ignored)
find /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target -type f -perm +111 -not \( -name 'libpthread*.so*' \) -print | xargs /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/host/usr/bin/arm-none-linux-gnueabi-strip --remove-section=.comment --remove-section=.note 2>/dev/null || true
find: invalid mode ‘+111’
find /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/lib/modules -type f -name '*.ko' | \
        xargs -r /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/host/usr/bin/arm-none-linux-gnueabi-strip --remove-section=.comment --remove-section=.note --strip-unneeded || true
find /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/lib -type f -name 'libpthread*.so*' | \
        xargs /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/host/usr/bin/arm-none-linux-gnueabi-strip --remove-section=.comment --remove-section=.note --strip-debug || true
mkdir -p /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/etc
# Mandatory configuration file and auxilliary cache directory
# for recent versions of ldconfig
touch /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/etc/ld.so.conf
mkdir -p /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/var/cache/ldconfig
if [ -x "/home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/host/usr/bin/arm-none-linux-gnueabi-ldconfig" ]; \
then \
        /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/host/usr/bin/arm-none-linux-gnueabi-ldconfig -r /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target; \
else \
        /sbin/ldconfig -r /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target; \
fi
( \
        echo "NAME=Buildroot"; \
        echo "VERSION=2013.02-g3562335f"; \
        echo "ID=buildroot"; \
        echo "VERSION_ID=2013.02"; \
        echo "PRETTY_NAME=\"Buildroot 2013.02\"" \
) >  /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/etc/os-release
>>>   Copying overlay board/9tripod/x210ii/rootfs-overlay
>>>   Generating root filesystem image rootfs.tar
rm -f /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/build/_fakeroot.fs
rm -f /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/THIS_IS_NOT_YOUR_ROOT_FILESYSTEM
echo "chown -R 0:0 /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target" >> /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/build/_fakeroot.fs
cat system/device_table.txt  > /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/build/_device_table.txt
printf '\n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n /bin/busybox                     f 4755 0 0 - - - - -\n/usr/share/udhcpc/default.script f 755  0 0 - - - - -\n \n \n \n \n \n \n \n /dev/console c 622 0 0 5 1 - - -\n' >> /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/build/_device_table.txt
echo "/home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/host/usr/bin/makedevs -d /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/build/_device_table.txt /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target" >> /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/build/_fakeroot.fs
echo " tar -c""f /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/images/rootfs.tar -C /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target ." >> /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/build/_fakeroot.fs
chmod a+x /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/build/_fakeroot.fs
/home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/host/usr/bin/fakeroot -- /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/build/_fakeroot.fs
rootdir=/home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target
table='/home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/build/_device_table.txt'
cp support/misc/target-dir-warning.txt /home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/target/THIS_IS_NOT_YOUR_ROOT_FILESYSTEM
```

#### 设置uboot的环境变量：

##### 通过tftp下载kernel和nfs挂载rootfs：

```bash
x210 # setenv bootcmd 'tftp 30008000 zImage;bootm 30008000'
x210 # setenv bootargs 'root=/dev/nfs nfsroot=192.168.199.179:/opt/rootfs ip=192.168.199.188:192.168.199.179:192.168.199.1:255.255.255.0::eth0:off  init=/linuxrc console=ttySAC2,115200'
x210 # saveenv
Saving Environment to SMDK bootable device...
done
```

-----------------------------

#### 编译出错1：

```bash
make[2]: Entering directory '/home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/build/host-ncurses-5.9/ncurses'
/usr/bin/gcc -DHAVE_CONFIG_H -I../ncurses -I. -I/home/$(USER)/x210bv3s/qt_x210v3s_160307/buildroot/output/host/usr/include -D_GNU_SOURCE -DNDEBUG -I. -I../include -I/home/$(USER)/x210bv3s/qt_x210v3s_160307/buildroot/output/host/usr/include/ncurses -O2 -I/home/$(USER)/x210bv3s/qt_x210v3s_160307/buildroot/output/host/usr/include  --param max-inline-insns-single=1200 -c ../ncurses/lib_gen.c -o ../objects/lib_gen.o
/usr/bin/gcc -DHAVE_CONFIG_H -I../ncurses -I. -I/home/$(USER)/x210bv3s/qt_x210v3s_160307/buildroot/output/host/usr/include -D_GNU_SOURCE -DNDEBUG -I. -I../include -I/home/$(USER)/x210bv3s/qt_x210v3s_160307/buildroot/output/host/usr/include/ncurses -O2 -I/home/$(USER)/x210bv3s/qt_x210v3s_160307/buildroot/output/host/usr/include  --param max-inline-insns-single=1200 -c ../ncurses/./base/lib_getstr.c -o ../objects/lib_getstr.o
In file included from ../ncurses/curses.priv.h:283:0,
                 from ../ncurses/lib_gen.c:19:
_71987.c:835:15: error: expected ‘)’ before ‘int’
../include/curses.h:1594:56: note: in definition of macro ‘mouse_trafo’
 #define mouse_trafo(y,x,to_screen) wmouse_trafo(stdscr,y,x,to_screen)
                                                        ^
Makefile:785: recipe for target '../objects/lib_gen.o' failed
make[2]: *** [../objects/lib_gen.o] Error 1
make[2]: *** Waiting for unfinished jobs....
make[2]: Leaving directory '/home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/build/host-ncurses-5.9/ncurses'
Makefile:307: recipe for target '../lib/libncurses.a' failed
make[1]: *** [../lib/libncurses.a] Error 2
make[1]: Leaving directory '/home/$(USER)/workSpace/x210bv3s/qt_x210v3s_160307/buildroot/output/build/host-ncurses-5.9/progs'
package/pkg-generic.mk:114: recipe for target '/home/$(USER)/x210bv3s/qt_x210v3s_160307/buildroot/output/build/host-ncurses-5.9/.stamp_built' failed
make: *** [/home/$(USER)/x210bv3s/qt_x210v3s_160307/buildroot/output/build/host-ncurses-5.9/.stamp_built] Error 2

```

解决办法：

```bash
[$(USER)@fly-vm qt_x210v3s_160307]$ vim buildroot/output/build/host-ncurses-5.9/include/curses.tail +114
```

找到`curses.tail`文件第`104`行，去掉该行的注释；

---------------------

## 4.烧录

### [fastboot](https://download.csdn.net/download/I_feige/76017771)烧录

开发板使用`USB OTG`连接**PC**，`UART2`连接**PC**，进入**uboot**命令行，然后输入`fastboot`；

```bash
OK

U-Boot 1.3.4 (Aug  5 2021 - 08:18:54) for x210@Flyer


CPU:  S5PV210@1000MHz(OK)
        APLL = 1000MHz, HclkMsys = 200MHz, PclkMsys = 100MHz
        MPLL = 667MHz, EPLL = 96MHz
                       HclkDsys = 166MHz, PclkDsys = 83MHz
                       HclkPsys = 133MHz, PclkPsys = 66MHz
                       SCLKA2M  = 200MHz
Serial = CLKUART
Board:   X210BV3S
DRAM:    512 MB
Flash:   8 MB
SD/MMC:  3728MB
In:      serial
Out:     serial
Err:     serial
[LEFT UP] boot mode
checking mode for fastboot ...
Hit any key to stop autoboot:  0
x210 #
x210 # fastboot
[Partition table on MoviNAND]
ptn 0 name='bootloader' start=0x0 len=N/A (use hard-coded info. (cmd: movi))
ptn 1 name='kernel' start=N/A len=N/A (use hard-coded info. (cmd: movi))
ptn 2 name='ramdisk' start=N/A len=0x300000(~3072KB) (use hard-coded info. (cmd: movi))
ptn 3 name='config' start=0xAECC00 len=0x1028DC00(~264759KB)
ptn 4 name='system' start=0x10D7A800 len=0x1028DC00(~264759KB)
ptn 5 name='cache' start=0x21008400 len=0x65F7000(~104412KB)
ptn 6 name='userdata' start=0x275FF400 len=0xC0C6FC00(~3158463KB)
```

#### 驱动安装：

进入设备管理器查看，发现“其他设备-》Android 1.0”有一个感叹号；

![fastboot-01](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/fastboot-01.png)

安装[`x210_android_driver`](https://download.csdn.net/download/I_feige/76017771)驱动，电脑会重启，安装成功后，感叹号消失；

![fastboot-02](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/fastboot-02.png)

测试fastboot.exe命令，发现卡住不动：

```bash
fastboot.exe reboot
< waiting for device >
```

重新安装驱动出现如下问题:[安装驱动提示文件的哈希值不在指定的目录文件中的解决办法](https://jingyan.baidu.com/article/0f5fb09930de116d8334ea12.html)

![fastboot-03](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/fastboot-03.png)

禁用驱动程序签名，重新安装驱动；

![fastboot-04](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/fastboot-04.png)

#### 1.烧录linux+QT

##### 烧写uboot：

```bash
C:\Users\flyer\OneDrive\桌面\x210_fastboot\fastboot>fastboot.exe flash bootloader linuxQT4.8\uboot.bin
```

##### 烧写Linux Kernel:

```bash
C:\Users\flyer\OneDrive\桌面\x210_fastboot\fastboot>fastboot.exe flash kernel linuxQT4.8\zImage-qt
```

##### 烧写rootfs：

```bash
C:\Users\flyer\OneDrive\桌面\x210_fastboot\fastboot>fastboot.exe flash system linuxQT4.8\rootfs_qt4.ext3
```

### 通过tftp烧录

通过网线连接开发板，使开发板/PC/开发用的虚拟机处于同一网络下，相互可以通讯。

#### 烧写uboot：

```bash
A.cp u-boot.bin /tftpboot
B.tftp 0x40008000 u-boot.bin
C.nand erase 0 0x100000// nand erase bootloader
D.nand write 0x40008000 0 0x100000// nand write 0x40008000 bootloader
E.重启开发版
```



## 5.问题

### 5-1.挂载根文件系统失败

```bash
[    6.123311] eth0: link down
[    6.124780] ADDRCONF(NETDEV_UP): eth0: link is not ready
[    7.131110] IP-Config: Complete:
[    7.132683]      device=eth0, addr=192.168.199.188, mask=255.255.255.0, gw=192.168.199.1,
[    7.140743]      host=192.168.199.188, domain=, nis-domain=(none),
[    7.146914]      bootserver=192.168.199.179, rootserver=192.168.199.179, rootpath=
[    7.154828] DBUG_PORT must not use AFC!
[    7.158575] Looking up port of RPC 100003/2 on 192.168.199.179
[    7.877084] eth0: link up, 100Mbps, full-duplex, lpa 0x4DE1
[    7.881423] ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
[    8.229310] Looking up port of RPC 100005/1 on 192.168.199.179
[    8.261806] VFS: Unable to mount root fs via NFS, trying floppy.
[    8.266621] VFS: Cannot open root device "nfs" or unknown-block(2,0)
[    8.272798] Please append a correct "root=" boot option; here are the available partitions:
[    8.281017] b300         3817472 mmcblk0 driver: mmcblk
[    8.286175]   b301          264759 mmcblk0p1
[    8.290420]   b302          264759 mmcblk0p2
[    8.294666]   b303          104412 mmcblk0p3
[    8.298925]   b304         3158463 mmcblk0p4
[    8.303160] Kernel panic - not syncing: VFS: Unable to mount root fs on unknown-block(2,0)
[    8.311409] Backtrace:
[    8.313838] [<c0037fb8>] (dump_backtrace+0x0/0x110) from [<c0509828>] (dump_stack+0x18/0x1c)
[    8.322241]  r6:00008000 r5:dfecc000 r4:c0701b6c r3:00000002
[    8.327860] [<c0509810>] (dump_stack+0x0/0x1c) from [<c05098a4>] (panic+0x78/0xf8)
[    8.335420] [<c050982c>] (panic+0x0/0xf8) from [<c0008fa8>] (mount_block_root+0x1d8/0x218)
[    8.345027]  r3:00000002 r2:00000001 r1:dfc37f60 r0:c064f8aa
[    8.349263] [<c0008dd0>] (mount_block_root+0x0/0x218) from [<c00090ac>] (mount_root+0xc4/0xf8)
[    8.357861] [<c0008fe8>] (mount_root+0x0/0xf8) from [<c0009244>] (prepare_namespace+0x164/0x1bc)
[    8.366611]  r5:c002b331 r4:c073d900
[    8.370147] [<c00090e0>] (prepare_namespace+0x0/0x1bc) from [<c00084fc>] (kernel_init+0x128/0x170)
[    8.379086]  r5:c00083d4 r4:c073d6c0
[    8.382631] [<c00083d4>] (kernel_init+0x0/0x170) from [<c005b894>] (do_exit+0x0/0x5f0)
[    8.390528]  r4:00000000 r3:00000000
[    8.394063] Rebooting in 5 seconds..
[    8.828774] eth0: IPv6 duplicate address fe80::209:c0ff:feff:ec48 detected!
[   13.439046]
[   13.439066] Restarting Linux version 2.6.35.7+ ($(USER)@fly-vm) (gcc version 4.4.1 (Sourcery G++ Lite 2009q3-67) ) #1 PREEMPT Wed Jan 12 22:12:04 CST 2022
[   13.439076]
[   13.456122] arch_reset: attempting watchdog reOK
```

#### 解决办法：

打开文件：

```bash
[$(USER)@fly-vm ~]$ sudo vim /etc/default/nfs-kernel-server
```

加入：

```bash
RPCNFSDOPTS="--nfs-version 2,3,4 --debug --syslog"
```

然后重启nfs：

```bash
[$(USER)@fly-vm ~]$ sudo /etc/init.d/nfs-kernel-server restart
[ ok ] Restarting nfs-kernel-server (via systemctl): nfs-kernel-server.service.
```

### 5-2.根文件挂载启动失败：

```bash
[    7.119394] IP-Config: Complete:
[    7.120967]      device=eth0, addr=192.168.199.188, mask=255.255.255.0, gw=192.168.199.1,
[    7.129028]      host=192.168.199.188, domain=, nis-domain=(none),
[    7.135198]      bootserver=192.168.199.179, rootserver=192.168.199.179, rootpath=
[    7.143104] DBUG_PORT must not use AFC!
[    7.146853] Looking up port of RPC 100003/2 on 192.168.199.179
[    7.728631] eth0: link up, 100Mbps, full-duplex, lpa 0x4DE1
[    7.732842] ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
[    8.213659] Looking up port of RPC 100005/1 on 192.168.199.179
[    8.248699] VFS: Mounted root (nfs filesystem) on device 0:12.
[    8.253104] Freeing init memory: 172K
[    8.761015] eth0: IPv6 duplicate address fe80::209:c0ff:feff:ec48 detected!
Starting logging: OK
Starting mdev...
Initializing random number generator... done.
Starting network...
ip: RTNETLINK answers: File exists
udhcpc (v1.21.0) started
Sending discover...
[   29.607551] nfs: server 192.168.199.179 not responding, still trying
[  241.115338] INFO: task udhcpc:81 blocked for more than 120 seconds.
[  241.120138] "echo 0 > /proc/sys/kernel/hung_task_timeout_secs" disables this message.
[  241.127935] udhcpc          D c050a034     0    81     75 0x00000000
[  241.134240] Backtrace:
[  241.136684] [<c0509dc4>] (schedule+0x0/0x2e8) from [<c050a0f4>] (io_schedule+0x48/0x74)
[  241.144671] [<c050a0ac>] (io_schedule+0x0/0x74) from [<c0094444>] (sync_page+0x40/0x48)
[  241.152631]  r6:c0bfd498 r5:df415d6c r4:df415d64 r3:00000000
[  241.158269] [<c0094404>] (sync_page+0x0/0x48) from [<c050a594>] (__wait_on_bit_lock+0x5c/0xa4)
[  241.166848] [<c050a538>] (__wait_on_bit_lock+0x0/0xa4) from [<c00943dc>] (__lock_page+0x6c/0x7c)
[  241.175726] [<c0094370>] (__lock_page+0x0/0x7c) from [<c00945a8>] (find_lock_page+0x60/0x8c)
[  241.184004]  r6:000000fa r5:df82a21c r4:c08649c0
[  241.188581] [<c0094548>] (find_lock_page+0x0/0x8c) from [<c0094dc8>] (filemap_fault+0x248/0x440)
[  241.197346]  r6:000000fa r5:00000000 r4:dffbf000 r3:00000000
[  241.202972] [<c0094b80>] (filemap_fault+0x0/0x440) from [<c00a7cc8>] (__do_fault+0x54/0x438)
[  241.211391] [<c00a7c74>] (__do_fault+0x0/0x438) from [<c00a8a6c>] (handle_mm_fault+0x36c/0xc3c)
[  241.220065] [<c00a8700>] (handle_mm_fault+0x0/0xc3c) from [<c003a9f8>] (do_page_fault+0x10c/0x1fc)
[  241.228989] [<c003a8ec>] (do_page_fault+0x0/0x1fc) from [<c0033254>] (do_PrefetchAbort+0x3c/0xa0)
[  241.239200] [<c0033218>] (do_PrefetchAbort+0x0/0xa0) from [<c0033f64>] (ret_from_exception+0x0/0x10)
[  241.246919] Exception stack(0xdf415fb0 to 0xdf415ff8)
[  241.251938] 5fa0:                                     87c7a8c0 000a59f0 000002e0 bee126bc
[  241.260133] 5fc0: 87c7a8c0 01c7a8c0 05a91a13 05a91a13 00000000 00000000 87c7a8c0 000a800b
[  241.268236] 5fe0: 401250dc bee126b8 0002b5b4 401250dc 60000010 ffffffff
[  241.274927]  r7:05a91a13 r6:05a91a13 r5:01c7a8c0 r4:ffffffff
```

查看文件`interfaces`：

```bash
[$(USER)@fly-vm network]$ pwd
/opt/rootfs/etc/network
[$(USER)@fly-vm network]$ cat interfaces
# Configure Loopback
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp
```

进行如下修改：

```bash
# Configure Loopback
auto lo
iface lo inet loopback

#auto eth0
#iface eth0 inet dhcp

auto eht0
address 192.168.199.188
gateway 192.168.199.1
netmask 255.255.255.0
```



## 6.拓展

### 1. 关于buildroot下载路径dl目录的设置：

> [https://blog.csdn.net/xiang_shao344/article/details/84839623](https://blog.csdn.net/xiang_shao344/article/details/84839623)

### 2. 6.Linux驱动及内核开发：

> [https://blog.csdn.net/i_feige/category_6738193.html?spm=1001.2014.3001.5482](https://blog.csdn.net/i_feige/category_6738193.html?spm=1001.2014.3001.5482)

### 3. 如何使用busybox编译和生成最简linux根文件系统（rootfs）：

> [https://blog.csdn.net/LEON1741/article/details/54848842](https://blog.csdn.net/LEON1741/article/details/54848842)

### 4.烧写系统:SD卡、fastboot、DNW刷机

> [https://blog.csdn.net/I_feige/article/details/89298899](https://blog.csdn.net/I_feige/article/details/89298899)

### 5.fastboot/dnw工具及驱动

> 链接：https://pan.xunlei.com/s/VMtT3lVjhfH-pm0C7T8hNvLIA1
> 提取码：dm6z
> 复制这段内容后打开手机迅雷App，查看更方便

### 6.[Linux sync命令的作用](https://blog.csdn.net/vict_wang/article/details/81099544)

> https://blog.csdn.net/vict_wang/article/details/81099544

### 7.[Linux 下的dd命令使用详解(摘录)](https://www.cnblogs.com/jikexianfeng/p/6103500.html)

> https://www.cnblogs.com/jikexianfeng/p/6103500.html

### 8.[安装、配置、测试NFS和TFTP服务器](https://gitee.com/c-program/mark-down-doc/blob/master/01-LinuxSys/nfs-tftp-srv.md)

> [https://gitee.com/c-program/mark-down-doc/blob/master/01-LinuxSys/nfs-tftp-srv.md](https://gitee.com/c-program/mark-down-doc/blob/master/01-LinuxSys/nfs-tftp-srv.md)

### 9.U-Boot下载

> [https://ftp.denx.de/pub/u-boot/](https://ftp.denx.de/pub/u-boot/)

### 10.Kernel下载

> [https://mirror.bjtu.edu.cn/kernel/linux/kernel/](https://mirror.bjtu.edu.cn/kernel/linux/kernel/)

### 11.s5pv210学习笔记和课件

> [https://gitee.com/x210bv3s/v210-note_code](https://gitee.com/x210bv3s/v210-note_code)

### 12.代码主页

> [https://gitee.com/x210bv3s](https://gitee.com/x210bv3s)

## 7.工具

### 7-1.串口工具

> [Xshell 7 Personal个人免费版(SSH终端管理器) v7.0.0128 官方中文版](https://www.jb51.net/softs/753107.html)
