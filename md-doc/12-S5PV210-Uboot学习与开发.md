## `S5PV210` | `Uboot`学习与开发-`20221112`

-----

**时间：**`2023年12月4日09:26:20`

[TOC]



## 1.代码及工作路径

### 1-1.代码仓库

> https://gitee.com/x210bv3s/qt_x210v3s_160307/tree/master/uboot
>
> [qt_x210v3s_160307: x210开发板的linux+QT的bsp - Gitee.com](https://gitee.com/x210bv3s/qt_x210v3s_160307/tree/master/uboot)

### 1-2.工作目录

> [**`\\192.168.52.129\fly\project\x210bv3s\qt_x210v3s_160307\uboot\ 的索引`**](file://192.168.52.129/fly/project/x210bv3s/qt_x210v3s_160307/uboot/)

```bash
fly@fly-vm:uboot$ pwd
/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot
```

----------------------------------

## 2.编译`UBOOT`

### 2-1.编译

```bash
fly@fly-vm:uboot$ make distclean
fly@fly-vm:uboot$ make x210_
x210_nand_config  x210_sd_config
fly@fly-vm:uboot$ make x210_sd_config
Configuring for x210_sd board...
fly@fly-vm:uboot$ make -j4
```

### 2-2.编译`LOG`

```bash
Generating include/autoconf.mk.dep
fatal: No names found, cannot describe anything.
Generating include/autoconf.mk
for dir in tools examples api_examples ; do make -C $dir _depend ; done
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/tools'
ln -s ../common/environment.c environment.c
ln -s ../include/zlib.h zlib.h
ln -s ../lib_generic/md5.c md5.c
ln -s ../lib_generic/sha1.c sha1.c
ln -s ../common/image.c image.c
if [ ! -f mkimage.h ] ; then \
                ln -s ../tools/mkimage.h mkimage.h; \
        fi
if [ ! -f fdt_host.h ] ; then \
                ln -s ../tools/fdt_host.h fdt_host.h; \
        fi
ln -s ../lib_generic/crc32.c crc32.c
make[1]: Nothing to be done for '_depend'.
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/tools'
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/examples'
make[1]: Nothing to be done for '_depend'.
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/examples'
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/api_examples'
make[1]: Nothing to be done for '_depend'.
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/api_examples'
make -C tools all
make -C examples all
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/tools'
make -C api_examples all
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/examples'
make -C cpu/s5pc11x start.o
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/api_examples'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o hello_world.o hello_world.c
gcc -Wall -pedantic -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O -c -o img2srec.o img2srec.c
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/cpu/s5pc11x'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libglue.a
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -M -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector start.S nand_cp.c serial.c usb_ohci.c interrupts.c cpu.c nand.c onenand.c onenand_cp.c usbd-otg-hs.c movi.c setup_hsmmc.c setup_ide.c fastboot.c fimd.c pmic.c > .depend
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/api_examples'
make -C lib_generic/
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/lib_generic'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o stubs.o stubs.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libstubs.a stubs.o
a - stubs.o
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ld -g  -Ttext 0xc100000 \
                -o hello_world -e hello_world hello_world.o libstubs.a \
                -L/usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1 -lgcc
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-objcopy -O srec hello_world hello_world.srec 2>/dev/null
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-objcopy -O binary hello_world hello_world.bin 2>/dev/null
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/examples'
make -C cpu/s5pc11x/
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/cpu/s5pc11x'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc  -D__ASSEMBLY__ -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -c -o start.o start.S
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o nand_cp.o nand_cp.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o serial.o serial.c
serial.c: In function 'serial_setbrg':
serial.c:43: warning: unused variable 'gd'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o bzlib.o bzlib.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o usb_ohci.o usb_ohci.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o bzlib_crctable.o bzlib_crctable.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o bzlib_decompress.o bzlib_decompress.c
gcc -g -Wall -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O -c -o mkimage.o mkimage.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o bzlib_randtable.o bzlib_randtable.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o interrupts.o interrupts.c
In file included from mkimage.c:26:0:
/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include/image.h:479:12: warning: inline function ‘fit_parse_subimage’ declared but never defined
 inline int fit_parse_subimage (const char *spec, ulong addr_curr,
            ^
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o bzlib_huffman.o bzlib_huffman.c
/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include/image.h:477:12: warning: inline function ‘fit_parse_conf’ declared but never defined
 inline int fit_parse_conf (const char *spec, ulong addr_curr,
            ^
make[1]: 'start.o' is up to date.
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/cpu/s5pc11x'
make -C cpu/s5pc11x/s5pc110/
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o crc32.o crc32.c
interrupts.c: In function 'interrupt_init':
interrupts.c:192: warning: implicit declaration of function 'get_PCLK'
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/cpu/s5pc11x/s5pc110'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ctype.o ctype.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -M -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector cpu_init.S speed.c > .depend
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cpu.o cpu.c
gcc -g -Wall -pedantic -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O -c -o crc32.o crc32.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o display_options.o display_options.c
cpu.c: In function 'cleanup_before_linux':
cpu.c:119: warning: implicit declaration of function 'arm_cache_flush'
cpu.c: In function 'cpu_mmc_init':
cpu.c:235: warning: implicit declaration of function 'setup_hsmmc_clock'
cpu.c:236: warning: implicit declaration of function 'setup_hsmmc_cfg_gpio'
cpu.c:237: warning: implicit declaration of function 'smdk_s3c_hsmmc_init'
gcc -g -Wall -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O -c -o image.o image.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o nand.o nand.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc  -D__ASSEMBLY__ -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -c -o cpu_init.o cpu_init.S
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o div64.o div64.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o onenand.o onenand.c
In file included from image.c:74:0:
/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include/image.h:479:12: warning: inline function ‘fit_parse_subimage’ declared but never defined
 inline int fit_parse_subimage (const char *spec, ulong addr_curr,
            ^
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o speed.o speed.c
/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include/image.h:477:12: warning: inline function ‘fit_parse_conf’ declared but never defined
 inline int fit_parse_conf (const char *spec, ulong addr_curr,
            ^
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o lmb.o lmb.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o onenand_cp.o onenand_cp.c
onenand_cp.c: In function 'onenand_bl2_copy':
onenand_cp.c:139: warning: unused variable 'rv'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o usbd-otg-hs.o usbd-otg-hs.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libs5pc110.a cpu_init.o speed.o
a - cpu_init.o
a - speed.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/cpu/s5pc11x/s5pc110'
make -C lib_arm/
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ldiv.o ldiv.c
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/lib_arm'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o sha1.o sha1.c
gcc -g -Wall -pedantic -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O -c -o md5.o md5.c
usbd-otg-hs.c: In function 's3c_usb_transfer_ep0':
usbd-otg-hs.c:1175: warning: array subscript is above array bounds
usbd-otg-hs.c:1183: warning: array subscript is above array bounds
gcc -g -Wall -pedantic -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O -c -o sha1.o sha1.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o string.o string.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o movi.o movi.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc  -D__ASSEMBLY__ -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -c -o _ashldi3.o _ashldi3.S
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc  -D__ASSEMBLY__ -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -c -o _ashrdi3.o _ashrdi3.S
movi.c: In function 'movi_bl2_copy':
movi.c:38: warning: passing argument 4 of 'copy_bl2' makes pointer from integer without a cast
movi.c:38: note: expected 'u32 *' but argument is of type 'int'
movi.c:52: warning: passing argument 4 of 'copy_bl2' makes pointer from integer without a cast
movi.c:52: note: expected 'u32 *' but argument is of type 'int'
movi.c: In function 'movi_zImage_copy':
movi.c:91: warning: passing argument 4 of 'copy_zImage' makes pointer from integer without a cast
movi.c:91: note: expected 'u32 *' but argument is of type 'int'
movi.c: In function 'movi_write_env':
movi.c:108: warning: implicit declaration of function 'movi_write'
movi.c: In function 'movi_read_env':
movi.c:114: warning: implicit declaration of function 'movi_read'
movi.c: In function 'movi_write_bl1':
movi.c:131: warning: format '%x' expects type 'unsigned int', but argument 2 has type 'ulong'
movi.c:123: warning: unused variable 'tmp'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc  -D__ASSEMBLY__ -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -c -o _divsi3.o _divsi3.S
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o setup_hsmmc.o setup_hsmmc.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o vsprintf.o vsprintf.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc  -D__ASSEMBLY__ -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -c -o _modsi3.o _modsi3.S
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc  -D__ASSEMBLY__ -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -c -o _udivsi3.o _udivsi3.S
setup_hsmmc.c: In function 'setup_hsmmc_clock':
setup_hsmmc.c:19: warning: implicit declaration of function 'get_MPLL_CLK'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o setup_ide.o setup_ide.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc  -D__ASSEMBLY__ -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -c -o _umodsi3.o _umodsi3.S
ln -s ../libfdt/libfdt_internal.h libfdt_internal.h
gcc -g -Wall -pedantic -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O -c -o envcrc.o envcrc.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o board.o board.c
setup_ide.c:35: warning: "ATA_COMMAND" redefined
/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include/ata.h:58: note: this is the location of the previous definition
setup_ide.c: In function 'ide_preinit':
setup_ide.c:67: warning: unused variable 'tmp'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o fastboot.o fastboot.c
gcc -g  -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -c -o environment.o environment.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o zlib.o zlib.c
gcc -g -Wall -pedantic -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O -c -o ubsha1.o ubsha1.c
board.c: In function 'open_backlight':
board.c:195: warning: suggest parentheses around arithmetic in operand of '|'
board.c:199: warning: suggest parentheses around arithmetic in operand of '|'
board.c: In function 'check_menu_update_from_sd':
board.c:217: warning: suggest parentheses around arithmetic in operand of '|'
board.c: In function 'open_gprs':
board.c:245: warning: suggest parentheses around arithmetic in operand of '|'
board.c:248: warning: suggest parentheses around arithmetic in operand of '|'
board.c:252: warning: suggest parentheses around arithmetic in operand of '|'
board.c:255: warning: suggest parentheses around arithmetic in operand of '|'
board.c:259: warning: suggest parentheses around arithmetic in operand of '|'
board.c:262: warning: suggest parentheses around arithmetic in operand of '|'
board.c:266: warning: suggest parentheses around arithmetic in operand of '|'
board.c:269: warning: suggest parentheses around arithmetic in operand of '|'
board.c: At top level:
board.c:238: warning: 'open_gprs' defined but not used
fastboot.c: In function 'fboot_usb_int_bulkin':
fastboot.c:1699: warning: passing argument 1 of 'fboot_usb_write_in_fifo' discards qualifiers from pointer target type
fastboot.c:760: note: expected 'u8 *' but argument is of type 'const char *'
fastboot.c:1684: warning: unused variable 'DIEPCTL_IN'
fastboot.c:1683: warning: unused variable 'remain_cnt'
fastboot.c:1682: warning: unused variable 'bulkin_buf'
fastboot.c: In function 'fboot_usb_int_bulkout':
fastboot.c:1922: warning: pointer targets in passing argument 1 of 'sprintf' differ in signedness
/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include/common.h:616: note: expected 'char *' but argument is of type 'u8 *'
fastboot.c:1922: warning: embedded '\0' in format
fastboot.c: In function 'fboot_usb_transfer':
fastboot.c:2357: warning: unused variable 'tmp'
fastboot.c:2356: warning: unused variable 'DIEPINT_IN'
fastboot.c:2355: warning: unused variable 'DIEPCTL_IN'
fastboot.c: In function 'fboot_usb_int_hndlr':
fastboot.c:2437: warning: unused variable 'val'
fastboot.c: In function 'fastboot_init':
fastboot.c:2591: warning: unused variable 'devctl'
fastboot.c: In function 'fastboot_poll':
fastboot.c:2661: warning: unused variable 'intrrx'
fastboot.c:2660: warning: unused variable 'intrtx'
fastboot.c: In function 'fastboot_tx_status':
fastboot.c:2692: warning: unused variable 'i'
fastboot.c:2691: warning: unused variable 'ret'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o bootm.o bootm.c
gcc -g -Wall -pedantic -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O -c -o gen_eth_addr.o gen_eth_addr.c
bootm.c: In function 'do_bootm_linux':
bootm.c:140: warning: implicit declaration of function 'setup_mtdpartition_tag'
bootm.c: At top level:fastboot.c: In function 'fboot_usb_transfer_ep0':
bootm.c:308: warning: function declaration isn't a prototype
bootm.c:307: warning: conflicting types for 'setup_mtdpartition_tag'
bootm.c:140: note: previous implicit declaration of 'setup_mtdpartition_tag' was here

fastboot.c:1395: warning: array subscript is above array bounds
fastboot.c:1403: warning: array subscript is above array bounds
gcc -Wall -pedantic -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O -c -o bmp_logo.o bmp_logo.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cache.o cache.c
bmp_logo.c: In function ‘main’:
bmp_logo.c:71:2: warning: ignoring return value of ‘fread’, declared with attribute warn_unused_result [-Wunused-result]
  fread (&data_offset, sizeof (uint16_t), 1, fp);
  ^
bmp_logo.c:73:2: warning: ignoring return value of ‘fread’, declared with attribute warn_unused_result [-Wunused-result]
  fread (&b->width,   sizeof (uint16_t), 1, fp);
  ^
bmp_logo.c:75:2: warning: ignoring return value of ‘fread’, declared with attribute warn_unused_result [-Wunused-result]
  fread (&b->height,  sizeof (uint16_t), 1, fp);
  ^
bmp_logo.c:77:2: warning: ignoring return value of ‘fread’, declared with attribute warn_unused_result [-Wunused-result]
  fread (&n_colors, sizeof (uint16_t), 1, fp);
  ^
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o div0.o div0.c
gcc -Wall -pedantic -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O  -o img2srec img2srec.o
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o interrupts.o interrupts.c
strip img2srec
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libarm.a _ashldi3.o _ashrdi3.o _divsi3.o _modsi3.o _udivsi3.o _umodsi3.o board.o bootm.o cache.o div0.o interrupts.o
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o fimd.o fimd.c
ln -s ../libfdt/fdt.c fdt.c
a - _ashldi3.o
a - _ashrdi3.o
a - _divsi3.o
a - _modsi3.o
a - _udivsi3.o
a - _umodsi3.o
a - board.o
a - bootm.o
a - cache.o
a - div0.o
a - interrupts.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/lib_arm'
make -C fs/cramfs/
ln -s ../libfdt/fdt_ro.c fdt_ro.c
ln -s ../libfdt/fdt_rw.c fdt_rw.c
fimd.c: In function 'LCD_Initialize_NONAME1':
fimd.c:454: warning: implicit declaration of function 'LCD_setprogress'
fimd.c:333: warning: unused variable 'pBuffer'
fimd.c:332: warning: unused variable 'i'
fimd.c: At top level:
fimd.c:482: warning: conflicting types for 'LCD_setprogress'
fimd.c:454: note: previous implicit declaration of 'LCD_setprogress' was here
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/fs/cramfs'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o pmic.o pmic.c
ln -s ../libfdt/fdt_strerror.c fdt_strerror.c
ln -s ../libfdt/fdt_wip.c fdt_wip.c
gcc -Wall -pedantic -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O -o envcrc envcrc.o crc32.o environment.o sha1.o
pmic.c: In function 'Delay':
pmic.c:6: warning: unused variable 'j'
pmic.c: At top level:
pmic.c:11: warning: function declaration isn't a prototype
pmic.c:18: warning: function declaration isn't a prototype
pmic.c:25: warning: function declaration isn't a prototype
pmic.c:32: warning: function declaration isn't a prototype
pmic.c:39: warning: function declaration isn't a prototype
pmic.c:47: warning: function declaration isn't a prototype
pmic.c:55: warning: function declaration isn't a prototype
pmic.c:63: warning: function declaration isn't a prototype
pmic.c:71: warning: function declaration isn't a prototype
gcc -Wall -pedantic -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O -o ubsha1 ubsha1.o sha1.o
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libs5pc11x.a nand_cp.o serial.o  usb_ohci.o interrupts.o cpu.o nand.o onenand.o onenand_cp.o usbd-otg-hs.o movi.o setup_hsmmc.o setup_ide.o fastboot.o fimd.o pmic.o
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libgeneric.a bzlib.o bzlib_crctable.o bzlib_decompress.o bzlib_randtable.o bzlib_huffman.o crc32.o ctype.o display_options.o div64.o lmb.o ldiv.o sha1.o string.o vsprintf.o zlib.o
a - nand_cp.o
gcc -Wall -pedantic -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O  -o gen_eth_addr gen_eth_addr.o
a - serial.o
a - usb_ohci.o
a - interrupts.o
a - cpu.o
a - nand.o
a - onenand.o
a - onenand_cp.o
a - usbd-otg-hs.o
a - movi.o
a - setup_hsmmc.o
a - setup_ide.o
a - fastboot.o
a - fimd.o
a - pmic.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/cpu/s5pc11x'
make -C fs/fat/
a - bzlib.o
a - bzlib_crctable.o
a - bzlib_decompress.o
a - bzlib_randtable.o
a - bzlib_huffman.o
a - crc32.o
a - ctype.o
a - display_options.o
a - div64.o
a - lmb.o
a - ldiv.o
a - sha1.o
a - string.o
a - vsprintf.o
a - zlib.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/lib_generic'
gcc -Wall -pedantic -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O  -o bmp_logo bmp_logo.o
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/fs/fat'
strip gen_eth_addr
make -C fs/fdos/
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cramfs.o cramfs.c
strip bmp_logo
gcc -g -Wall -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O -c -o fdt.o fdt.c
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/fs/fdos'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o uncompress.o uncompress.c
gcc -g -Wall -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O -c -o fdt_ro.o fdt_ro.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libcramfs.a cramfs.o uncompress.o
a - cramfs.o
a - uncompress.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/fs/cramfs'
make -C fs/jffs2/
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o fat.o fat.c
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/fs/jffs2'
In file included from fat.c:36:
/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include/fat.h:195: warning: function declaration isn't a prototype
fat.c: In function 'fat_register_device':
fat.c:131: warning: format '%x' expects type 'unsigned int', but argument 3 has type 'ulong'
fat.c:131: warning: format '%x' expects type 'unsigned int', but argument 4 has type 'ulong'
fat.c: In function 'write_pbr':
fat.c:157: warning: implicit declaration of function 'malloc'
fat.c:157: warning: assignment makes pointer from integer without a cast
fat.c:241: warning: format '%x' expects type 'unsigned int', but argument 2 has type 'long unsigned int'
fat.c:153: warning: unused variable 'reserved_cnt'
fat.c: In function 'write_reserved':
fat.c:283: warning: assignment makes pointer from integer without a cast
fat.c:282: warning: unused variable 'i'
fat.c: In function 'write_fat':
fat.c:325: warning: assignment makes pointer from integer without a cast
fat.c:333: warning: assignment makes pointer from integer without a cast
fat.c:342: warning: unused variable 'temp'
fat.c: In function 'mklabel':
fat.c:389: warning: implicit declaration of function '__toupper'
fat.c: In function 'fat_format_device':
fat.c:437: warning: format '%x' expects type 'unsigned int', but argument 3 has type 'ulong'
fat.c:437: warning: format '%x' expects type 'unsigned int', but argument 4 has type 'ulong'
fat.c: At top level:
fat.c:384: warning: 'mklabel' defined but not used
gcc -g -Wall -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O -c -o fdt_rw.o fdt_rw.c
fat.c: In function 'read_bootsectandvi':/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o fat.o fat.c

fat.c:1033: warning: array subscript is above array bounds
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o vfat.o vfat.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o jffs2_1pass.o jffs2_1pass.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o dev.o dev.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o compr_rtime.o compr_rtime.c
gcc -g -Wall -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O -c -o fdt_strerror.o fdt_strerror.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o fdos.o fdos.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o compr_rubin.o compr_rubin.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o file.o file.c
gcc -g -Wall -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O -c -o fdt_wip.o fdt_wip.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o fs.o fs.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o compr_zlib.o compr_zlib.c
In file included from file.c:31:
/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include/fat.h:195: warning: function declaration isn't a prototype
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o subdir.o subdir.c
./bmp_logo logos/denx.bmp >/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include/bmp_logo.h
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o mini_inflate.o mini_inflate.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libfat.a fat.o file.o
gcc -Wall -pedantic -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include2 -idirafter /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -DTEXT_BASE=0xc3e00000 -DUSE_HOSTCC -O  -o mkimage mkimage.o crc32.o image.o md5.o sha1.o fdt.o fdt_ro.o fdt_rw.o fdt_strerror.o fdt_wip.o
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o compr_lzo.o compr_lzo.c
a - fat.o
a - file.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/fs/fat'
make -C fs/reiserfs/
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libfdos.a fat.o vfat.o dev.o fdos.o fs.o subdir.o
strip mkimage
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/tools'
make -C fs/ext2/
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/fs/reiserfs'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o compr_lzari.o compr_lzari.c
a - fat.o
a - vfat.o
a - dev.o
a - fdos.o
a - fs.o
a - subdir.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/fs/fdos'
make -C net/
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/fs/ext2'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libjffs2.a jffs2_1pass.o compr_rtime.o compr_rubin.o compr_zlib.o mini_inflate.o compr_lzo.o compr_lzari.o
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/net'
a - jffs2_1pass.o
a - compr_rtime.o
a - compr_rubin.o
a - compr_zlib.o
a - mini_inflate.o
a - compr_lzo.o
a - compr_lzari.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/fs/jffs2'
make -C disk/
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/disk'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o reiserfs.o reiserfs.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ext2fs.o ext2fs.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o dev.o dev.c
ext2fs.c: In function 'ext2fs_format':
ext2fs.c:1051: warning: format '%x' expects type 'unsigned int', but argument 3 has type 'ulong'
ext2fs.c:1051: warning: format '%x' expects type 'unsigned int', but argument 4 has type 'ulong'
ext2fs.c:1561: warning: assignment from incompatible pointer type
ext2fs.c:1585: warning: assignment from incompatible pointer type
ext2fs.c:1598: warning: assignment from incompatible pointer type
ext2fs.c:1647: warning: assignment from incompatible pointer type
ext2fs.c:1652: warning: assignment from incompatible pointer type
ext2fs.c:1700: warning: assignment from incompatible pointer type
ext2fs.c:1706: warning: assignment from incompatible pointer type
ext2fs.c:1792: warning: assignment from incompatible pointer type
ext2fs.c:1836: warning: assignment from incompatible pointer type
ext2fs.c:1971: warning: format '%x' expects type 'unsigned int', but argument 2 has type 'long unsigned int'
ext2fs.c:1984: warning: assignment from incompatible pointer type
ext2fs.c:1241: warning: unused variable 'default_used_blk'
ext2fs.c:1240: warning: unused variable 'normal_used_blk'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o mode_string.o mode_string.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o part.o part.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o net.o net.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libreiserfs.a reiserfs.o dev.o mode_string.o
ext2fs.c:1434: warning: 'temp_val' may be used uninitialized in this function
ext2fs.c:1841: warning: 'temp_val' may be used uninitialized in this function
ext2fs.c:1060: warning: 'uuid[3]' may be used uninitialized in this function
ext2fs.c:1060: warning: 'uuid[2]' may be used uninitialized in this function
ext2fs.c:1060: warning: 'uuid[1]' may be used uninitialized in this function
ext2fs.c:1060: warning: 'uuid[0]' may be used uninitialized in this function
a - reiserfs.o
a - dev.o
a - mode_string.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/fs/reiserfs'
make -C drivers/bios_emulator/
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/bios_emulator'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o part_mac.o part_mac.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o part_dos.o part_dos.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o part_iso.o part_iso.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o tftp.o tftp.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o part_amiga.o part_amiga.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libdisk.a part.o part_mac.o part_dos.o part_iso.o part_amiga.o
a - part.o
a - part_mac.o
a - part_dos.o
a - part_iso.o
a - part_amiga.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/disk'
make -C drivers/block/
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o bootp.o bootp.c
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/block'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o dev.o dev.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -I./include -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -D__PPC__  -D__BIG_ENDIAN__ -c -o atibios.o atibios.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -I./include -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -D__PPC__  -D__BIG_ENDIAN__ -c -o biosemu.o biosemu.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libext2fs.a ext2fs.o dev.o
a - ext2fs.o
a - dev.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/fs/ext2'
make -C drivers/dma/
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o rarp.o rarp.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -I./include -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -D__PPC__  -D__BIG_ENDIAN__ -c -o besys.o besys.c
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/dma'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -I./include -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -D__PPC__  -D__BIG_ENDIAN__ -c -o bios.o bios.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o eth.o eth.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ahci.o ahci.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -I./include -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -D__PPC__  -D__BIG_ENDIAN__ -c -o x86emu/decode.o x86emu/decode.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o nfs.o nfs.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ata_piix.o ata_piix.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -I./include -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -D__PPC__  -D__BIG_ENDIAN__ -c -o x86emu/ops2.o x86emu/ops2.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o MCD_tasksInit.o MCD_tasksInit.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o sil680.o sil680.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -I./include -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -D__PPC__  -D__BIG_ENDIAN__ -c -o x86emu/ops.o x86emu/ops.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o MCD_dmaApi.o MCD_dmaApi.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o sym53c8xx.o sym53c8xx.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -I./include -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -D__PPC__  -D__BIG_ENDIAN__ -c -o x86emu/prim_ops.o x86emu/prim_ops.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o MCD_tasks.o MCD_tasks.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o sntp.o sntp.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -I./include -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -D__PPC__  -D__BIG_ENDIAN__ -c -o x86emu/sys.o x86emu/sys.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o systemace.o systemace.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libdma.a MCD_tasksInit.o MCD_dmaApi.o MCD_tasks.o
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -I./include -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -D__PPC__  -D__BIG_ENDIAN__ -c -o x86emu/debug.o x86emu/debug.c
In file included from systemace.c:43:
/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include/systemace.h:22: warning: #ident is a deprecated GCC extension
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libblock.a ahci.o ata_piix.o sil680.o sym53c8xx.o systemace.o
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libnet.a net.o tftp.o bootp.o rarp.o eth.o nfs.o sntp.o
a - MCD_tasksInit.o
a - MCD_dmaApi.o
a - MCD_tasks.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/dma'
make -C drivers/hwmon/
a - ahci.o
a - ata_piix.o
a - sil680.o
a - sym53c8xx.o
a - systemace.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/block'
a - net.o
a - tftp.o
a - bootp.o
a - rarp.o
a - eth.o
a - nfs.o
a - sntp.o
make -C drivers/i2c/
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/net'
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/hwmon'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libatibiosemu.a atibios.o biosemu.o besys.o bios.o x86emu/decode.o x86emu/ops2.o x86emu/ops.o x86emu/prim_ops.o x86emu/sys.o x86emu/debug.o
make -C drivers/input/
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/i2c'
a - atibios.o
a - biosemu.o
a - besys.o
a - bios.o
a - x86emu/decode.o
a - x86emu/ops2.o
a - x86emu/ops.o
a - x86emu/prim_ops.o
a - x86emu/sys.o
a - x86emu/debug.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/bios_emulator'
make -C drivers/misc/
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/input'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libhwmon.a
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/misc'
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/hwmon'
make -C drivers/mmc/
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/mmc'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ali512x.o ali512x.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o fsl_i2c.o fsl_i2c.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ns87308.o ns87308.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o mmc.o mmc.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o i8042.o i8042.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o status_led.o status_led.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o omap1510_i2c.o omap1510_i2c.c
mmc.c: In function 'mmc_initialize':
mmc.c:1206: warning: format '%ld' expects type 'long int', but argument 2 has type 'u32'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o keyboard.o keyboard.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libmisc.a ali512x.o ns87308.o status_led.o
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o omap24xx_i2c.o omap24xx_i2c.c
a - ali512x.o
a - ns87308.o
a - status_led.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/misc'
make -C drivers/mtd/
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o pc_keyb.o pc_keyb.c
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/mtd'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o tsi108_i2c.o tsi108_i2c.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ps2ser.o ps2ser.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o mxc_i2c.o mxc_i2c.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ps2mult.o ps2mult.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o s3c_hsmmc.o s3c_hsmmc.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libi2c.a fsl_i2c.o omap1510_i2c.o omap24xx_i2c.o tsi108_i2c.o mxc_i2c.o
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o mw_eeprom.o mw_eeprom.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libinput.a i8042.o keyboard.o pc_keyb.o ps2ser.o ps2mult.o
a - fsl_i2c.o
a - omap1510_i2c.o
a - omap24xx_i2c.o
a - tsi108_i2c.o
a - mxc_i2c.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/i2c'
make -C drivers/mtd/nand/
s3c_hsmmc.c: In function 'sdhci_prepare_data':
s3c_hsmmc.c:69: warning: implicit declaration of function 'virt_to_phy_smdkc110'
s3c_hsmmc.c: In function 's3c_hsmmc_set_ios':
s3c_hsmmc.c:319: warning: implicit declaration of function 'setup_sdhci0_cfg_card'
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/mtd/nand'
a - i8042.o
a - keyboard.o
a - pc_keyb.o
a - ps2ser.o
a - ps2mult.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/input'
make -C drivers/mtd/nand_legacy/
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o jedec_flash.o jedec_flash.c
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/mtd/nand_legacy'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libmmc.a mmc.o s3c_hsmmc.o
a - mmc.o
a - s3c_hsmmc.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/mmc'
make -C drivers/mtd/onenand/
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libmtd.a mw_eeprom.o jedec_flash.o
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/mtd/onenand'
a - mw_eeprom.o
a - jedec_flash.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/mtd'
make -C drivers/mtd/ubi/
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/mtd/ubi'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o nand_legacy.o nand_legacy.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libnand_legacy.a nand_legacy.o
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libubi.a
a - nand_legacy.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/mtd/nand_legacy'
make -C drivers/mtd/spi/
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/mtd/ubi'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o nand.o nand.c
make -C drivers/net/
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/mtd/spi'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o onenand_uboot.o onenand_uboot.c
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/net'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o nand_base.o nand_base.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o onenand_base.o onenand_base.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libspi_flash.a
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o nand_ids.o nand_ids.c
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/mtd/spi'
make -C drivers/net/sk98lin/
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o onenand_bbt.o onenand_bbt.c
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/net/sk98lin'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o nand_ecc.o nand_ecc.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o dm9000x.o dm9000x.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libonenand.a onenand_uboot.o onenand_base.o onenand_bbt.o
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o nand_bbt.o nand_bbt.c
a - onenand_uboot.o
a - onenand_base.o
a - onenand_bbt.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/mtd/onenand'
make -C drivers/pci/
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/pci'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o nand_util.o nand_util.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o fsl_upm.o fsl_upm.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libnand.a nand.o nand_base.o nand_ids.o nand_ecc.o nand_bbt.o nand_util.o fsl_upm.o
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libnet.a dm9000x.o
a - nand.o
a - nand_base.o
a - nand_ids.o
a - nand_ecc.o
a - nand_bbt.o
a - nand_util.o
a - fsl_upm.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/mtd/nand'
make -C drivers/pcmcia/
a - dm9000x.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/net'
make -C drivers/spi/
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/pcmcia'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -DSK_USE_CSUM  -c -o skge.o skge.c
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/spi'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -DSK_USE_CSUM  -c -o skaddr.o skaddr.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o fsl_pci_init.o fsl_pci_init.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -DSK_USE_CSUM  -c -o skgehwt.o skgehwt.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libspi.a
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/spi'
make -C drivers/rtc/
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -DSK_USE_CSUM  -c -o skgeinit.o skgeinit.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o pci.o pci.c
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/rtc'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -DSK_USE_CSUM  -c -o skgepnmi.o skgepnmi.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o pci_auto.o pci_auto.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o mpc8xx_pcmcia.o mpc8xx_pcmcia.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -DSK_USE_CSUM  -c -o skgesirq.o skgesirq.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -DSK_USE_CSUM  -c -o ski2c.o ski2c.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o pci_indirect.o pci_indirect.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o rpx_pcmcia.o rpx_pcmcia.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -DSK_USE_CSUM  -c -o sklm80.o sklm80.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o tsi108_pci.o tsi108_pci.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o tqm8xx_pcmcia.o tqm8xx_pcmcia.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -DSK_USE_CSUM  -c -o skqueue.o skqueue.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o w83c553f.o w83c553f.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -DSK_USE_CSUM  -c -o skrlmt.o skrlmt.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libpcmcia.a mpc8xx_pcmcia.o rpx_pcmcia.o tqm8xx_pcmcia.o
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -DSK_USE_CSUM  -c -o sktimer.o sktimer.c
a - mpc8xx_pcmcia.o
a - rpx_pcmcia.o
a - tqm8xx_pcmcia.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/pcmcia'
make -C drivers/serial/
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libpci.a fsl_pci_init.o pci.o pci_auto.o pci_indirect.o tsi108_pci.o w83c553f.o
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -DSK_USE_CSUM  -c -o skvpd.o skvpd.c
a - fsl_pci_init.o
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/serial'
a - pci.o
a - pci_auto.o
a - pci_indirect.o
a - tsi108_pci.o
a - w83c553f.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/pci'
make -C drivers/usb/
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/usb'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -DSK_USE_CSUM  -c -o skxmac2.o skxmac2.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -DSK_USE_CSUM  -c -o skcsum.o skcsum.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -DSK_USE_CSUM  -c -o uboot_skb.o uboot_skb.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -I. -DSK_USE_CSUM  -c -o uboot_drv.o uboot_drv.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libsk98lin.a skge.o skaddr.o skgehwt.o skgeinit.o skgepnmi.o skgesirq.o ski2c.o sklm80.o skqueue.o skrlmt.o sktimer.o skvpd.o skxmac2.o skcsum.o uboot_skb.o uboot_drv.o
a - skge.o
a - skaddr.o
a - skgehwt.o
a - skgeinit.o
a - skgepnmi.o
a - skgesirq.o
a - ski2c.o
a - sklm80.o
a - skqueue.o
a - skrlmt.o
a - sktimer.o
a - skvpd.o
a - skxmac2.o
a - skcsum.o
a - uboot_skb.o
a - uboot_drv.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/net/sk98lin'
make -C drivers/video/
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ns9750_serial.o ns9750_serial.c
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/video'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ns16550.o ns16550.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o isp116x-hcd.o isp116x-hcd.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o serial.o serial.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o sl811_usb.o sl811_usb.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o serial_pl010.o serial_pl010.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o usb_ohci.o usb_ohci.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o serial_pl011.o serial_pl011.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o usbdcore.o usbdcore.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o bfin_rtc.o bfin_rtc.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o serial_sh.o serial_sh.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o date.o date.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libserial.a ns9750_serial.o ns16550.o serial.o serial_pl010.o serial_pl011.o serial_sh.o
a - ns9750_serial.o
a - ns16550.o
a - serial.o
a - serial_pl010.o
a - serial_pl011.o
a - serial_sh.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/serial'
make -C common/
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/common'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o usbdcore_ep0.o usbdcore_ep0.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ds12887.o ds12887.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o fbpixel.o fbpixel.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o usbdcore_mpc8xx.o usbdcore_mpc8xx.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ds1302.o ds1302.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o fbblit.o fbblit.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o usbdcore_omap1510.o usbdcore_omap1510.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ds1306.o ds1306.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libusb.a isp116x-hcd.o sl811_usb.o usb_ohci.o usbdcore.o usbdcore_ep0.o usbdcore_mpc8xx.o usbdcore_omap1510.o
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ds1307.o ds1307.c
a - isp116x-hcd.o
a - sl811_usb.o
a - usb_ohci.o
a - usbdcore.o
a - usbdcore_ep0.o
a - usbdcore_mpc8xx.o
a - usbdcore_omap1510.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/usb'
make -C libfdt/
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/libfdt'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ds1337.o ds1337.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o fbfill.o fbfill.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ds1374.o ds1374.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libfdt.a
fbfill.c:15: warning: "cpu_to_le32" redefinedmake[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/libfdt'
make -C api/

/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include/linux/byteorder/generic.h:89: note: this is the location of the previous definition
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ds1556.o ds1556.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o rect.o rect.c
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/api'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ds164x.o ds164x.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o fbviewport.o fbviewport.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libapi.a
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/api'
make -C post/
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ds174x.o ds174x.c
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/post'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o graphic.o graphic.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ds3231.o ds3231.c
graphic.c: In function 'fb_set_viewport':
graphic.c:27: warning: implicit declaration of function 'bitmap_set_viewport'
graphic.c: In function 'bitmap_fill_rect':
graphic.c:113: warning: implicit declaration of function 'common_bitmap_fill_rect'
graphic.c: In function 'bitmap_blit':
graphic.c:146: warning: implicit declaration of function 'common_bitmap_blit'
(echo create libpost.a; for lib in   ; \
 do echo addlib $lib; done; echo save) \
| /usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar -M
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o m41t11.o m41t11.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o fbcolor.o fbcolor.c
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/post'
make -C /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/board/samsung/x210/ u-boot.lds
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/board/samsung/x210'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o m41t60.o m41t60.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o fbsoft.o fbsoft.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o m48t35ax.o m48t35ax.c
make[1]: Nothing to be done for 'u-boot.lds'.
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/board/samsung/x210'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o bitmap.o bitmap.c
fbsoft.c: In function 'fb_soft_map_color':
fbsoft.c:24: warning: implicit declaration of function 'bitmap_map_color'
fbsoft.c: In function 'fb_soft_unmap_color':
fbsoft.c:32: warning: implicit declaration of function 'bitmap_unmap_color'
fbsoft.c: In function 'fb_soft_fill_rect':
fbsoft.c:60: warning: implicit declaration of function 'common_bitmap_fill_rect'
fbsoft.c: In function 'fb_soft_blit_bitmap':
fbsoft.c:90: warning: implicit declaration of function 'common_bitmap_blit'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o logo.o logo.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o max6900.o max6900.c
logo.c:6167: warning: trigraph ??> ignored, use -trigraphs to enable
logo.c: In function 'display_logo':/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o mpadfb.o mpadfb.c

logo.c:6534: warning: implicit declaration of function 'fb_get_viewport'
logo.c:6536: warning: implicit declaration of function 'fb_set_viewport'
logo.c:6537: warning: implicit declaration of function 'fb_map_color'
logo.c:6539: warning: implicit declaration of function 'fb_fill_rect'
logo.c:6541: warning: implicit declaration of function 'fb_blit_bitmap'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o mc146818.o mc146818.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o mcfrtc.o mcfrtc.c
mpadfb.c:108: warning: initialization makes pointer from integer without a cast
mpadfb.c: In function 's5pv210fb_set_clock':
mpadfb.c:427: warning: implicit declaration of function 'get_HCLKD'
mpadfb.c:431: warning: implicit declaration of function 'div64'
mpadfb.c:432: warning: implicit declaration of function 'mod64'
mpadfb.c: At top level:/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o mk48t59.o mk48t59.c

mpadfb.c:713: warning: function declaration isn't a prototype
mpadfb.c:753: warning: function declaration isn't a prototype
mpadfb.c:797: warning: function declaration isn't a prototype
mpadfb.c:878: warning: function declaration isn't a prototype
mpadfb.c: In function 'mpadfb_init':
mpadfb.c:887: warning: implicit declaration of function 'display_logo'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o mpc5xxx.o mpc5xxx.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o mpc8xx.o mpc8xx.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libvideo.a fbpixel.o fbblit.o fbfill.o rect.o fbviewport.o graphic.o fbcolor.o fbsoft.o bitmap.o logo.o mpadfb.o
a - fbpixel.o
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o pcf8563.o pcf8563.c
a - fbblit.o
a - fbfill.o
a - rect.o
a - fbviewport.o
a - graphic.o
a - fbcolor.o
a - fbsoft.o
a - bitmap.o
a - logo.o
a - mpadfb.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/video'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o rs5c372.o rs5c372.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o rx8025.o rx8025.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o s3c24x0_rtc.o s3c24x0_rtc.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o x1205.o x1205.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv librtc.a bfin_rtc.o date.o ds12887.o ds1302.o ds1306.o ds1307.o ds1337.o ds1374.o ds1556.o ds164x.o ds174x.o ds3231.o m41t11.o m41t60.o m48t35ax.o max6900.o mc146818.o mcfrtc.o mk48t59.o mpc5xxx.o mpc8xx.o pcf8563.o rs5c372.o rx8025.o s3c24x0_rtc.o x1205.o
a - bfin_rtc.o
a - date.o
a - ds12887.o
a - ds1302.o
a - ds1306.o
a - ds1307.o
a - ds1337.o
a - ds1374.o
a - ds1556.o
a - ds164x.o
a - ds174x.o
a - ds3231.o
a - m41t11.o
a - m41t60.o
a - m48t35ax.o
a - max6900.o
a - mc146818.o
a - mcfrtc.o
a - mk48t59.o
a - mpc5xxx.o
a - mpc8xx.o
a - pcf8563.o
a - rs5c372.o
a - rx8025.o
a - s3c24x0_rtc.o
a - x1205.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/drivers/rtc'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o main.o main.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ACEX1K.o ACEX1K.c
main.c:1075: warning: 'delete_char' defined but not used
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o altera.o altera.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o bedbug.o bedbug.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o circbuf.o circbuf.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_autoscript.o cmd_autoscript.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_bdinfo.o cmd_bdinfo.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o image.o image.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o gunzip.o gunzip.c
image.c: In function 'genimg_get_format':
image.c:643: warning: format '%x' expects type 'unsigned int', but argument 2 has type 'ulong'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_boot.o cmd_boot.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_bootm.o cmd_bootm.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_cache.o cmd_cache.c
cmd_bootm.c: In function 'do_bootm':/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_console.o cmd_console.c

cmd_bootm.c:211: warning: implicit declaration of function 'virt_to_phy_smdkc110'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_date.o cmd_date.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_eeprom.o cmd_eeprom.c
cmd_bootm.c:137: warning: 'hdr' may be used uninitialized in this function
cmd_bootm.c:139: warning: 'iflag' may be used uninitialized in this function
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_elf.o cmd_elf.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_ext2.o cmd_ext2.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_fat.o cmd_fat.c
cmd_ext2.c: In function 'do_ext3_format':/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_fdc.o cmd_fdc.c

cmd_ext2.c:316: warning: control reaches end of non-void function
cmd_ext2.c: In function 'do_ext2_format':
cmd_ext2.c:304: warning: control reaches end of non-void function
In file included from cmd_fat.c:37:
/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include/fat.h:195: warning: function declaration isn't a prototype
cmd_fat.c: In function 'do_fat_fsload':/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_flash.o cmd_flash.c

cmd_fat.c:59: warning: implicit declaration of function 'find_mmc_device'
cmd_fat.c:59: warning: assignment makes pointer from integer without a cast
cmd_fat.c:60: warning: implicit declaration of function 'mmc_init'
cmd_fat.c: In function 'do_fat_fsinfo':
cmd_fat.c:189: warning: too many arguments for format
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_itest.o cmd_itest.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_load.o cmd_load.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_mem.o cmd_mem.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_misc.o cmd_misc.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_mmc.o cmd_mmc.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_nand.o cmd_nand.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_net.o cmd_net.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_nvedit.o cmd_nvedit.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_pcmcia.o cmd_pcmcia.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_reginfo.o cmd_reginfo.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_usbd.o cmd_usbd.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_ximg.o cmd_ximg.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_vfd.o cmd_vfd.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o command.o command.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o console.o console.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cyclon2.o cyclon2.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o stratixII.o stratixII.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o devices.o devices.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o dlmalloc.o dlmalloc.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o docecc.o docecc.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc  -D__ASSEMBLY__ -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wa,--no-warn \
        -DENV_CRC=0 \
        -c -o environment.o environment.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o env_common.o env_common.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o env_dataflash.o env_dataflash.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o env_flash.o env_flash.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o env_eeprom.o env_eeprom.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o env_sf.o env_sf.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o env_nvram.o env_nvram.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o env_nowhere.o env_nowhere.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o exports.o exports.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o flash.o flash.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o fpga.o fpga.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o hush.o hush.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o kgdb.o kgdb.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o lcd.o lcd.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o lists.o lists.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o lynxkdi.o lynxkdi.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o memsize.o memsize.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o miiphybb.o miiphybb.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o miiphyutil.o miiphyutil.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o s_record.o s_record.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o serial.o serial.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o soft_i2c.o soft_i2c.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o soft_spi.o soft_spi.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o spartan2.o spartan2.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o spartan3.o spartan3.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o usb.o usb.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o usb_kbd.o usb_kbd.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o usb_storage.o usb_storage.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o virtex2.o virtex2.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o xilinx.o xilinx.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o crc16.o crc16.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o xyzModem.o xyzModem.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_mac.o cmd_mac.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_movi.o cmd_movi.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_android.o cmd_android.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_fastboot.o cmd_fastboot.c
cmd_movi.c: In function 'do_movi':
cmd_movi.c:732: warning: implicit declaration of function 'movi_write_bl1'
cmd_movi.c: In function 'init_raw_area_table':
cmd_movi.c:606: warning: control reaches end of non-void function
xyzModem.c: In function 'xyzModem_stream_open':
xyzModem.c:564: warning: 'dummy' is used uninitialized in this function
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_mmc_fdisk.o cmd_mmc_fdisk.c
cmd_fastboot.c: In function 'rx_handler':
cmd_fastboot.c:789: warning: unused variable 'length'
cmd_fastboot.c:789: warning: unused variable 'start'
cmd_fastboot.c:1007: warning: unused variable 'hdr'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o ace_sha1.o ace_sha1.c
cmd_fastboot.c: At top level:
cmd_fastboot.c:1369: warning: function declaration isn't a prototype
In file included from cmd_fastboot.c:1595:
/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include/fat.h:195: warning: function declaration isn't a prototype
cmd_fastboot.c: In function 'update_from_sd':
cmd_fastboot.c:1645: warning: format '%08x' expects type 'unsigned int', but argument 3 has type 'long int'
cmd_fastboot.c:1664: warning: pointer targets in passing argument 1 of 'rx_handler' differ in signedness
cmd_fastboot.c:629: note: expected 'const unsigned char *' but argument is of type 'char *'
cmd_fastboot.c: In function 'update_all':
cmd_fastboot.c:1789: warning: 'return' with a value, in function returning void
cmd_fastboot.c: At top level:
cmd_fastboot.c:1182: warning: 'add_partition_from_environment' defined but not used
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o secure_boot.o secure_boot.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_hello.o cmd_hello.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o cmd_led.o cmd_led.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o env_auto.o env_auto.c
env_auto.c: In function 'saveenv_movinand':
env_auto.c:281: warning: implicit declaration of function 'movi_write_env'
env_auto.c:281: warning: implicit declaration of function 'virt_to_phy_smdkc110'
env_auto.c: In function 'saveenv_nor':
env_auto.c:416: warning: pointer targets in passing argument 1 of 'flash_write' differ in signedness
/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include/flash.h:101: note: expected 'char *' but argument is of type 'u8 *'
env_auto.c: In function 'env_relocate_spec_nor':
env_auto.c:497: warning: assignment from incompatible pointer type
env_auto.c:489: warning: unused variable 'i'
env_auto.c:489: warning: unused variable 'ret'
env_auto.c: In function 'env_relocate_spec_movinand':
env_auto.c:511: warning: implicit declaration of function 'movi_read_env'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libcommon.a main.o ACEX1K.o altera.o bedbug.o circbuf.o cmd_autoscript.o cmd_bdinfo.o image.o gunzip.o cmd_boot.o cmd_bootm.o cmd_cache.o cmd_console.o cmd_date.o cmd_eeprom.o cmd_elf.o cmd_ext2.o cmd_fat.o cmd_fdc.o cmd_flash.o cmd_itest.o cmd_load.o cmd_mem.o cmd_misc.o cmd_mmc.o cmd_nand.o cmd_net.o cmd_nvedit.o cmd_pcmcia.o cmd_reginfo.o cmd_usbd.o cmd_ximg.o cmd_vfd.o command.o console.o cyclon2.o stratixII.o devices.o dlmalloc.o docecc.o environment.o env_common.o env_dataflash.o env_flash.o env_eeprom.o env_sf.o env_nvram.o env_nowhere.o exports.o flash.o fpga.o hush.o kgdb.o lcd.o lists.o lynxkdi.o memsize.o miiphybb.o miiphyutil.o s_record.o serial.o soft_i2c.o soft_spi.o spartan2.o spartan3.o usb.o usb_kbd.o usb_storage.o virtex2.o xilinx.o crc16.o xyzModem.o cmd_mac.o cmd_movi.o cmd_android.o cmd_fastboot.o cmd_mmc_fdisk.o ace_sha1.o secure_boot.o cmd_hello.o cmd_led.o env_auto.o
a - main.o
a - ACEX1K.o
a - altera.o
a - bedbug.o
a - circbuf.o
a - cmd_autoscript.o
a - cmd_bdinfo.o
a - image.o
a - gunzip.o
a - cmd_boot.o
a - cmd_bootm.o
a - cmd_cache.o
a - cmd_console.o
a - cmd_date.o
a - cmd_eeprom.o
a - cmd_elf.o
a - cmd_ext2.o
a - cmd_fat.o
a - cmd_fdc.o
a - cmd_flash.o
a - cmd_itest.o
a - cmd_load.o
a - cmd_mem.o
a - cmd_misc.o
a - cmd_mmc.o
a - cmd_nand.o
a - cmd_net.o
a - cmd_nvedit.o
a - cmd_pcmcia.o
a - cmd_reginfo.o
a - cmd_usbd.o
a - cmd_ximg.o
a - cmd_vfd.o
a - command.o
a - console.o
a - cyclon2.o
a - stratixII.o
a - devices.o
a - dlmalloc.o
a - docecc.o
a - environment.o
a - env_common.o
a - env_dataflash.o
a - env_flash.o
a - env_eeprom.o
a - env_sf.o
a - env_nvram.o
a - env_nowhere.o
a - exports.o
a - flash.o
a - fpga.o
a - hush.o
a - kgdb.o
a - lcd.o
a - lists.o
a - lynxkdi.o
a - memsize.o
a - miiphybb.o
a - miiphyutil.o
a - s_record.o
a - serial.o
a - soft_i2c.o
a - soft_spi.o
a - spartan2.o
a - spartan3.o
a - usb.o
a - usb_kbd.o
a - usb_storage.o
a - virtex2.o
a - xilinx.o
a - crc16.o
a - xyzModem.o
a - cmd_mac.o
a - cmd_movi.o
a - cmd_android.o
a - cmd_fastboot.o
a - cmd_mmc_fdisk.o
a - ace_sha1.o
a - secure_boot.o
a - cmd_hello.o
a - cmd_led.o
a - env_auto.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/common'
make -C board/samsung/x210/
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/board/samsung/x210'
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o x210.o x210.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -Wall -Wstrict-prototypes -fno-stack-protector -c -o flash.o flash.c
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-gcc  -D__ASSEMBLY__ -g  -Os   -fno-strict-aliasing  -fno-common -ffixed-r8 -msoft-float  -D__KERNEL__ -DTEXT_BASE=0xc3e00000 -I/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/include -fno-builtin -ffreestanding -nostdinc -isystem /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1/include -pipe  -DCONFIG_ARM -D__ARM__ -march=armv5te -mabi=apcs-gnu -mno-thumb-interwork -c -o lowlevel_init.o lowlevel_init.S
flash.c: In function 'flash_erase':
flash.c:215: warning: unused variable 'cflag'
flash.c:215: warning: unused variable 'iflag'
flash.c: In function 'flash_erase_all':
flash.c:342: warning: unused variable 'chip'
flash.c:340: warning: unused variable 'sect'
flash.c:340: warning: unused variable 'prot'
flash.c:340: warning: unused variable 'cflag'
flash.c:340: warning: unused variable 'iflag'x210.c: In function 'virt_to_phy_smdkc110':
x210.c:193: warning: multi-line comment

flash.c:339: warning: unused variable 'result'
flash.c: In function 'read_hword':
flash.c:592: warning: initialization makes pointer from integer without a cast
flash.c:593: warning: initialization makes pointer from integer without a cast
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ar crv libx210.a lowlevel_init.o x210.o flash.o
a - lowlevel_init.o
a - x210.o
a - flash.o
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/board/samsung/x210'
UNDEF_SYM=`/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-objdump -x board/samsung/x210/libx210.a lib_generic/libgeneric.a cpu/s5pc11x/libs5pc11x.a cpu/s5pc11x/s5pc110/libs5pc110.a lib_arm/libarm.a fs/cramfs/libcramfs.a fs/fat/libfat.a fs/fdos/libfdos.a fs/jffs2/libjffs2.a fs/reiserfs/libreiserfs.a fs/ext2/libext2fs.a net/libnet.a disk/libdisk.a drivers/bios_emulator/libatibiosemu.a drivers/block/libblock.a drivers/dma/libdma.a drivers/hwmon/libhwmon.a drivers/i2c/libi2c.a drivers/input/libinput.a drivers/misc/libmisc.a drivers/mmc/libmmc.a drivers/mtd/libmtd.a drivers/mtd/nand/libnand.a drivers/mtd/nand_legacy/libnand_legacy.a drivers/mtd/onenand/libonenand.a drivers/mtd/ubi/libubi.a drivers/mtd/spi/libspi_flash.a drivers/net/libnet.a drivers/net/sk98lin/libsk98lin.a drivers/pci/libpci.a drivers/pcmcia/libpcmcia.a drivers/spi/libspi.a drivers/rtc/librtc.a drivers/serial/libserial.a drivers/usb/libusb.a drivers/video/libvideo.a common/libcommon.a libfdt/libfdt.a api/libapi.a post/libpost.a | \
        sed  -n -e 's/.*\(__u_boot_cmd_.*\)/-u\1/p'|sort|uniq`;\
        cd /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot && /usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-ld -Bstatic -T /home/fly/project/x210bv3s/qt_x210v3s_160307/uboot/board/samsung/x210/u-boot.lds  -Ttext 0xc3e00000 $UNDEF_SYM cpu/s5pc11x/start.o \
                --start-group lib_generic/libgeneric.a cpu/s5pc11x/libs5pc11x.a cpu/s5pc11x/s5pc110/libs5pc110.a lib_arm/libarm.a fs/cramfs/libcramfs.a fs/fat/libfat.a fs/fdos/libfdos.a fs/jffs2/libjffs2.a fs/reiserfs/libreiserfs.a fs/ext2/libext2fs.a net/libnet.a disk/libdisk.a drivers/bios_emulator/libatibiosemu.a drivers/block/libblock.a drivers/dma/libdma.a drivers/hwmon/libhwmon.a drivers/i2c/libi2c.a drivers/input/libinput.a drivers/misc/libmisc.a drivers/mmc/libmmc.a drivers/mtd/libmtd.a drivers/mtd/nand/libnand.a drivers/mtd/nand_legacy/libnand_legacy.a drivers/mtd/onenand/libonenand.a drivers/mtd/ubi/libubi.a drivers/mtd/spi/libspi_flash.a drivers/net/libnet.a drivers/net/sk98lin/libsk98lin.a drivers/pci/libpci.a drivers/pcmcia/libpcmcia.a drivers/spi/libspi.a drivers/rtc/librtc.a drivers/serial/libserial.a drivers/usb/libusb.a drivers/video/libvideo.a common/libcommon.a libfdt/libfdt.a api/libapi.a post/libpost.a board/samsung/x210/libx210.a --end-group -L /usr/local/arm/arm-2009q3/bin/../lib/gcc/arm-none-linux-gnueabi/4.4.1 -lgcc \
                -Map u-boot.map -o u-boot
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-objcopy --gap-fill=0xff -O srec u-boot u-boot.srec
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-objcopy --gap-fill=0xff -O binary u-boot u-boot.bin
/usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-objdump -d u-boot > u-boot.dis

```

### 2-3.为uboot添加校验头

> `fly@fly-vm:uboot$ ../../s5pv210-noos-dev/tools/mk_image/mkv210 u-boot.bin`

```bash
fly@fly-vm:uboot$ ls
api                            COPYING      include      mkmovi       System.map  UBOOT.IMD
api_examples                   cpu          lib_arm      nand_spl     test.mk     u-boot.map
arm_config.mk                  CREDITS      libfdt       net          tools       UBOOT.PFI
board                          disk         lib_generic  onenand_bl1  u-boot      UBOOT.PO
CHANGELOG                      doc          MAINTAINERS  onenand_ipl  u-boot.bin  UBOOT.PR
CHANGELOG-before-U-Boot-1.1.5  drivers      MAKEALL      post         u-boot.dis  UBOOT.PRI
Changelog_Samsung              examples     Makefile     README       UBOOT.IAB   UBOOT.PS
common                         fs           mk           rules.mk     UBOOT.IAD   u-boot.srec
config.mk                      image_split  mkconfig     sd_fusing    UBOOT.IMB
fly@fly-vm:uboot$ cp u-boot.
u-boot.bin   u-boot.dis   u-boot.map   u-boot.srec
fly@fly-vm:uboot$ cp u-boot.bin u-boot.old.bin
fly@fly-vm:uboot$ ../../s5pv210-noos-dev/tools/mk_image/mkv210 u-boot.bin
fileLen:393216Bytes
checkSum:0x000ed555 for 8176 bytes.
fly@fly-vm:uboot$ ls -l u-boot.*
-rwxrwxr-x 1 fly fly  393216 11月 12 18:15 u-boot.bin
-rw-rw-r-- 1 fly fly 1828423 11月 12 18:13 u-boot.dis
-rw-rw-r-- 1 fly fly  189650 11月 12 18:13 u-boot.map
-rwxrwxr-x 1 fly fly  393216 11月 12 18:15 u-boot.old.bin
-rwxrwxr-x 1 fly fly 1179730 11月 12 18:13 u-boot.srec
```

------------------

![uboot-01](.\pic\uboot-01.png)

## 3.启动uboot

> 将添加校验后的`bin`文件使用刷机工具烧录到`SD`卡，然后将`SD`卡插入机器，通电，长按机器电源键，启动机器，`uboot`运行；

```bash
SD checksum Error
OK

U-Boot 1.3.4 (Nov 10 2022 - 22:54:35) for x210@Flyer


CPU:  S5PV210@1000MHz(OK)
        APLL = 1000MHz, HclkMsys = 200MHz, PclkMsys = 100MHz
        MPLL = 667MHz, EPLL = 96MHz
                       HclkDsys = 166MHz, PclkDsys = 83MHz
                       HclkPsys = 133MHz, PclkPsys = 66MHz
                       SCLKA2M  = 200MHz
Serial = CLKUART
Board:   X210BV3S
DRAM:    512 MB
Flash:   8 MB
SD/MMC:  3728MB
In:      serial
Out:     serial
Err:     serial
[LEFT UP] boot mode
checking mode for fastboot ...
Hit any key to stop autoboot:  0
x210 #
x210 #
x210 # ?
?       - alias for 'help'
autoscr - run script from memory
base     - print or set address offset
bdinfo  - print Board Info structure
boot    - boot default, i.e., run 'bootcmd'
bootd   - boot default, i.e., run 'bootcmd'
bootelf - Boot from an ELF image in memory
bootm   - boot application image from memory
bootp   - boot image via network using BootP/TFTP protocol
bootvx  - Boot vxWorks from an ELF image
cmp      - memory compare
coninfo - print console devices and information
cp       - memory copy
crc32    - checksum calculation
dcache  - enable or disable data cache
dhcp    - invoke DHCP client to obtain IP/boot params
dnw     - initialize USB device and ready to receive for Windows server (specific)
echo    - echo args to console
erase   - erase FLASH memory
exit    - exit script
ext2format - disk format by ext2
ext2load- load binary file from a Ext2 filesystem
ext2ls  - list files in a directory (default /)
ext3format - disk format by ext3
fastboot- use USB Fastboot protocol
fatformat - disk format by FAT32
fatinfo - print information about filesystem
fatload - load binary file from a dos filesystem
fatls   - list files in a directory (default /)
fdisk   - fdisk for sd/mmc.
flinfo  - print FLASH memory information
go      - start application at address 'addr'
hello  - test
help    - print online help
icache  - enable or disable instruction cache
iminfo  - print header information for application image
imls    - list all images found in flash
imxtract- extract a part of a multi-image
itest   - return true/false on integer compare
led  - test for led
loadb   - load binary file over serial line (kermit mode)
loads   - load S-Record file over serial line
loady   - load binary file over serial line (ymodem mode)
loop     - infinite loop on address range
md       - memory display
mm       - memory modify (auto-incrementing)
MMC sub systemprint MMC informationmovi - sd/mmc r/w sub system for SMDK board
mtest    - simple RAM test
mw       - memory write (fill)
nfs     - boot image via network using NFS protocol
nm       - memory modify (constant address)
ping    - send ICMP ECHO_REQUEST to network host
printenv- print environment variables
protect - enable or disable FLASH write protection
rarpboot- boot image via network using RARP/TFTP protocol
reset   - Perform RESET of the CPU
reginfo - print register information
reset   - Perform RESET of the CPU
run     - run commands in an environment variable
saveenv - save environment variables to persistent storage
sdfuse  - read images from FAT partition of SD card and write them to booting device.
setenv  - set environment variables
sleep   - delay execution for some time
test    - minimal test like /bin/sh
tftpboot- boot image via network using TFTP protocol
version - print monitor version
x210 # hello
Hello, This is my U-Boot command test. @fly
x210 # version

U-Boot 1.3.4 (Nov 10 2022 - 22:54:35) for x210@Flyer
x210 # printenv
baudrate=115200
ethact=dm9000
ethaddr=00:40:5c:26:0a:5b
mtdpart=80000 400000 3000000
bootdelay=2
filesize=60000
fileaddr=C0008000
netmask=255.255.255.0
gatewayip=192.168.199.1
serverip=192.168.199.179
ipaddr=192.168.199.188
bootcmd=tftp 30008000 zImage;bootm 30008000
bootargs=root=/dev/nfs nfsroot=192.168.199.179:/opt/rootfs ip=192.168.199.188:192.168.199.179:192.168.199.1:255.255.255.0::eth0:off  init=/linuxrc console=ttySAC2,115200

Environment size: 438/16380 bytes
x210 #
```

## 4.修改uboot

### 4-1.uboot功能配置头文件

```bash
\\192.168.30.128\project\x210bv3s\qt_x210v3s_160307\uboot\include\configs\x210_sd.h
```

### 4-2.uboot开机logo修改

```bash
\\192.168.30.128\project\x210bv3s\qt_x210v3s_160307\uboot\drivers\video\logo.c
```

----------

```c
/*
 * the gimp's c source format picture
 */
struct picture
{
	/* width of the picture*/
	u32 width;

	/* height of the picture*/
	u32 height;

	/* bytes per pixel */
	u32 bytes_per_pixel;

	/* the pixel data */
	u8 * data;
};
```

-------------

> 通过此处打印：`bitmap_load_from_picture()[193]:W = 250, H = 186, 3bytes`;
>
> 推知`LOGO`数据为，替换`LOGO`，就是替换下面的数据：
>
> > static const struct picture default_xboot_logo = {
> > 	250, 186, 3, (u8 *)
> >
> > // ...省略...
> >
> > };

---------

#### 4-2-1.准备图片

> 准备了一张`200*100`的`BMP`图片（不宜太大，太大升级回报“`SD Check ERR`”，暂未研究为什么会这样），位深度`24`位（像素格式`RGB888`）；

#### 4-2-2.导出图片数据

> 使用工具`Image2Lcd v2.9`导出图片数据到bin文件；

![UBOOT-BMP-01](.\pic\UBOOT-BMP-01.png)

----

#### 4-2-3.将bin数据转换成字符串

```c
/*******************************************************************
 *   > File Name: printByte.c
 *   > Create Time: 2022年11月13日 星期日 01时54分16秒
 ******************************************************************/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
    FILE *fp;

    if(argc != 2){
        printf("Usage: %s file\n", argv[0]);exit(-1);
    }

    if((fp = fopen(argv[1], "r")) == NULL){
        printf("fail to fopen: %s\n", strerror(errno));
        return (-1);
    }

    int c;
    int i = 1;
    printf("\"");
    while((c = fgetc(fp)) != EOF)
    {
        //printf("%02X ", c);
        printf("\\%o", c);
        if((i != 0)&&(i % (0xf + 1 )== 0)){
            printf("\"\n\"");
        }
        i ++;

    }
    printf("\"\n");

    fclose(fp);

    return 0;
}
```

-----------

## 5.参考

> 1.[(1条消息) U-Boot 顶层 Makefile 详解_River-D的博客-CSDN博客](https://blog.csdn.net/liurunjiang/article/details/107386600)
>
> 2.[Linux 命令大全 | 菜鸟教程 (runoob.com)](https://www.runoob.com/linux/linux-command-manual.html)
>
> 3.[linux开发 | uboot环境变量及命令使用_1234567890@world的博客-CSDN博客](https://blog.csdn.net/I_feige/article/details/119361839?ops_request_misc=&request_id=c434954d633f4ecdb4bc0a322d49ca7b&biz_id=&utm_medium=distribute.pc_search_result.none-task-blog-2~blog~koosearch~default-1-119361839-null-null.268^v1^control&utm_term=uboot&spm=1018.2226.3001.4450)
>
> 4.[ Linux开发 | uboot中添加命令之helloworld_uboot添加命令hello_1234567890@world的博客-CSDN博客](https://blog.csdn.net/I_feige/article/details/119090052?ops_request_misc=&request_id=c434954d633f4ecdb4bc0a322d49ca7b&biz_id=&utm_medium=distribute.pc_search_result.none-task-blog-2~blog~koosearch~default-3-119090052-null-null.268^v1^control&utm_term=uboot&spm=1018.2226.3001.4450)
>
> 5.[课件/2.uboot和linux内核移植 · x210bv3s/v210-note_code - 码云 - 开源中国 (gitee.com)](https://gitee.com/x210bv3s/v210-note_code/tree/master/课件/2.uboot和linux内核移植)
>
> 6.[u-boot-2022.01-rc4: u-boot-2022.01-rc4 (gitee.com)](https://gitee.com/x210bv3s/u-boot-2022.01-rc4)
>
> 7.[u-boot-2013.10: 移植u-boot-2013.10到S5PV210微处理器上，用于启动x210bv3s开发板 - Gitee.com](https://gitee.com/x210bv3s/u-boot-2013.10/tree/s5pv210/)

