## S5PV210 | 异步串行通信实例（UART printf实现）

-----------------

[TOC]



- ### 电路连接

![HW-UART](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/UART/HW-UART.PNG)

------

![HW-UART](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/UART/HW-UART0-2.PNG)

> `s5pv210（Cortex A8）`处理器集成了串口控制功能。为了实现**RS-232C**标准的串口通信功能，需要连接一个**SP3232E**电压转换芯片及一个**DB9**接口。

> 图中：
>
> ​			**UART0**：RX和TX对应的**GPA0_0**、**GPA0_1**；    
> ​			**UART1**：RX和TX对应的**GPA1_0**、**GPA1_1**  。 

​			

- ### 资料查阅

Datasheet关于串口部分资料详见芯片手册如下章节：   
<u>Section 8 CONNECTIVITY/STORAGE-》1 UNIVERSAL ASYNCHRONOUS RECEIVER AND TRANSMITTER</u>

- #### 1 通用异步收发器

- #### 1.1 通用异步收发器概述

S5PV210 中的通用异步收发器 (UART) 提供四个独立的异步和串行输入/输出 (I/O) 端口。所有端口均以基于中断或基于 DMA 的模式运行。 UART 生成中断或 DMA 请求，以在 CPU 和 UART 之间传输数据。UART 支持高达 3Mbps 的比特率。每个 UART 通道包含两个 FIFO 来接收和发送数据：ch0 中的 256 个字节，ch1 中的 64 个字节和 ch2 和 ch3 中的 16 个字节。

UART 包括可编程波特率、红外 (IR) 发送器/接收器、一或两个停止位插入、5 位、6 位、7 位或 8 位数据宽度和奇偶校验。

每个 UART 包含一个波特率发生器、一个发送器、一个接收器和一个控制单元，如图 1-1 所示。波特率发生器使用 PCLK 或 SCLK_UART。发送器和接收器包含 FIFO 和数据移位器。要发送的数据写入 Tx FIFO，并复制到发送移位器。然后数据由发送数据引脚 (TxDn) 移出。接收到的数据从接收数据引脚 (RxDn) 移位，并从移位器复制到 Rx FIFO。

- #### 1.2 通用异步收发器的主要特性

• RxD0、TxD0、RxD1、TxD1、RxD2、TxD2、RxD3 和 TxD3，具有基于 DMA 或基于中断的操作  
• 带有 IrDA 1.0 的 UART Ch 0、1、2 和 3   
• UART Ch 0 具有 256 字节 FIFO，Ch 1 具有 64 字节 FIFO，Ch2 和 3 具有 16 字节 FIFO   
• UART Ch 0、1 和 2，带有 nRTS0、nCTS0、nRTS1、nCTS1、nCTS2 和 nRTS2，用于自动流量控制  
• 支持握手发送/接收   

<img src="https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/UART/HW-UART_BLOCK_DIAGRAM.PNG" alt="HW-UART_BLOCK_DIAGRAM" style="zoom: 67%;" />

- #### 1.3串口描述

以下部分描述了 UART 操作，例如数据发送、数据接收、中断产生、波特率产生、环回模式、红外模式和自动流量控制。

- ##### 1.3.1 数据传输

用于传输的数据帧是可编程的。 它由一个起始位、五到八个数据位、一个可选的奇偶校验位和一到两个停止位组成，由线路控制寄存器 (ULCONn) 指定。 发送器还可以产生中断条件，强制串行输出在一帧传输时间内变为逻辑 0 状态。 该块在当前传输字完全传输后传输中断信号。 发送中断信号后，发送器继续向 Tx FIFO（Tx 保持寄存器，在 Non-FIFO 模式下）发送数据。

- ##### 1.3.2 数据接收

与数据传输类似，用于接收的数据帧也是可编程的。 它由一个起始位、五到八个数据位、一个可选的奇偶校验位和线路控制寄存器 (ULCONn) 中的一到两个停止位组成。 接收器检测溢出错误、奇偶校验错误、帧错误和中断条件，每一个都设置一个错误标志。

• 溢出错误表示在读取旧数据之前新数据已覆盖旧数据。  
• 奇偶校验错误表示接收器检测到意外的奇偶校验条件。   
• 帧错误表示接收到的数据没有有效的停止位。   
• 中断条件表示RxDn 输入保持逻辑0 状态超过一帧传输时间。   

如果在 3 字时间内（此间隔跟随字长位的设置）没有接收到数据，并且在 FIFO 模式下 Rx FIFO 不为空，则会发生接收超时条件。

- ##### 1.3.3 自动流量控制（AFC）

S5PV210 中的 UART0 和 UART1 支持使用 nRTS 和 nCTS 信号的自动流控制 (AFC)。 如果 GPA1CON(GPIO SFR) 将 TxD3 和 RxD3 设置为 nRTS2 和 nCTS2，则 UART2 支持自动流控。 在这种情况下，它可以连接到外部 UART。 要将 UART 连接到调制解调器，请禁用 UMCONn 寄存器中的 AFC 位并使用软件控制 nRTS 的信号。
在 AFC 中，nRTS 信号取决于接收器的状态，而 nCTS 信号控制发射器的操作。 如果 nCTS 信号被激活，UART 的发送器将数据传输到 FIFO（在 AFC 中，nCTS 信号意味着其他 UART 的 FIFO 已准备好接收数据）。 在 UART 接收数据之前，如果其接收 FIFO 有 2 字节以上的空闲空间，则必须激活 nRTS 信号。 如果 nRTS 信号的接收 FIFO 少于 1 字节作为备用，则必须禁用 nRTS 信号（在 AFC 中，nRTS 信号意味着它自己的接收 FIFO 已准备好接收数据）。

![HW-UART_AFC_INTERFACE](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/UART/HW-UART_AFC_INTERFACE.PNG)

- ##### 1.3.4 非自动流量控制示例（通过软件控制 NRTS 和 NCTS）

- ###### 1.3.4.1 使用 FIFO 的 Rx 操作

1. 选择传输模式（中断或 DMA 模式）。     
2. 检查 UFSTATn 寄存器中 Rx FIFO 计数的值。 如果该值小于 16，则必须将 UMCONn[0] 的值设置为“1”（激活 nRTS）。 但是，如果该值等于或大于 16，则必须将该值设置为“0”（停用 nRTS）。    
3. 重复步骤 2。    

- ###### 1.3.4.2 使用 FIFO 的 Tx 操作

1. 选择传输模式（中断或 DMA 模式）。   
2. 检查 UMSTATn[0] 的值。 如果值为“1”（激活 nCTS），则必须将数据写入 Tx FIFO 寄存器。
3. 重复步骤 2

- ##### 1.3.5 DMA 模式下的 TX/RX FIFO 触发电平和 DMA 突发大小

如果 Tx/Rx 数据在 DMA 模式下达到 UFCONn 寄存器的 Tx/Rx FIFO 触发电平，则 DMA 事务开始。
单个 DMA 事务传输一个数据，其大小指定为 UCONn 寄存器的 DMA 突发大小，并且重复 DMA 事务，直到传输的数据大小达到 Tx/Rx FIFO 触发级别。 因此，DMA 突发大小应小于或等于 Tx/Rx FIFO 触发级别。 一般情况下，建议确保 Tx/Rx FIFO 触发电平和 DMA 突发大小匹配。

- ##### 1.3.6 RS-232C 接口

要将 UART 连接到调制解调器接口（而不是空调制解调器），需要 nRTS、nCTS、nDSR、nDTR、DCD 和 nRI 信号。 由于 AFC 不支持 RS-232C 接口，因此您可以使用软件通过通用 I/O 端口控制这些信号。

- ##### 1.3.7 中断/DMA 请求生成

S5PV210 中的每个 UART 包含 7 个状态（Tx/Rx/Error）信号，即 Overrun error、Parity error、Frame error、Break、Receive buffer data ready、Transmit buffer empty 和 Transmit shifter empty。这些条件由相应的 UART 状态寄存器 (UTRSTATn/UERSTATn) 指示。

溢出错误、奇偶校验错误、帧错误和中断条件指定接收错误状态。如果控制寄存器 (UCONn) 中的接收错误状态中断使能位设置为 1，则接收错误状态会产生接收错误状态中断。如果检测到接收错误状态中断请求，您可以通过读取 UERSTATn 的值来识别中断源。

如果接收器在 FIFO 模式下将接收移位器的数据传输到接收 FIFO 寄存器，并且接收到的数据数量大于或等于 Rx FIFO 触发电平，如果控制寄存器（UCONn）中的接收模式为接收模式，则产生 Rx 中断设置为 1（中断请求或轮询模式）。

在 Non-FIFO 模式下，在中断请求和轮询模式下，将接收移位器的数据传输到接收保持寄存器会导致 Rx 中断。

如果发送器将数据从其发送 FIFO 寄存器传输到发送移位器，并且发送 FIFO 中剩余的数据数小于或等于 Tx FIFO 触发电平，则产生 Tx 中断（前提是控制寄存器中的发送模式被选为中断请求）或轮询模式）。在非 FIFO 模式下，将数据从发送保持寄存器传输到发送移位器会导致中断请求和轮询模式下的 Tx 中断。

请记住，如果发送 FIFO 中的数据数量小于触发电平，则始终会请求 Tx 中断。这意味着一旦您启用 Tx 中断，就会请求中断，除非您填充 Tx 缓冲区。建议先填充 Tx 缓冲区，然后使能 Tx 中断。

S5PV210 的中断控制器为电平触发型。如果您对 UART 控制寄存器进行编程，则必须将中断类型设置为“级别”。

如果控制寄存器中的接收和发送模式被选择为 DMAn 请求模式，则在上述情况下发生 DMAn 请求而不是 Rx 或 Tx 中断。
表 1-1 与 FIFO 相关的中断

| 类型     | FIFO模式                                                     | 无FIFO模式                                                   |
| -------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 发送中断 | 如果 Rx FIFO 计数大于或等于接收 FIFO 的触发电平，则生成。<br/>如果 FIFO 中的数据数量未达到 Rx FIFO 触发级别并且在 3 字时间（接收超时）内未接收任何数据，则生成。 该间隔遵循字长位的设置。 | 每当接收缓冲区变满时由接收保持寄存器生成。                   |
| 接收中断 | 如果 Tx FIFO 计数小于或等于发送 FIFO 的触发电平（Tx FIFO 触发电平），则生成。 | 每当发送缓冲区变空时由发送保持寄存器生成。                   |
| 错误中断 | 在检测到帧错误、奇偶校验错误或中断信号时生成。<br/>当 Rx FIFO 已满（溢出错误）时 UART 接收到新数据时生成。 | 由所有错误生成。 但是，如果同时发生另一个错误，则只会产生一个中断。 |

- ##### 1.3.8 UART 错误状态 FIFO

除了 Rx FIFO 寄存器之外，UART 还包含错误状态 FIFO。 错误状态 FIFO 指示接收到的 FIFO 寄存器中的哪些数据有错误。 仅当包含错误的数据准备好读出时，才会发出错误中断。 要清除错误状态 FIFO，必须读出带有错误的 URXHn 和 UERSTATn。

例如，假设UART Rx FIFO 依次接收A、B、C、D 和E 字符，并且在接收“B”时发生帧错误，在接收“D”时发生奇偶校验错误。

实际的 UART 接收错误不会产生任何错误中断，因为没有读取接收到的错误字符。 如果字符被读出，就会发生错误中断。

| 时间 | 序列流            | 错误中断                    | 备注          |
| ---- | ----------------- | --------------------------- | ------------- |
| #0   | 如果没有读出字符  | -                           |               |
| #1   | 收到A、B、C、D、E | -                           |               |
| #2   | A 读出后          | 发生帧错误（在 B 中）中断。 | 必须读出“B”。 |
| #3   | B 读出后          | -                           |               |
| #4   | C 读出后          | 发生帧错误（在 D 中）中断。 | 必须读出“D”。 |
| #5   | D 读出后          | -                           |               |
| #6   | E 读出后          | -                           |               |

![HW-UART_ERR_STATUS_FIFO](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/UART/HW-UART_ERR_STATUS_FIFO.png)

- ##### 1.3.8.1 红外线 (IR) 模式

S5PV210 UART 模块支持红外线 (IR) 传输和接收。 它是通过设置 UART 线路控制寄存器 (ULCONn) 中的红外线模式位来选择的。 图 1-4 说明了如何实现 IR 模式。

在IR发射模式下，发射脉冲以3/16的速率出来，即正常的串行发射速率（如果发射数据位为0）。 但是，在 IR 接收模式下，接收器必须检测 3/16 脉冲周期才能识别 0 值（请参阅图 1-5 和图 1-7 中所示的帧时序图（省略））。

- ##### 1.4 UART 输入时钟图说明

![HW-INPUT_CLOCK_DIAGRAM_UART](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/UART/HW-INPUT_CLOCK_DIAGRAM_UART.PNG)

S5PV210 为UART 提供多种时钟。 如图 1-8 所示，UART 能够从 PCLK 或 SCLK_UART 中选择时钟，后者来自时钟控制器。 您还可以从 PLL 中选择 SCLK_UART。 SCLK_UART 的选择请参考2-3 节时钟控制器。

- ##### 1.5 IO描述

| 信号        | I/O  | 描述                          | 引脚      | 类型 |
| ----------- | ---- | ----------------------------- | --------- | ---- |
| UART_0_RXD  | 输入 | 接收UART0的数据               | XuRXD[0]  | 复用 |
| UART_0_TXD  | 输出 | 为UART0传输数据               | XuTXD[0]  | 复用 |
| UART_0_CTSn | 输入 | 清除发送给UART0（低电平有效） | XuCTSn[0] | 复用 |
| UART_0_RTSn | 输出 | 请求发送（低电平有效）UART0   | XuRTSn[0] | 复用 |
| UART_1_RXD  | 输入 | 接收UART1的数据               | XuRXD[1]  | 复用 |
| UART_1_TXD  | 输出 | 为UART1传输数据               | XuTXD[1]  | 复用 |
| UART_1_CTSn | 输入 | 清除UART1的发送（低电平有效） | XuCTSn[1] | 复用 |
| UART_1_RTSn | 输出 | 请求发送（低电平有效）UART1   | XuRTSn[1] | 复用 |
| UART_2_RXD  | 输入 | 接收UART2的数据               | XuRXD[2]  | 复用 |
| UART_2_TXD  | 输出 | 为UART2传输数据               | XuTXD[2]  | 复用 |
| UART_2_CTSn | 输入 | 清除UART2的发送（低电平有效） | XuRXD[3]  | 复用 |
| UART_2_RTSn | 输出 | 请求发送（低电平有效）UART2   | XuTXD[3]  | 复用 |
| UART_3_RXD  | 输入 | 接收UART3的数据               | XuRXD[3]  | 复用 |
| UART_3_TXD  | 输出 | 为UART3传输数据               | XuTXD[3]  | 复用 |

注意：“类型”字段指示焊盘是专用于信号还是焊盘已连接到多路复用信号。
UART外部焊盘与IrDA共享。 为了使用这些焊盘，必须在UART启动之前设置GPIO。

- ##### 1.6 寄存器描述（详见Datasheet第864页）

- ###### 1.6.1 REGISTER MAP（具体设置省略，详见Datasheet第867~882页）

| 寄存器    | 地址        | 读/写 | 描述                              | 复位值     |
| --------- | ----------- | ----- | --------------------------------- | ---------- |
| ULCON0    | 0xE290_0000 | R/W   | 指定UART通道0线路控制寄存器       | 0x00000000 |
| UCON0     | 0xE290_0004 | R/W   | 指定UART通道0控制寄存器           | 0x00000000 |
| UFCON0    | 0xE290_0008 | R/W   | 指定UART通道0 FIFO控制寄存器      | 0x00000000 |
| UMCON0    | 0xE290_000C | R/W   | 指定UART通道0调制解调器控制寄存器 | 0x00000000 |
| UTRSTAT0  | 0xE290_0010 | R     | 指定UART通道0 Tx / Rx状态寄存器   | 0x00000006 |
| UERSTAT0  | 0xE290_0014 | R     | 指定UART通道0 Rx错误状态寄存器    | 0x00000000 |
| UFSTAT0   | 0xE290_0018 | R     | 指定UART通道0 FIFO状态寄存器      | 0x00000000 |
| UMSTAT0   | 0xE290_001C | R     | 指定UART通道0调制解调器状态寄存器 | 0x00000000 |
| UTXH0     | 0xE290_0020 | W     | 指定UART通道0发送缓冲寄存器       | -          |
| URXH0     | 0xE290_0024 | R     | 指定UART通道0接收缓冲寄存器       | 0x00000000 |
| UBRDIV0   | 0xE290_0028 | R/W   | 指定UART通道0波特率除数寄存器     | 0x00000000 |
| UDIVSLOT0 | 0xE290_002C | R/W   | 指定UART通道0分频槽寄存器         | 0x00000000 |
| UINTP0    | 0xE290_0030 | R/W   | 指定UART通道0中断等待寄存器       | 0x00000000 |
| UINTSP0   | 0xE290_0034 | R/W   | 指定UART通道0中断源待处理寄存器   | 0x00000000 |
| UINTM0    | 0xE290_0038 | R/W   | 指定UART通道0中断屏蔽寄存器       | 0x00000000 |
| ULCON1    | 0xE290_0400 | R/W   | 指定UART通道1线路控制寄存器       | 0x00000000 |
| UCON1     | 0xE290_0404 | R/W   | 指定UART通道1控制寄存器           | 0x00000000 |
| UFCON1    | 0xE290_0408 | R/W   | 指定UART通道1 FIFO控制寄存器      | 0x00000000 |
| UMCON1    | 0xE290_040C | R/W   | 指定UART通道1调制解调器控制寄存器 | 0x00000000 |
| UTRSTAT1  | 0xE290_0410 | R     | 指定UART通道1 Tx / Rx状态寄存器   | 0x00000006 |
| UERSTAT1  | 0xE290_0414 | R     | 指定UART通道1 Rx错误状态          | 0x00000000 |
| UFSTAT1   | 0xE290_0418 | R     | 指定UART通道1 FIFO状态寄存器      | 0x00000000 |
| UMSTAT1   | 0xE290_041C | R     | 指定UART通道1调制解调器状态寄存器 | 0x00000000 |
| UTXH1     | 0xE290_0420 | W     | 指定UART通道1发送缓冲区寄存器     | -          |
| URXH1     | 0xE290_0424 | R     | 指定UART通道1接收缓冲寄存器       | 0x00000000 |
| UBRDIV1   | 0xE290_0428 | R/W   | 指定UART通道1波特率除数寄存器     | 0x00000000 |
| UDIVSLOT1 | 0xE290_042C | R/W   | 指定UART通道1分频插槽寄存器       | 0x00000000 |
| UINTP1    | 0xE290_0430 | R/W   | 指定UART通道1中断等待寄存器       | 0x00000000 |
| UINTSP1   | 0xE290_0434 | R/W   | 指定UART通道1中断源待处理寄存器   | 0x00000000 |
| UINTM1    | 0xE290_0438 | R/W   | 指定UART通道1中断屏蔽寄存器       | 0x00000000 |
| ULCON2    | 0xE290_0800 | R/W   | 指定UART通道2线路控制寄存器       | 0x00000000 |
| UCON2     | 0xE290_0804 | R/W   | 指定UART通道2控制寄存器           | 0x00000000 |
| UFCON2    | 0xE290_0808 | R/W   | 指定UART通道2 FIFO控制寄存器      | 0x00000000 |
| UMCON2    | 0xE290_080C | R/W   | 指定UART通道2调制解调器控制寄存器 | 0x00000000 |
| UTRSTAT2  | 0xE290_0810 | R     | 指定UART通道2 Tx / Rx状态寄存器   | 0x00000006 |
| UERSTAT2  | 0xE290_0814 | R     | 指定UART通道2 Rx错误状态寄存器    | 0x00000000 |
| UFSTAT2   | 0xE290_0818 | R     | 指定UART通道2 FIFO状态寄存器      | 0x00000000 |
| UMSTAT2   | 0xE290_081C | R     | 指定UART通道2调制解调器状态寄存器 | 0x00000000 |
| UTXH2     | 0xE290_0820 | W     | 指定UART通道2发送缓冲寄存器       | -          |
| URXH2     | 0xE290_0824 | R     | 指定UART通道2接收缓冲寄存器       | 0x00000000 |
| UBRDIV2   | 0xE290_0828 | R/W   | 指定UART通道2波特率除数寄存器     | 0x00000000 |
| UDIVSLOT2 | 0xE290_082C | R/W   | 指定UART通道2分频槽寄存器         | 0x00000000 |
| INTP2     | 0xE290_0830 | R/W   | 指定UART通道2中断等待寄存器       | 0x00000000 |
| UINTSP2   | 0xE290_0834 | R/W   | 指定UART通道2中断源待处理寄存器   | 0x00000000 |
| UINTM2    | 0xE290_0838 | R/W   | 指定UART通道2中断屏蔽寄存器       | 0x00000000 |
| ULCON3    | 0xE290_0C00 | R/W   | 指定UART通道3线路控制寄存器       | 0x00000000 |
| UCON3     | 0xE290_0C04 | R/W   | 指定UART通道3控制寄存器           | 0x00000000 |
| UFCON3    | 0xE290_0C08 | R/W   | 指定UART通道3 FIFO控制寄存器      | 0x00000000 |
| UMCON3    | 0xE290_0C0C | R/W   | 指定UART通道3调制解调器控制寄存器 | 0x00000000 |
| UTRSTAT3  | 0xE290_0C10 | R     | 指定UART通道3 Tx / Rx状态寄存器   | 0x00000006 |
| UERSTAT3  | 0xE290_0C14 | R     | 指定UART通道3 Rx错误状态寄存器    | 0x00000000 |
| UFSTAT3   | 0xE290_0C18 | R     | 指定UART通道3 FIFO状态寄存器      | 0x00000000 |
| UMSTAT3   | 0xE290_0C1C | R     | 指定UART通道3调制解调器状态寄存器 | 0x00000000 |
| UTXH3     | 0xE290_0C20 | W     | 指定UART通道3发送缓冲寄存器       | -          |
| URXH3     | 0xE290_0C24 | R     | 指定UART通道3接收缓冲寄存器       | 0x00000000 |
| UBRDIV3   | 0xE290_0C28 | R/W   | 指定UART通道3波特率除数寄存器     | 0x00000000 |
| UDIVSLOT3 | 0xE290_0C2C | R/W   | 指定UART通道3分频槽寄存器         | 0x00000000 |
| INTP3     | 0xE290_0C30 | R/W   | 指定UART通道3中断等待寄存器       | 0x00000000 |
| UINTSP3   | 0xE290_0C34 | R/W   | 指定UART通道3中断源待处理寄存器   | 0x00000000 |
| UINTM3    | 0xE290_0C38 | R/W   | 指定UART通道3中断屏蔽寄存器       | 0x00000000 |

------

- ### 代码示例

#### 源码工程文件详细（[UART实现printf函数打印](https://gitee.com/x210bv3s/s5pv210-noos-dev/tree/master/lessons-src/11-uart_print/source)）：

```shell
.
├── Makefile
├── source
│   ├── clock.c
│   ├── include
│   │   ├── include.h
│   │   ├── led.h
│   │   ├── libc
│   │   │   ├── ctype.h
│   │   │   ├── div64.h
│   │   │   ├── gcclib.h
│   │   │   ├── kernel.h
│   │   │   ├── printf.h
│   │   │   ├── stdio.h
│   │   │   ├── string.h
│   │   │   ├── system.h
│   │   │   ├── types.h
│   │   │   └── vsprintf.h
│   │   └── uart.h
│   ├── led.c
│   ├── lib
│   │   ├── ctype.c
│   │   ├── div64.S
│   │   ├── lib1funcs.S
│   │   ├── Makefile
│   │   ├── muldi3.c
│   │   ├── printf.c
│   │   ├── string.c
│   │   └── vsprintf.c
│   ├── link.lds
│   ├── main.c
│   ├── start.S
│   └── uart.c
└── test.mk

4 directories, 29 files
```

------

#### UART部分接口编写部分：

```c
/*****************************************************
 *   > File Name: uart.c
 *   > Author: fly
 *   > Create Time: 2021-07-19  1/29  16:16:44 +0800
 *==================================================*/
#include <uart.h>
#include <libc/stdio.h>

typedef struct{
    unsigned int GPA0CON;
    unsigned int GPA0DAT;
    unsigned int GPA0PUD;
    unsigned int GPA0DRV;
    unsigned int GPA0CONPDN;
    unsigned int GPA0PUDPDN;
}gpa0;
#define GPA0    (*(volatile gpa0*)0xE0200000)

typedef struct{
    unsigned int GPA1CON;
    unsigned int GPA1DAT;
    unsigned int GPA1PUD;
    unsigned int GPA1DRV;
    unsigned int GPA1CONPDN;
    unsigned int GPA1PUDPDN;
}gpa1;
#define GPA1    (*(volatile gpa1*)0xE0200020)

typedef struct{
    unsigned int ULCON0;
    unsigned int UCON0;
    unsigned int UFCON0;
    unsigned int UMCON0;
    unsigned int UTRSTAT0;
    unsigned int UERSTAT0;
    unsigned int UFSTAT0;
    unsigned int UMSTAT0;
    unsigned int UTXH0;
    unsigned int URXH0;
    unsigned int UBRDIV0;
    unsigned int UDIVSLOT0;
    unsigned int UINTP0;
    unsigned int UINTSP0;
    unsigned int UINTM0;
}uart0;
#define UART0   (*(volatile uart0*)0xE2900000)

typedef struct{
    unsigned int ULCON2;
    unsigned int UCON2;
    unsigned int UFCON2;
    unsigned int UMCON2;
    unsigned int UTRSTAT2;
    unsigned int UERSTAT2;
    unsigned int UFSTAT2;
    unsigned int UMSTAT2;
    unsigned int UTXH2;
    unsigned int URXH2;
    unsigned int UBRDIV2;
    unsigned int UDIVSLOT2;
    unsigned int INTP2;
    unsigned int UINTSP2;
    unsigned int UINTM2;
}uart2;
#define UART2   (*(volatile uart2*)0xE2900800)

void uart0_init(void)
{
    GPA0.GPA0CON &= ~(0xFF<<0); /* clear bit[7:0]  */
    GPA0.GPA0CON |= (0x22<<0);  /* enable GPA0_0/1 pin Uart Mode */
    UART0.ULCON0 = 0x3;         /*8 bit 1 stop No parity */
    UART0.UCON0 = 0x5;
    UART0.UMCON0 = 0;           /* Disable AFC */
    UART0.UFCON0 = 0;           /* Disable FIFO */
    UART0.UBRDIV0 = 35;         /* UBRDIV = (PCLK/(bpsx16)) - 1 */
    UART0.UDIVSLOT0 = 0x0888;   /* 精度 */
    printf("%s ok\n", __func__);
}

void uart0_putc(char c)
{
    while(!(UART0.UTRSTAT0 & (0x1<<1)));
    UART0.UTXH0 = c;
    if (c == '\n'){
        uart0_putc('\r');
    }
}

char uart0_getc(void)
{
    char data;
    while(!(UART0.UTRSTAT0 & (0x1<<0)));
    data = (UART0.URXH0 & 0x0f);

    if((data == '\n') || (data == '\r')){
        uart0_putc('\n');uart0_putc('\r');
    }else{
        uart0_putc(data);
    }

    return data;
}

void uart2_init(void)
{
    GPA1.GPA1CON = (GPA1.GPA1CON & ~(0xFF<<0)) | (0x22<<0);
    UART2.ULCON2 = 0x3;
    UART2.UCON2 = 0x5;
    UART2.UMCON2 = 0;
    UART2.UFCON2 = 0;
    UART2.UBRDIV2 = 35;
    UART2.UDIVSLOT2 = 0x0888;
    printf("%s ok\n", __func__);
}

void uart2_putc(char c)
{
    while(!(UART2.UTRSTAT2 & (0x1<<1)));
    UART2.UTXH2 = c;
    if (c == '\n'){
        uart2_putc('\r');
    }
}

char uart2_getc(void)
{
    char data;
    while(!(UART2.UTRSTAT2 & (0x1<<0)));
    data = (UART2.URXH2 & 0x0f);

    if((data == '\n') || (data == '\r')){
        uart2_putc('\n');uart2_putc('\r');
    }else{
        uart2_putc(data);
    }
    return data;
}

void putc(unsigned char c)
{
    uart0_putc(c);
}

unsigned char getc(void)
{
    return uart0_getc();
}

void uart0_puts(const char *pstr)
{
    while(*pstr != '\0'){
        uart0_putc(*pstr++);
    }
}

void uart0_gets(char *p)
{
    char data;

    while((data = uart0_getc()) != '\r'){
        if(data == '\b'){
            p--;
        }
        *p++ = data;
    }

    if(data == '\r')
        *p++ = '\n';

    *p = '\0';
}

static void delay(volatile unsigned int i)
{
    while(i--);
}

#define DELAY_COUNT     (90000000)
void tester_uart(void)
{
    uart0_init();
    uart2_init();

    static unsigned int a = 0;
    for(;;){
        printf("uart test , a = %d.\n", a);
        delay(DELAY_COUNT);
        a ++;
    }
}
```

#### 运行结果(`UART0`输出)：

```bash
uart0_init ok
uart2_init ok
uart test , a = 0.
uart test , a = 1.
uart test , a = 2.
uart test , a = 3.
uart test , a = 4.
uart test , a = 5.
uart test , a = 6.
uart test , a = 7.
uart test , a = 8.
uart test , a = 9.
uart test , a = 10.
```

