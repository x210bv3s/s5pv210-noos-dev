# `s5pv210` | 使用`BusyBox`制作根文件系统

----------

**时间**：`2023-06-30`

[TOC]



## 1.源码下载

> 项目主页：[`https://busybox.net/`](https://busybox.net/)
>
> 下载主页：[`https://busybox.net/downloads/`](https://busybox.net/downloads/)
>
> 我的项目：[busybox-1.35.0: busybox-1.35.0 制作s5pv210最小根文件系统 (gitee.com)](https://gitee.com/x210bv3s/busybox-1.35.0)
>
> 或者：[project / x210bv3s / busybox-1.35.0 · GitCode](https://gitcode.net/project1/x210bv3s/busybox-1.35.0)

### 1-1.下载代码

```bash
fly@fly-vm:x210bv3s$ wget https://busybox.net/downloads/busybox-1.35.0.tar.bz2
--2022-11-13 18:50:04--  https://busybox.net/downloads/busybox-1.35.0.tar.bz2
Resolving busybox.net (busybox.net)... 140.211.167.122
Connecting to busybox.net (busybox.net)|140.211.167.122|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 2480624 (2.4M) [application/x-bzip2]
Saving to: ‘busybox-1.35.0.tar.bz2’

busybox-1.35.0.tar.bz2    100%[====================================>]   2.37M  1.23MB/s    in 1.9s

2022-11-13 18:50:38 (1.23 MB/s) - ‘busybox-1.35.0.tar.bz2’ saved [2480624/2480624]

fly@fly-vm:x210bv3s$ ls busybox-1.35.0.tar.bz2
busybox-1.35.0.tar.bz2
```

### 1-2.最终git仓库

> `rootfs`代码仓库：[`git@gitee.com:x210bv3s/busybox-1.35.0.git`](https://gitee.com/x210bv3s/busybox-1.35.0)
>
> `uboot`及`kernel`代码仓库：[`git@gitee.com:x210bv3s/qt_x210v3s_160307.git`](https://gitee.com/x210bv3s/qt_x210v3s_160307)

## 2.工作环境

### 2-1.开发主机

 ```bash
 fly@fly-vm:x210bv3s$ uname -a
 Linux fly-vm 4.15.0-142-generic #146~16.04.1-Ubuntu SMP Tue Apr 13 09:27:15 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux
 ```

------------

### 2-2.交叉编译工具链

```makefile
CROSS_COMPILE      ?= /usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-
```

## 3.配置、编译

### 3-1.解压代码

```bash
fly@fly-vm:x210bv3s$ tar -jxvf busybox-1.35.0.tar.bz2
```

### 3-2.修改交叉编译工具链和架构

```bash
fly@fly-vm:x210bv3s$ cd busybox-1.35.0/
fly@fly-vm:busybox-1.35.0$ vim Makefile
```

--------------

> `CROSS_COMPILE      ?= /usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-`
>
> `ARCH ?= arm`

### 3-3.配置`busybox`

```bash
fly@fly-vm:busybox-1.35.0$ make menuconfig
```

------------

```bash
Busybox Settings--->
	Build Options--->
		[*]Build BusyBox as a static binary(no shared libs)
	
Busybox Library Tuning--->
	[*]vi-style line editing commands
	[*]Fancy shell prompts
	
Linux Module Utilities--->
	[ ]Simplified modutils
	[*]insmod
	[*]rmmod
	[*]lsmod
	[*]modprobe
	[*]depmod

Linux System Utilities--->[*]mdev
	[*]Support /etc/mdev.conf
	[*]Support subdirs/symlinks
	[*]Support regular expressions substitutions when renaming dev
	[*]Support command execution at device addition/removal
	[*]Support loading of firmwares
```

------------------

### 3-4.编译

```bash
fly@fly-vm:busybox-1.35.0$ make -j4
```

##### 3-4-1.编译报错1

```bash
fly@fly-vm:busybox-1.35.0$ make
  CC      networking/libiproute/ipaddress.o
networking/libiproute/ipaddress.c: In function 'print_addrinfo':
networking/libiproute/ipaddress.c:345: error: 'IFA_F_DADFAILED' undeclared (first use in this function)
networking/libiproute/ipaddress.c:345: error: (Each undeclared identifier is reported only once
networking/libiproute/ipaddress.c:345: error: for each function it appears in.)
scripts/Makefile.build:197: recipe for target 'networking/libiproute/ipaddress.o' failed
make[1]: *** [networking/libiproute/ipaddress.o] Error 1
Makefile:746: recipe for target 'networking/libiproute' failed
make: *** [networking/libiproute] Error 2
```

修改如下：

> 文件：networking\libiproute\ipaddress.c
>
> ```c
> #ifdef IFA_F_DAAFAILED
> 	if (ifa_flags & IFA_F_DADFAILED) {
> 		ifa_flags &= ~IFA_F_DADFAILED;
> 		printf("dadfailed ");
> 	}
> #endif
> ```

##### 3-4-2.编译报错2

```bash
libbb/lib.a(inet_common.o): In function `INET6_resolve':
inet_common.c:(.text.INET6_resolve+0x44): warning: Using 'getaddrinfo' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking
coreutils/lib.a(mktemp.o): In function `mktemp_main':
mktemp.c:(.text.mktemp_main+0x9c): warning: the use of `mktemp' is dangerous, better use `mkstemp'
networking/lib.a(ipcalc.o): In function `ipcalc_main':
ipcalc.c:(.text.ipcalc_main+0x24c): warning: Using 'gethostbyaddr' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking
libbb/lib.a(inet_common.o): In function `INET_resolve':
inet_common.c:(.text.INET_resolve+0x60): warning: Using 'gethostbyname' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking
networking/lib.a(inetd.o): In function `reread_config_file':
inetd.c:(.text.reread_config_file+0x230): warning: Using 'getservbyname' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking
networking/lib.a(netstat.o): In function `ip_port_str':
netstat.c:(.text.ip_port_str+0x5c): warning: Using 'getservbyport' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking
util-linux/lib.a(nsenter.o): In function `nsenter_main':
nsenter.c:(.text.nsenter_main+0x180): undefined reference to `setns'
coreutils/lib.a(sync.o): In function `sync_common':
sync.c:(.text.sync_common+0x30): undefined reference to `syncfs'
collect2: ld returned 1 exit status
Note: if build needs additional libraries, put them in CONFIG_EXTRA_LDLIBS.
Example: CONFIG_EXTRA_LDLIBS="pthread dl tirpc audit pam"
Makefile:721: recipe for target 'busybox_unstripped' failed
make: *** [busybox_unstripped] Error 1
```

---------

> 执行：`make menuconfig`
>
> 1.去掉`Coreutils`—>`sync`选项；
>
> 2.去掉`Linux System Utilities`—>`nsenter`选项；

##### 3-4-3.编译

```bash
fly@fly-vm:busybox-1.35.0$ make clean
  CLEAN   applets
  CLEAN   .tmp_versions
  CLEAN   busybox_unstripped.out .kernelrelease
fly@fly-vm:busybox-1.35.0$ make -j4
```

##### 3-4-4.编译`LOG`

```bash
  HOSTCC  scripts/basic/fixdep
  HOSTCC  scripts/basic/docproc
  HOSTCC  scripts/basic/split-include
  HOSTCC  scripts/kconfig/conf.o
  HOSTCC  scripts/kconfig/kxgettext.o
  HOSTCC  scripts/kconfig/mconf.o
  HOSTCC  scripts/kconfig/zconf.tab.o
  HOSTLD  scripts/kconfig/conf
scripts/kconfig/conf -s Config.in
#
# using defaults found in .config
#
  SPLIT   include/autoconf.h -> include/config/*
  GEN     include/bbconfigopts.h
  GEN     include/common_bufsiz.h
  GEN     include/embedded_scripts.h
  HOSTCC  applets/applet_tables
  HOSTCC  applets/usage
applets/usage.c: In function ‘main’:
applets/usage.c:52:3: warning: ignoring return value of ‘write’, declared with attribute warn_unused_result [-Wunused-result]
   write(STDOUT_FILENO, usage_array[i].usage, strlen(usage_array[i].usage) + 1);
   ^
  GEN     include/usage_compressed.h
  GEN     include/applet_tables.h include/NUM_APPLETS.h
  GEN     include/applet_tables.h include/NUM_APPLETS.h
  HOSTCC  applets/usage_pod
  CC      applets/applets.o
  LD      applets/built-in.o
  DOC     busybox.pod
  DOC     BusyBox.txt
  DOC     busybox.1
  DOC     BusyBox.html
  LD      archival/built-in.o
  CC      archival/bbunzip.o
  CC      archival/bzip2.o
  CC      archival/chksum_and_xwrite_tar_header.o
  LD      archival/libarchive/built-in.o
  CC      archival/libarchive/common.o
  CC      archival/cpio.o
  CC      archival/libarchive/data_align.o
  CC      archival/dpkg.o
  CC      archival/libarchive/data_extract_all.o
archival/dpkg.c: In function 'dpkg_main':
archival/dpkg.c:1790: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      archival/dpkg_deb.o
  CC      archival/libarchive/data_extract_to_command.o
  CC      archival/gzip.o
  CC      archival/libarchive/data_extract_to_stdout.o
archival/gzip.c: In function 'gzip_main':
archival/gzip.c:2218: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      archival/libarchive/data_skip.o
archival/dpkg.c:1790: warning: dereferencing pointer 'ptr_to_globals.176' does break strict-aliasing rules
archival/dpkg.c:1790: note: initialized from here
archival/gzip.c:2218: warning: dereferencing pointer 'ptr_to_globals.104' does break strict-aliasing rules
archival/gzip.c:2218: note: initialized from here
  CC      archival/lzop.o
  CC      archival/libarchive/decompress_bunzip2.o
  CC      archival/rpm.o
  CC      archival/tar.o
  CC      archival/libarchive/decompress_gunzip.o
  CC      archival/unzip.o
  LD      console-tools/built-in.o
  CC      console-tools/chvt.o
  CC      console-tools/clear.o
  CC      console-tools/deallocvt.o
  CC      console-tools/dumpkmap.o
  AR      archival/lib.a
  CC      archival/libarchive/decompress_unlzma.o
  CC      console-tools/fgconsole.o
  LD      coreutils/built-in.o
  CC      coreutils/basename.o
  CC      console-tools/kbd_mode.o
  CC      console-tools/loadfont.o
  CC      coreutils/cat.o
  CC      console-tools/loadkmap.o
  CC      archival/libarchive/decompress_unxz.o
  CC      coreutils/chgrp.o
  CC      console-tools/openvt.o
  CC      console-tools/reset.o
  CC      coreutils/chmod.o
  CC      console-tools/resize.o
  CC      console-tools/setconsole.o
  CC      coreutils/chown.o
  CC      console-tools/setkeycodes.o
  CC      console-tools/setlogcons.o
  CC      console-tools/showkey.o
  CC      coreutils/chroot.o
  CC      coreutils/cksum.o
console-tools/showkey.c: In function 'showkey_main':
console-tools/showkey.c:71: warning: dereferencing type-punned pointer will break strict-aliasing rules
console-tools/showkey.c:71: warning: dereferencing pointer 'ptr_to_globals.1' does break strict-aliasing rules
console-tools/showkey.c:71: note: initialized from here
  CC      coreutils/comm.o
  AR      console-tools/lib.a
  CC      archival/libarchive/filter_accept_all.o
  CC      coreutils/cp.o
  LD      coreutils/libcoreutils/built-in.o
  CC      coreutils/libcoreutils/cp_mv_stat.o
  CC      coreutils/cut.o
  CC      archival/libarchive/filter_accept_list.o
  CC      coreutils/date.o
  CC      coreutils/libcoreutils/getopt_mk_fifo_nod.o
  CC      archival/libarchive/filter_accept_list_reassign.o
  AR      coreutils/libcoreutils/lib.a
  CC      coreutils/dd.o
  CC      coreutils/df.o
  LD      debianutils/built-in.o
  CC      debianutils/pipe_progress.o
  CC      archival/libarchive/filter_accept_reject_list.o
  CC      debianutils/run_parts.o
  CC      archival/libarchive/find_list_entry.o
  CC      coreutils/dirname.o
  CC      coreutils/dos2unix.o
  CC      debianutils/start_stop_daemon.o
  CC      archival/libarchive/get_header_ar.o
  CC      coreutils/du.o
  CC      coreutils/echo.o
  CC      archival/libarchive/get_header_cpio.o
  CC      coreutils/env.o
  CC      debianutils/which.o
  CC      coreutils/expand.o
  CC      coreutils/expr.o
  AR      debianutils/lib.a
  CC      archival/libarchive/get_header_tar.o
  LD      klibc-utils/built-in.o
  CC      klibc-utils/resume.o
  CC      coreutils/factor.o
  AR      klibc-utils/lib.a
  LD      e2fsprogs/built-in.o
  CC      e2fsprogs/chattr.o
  CC      archival/libarchive/get_header_tar_bz2.o
  CC      coreutils/false.o
  CC      coreutils/fold.o
  CC      archival/libarchive/get_header_tar_gz.o
  CC      coreutils/head.o
  CC      e2fsprogs/e2fs_lib.o
  CC      coreutils/hostid.o
  CC      archival/libarchive/get_header_tar_lzma.o
  CC      e2fsprogs/fsck.o
  CC      coreutils/id.o
  CC      coreutils/install.o
  CC      archival/libarchive/get_header_tar_xz.o
  CC      coreutils/link.o
  CC      coreutils/ln.o
  CC      archival/libarchive/header_list.o
  CC      coreutils/logname.o
  CC      coreutils/ls.o
  CC      e2fsprogs/lsattr.o
  CC      archival/libarchive/header_skip.o
  CC      coreutils/md5_sha1_sum.o
  CC      archival/libarchive/header_verbose_list.o
  AR      e2fsprogs/lib.a
  LD      editors/built-in.o
  CC      editors/awk.o
  CC      coreutils/mkdir.o
  CC      archival/libarchive/init_handle.o
editors/awk.c: In function 'awk_main':
editors/awk.c:3571: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      coreutils/mkfifo.o
  CC      coreutils/mknod.o
  CC      archival/libarchive/lzo1x_1.o
  CC      coreutils/mktemp.o
  CC      coreutils/mv.o
  CC      archival/libarchive/lzo1x_1o.o
  CC      coreutils/nice.o
  CC      coreutils/nl.o
  CC      archival/libarchive/lzo1x_d.o
  CC      coreutils/nohup.o
  CC      coreutils/nproc.o
  CC      coreutils/od.o
  CC      coreutils/paste.o
  CC      archival/libarchive/open_transformer.o
  CC      coreutils/printenv.o
  CC      archival/libarchive/seek_by_jump.o
  CC      coreutils/printf.o
  CC      archival/libarchive/seek_by_read.o
  CC      coreutils/pwd.o
  CC      archival/libarchive/unpack_ar_archive.o
editors/awk.c:3571: warning: dereferencing pointer 'ptr_to_globals.243' does break strict-aliasing rules
editors/awk.c:3571: note: initialized from here
  CC      coreutils/readlink.o
  CC      coreutils/realpath.o
  CC      editors/cmp.o
  CC      archival/libarchive/unsafe_prefix.o
  CC      coreutils/rm.o
  CC      coreutils/rmdir.o
  CC      archival/libarchive/unsafe_symlink_target.o
  CC      editors/diff.o
  CC      coreutils/seq.o
editors/diff.c: In function 'diff_main':  CC      coreutils/shred.o

editors/diff.c:982: warning: dereferencing type-punned pointer will break strict-aliasing rules
  AR      archival/libarchive/lib.a
  LD      findutils/built-in.o
  CC      findutils/find.o
  CC      coreutils/shuf.o
  CC      coreutils/sleep.o
  CC      coreutils/sort.o
  CC      coreutils/split.o
editors/diff.c:982: warning: dereferencing pointer 'ptr_to_globals.83' does break strict-aliasing rules
editors/diff.c:982: note: initialized from here
  CC      coreutils/stat.o
  CC      editors/ed.o
  CC      findutils/grep.o
  CC      coreutils/stty.o
editors/ed.c: In function 'ed_main':
editors/ed.c:999: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      coreutils/sum.o
editors/ed.c:999: warning: dereferencing pointer 'ptr_to_globals.60' does break strict-aliasing rules
editors/ed.c:999: note: initialized from here
  CC      coreutils/sync.o
  CC      findutils/xargs.o
  CC      editors/patch.o
  CC      coreutils/tac.o
editors/patch.c: In function 'patch_main':  CC      coreutils/tail.o

editors/patch.c:399: warning: dereferencing type-punned pointer will break strict-aliasing rules
editors/patch.c:399: warning: dereferencing pointer 'ptr_to_globals.17' does break strict-aliasing rules
editors/patch.c:399: note: initialized from here
  CC      coreutils/tee.o
  AR      findutils/lib.a
  LD      init/built-in.o
  CC      init/bootchartd.o
  CC      editors/sed.o
  CC      coreutils/test.o
  CC      coreutils/test_ptr_hack.o
  CC      coreutils/timeout.o
coreutils/test.c: In function 'test_main':
coreutils/test.c:926: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      init/halt.o
  CC      coreutils/touch.o
coreutils/test.c:926: warning: dereferencing pointer 'test_ptr_to_statics.33' does break strict-aliasing rules
coreutils/test.c:926: note: initialized from here
  CC      coreutils/tr.o
  CC      init/init.o
  CC      coreutils/true.o
  CC      coreutils/truncate.o
  CC      coreutils/tty.o
  CC      editors/vi.o
  CC      coreutils/uname.o
  CC      coreutils/uniq.o
editors/vi.c: In function 'setops':
editors/vi.c:2622: warning: declaration of 'index' shadows a global declaration
/usr/local/arm/arm-2009q3/bin/../arm-none-linux-gnueabi/libc/usr/include/string.h:487: warning: shadowed declaration is here
editors/vi.c: In function 'vi_main':  AR      init/lib.a

editors/vi.c:4911: warning: dereferencing type-punned pointer will break strict-aliasing rules
  LD      libbb/built-in.o
  CC      libbb/appletlib.o
  CC      coreutils/unlink.o
  CC      coreutils/usleep.o
  CC      coreutils/uudecode.o
libbb/appletlib.c: In function 'lbb_prepare':
libbb/appletlib.c:250: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      coreutils/uuencode.o
  CC      coreutils/wc.o
  CC      coreutils/who.o
  CC      libbb/ask_confirmation.o
  CC      coreutils/whoami.o
  CC      coreutils/yes.o
  CC      libbb/auto_string.o
  CC      libbb/bb_askpass.o
  AR      coreutils/lib.a
  LD      libpwdgrp/built-in.o
  CC      libpwdgrp/pwd_grp.o
  CC      libbb/bb_bswap_64.o
  CC      libbb/bb_cat.o
  CC      libbb/bb_do_delay.o
  CC      libbb/bb_getgroups.o
  CC      libbb/bb_getsockname.o
  CC      libpwdgrp/uidgid_get.o
  CC      libbb/bb_pwd.o
editors/vi.c: In function 'do_cmd':
editors/vi.c:3659: warning: 'save_dot' may be used uninitialized in this function
  CC      libbb/bb_qsort.o
  AR      libpwdgrp/lib.a
  LD      loginutils/built-in.o
  CC      libbb/bb_strtonum.o
  CC      loginutils/add-remove-shell.o
  CC      libbb/capability.o
  CC      loginutils/addgroup.o
editors/vi.c: In function 'vi_main':
editors/vi.c:4911: warning: dereferencing pointer 'ptr_to_globals.362' does break strict-aliasing rules
editors/vi.c:4911: note: initialized from here
  CC      libbb/change_identity.o
  CC      libbb/chomp.o
  AR      editors/lib.a
  LD      mailutils/built-in.o
  CC      mailutils/mail.o
  CC      loginutils/adduser.o
  CC      libbb/common_bufsiz.o
  CC      libbb/compare_string_array.o
  CC      libbb/concat_path_file.o
  CC      mailutils/makemime.o
  CC      loginutils/chpasswd.o
  CC      libbb/concat_subpath_file.o
  CC      libbb/const_hack.o
mailutils/makemime.c: In function 'makemime_main':
mailutils/makemime.c:192: warning: dereferencing type-punned pointer will break strict-aliasing rules
mailutils/makemime.c:192: warning: dereferencing pointer 'ptr_to_globals.0' does break strict-aliasing rules
mailutils/makemime.c:192: note: initialized from here
  CC      mailutils/popmaildir.o
  CC      libbb/copy_file.o
  CC      loginutils/cryptpw.o
  CC      libbb/copyfd.o
mailutils/popmaildir.c: In function 'popmaildir_main':
mailutils/popmaildir.c:127: warning: dereferencing type-punned pointer will break strict-aliasing rules
mailutils/popmaildir.c:127: warning: dereferencing pointer 'ptr_to_globals.4' does break strict-aliasing rules
mailutils/popmaildir.c:127: note: initialized from here
  CC      loginutils/deluser.o
  CC      mailutils/reformime.o
  CC      libbb/correct_password.o
  CC      libbb/crc32.o
mailutils/reformime.c: In function 'reformime_main':
mailutils/reformime.c:281: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      loginutils/getty.o
mailutils/reformime.c:281: warning: dereferencing pointer 'ptr_to_globals.24' does break strict-aliasing rules
mailutils/reformime.c:281: note: initialized from here
  CC      libbb/default_error_retval.o
  CC      mailutils/sendmail.o
  CC      libbb/device_open.o
loginutils/getty.c: In function 'getty_main':
loginutils/getty.c:556: warning: dereferencing type-punned pointer will break strict-aliasing rules
loginutils/getty.c:556: warning: dereferencing pointer 'ptr_to_globals.25' does break strict-aliasing rules
loginutils/getty.c:556: note: initialized from here
  CC      libbb/dump.o
mailutils/sendmail.c: In function 'sendmail_main':
mailutils/sendmail.c:253: warning: dereferencing type-punned pointer will break strict-aliasing rules
mailutils/sendmail.c:253: warning: dereferencing pointer 'ptr_to_globals.8' does break strict-aliasing rules
mailutils/sendmail.c:253: note: initialized from here
  CC      libbb/duration.o
  CC      loginutils/login.o
  AR      mailutils/lib.a
  CC      libbb/endofname.o
  LD      miscutils/built-in.o
  CC      miscutils/adjtimex.o
  CC      loginutils/passwd.o
  CC      libbb/executable.o
  CC      miscutils/ascii.o
  CC      libbb/fclose_nonstdin.o
  CC      libbb/fflush_stdout_and_exit.o
  CC      miscutils/bc.o
  CC      loginutils/su.o
  CC      libbb/fgets_str.o
  CC      libbb/find_mount_point.o
  CC      libbb/find_pid_by_name.o
  CC      loginutils/sulogin.o
miscutils/bc.c: In function 'bc_main':
miscutils/bc.c:7532: warning: dereferencing type-punned pointer will break strict-aliasing rules
miscutils/bc.c: In function 'dc_main':
miscutils/bc.c:7551: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      libbb/find_root_device.o
  CC      libbb/full_write.o
  CC      loginutils/vlock.o
  CC      libbb/get_console.o
  CC      libbb/get_cpu_count.o
  AR      loginutils/lib.a
  LD      modutils/built-in.o
  CC      modutils/depmod.o
  CC      libbb/get_last_path_component.o
  CC      libbb/get_line_from_file.o
  CC      libbb/get_shell_name.o
  CC      modutils/insmod.o
  CC      libbb/get_volsize.o
  CC      libbb/getopt32.o
  CC      modutils/lsmod.o
  CC      libbb/getopt_allopts.o
  CC      libbb/getpty.o
  CC      modutils/modinfo.o
  CC      libbb/hash_md5_sha.o
  CC      libbb/herror_msg.o
  CC      modutils/modprobe.o
  CC      libbb/human_readable.o
modutils/modprobe.c: In function 'modprobe_main':
modutils/modprobe.c:563: warning: dereferencing type-punned pointer will break strict-aliasing rules
modutils/modprobe.c:563: warning: dereferencing pointer 'ptr_to_globals.31' does break strict-aliasing rules
modutils/modprobe.c:563: note: initialized from here
  CC      libbb/in_ether.o
  CC      libbb/inet_cksum.o
  CC      modutils/modutils.o
  CC      libbb/inet_common.o
  CC      libbb/inode_hash.o
  CC      modutils/rmmod.o
  CC      libbb/isdirectory.o
  CC      libbb/isqrt.o
  AR      modutils/lib.a
  LD      networking/built-in.o
  CC      libbb/iterate_on_dir.o
  CC      networking/arp.o
  CC      libbb/kernel_version.o
  CC      libbb/last_char_is.o
  CC      libbb/lineedit.o
  CC      libbb/lineedit_ptr_hack.o
  CC      networking/arping.o
libbb/lineedit.c: In function 'read_line_input':
libbb/lineedit.c:2430: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      libbb/llist.o
networking/arping.c: In function 'arping_main':
networking/arping.c:302: warning: dereferencing type-punned pointer will break strict-aliasing rules
networking/arping.c:302: warning: dereferencing pointer 'ptr_to_globals.20' does break strict-aliasing rules
networking/arping.c:302: note: initialized from here
  CC      libbb/logenv.o
miscutils/bc.c: In function 'bc_main':
miscutils/bc.c:7532: warning: dereferencing pointer 'ptr_to_globals.635' does break strict-aliasing rules
miscutils/bc.c:7532: note: initialized from here
miscutils/bc.c: In function 'dc_main':
miscutils/bc.c:7551: warning: dereferencing pointer 'ptr_to_globals.638' does break strict-aliasing rules
miscutils/bc.c:7551: note: initialized from here
  CC      networking/brctl.o
  CC      miscutils/beep.o
  CC      libbb/login.o
  CC      miscutils/chat.o
  CC      libbb/loop.o
  CC      networking/dnsd.o
libbb/lineedit.c:2430: warning: dereferencing pointer 'lineedit_ptr_to_statics.164' does break strict-aliasing rules
libbb/lineedit.c:2430: note: initialized from here
  CC      libbb/make_directory.o
  CC      miscutils/conspy.o
miscutils/conspy.c: In function 'conspy_main':
miscutils/conspy.c:383: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      networking/ether-wake.o
  CC      libbb/makedev.o
  CC      libbb/match_fstype.o
  CC      libbb/messages.o
miscutils/conspy.c:383: warning: dereferencing pointer 'ptr_to_globals.26' does break strict-aliasing rules
miscutils/conspy.c:383: note: initialized from here
  CC      networking/ftpd.o
  CC      libbb/missing_syscalls.o
  CC      miscutils/crond.o
  CC      libbb/mode_string.o
networking/ftpd.c: In function 'ftpd_main':
networking/ftpd.c:1183: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      libbb/nuke_str.o
  CC      libbb/obscure.o
  CC      libbb/parse_config.o
networking/ftpd.c:1183: warning: dereferencing pointer 'ptr_to_globals.84' does break strict-aliasing rules
networking/ftpd.c:1183: note: initialized from here
  CC      libbb/parse_mode.o
  CC      miscutils/crontab.o
  CC      libbb/percent_decode.o
  CC      networking/ftpgetput.o
  CC      libbb/perror_msg.o
  CC      libbb/perror_nomsg.o
  CC      miscutils/devmem.o
  CC      libbb/perror_nomsg_and_die.o
  CC      libbb/pidfile.o
  CC      libbb/platform.o
  CC      networking/hostname.o
  CC      miscutils/fbsplash.o
  CC      libbb/print_flags.o
  CC      libbb/print_numbered_lines.o
miscutils/fbsplash.c: In function 'fbsplash_main':
miscutils/fbsplash.c:500: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      networking/httpd.o
  CC      libbb/printable.o
miscutils/fbsplash.c:500: warning: dereferencing pointer 'ptr_to_globals.33' does break strict-aliasing rules
miscutils/fbsplash.c:500: note: initialized from here
  CC      libbb/printable_string.o
networking/httpd.c: In function 'httpd_main':
networking/httpd.c:2783: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      miscutils/hdparm.o
  CC      libbb/process_escape_sequence.o
  CC      libbb/procps.o
  CC      libbb/progress.o
  CC      libbb/ptr_to_globals.o
  CC      libbb/pw_encrypt.o
  CC      libbb/read.o
networking/httpd.c:2783: warning: dereferencing pointer 'ptr_to_globals.103' does break strict-aliasing rules
networking/httpd.c:2783: note: initialized from here
  CC      libbb/read_key.o
  CC      networking/ifconfig.o
  CC      libbb/read_printf.o
  CC      networking/ifenslave.o
  CC      miscutils/hexedit.o
  CC      libbb/recursive_action.o
networking/ifenslave.c: In function 'ifenslave_main':
networking/ifenslave.c:496: warning: dereferencing type-punned pointer will break strict-aliasing rules
miscutils/hexedit.c: In function 'hexedit_main':
miscutils/hexedit.c:256: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      libbb/remove_file.o
networking/ifenslave.c:496: warning: dereferencing pointer 'ptr_to_globals.18' does break strict-aliasing rules
networking/ifenslave.c:496: note: initialized from here
miscutils/hexedit.c:256: warning: dereferencing pointer 'ptr_to_globals.14' does break strict-aliasing rules
miscutils/hexedit.c:256: note: initialized from here
  CC      libbb/replace.o
  CC      networking/ifplugd.o
  CC      libbb/rtc.o
  CC      miscutils/i2c_tools.o
  CC      libbb/run_shell.o
networking/ifplugd.c: In function 'ifplugd_main':
networking/ifplugd.c:572: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      libbb/safe_gethostname.o
  CC      libbb/safe_poll.o
networking/ifplugd.c:572: warning: dereferencing pointer 'ptr_to_globals.34' does break strict-aliasing rules
networking/ifplugd.c:572: note: initialized from here
  CC      libbb/safe_strncpy.o
  CC      networking/ifupdown.o
  CC      libbb/safe_write.o
  CC      libbb/securetty.o
  CC      libbb/setup_environment.o
  CC      libbb/signals.o
  CC      miscutils/less.o
  CC      libbb/simplify_path.o
  CC      libbb/single_argv.o
miscutils/less.c: In function 'less_main':
miscutils/less.c:1812: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      libbb/skip_whitespace.o
  CC      libbb/speed_table.o
  CC      networking/inetd.o
  CC      libbb/str_tolower.o
  CC      libbb/strrstr.o
  CC      libbb/sysconf.o
  CC      libbb/time.o
miscutils/less.c:1812: warning: dereferencing pointer 'ptr_to_globals.135' does break strict-aliasing rules
miscutils/less.c:1812: note: initialized from here
  CC      libbb/trim.o
  CC      libbb/u_signal_names.o
  CC      libbb/ubi.o
  CC      miscutils/lsscsi.o
  CC      libbb/udp_io.o
  CC      libbb/unicode.o
  CC      networking/interface.o
  CC      miscutils/makedevs.o
libbb/udp_io.c: In function 'send_to_from':
libbb/udp_io.c:109: warning: dereferencing pointer 'cmsgptr' does break strict-aliasing rules
libbb/udp_io.c:101: warning: dereferencing pointer 'cmsgptr' does break strict-aliasing rules
libbb/udp_io.c:85: warning: dereferencing pointer 'cmsgptr' does break strict-aliasing rules
libbb/udp_io.c:87: warning: dereferencing pointer 'cmsgptr' does break strict-aliasing rules
libbb/udp_io.c:86: warning: dereferencing pointer 'cmsgptr' does break strict-aliasing rules
libbb/udp_io.c:103: warning: dereferencing pointer 'cmsgptr' does break strict-aliasing rules
libbb/udp_io.c:102: warning: dereferencing pointer 'cmsgptr' does break strict-aliasing rules
libbb/udp_io.c:76: note: initialized from here
  CC      libbb/update_passwd.o
  CC      libbb/utmp.o
  CC      miscutils/man.o
  CC      libbb/uuencode.o
  CC      libbb/verror_msg.o
  CC      networking/ip.o
  CC      miscutils/microcom.o
  CC      libbb/vfork_daemon_rexec.o
  CC      networking/ipcalc.o
  CC      libbb/warn_ignoring_args.o
  CC      libbb/wfopen.o
  CC      miscutils/mt.o
  CC      libbb/wfopen_input.o
  CC      networking/isrv.o
  CC      libbb/write.o
  CC      miscutils/nandwrite.o
  CC      libbb/xatonum.o
  CC      libbb/xconnect.o
  CC      networking/isrv_identd.o
  CC      miscutils/partprobe.o
  CC      libbb/xfunc_die.o
  CC      networking/nameif.o
  CC      miscutils/raidautorun.o
  CC      libbb/xfuncs.o
  CC      libbb/xfuncs_printf.o
  CC      miscutils/readahead.o
  CC      networking/nbd-client.o
  CC      miscutils/runlevel.o
  CC      libbb/xgetcwd.o
  CC      networking/nc.o
  CC      miscutils/rx.o
In file included from networking/nc.c:59:
networking/nc_bloaty.c: In function 'nc_main':
networking/nc_bloaty.c:751: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      libbb/xgethostbyname.o
  CC      libbb/xreadlink.o
  CC      libbb/xrealloc_vector.o
networking/nc_bloaty.c:751: warning: dereferencing pointer 'ptr_to_globals.49' does break strict-aliasing rules  CC      miscutils/setfattr.o

networking/nc_bloaty.c:751: note: initialized from here
  CC      libbb/xregcomp.o
  CC      networking/netstat.o
  CC      networking/nslookup.o
  CC      miscutils/setserial.o
  AR      libbb/lib.a
networking/netstat.c: In function 'netstat_main':
networking/netstat.c:688: warning: dereferencing type-punned pointer will break strict-aliasing rules
  LD      networking/libiproute/built-in.o
  CC      networking/libiproute/ip_parse_common_args.o
networking/netstat.c:688: warning: dereferencing pointer 'ptr_to_globals.48' does break strict-aliasing rules
networking/netstat.c:688: note: initialized from here
  CC      networking/libiproute/ipaddress.o
  CC      miscutils/strings.o
  CC      networking/ntpd.o
  CC      networking/parse_pasv_epsv.o
  CC      miscutils/time.o
networking/ntpd.c: In function 'ntpd_main':
networking/ntpd.c:2475: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      networking/ping.o
  CC      miscutils/ts.o
  CC      networking/libiproute/iplink.o
  CC      miscutils/ttysize.o
  CC      networking/pscan.o
  CC      miscutils/ubi_tools.o
  CC      networking/route.o
  CC      networking/libiproute/ipneigh.o
  CC      networking/slattach.o
  CC      miscutils/ubirename.o
  CC      miscutils/volname.o
  CC      networking/ssl_client.o
  CC      networking/tc.o
  CC      networking/libiproute/iproute.o
  CC      networking/tcpudp.o
  CC      miscutils/watchdog.o
  AR      miscutils/lib.a
  LD      networking/udhcp/built-in.o
  CC      networking/udhcp/arpping.o
  CC      networking/tcpudp_perhost.o
  CC      networking/telnet.o
  CC      networking/telnetd.o
  CC      networking/udhcp/common.o
  CC      networking/libiproute/iprule.o
  CC      networking/tftp.o
networking/libiproute/iprule.c: In function 'print_rule':
networking/libiproute/iprule.c:122: warning: array subscript is above array bounds
networking/libiproute/iprule.c:127: warning: array subscript is above array bounds
  CC      networking/libiproute/iptunnel.o
  CC      networking/tls.o
  CC      networking/udhcp/d6_dhcpc.o
  CC      networking/tls_aes.o
  CC      networking/libiproute/libnetlink.o
  CC      networking/tls_aesgcm.o
  CC      networking/tls_fe.o
  CC      networking/udhcp/d6_packet.o
  CC      networking/libiproute/ll_addr.o
  CC      networking/tls_pstm.o
networking/tls_fe.c: In function 'curve25519':
networking/tls_fe.c:599: warning: array subscript is above array bounds
  CC      networking/udhcp/d6_socket.o
  CC      networking/tls_pstm_montgomery_reduce.o
  CC      networking/libiproute/ll_map.o
  CC      networking/tls_pstm_mul_comba.o
  CC      networking/udhcp/dhcpc.o
  CC      networking/libiproute/ll_proto.o
  CC      networking/tls_pstm_sqr_comba.o
  CC      networking/libiproute/ll_types.o
  CC      networking/tls_rsa.o
  CC      networking/udhcp/dhcpd.o
  CC      networking/libiproute/rt_names.o
networking/udhcp/dhcpd.c: In function 'udhcpd_main':
networking/udhcp/dhcpd.c:954: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      networking/tls_sp_c32.o
  CC      networking/udhcp/dhcprelay.o
  CC      networking/libiproute/rtm_map.o
networking/udhcp/dhcpd.c:954: warning: dereferencing pointer 'ptr_to_globals.88' does break strict-aliasing rules
networking/udhcp/dhcpd.c:954: note: initialized from here
  CC      networking/libiproute/utils.o
  CC      networking/udhcp/domain_codec.o
  CC      networking/udhcp/dumpleases.o
  CC      networking/traceroute.o
networking/traceroute.c: In function 'traceroute_init':
networking/traceroute.c:895: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      networking/udhcp/packet.o
networking/traceroute.c:895: warning: dereferencing pointer 'ptr_to_globals.37' does break strict-aliasing rules
networking/traceroute.c:895: note: initialized from here
  AR      networking/libiproute/lib.a
  CC      networking/udhcp/signalpipe.o
  LD      printutils/built-in.o
  CC      printutils/lpd.o
  CC      networking/udhcp/socket.o
  CC      networking/tunctl.o
  CC      printutils/lpr.o
  CC      networking/vconfig.o
  AR      networking/udhcp/lib.a
  CC      networking/wget.o
  LD      procps/built-in.o
  CC      procps/free.o
  CC      networking/whois.o
networking/wget.c: In function 'wget_main':  AR      printutils/lib.a

networking/wget.c:1536: warning: dereferencing type-punned pointer will break strict-aliasing rules
  LD      runit/built-in.o
  CC      runit/chpst.o
  CC      procps/fuser.o
  CC      networking/zcip.o
networking/wget.c:1536: warning: dereferencing pointer 'ptr_to_globals.78' does break strict-aliasing rules
networking/wget.c:1536: note: initialized from here
  CC      runit/runsv.o
  CC      procps/iostat.o
  CC      procps/kill.o
procps/iostat.c: In function 'iostat_main':
procps/iostat.c:406: warning: dereferencing type-punned pointer will break strict-aliasing rules
procps/iostat.c:406: warning: dereferencing pointer 'ptr_to_globals.28' does break strict-aliasing rules
procps/iostat.c:406: note: initialized from here
  AR      networking/lib.a
  LD      selinux/built-in.o
  AR      selinux/lib.a
  LD      shell/built-in.o
  CC      shell/ash.o
  CC      procps/lsof.o
  CC      procps/mpstat.o
  CC      runit/runsvdir.o
procps/mpstat.c: In function 'mpstat_main':
procps/mpstat.c:858: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      procps/nmeter.o
shell/ash.c: In function 'ash_main':
shell/ash.c:14589: warning: dereferencing type-punned pointer will break strict-aliasing rules
shell/ash.c:14590: warning: dereferencing type-punned pointer will break strict-aliasing rules
shell/ash.c:14591: warning: dereferencing type-punned pointer will break strict-aliasing rules
procps/nmeter.c: In function 'nmeter_main':
procps/nmeter.c:866: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      runit/sv.o
procps/mpstat.c:858: warning: dereferencing pointer 'ptr_to_globals.73' does break strict-aliasing rules
procps/mpstat.c:858: note: initialized from here
  CC      procps/pgrep.o
procps/nmeter.c:866: warning: dereferencing pointer 'ptr_to_globals.56' does break strict-aliasing rules
procps/nmeter.c:866: note: initialized from here
  CC      procps/pidof.o
  CC      runit/svlogd.o
  CC      procps/pmap.o
runit/svlogd.c: In function 'svlogd_main':
runit/svlogd.c:1059: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      procps/powertop.o
  CC      procps/ps.o
procps/powertop.c: In function 'powertop_main':
procps/powertop.c:701: warning: dereferencing type-punned pointer will break strict-aliasing rules
procps/powertop.c:701: warning: dereferencing pointer 'ptr_to_globals.39' does break strict-aliasing rules
procps/powertop.c:701: note: initialized from here
  CC      procps/pstree.o
runit/svlogd.c:1059: warning: dereferencing pointer 'ptr_to_globals.55' does break strict-aliasing rules
runit/svlogd.c:1059: note: initialized from here
  CC      procps/pwdx.o
procps/pstree.c: In function 'pstree_main':
procps/pstree.c:385: warning: dereferencing type-punned pointer will break strict-aliasing rules
  AR      runit/lib.a
  CC      procps/smemcap.o
  LD      sysklogd/built-in.o
  CC      sysklogd/klogd.o
procps/pstree.c:385: warning: dereferencing pointer 'ptr_to_globals.24' does break strict-aliasing rules
procps/pstree.c:385: note: initialized from here
  CC      procps/sysctl.o
  CC      sysklogd/logread.o
  CC      procps/top.o
procps/top.c: In function 'top_main':
procps/top.c:1110: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      procps/uptime.o
  CC      sysklogd/syslogd_and_logger.o
  CC      procps/watch.o
In file included from sysklogd/syslogd_and_logger.c:58:
sysklogd/syslogd.c: In function 'syslogd_main':
sysklogd/syslogd.c:1138: warning: dereferencing type-punned pointer will break strict-aliasing rules
procps/top.c:1110: warning: dereferencing pointer 'ptr_to_globals.65' does break strict-aliasing rules
procps/top.c:1110: note: initialized from here
  CC      shell/ash_ptr_hack.o
sysklogd/syslogd.c:1138: warning: dereferencing pointer 'ptr_to_globals.56' does break strict-aliasing rules
sysklogd/syslogd.c:1138: note: initialized from here
  CC      shell/cttyhack.o
  AR      procps/lib.a
  LD      util-linux/built-in.o
  CC      util-linux/acpid.o
  AR      sysklogd/lib.a
  CC      shell/hush.o
util-linux/acpid.c: In function 'acpid_main':  LD      util-linux/volume_id/built-in.o

util-linux/acpid.c:266: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      util-linux/volume_id/bcache.o
util-linux/acpid.c:266: warning: dereferencing pointer 'ptr_to_globals.8' does break strict-aliasing rules
util-linux/acpid.c:266: note: initialized from here
  CC      util-linux/blkdiscard.o
  CC      util-linux/volume_id/btrfs.o
shell/hush.c: In function 'hush_main':
shell/hush.c:10209: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      util-linux/blkid.o
  CC      util-linux/volume_id/cramfs.o
  CC      util-linux/blockdev.o
  CC      util-linux/volume_id/erofs.o
  CC      util-linux/volume_id/exfat.o
  CC      util-linux/cal.o
  CC      util-linux/volume_id/ext.o
  CC      util-linux/chrt.o
  CC      util-linux/volume_id/f2fs.o
  CC      util-linux/volume_id/fat.o
  CC      util-linux/dmesg.o
  CC      util-linux/eject.o
  CC      util-linux/volume_id/get_devname.o
  CC      util-linux/fallocate.o
  CC      util-linux/volume_id/hfs.o
  CC      util-linux/fatattr.o
shell/ash.c:14589: warning: dereferencing pointer 'ash_ptr_to_globals_misc.990' does break strict-aliasing rules
shell/ash.c:14589: note: initialized from here
shell/ash.c:14590: warning: dereferencing pointer 'ash_ptr_to_globals_memstack.992' does break strict-aliasing rules
shell/ash.c:14590: note: initialized from here
shell/ash.c:14591: warning: dereferencing pointer 'ash_ptr_to_globals_var.994' does break strict-aliasing rules
shell/ash.c:14591: note: initialized from here
  CC      util-linux/volume_id/iso9660.o
  CC      util-linux/fbset.o
  CC      util-linux/volume_id/jfs.o
  CC      shell/match.o
  CC      util-linux/volume_id/lfs.o
  CC      util-linux/fdformat.o
  CC      shell/math.o
  CC      util-linux/volume_id/linux_raid.o
  CC      util-linux/fdisk.o
  CC      util-linux/volume_id/linux_swap.o
util-linux/fdisk.c: In function 'fdisk_main':
util-linux/fdisk.c:3036: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      shell/random.o
  CC      util-linux/volume_id/luks.o
  CC      shell/shell_common.o
  CC      util-linux/volume_id/minix.o
shell/hush.c:10209: warning: dereferencing pointer 'ptr_to_globals.429' does break strict-aliasing rules
shell/hush.c:10209: note: initialized from here
  CC      util-linux/volume_id/nilfs.o
  CC      util-linux/volume_id/ntfs.o
  CC      util-linux/volume_id/ocfs2.o
  AR      shell/lib.a
  CC      util-linux/findfs.o
  CC      util-linux/volume_id/reiserfs.o
  CC      util-linux/volume_id/romfs.o
util-linux/fdisk.c:3036: warning: dereferencing pointer 'ptr_to_globals.185' does break strict-aliasing rules
util-linux/fdisk.c:3036: note: initialized from here
  CC      util-linux/flock.o
  CC      util-linux/volume_id/squashfs.o
  CC      util-linux/volume_id/sysv.o
  CC      util-linux/freeramdisk.o
  CC      util-linux/fsck_minix.o
  CC      util-linux/volume_id/ubifs.o
  CC      util-linux/volume_id/udf.o
util-linux/fsck_minix.c: In function 'fsck_minix_main':
util-linux/fsck_minix.c:1236: warning: dereferencing type-punned pointer will break strict-aliasing rules
  CC      util-linux/fsfreeze.o
  CC      util-linux/volume_id/util.o
  CC      util-linux/volume_id/volume_id.o
  CC      util-linux/fstrim.o
  CC      util-linux/volume_id/xfs.o
  CC      util-linux/getopt.o
  CC      util-linux/hexdump.o
util-linux/fsck_minix.c:1236: warning: dereferencing pointer 'ptr_to_globals.136' does break strict-aliasing rules
util-linux/fsck_minix.c:1236: note: initialized from here
  AR      util-linux/volume_id/lib.a
  CC      util-linux/hexdump_xxd.o
  CC      util-linux/hwclock.o
  CC      util-linux/ionice.o
util-linux/hwclock.c: In function 'set_kernel_tz':
util-linux/hwclock.c:144: warning: null argument where non-null required (argument 1)
  CC      util-linux/ipcrm.o
  CC      util-linux/ipcs.o
  CC      util-linux/last_fancy.o
  CC      util-linux/losetup.o
  CC      util-linux/lspci.o
  CC      util-linux/lsusb.o
  CC      util-linux/mdev.o
  CC      util-linux/mesg.o
  CC      util-linux/mkfs_ext2.o
  CC      util-linux/mkfs_minix.o
  CC      util-linux/mkfs_vfat.o
util-linux/mkfs_minix.c: In function 'mkfs_minix_main':
util-linux/mkfs_minix.c:618: warning: dereferencing type-punned pointer will break strict-aliasing rules
util-linux/mkfs_minix.c:618: warning: dereferencing pointer 'ptr_to_globals.62' does break strict-aliasing rules
util-linux/mkfs_minix.c:618: note: initialized from here
  CC      util-linux/mkswap.o
  CC      util-linux/more.o
  CC      util-linux/mount.o
  CC      util-linux/mountpoint.o
  CC      util-linux/pivot_root.o
  CC      util-linux/rdate.o
  CC      util-linux/rdev.o
  CC      util-linux/readprofile.o
  CC      util-linux/renice.o
  CC      util-linux/rev.o
  CC      util-linux/rtcwake.o
  CC      util-linux/script.o
  CC      util-linux/scriptreplay.o
  CC      util-linux/setarch.o
  CC      util-linux/setpriv.o
  CC      util-linux/setsid.o
  CC      util-linux/swaponoff.o
  CC      util-linux/switch_root.o
  CC      util-linux/taskset.o
  CC      util-linux/uevent.o
  CC      util-linux/umount.o
  CC      util-linux/unshare.o
  CC      util-linux/wall.o
  AR      util-linux/lib.a
  LINK    busybox_unstripped
Static linking against glibc, can't use --gc-sections
Trying libraries: crypt m resolv rt
 Library crypt is not needed, excluding it
 Library m is needed, can't exclude it (yet)
 Library resolv is needed, can't exclude it (yet)
 Library rt is needed, can't exclude it (yet)
Final link with: m resolv rt
```

##### 3-4-5.查看`busybox`属性

```bash
fly@fly-vm:busybox-1.35.0$ file busybox
busybox: ELF 32-bit LSB executable, ARM, EABI5 version 1 (SYSV), statically linked, for GNU/Linux 2.6.16, stripped
```

##### 3-4-6.安装

```bash
fly@fly-vm:busybox-1.35.0$ make install
```

> 源码目录下会生成一个`_install`目录；

###### 3-4-6-1.新建目录和设置文件

```bash
# 将_install目录文件copy到新目录
fly@fly-vm:busybox-1.35.0$ make x210_rootfs
make x210 rootfs
# 在新目录下创建新文件
fly@fly-vm:busybox-1.35.0$ make x210_dir
mkdir -p x210_rootfs/dev
mkdir -p x210_rootfs/etc
mkdir -p x210_rootfs/etc/init.d
mkdir -p x210_rootfs/proc
mkdir -p x210_rootfs/sys
mkdir -p x210_rootfs/tmp
mkdir -p x210_rootfs/var
# 添加所需的设备文件
fly@fly-vm:busybox-1.35.0$ make x210_mknod
sudo cp /dev/console x210_rootfs/dev -a
[sudo] password for fly:
sudo cp /dev/null x210_rootfs/dev -a
fly@fly-vm:dev$ ls -al
total 8
drwxrwxr-x  2 fly  fly  4096 11月 13 22:01 .
drwxrwxr-x 11 fly  fly  4096 11月 13 21:50 ..
crw-------  1 root root 5, 1 11月 12 01:06 console
crw-rw-rw-  1 root root 1, 3 11月 12 01:06 null
```

----------

###### 3-4-6-2.新建`init.d\rcS`文件

```bash
#!/bin/sh

echo
echo "X210BV3S FileSystem is Ready ..."
echo

PATH=/sbin:/bin:/usr/sbin:/usr/bin

runlevel=S
prevlevel=N

umask 022

export PATH runlevel prevlevel

mount -a

hostname x210bv3s

echo /sbin/mdev > /proc/sys/kernel/hotplug
mdev -s

#/bin/hostname -F /etc/sysconfig/HOSTNAME
/bin/hostname -F /etc/hostname

ifconfig eth0 192.168.0.188
```

----------

###### 3-4-6-3.新建`fstab`文件

```bash
# /etc/fstab: static file system information.
#
# Use 'vol_id --uuid' to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
#       <file system>   <mount point>   <type>  <options>       <dump>  <pass>
        proc                    /proc                   proc    defaults        0               0
        sysfs                   /sys                    sysfs   defaults        0               0
        tmpfs                   /var                    tmpfs   defaults        0               0
        tmpfs                   /tmp                    tmpfs   defaults        0               0
        tmpfs                   /dev                    tmpfs   defaults        0               0

```

###### 3-4-6-4.新建`group`文件

```bash
root:x:0:
daemon:x:1:
bin:x:2:
sys:x:3:
adm:x:4:
tty:x:5:
disk:x:6:
wheel:x:10:root
audio:x:29:
www-data:x:33:
utmp:x:43:
staff:x:50:
lock:x:54:
haldaemon:x:68:
dbus:x:81:
netdev:x:82:
ftp:x:83
nobody:x:99:
nogroup:x:99:
users:x:100:
default:x:1000:
```

---------------

###### 3-4-6-5.新建`hostname`文件

```bash
x210bv3s
```

---------

###### 3-4-5-6.新建`inittab`  `passwd`  `profile`文件

```bash
fly@fly-vm:etc$ cat inittab
#first:run the system script file
::sysinit:/etc/init.d/rcS
::askfirst:-/bin/sh
::ctrlaltdel:-/sbin/reboot
#umount all filesystem
::shutdown:/bin/umount -a -r
#restart init process
::restart:/sbin/init
```

-------

```bash
fly@fly-vm:etc$ cat passwd
root:x:0:0:root:/root:/bin/sh
daemon:x:1:1:daemon:/usr/sbin:/bin/sh
bin:x:2:2:bin:/bin:/bin/sh
sys:x:3:3:sys:/dev:/bin/sh
sync:x:4:100:sync:/bin:/bin/sync
mail:x:8:8:mail:/var/spool/mail:/bin/sh
proxy:x:13:13:proxy:/bin:/bin/sh
www-data:x:33:33:www-data:/var/www:/bin/sh
backup:x:34:34:backup:/var/backups:/bin/sh
operator:x:37:37:Operator:/var:/bin/sh
haldaemon:x:68:68:hald:/:/bin/sh
dbus:x:81:81:dbus:/var/run/dbus:/bin/sh
ftp:x:83:83:ftp:/home/ftp:/bin/sh
nobody:x:99:99:nobody:/home:/bin/sh
sshd:x:103:99:Operator:/var:/bin/sh
default:x:1000:1000:Default non-root user:/home/default:/bin/sh
```

-----------

```bash
fly@fly-vm:etc$ cat profile
# Ash profile
# vim: syntax=sh

# No core files by default
ulimit -S -c 0 > /dev/null 2>&1

USER="`id -un`"
LOGNAME=$USER
PS1='[\u@\h \W]\# '
PATH=$PATH

HOSTNAME=`/bin/hostname`

export USER LOGNAME PS1 PATH
```

------------

###### 3-4-5-7.新建`shadow`文件

```bash
fly@fly-vm:etc$ sudo cat shadow
root:$1$qlBGI0M6$eBiEsOdYPx0ZfujhTv5o00:10933:0:99999:7:::
bin:*:10933:0:99999:7:::
daemon:*:10933:0:99999:7:::
adm:*:10933:0:99999:7:::
lp:*:10933:0:99999:7:::
sync:*:10933:0:99999:7:::
shutdown:*:10933:0:99999:7:::
halt:*:10933:0:99999:7:::
uucp:*:10933:0:99999:7:::
operator:*:10933:0:99999:7:::
ftp:*:10933:0:99999:7:::
nobody:*:10933:0:99999:7:::
default::10933:0:99999:7:::
```

--------------

##### 3-4-7.通过`NFS`挂载根文件系统

```bash
# UBOOT设置的环境变量
x210 # printenv
baudrate=115200
ethact=dm9000
ethaddr=00:40:5c:26:0a:5b
mtdpart=80000 400000 3000000
bootdelay=2
filesize=60000
fileaddr=C0008000
netmask=255.255.255.0
gatewayip=192.168.0.1
ipaddr=192.168.0.188
bootcmd=tftp 30008000 zImage;bootm 30008000
serverip=192.168.0.105
bootargs=root=/dev/nfs nfsroot=192.168.0.105:/opt/v210_rootfs ip=192.168.0.188:192.168.0.105:192.168.0.1:255.255.255.0::eth0:off init=/linuxrc console=ttySAC2,115200

Environment size: 428/16380 bytes
```

------------------------------

> 为根文件系统目录创建软连接。

```bash
fly@fly-vm:opt$ sudo ln -s /home/fly/project/x210bv3s/busybox-1.35.0/x210_rootfs v210_rootfs
fly@fly-vm:opt$ ls -l v210_rootfs
lrwxrwxrwx 1 root root 53 11月 13 22:21 v210_rootfs -> /home/fly/project/x210bv3s/busybox-1.35.0/x210_rootfs
```

----

###### 3-4-7-1.挂载失败1

```bash
[    8.172292] Root-NFS: Server returned error -13 while mounting /opt/v210_rootfs
[    8.178172] VFS: Unable to mount root fs via NFS, trying floppy.
[    8.184379] VFS: Cannot open root device "nfs" or unknown-block(2,0)
[    8.190438] Please append a correct "root=" boot option; here are the available partitions:
[    8.198778] b300         3817472 mmcblk0 driver: mmcblk
[    8.203935]   b301          264759 mmcblk0p1
[    8.208180]   b302          264759 mmcblk0p2
[    8.212426]   b303          104412 mmcblk0p3
[    8.216672]   b304         3158463 mmcblk0p4
[    8.220920] b308         7761920 mmcblk1 driver: mmcblk
[    8.226131]   b309         7757824 mmcblk1p1
[    8.230364] Kernel panic - not syncing: VFS: Unable to mount root fs on unknown-block(2,0)
[    8.238611] Backtrace:
[    8.241043] [<c0037fb8>] (dump_backtrace+0x0/0x110) from [<c0509828>] (dump_stack+0x18/0x1c)
[    8.249445]  r6:00008000 r5:dfecc000 r4:c0701b6c r3:00000002
[    8.255065] [<c0509810>] (dump_stack+0x0/0x1c) from [<c05098a4>] (panic+0x78/0xf8)
[    8.262751] [<c050982c>] (panic+0x0/0xf8) from [<c0008fa8>] (mount_block_root+0x1d8/0x218)
[    8.270849]  r3:00000002 r2:00000001 r1:dfc37f60 r0:c064f8aa
[    8.276468] [<c0008dd0>] (mount_block_root+0x0/0x218) from [<c00090ac>] (mount_root+0xc4/0xf8)
[    8.285068] [<c0008fe8>] (mount_root+0x0/0xf8) from [<c0009244>] (prepare_namespace+0x164/0x1bc)
[    8.293812]  r5:c002b331 r4:c073d900
[    8.297352] [<c00090e0>] (prepare_namespace+0x0/0x1bc) from [<c00084fc>] (kernel_init+0x128/0x170)
[    8.306294]  r5:c00083d4 r4:c073d6c0
[    8.309836] [<c00083d4>] (kernel_init+0x0/0x170) from [<c005b894>] (do_exit+0x0/0x5f0)
[    8.317730]  r4:00000000 r3:00000000
[    8.321268] Rebooting in 5 seconds..
```

--------------

> 重启NFS服务后挂载OK。

```bash
fly@fly-vm:opt$ sudo /etc/init.d/nfs-kernel-server restart
[ ok ] Restarting nfs-kernel-server (via systemctl): nfs-kernel-server.service.
```

--------

```bash
[    7.127198] IP-Config: Complete:
[    7.128768]      device=eth0, addr=192.168.0.188, mask=255.255.255.0, gw=192.168.0.1,
[    7.136482]      host=192.168.0.188, domain=, nis-domain=(none),
[    7.142477]      bootserver=192.168.0.105, rootserver=192.168.0.105, rootpath=
[    7.150039] DBUG_PORT must not use AFC!
[    7.153788] Looking up port of RPC 100003/2 on 192.168.0.105
[    7.806775] eth0: link up, 100Mbps, full-duplex, lpa 0x4DE1
[    7.810990] ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
[    8.273117] Looking up port of RPC 100005/1 on 192.168.0.105
[    8.391554] VFS: Mounted root (nfs filesystem) on device 0:12.
[    8.395963] Freeing init memory: 172K
can't run '/etc/init.d/rcS': Permission denied

Please press Enter to activate this console.
[root@192 ]#
```

---------

###### 3-4-7-2.挂载后报异常

```bash
can't run '/etc/init.d/rcS': Permission denied

Please press Enter to activate this console.
[root@192 ]#
```

-------------

修改文件的权限

```bash
fly@fly-vm:v210_rootfs$ sudo chown root:root etc/*
fly@fly-vm:v210_rootfs$ cd etc/
fly@fly-vm:etc$ ls -l
total 32
-rw-r--r-- 1 root root  532 11月 13 21:30 fstab
-rw-r--r-- 1 root root  250 11月 13 21:30 group
-rw-r--r-- 1 root root    9 11月 13 21:30 hostname
drwxrwxr-x 2 root root 4096 11月 13 22:05 init.d
-rw-r--r-- 1 root root  203 11月 13 21:30 inittab
-rw-r--r-- 1 root root  596 11月 13 21:30 passwd
-rw-r--r-- 1 root root  211 11月 13 21:30 profile
-rw------- 1 root root  380 11月 13 22:14 shadow
fly@fly-vm:etc$ cd init.d/
fly@fly-vm:init.d$ ls
rcS
fly@fly-vm:init.d$ sudo chmod 755 rcS
fly@fly-vm:init.d$ ls -l
total 4
-rwxr-xr-x 1 fly fly 350 11月 13 21:30 rcS
```

---------

重新挂载后log

```bash
[    7.145998] Looking up port of RPC 100003/2 on 192.168.0.105
[    7.736455] eth0: link up, 100Mbps, full-duplex, lpa 0x4DE1
[    7.740668] ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
[    8.268544] Looking up port of RPC 100005/1 on 192.168.0.105
[    8.292823] VFS: Mounted root (nfs filesystem) on device 0:12.
[    8.297229] Freeing init memory: 172K

X210BV3S FileSystem is Ready ...


Please press Enter to activate this console.
[root@x210bv3s ]#
```

-----------

**至此，一个最简版的`rootfs`根文件系统就制作完成了。**

--------------------

## 4.参考与拓展

### 4-1.复制`lib`库

#### 4-1-1.执行测试应用程序报错

```bash
[root@x210bv3s bin]# ./lcdTest.bin
-/bin/sh: ./lcdTest.bin: not found
[root@x210bv3s bin]# ls -l lcdTest.bin
-rwxrwxr-x    1 ls: /etc/group: bad record
default  default       6662 Nov 17  2022 lcdTest.bin
```

-------------

#### 4-1-2.查看交叉编译工具链

```bash
fly@fly-vm:busybox-1.35.0$ grep "CROSS_COMPILE" ./Makefile -rHn
./Makefile:155:# CROSS_COMPILE specify the prefix used for all executables used
./Makefile:157:# are prefixed with $(CROSS_COMPILE).
./Makefile:158:# CROSS_COMPILE can be set on the command line
./Makefile:159:# make CROSS_COMPILE=ia64-linux-
./Makefile:160:# Alternatively CROSS_COMPILE can be set in the environment.
./Makefile:161:# Default value for CROSS_COMPILE is not to prefix executables
./Makefile:162:# Note: Some architectures assign CROSS_COMPILE in their arch/*/Makefile
./Makefile:164:#CROSS_COMPILE ?=
./Makefile:165:CROSS_COMPILE      ?= /usr/local/arm/arm-2009q3/bin/arm-none-linux-gnueabi-
./Makefile:166:# bbox: we may have CONFIG_CROSS_COMPILER_PREFIX in .config,
./Makefile:168:ifeq ($(CROSS_COMPILE),)
./Makefile:169:CROSS_COMPILE := $(shell grep ^CONFIG_CROSS_COMPILER_PREFIX .config 2>/dev/null)
./Makefile:170:CROSS_COMPILE := $(subst CONFIG_CROSS_COMPILER_PREFIX=,,$(CROSS_COMPILE))
./Makefile:171:CROSS_COMPILE := $(subst ",,$(CROSS_COMPILE))
./Makefile:181:ifneq ($(CROSS_COMPILE),)
./Makefile:182:SUBARCH := $(shell echo $(CROSS_COMPILE) | cut -d- -f1 | sed 's:^.*/::g')
./Makefile:293:AS               = $(CROSS_COMPILE)as
./Makefile:294:CC               = $(CROSS_COMPILE)gcc
./Makefile:297:AR               = $(CROSS_COMPILE)ar
./Makefile:298:NM               = $(CROSS_COMPILE)nm
./Makefile:299:STRIP            = $(CROSS_COMPILE)strip
./Makefile:300:OBJCOPY          = $(CROSS_COMPILE)objcopy
./Makefile:301:OBJDUMP          = $(CROSS_COMPILE)objdump
./Makefile:302:PKG_CONFIG       ?= $(CROSS_COMPILE)pkg-config
./Makefile:334: ARCH CONFIG_SHELL HOSTCC HOSTCFLAGS CROSS_COMPILE AS LD CC \
```

-----------------------

#### 4-1-3.复制库到根文件系统中

```bash
fly@fly-vm:arm-2009q3$ cd arm-none-linux-gnueabi/libc/lib/
fly@fly-vm:lib$ ls
ld-2.10.1.so               libdl-2.10.1.so          libnss_dns.so.2           libresolv-2.10.1.so
ld-linux.so.3              libdl.so.2               libnss_files-2.10.1.so    libresolv.so.2
libanl-2.10.1.so           libgcc_s.so              libnss_files.so.2         librt-2.10.1.so
libanl.so.1                libgcc_s.so.1            libnss_hesiod-2.10.1.so   librt.so.1
libBrokenLocale-2.10.1.so  libm-2.10.1.so           libnss_hesiod.so.2        libSegFault.so
libBrokenLocale.so.1       libmemusage.so           libnss_nis-2.10.1.so      libthread_db-1.0.so
libc-2.10.1.so             libm.so.6                libnss_nisplus-2.10.1.so  libthread_db.so.1
libcidn-2.10.1.so          libnsl-2.10.1.so         libnss_nisplus.so.2       libutil-2.10.1.so
libcidn.so.1               libnsl.so.1              libnss_nis.so.2           libutil.so.1
libcrypt-2.10.1.so         libnss_compat-2.10.1.so  libpcprofile.so
libcrypt.so.1              libnss_compat.so.2       libpthread-2.10.1.so
libc.so.6                  libnss_dns-2.10.1.so     libpthread.so.0
fly@fly-vm:lib$ cp *so* /opt/v210_rootfs/lib -rdf
```

-------------------

#### 4-1-4.为库文件瘦身

```bash
fly@fly-vm:lib$ du -sh ./
3.8M    ./
fly@fly-vm:lib$ arm-linux-strip *so*
fly@fly-vm:lib$ du -sh ./
3.0M    ./
```

--------------------

### 4-2.简单的linux应用测试

#### 4-2-1.hello程序

```c
#include <stdio.h>

int main(int argc, char* argv[])
{
    printf("Hello, world.\n");

    return 0;
}
```

##### 4-2-1-1.编译

```bash
fly@fly-vm:simple$ make hello
arm-linux-gcc -o hello hello.c -g -Wall
```

##### 4-2-1-2.测试

```bash
[root@x210bv3s bin]# hello
Hello, world.
```

#### 4-2-2.`LCD`测试程序

```c
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>

// 宏定义
#define FBDEVICE        "/dev/fb0"

// 新开发板LCD分辨率
#define WIDTH           1024
#define HEIGHT          600

#define WHITE           0xffffffff                      // test ok
#define BLACK           0x00000000
#define RED                     0xffff0000
#define GREEN           0xff00ff00                      // test ok
#define BLUE            0xff0000ff

#define GREENP          0x0000ff00                      // 一样，说明前2个ff透明位不起作用

// 函数声明
void draw_back(unsigned int width, unsigned int height, unsigned int color);
void draw_line(unsigned int color);

// 全局变量
unsigned int *pfb = NULL;

int main(void)
{
    int fd = -1, ret = -1;

    struct fb_fix_screeninfo finfo = {0};
    struct fb_var_screeninfo vinfo = {0};

    // 第1步：打开设备
    fd = open(FBDEVICE, O_RDWR);
    if (fd < 0)
    {
        perror("open");
        return -1;
    }
    printf("open %s success.\n", FBDEVICE);

    // 第2步：获取设备的硬件信息
    ret = ioctl(fd, FBIOGET_FSCREENINFO, &finfo);
    if (ret < 0)
    {
        perror("ioctl");
        return -1;
    }

    printf("smem_start = 0x%lx, smem_len = %u.\n", finfo.smem_start, finfo.smem_len);

    ret = ioctl(fd, FBIOGET_VSCREENINFO, &vinfo);
    if (ret < 0)
    {
        perror("ioctl");
        return -1;
    }

    printf("xres = %u, yres = %u.\n", vinfo.xres, vinfo.yres);
    printf("xres_virtual = %u, yres_virtual = %u.\n", vinfo.xres_virtual, vinfo.yres_virtual);
    printf("bpp = %u.\n", vinfo.bits_per_pixel);

    // 修改驱动中屏幕的分辨率
    vinfo.xres = 1024;
    vinfo.yres = 600;
    vinfo.xres_virtual = 1024;
    vinfo.yres_virtual = 1200;
    ret = ioctl(fd, FBIOPUT_VSCREENINFO, &vinfo);
    if (ret < 0)
    {
        perror("ioctl");
        return -1;
    }

    // 再次读出来检验一下
    ret = ioctl(fd, FBIOGET_VSCREENINFO, &vinfo);
    if (ret < 0)
    {
        perror("ioctl");
        return -1;
    }

    printf("修改过之后的参数：\n");
    printf("xres = %u, yres = %u.\n", vinfo.xres, vinfo.yres);
    printf("xres_virtual = %u, yres_virtual = %u.\n", vinfo.xres_virtual, vinfo.yres_virtual);
    printf("bpp = %u.\n", vinfo.bits_per_pixel);

    // 第3步：进行mmap
    unsigned long len = vinfo.xres_virtual * vinfo.yres_virtual * vinfo.bits_per_pixel / 8;
    printf("len = %ld\n", len);
    pfb = mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (NULL == pfb)
    {
        perror("mmap");
        return -1;
    }
    printf("pfb = %p.\n", pfb);

    draw_back(WIDTH, HEIGHT, GREEN);
    draw_line(RED);

    close(fd);

    return 0;
}

void draw_back(unsigned int width, unsigned int height, unsigned int color)
{
    unsigned int x, y;

    for (y=0; y<height; y++)
    {
        for (x=0; x<width; x++)
        {
            *(pfb + y * WIDTH + x) = color;
        }
    }
}

void draw_line(unsigned int color)
{
    unsigned int x/*, y*/;

    for (x=50; x<600; x++)
    {
        *(pfb + 200 * WIDTH + x) = color;
    }
}
```

-----------

###### 4-2-2-1.编译

```bash
fly@fly-vm:simple$ make lcd_test
arm-linux-gcc -o lcd_test lcd_test.c -g -Wall
lcd_test.c: In function 'main':
lcd_test.c:42: warning: missing braces around initializer
lcd_test.c:42: warning: (near initialization for 'finfo.id')
fly@fly-vm:simple$ make cp
cp  hello  lcd_test ../../x210_rootfs/usr/bin/
```

###### 4-2-2-2.测试

```bash
[root@x210bv3s bin]# lcd_test
open /dev/fb0 success.
smem_start = 0x475ab000, smem_len = 4915200.
xres = 1024, yres = 600.
xres_virtual = 1024, yres_virtual = 1200.
bpp = 32.
修改过之后的参数：
xres = 1024, yres = 600.
xres_virtual = 1024, yres_virtual = 1200.
bpp = 32.
len = 4915200
pfb = 0x40183000.
```

-----------------------

### 4-3.拓展

 #### 4-3-1.[根文件系统构建实验及过程详解](https://gitee.com/x210bv3s/v210-note_code/blob/master/%E8%AF%BE%E4%BB%B6/2.uboot%E5%92%8Clinux%E5%86%85%E6%A0%B8%E7%A7%BB%E6%A4%8D/2.19.%E6%A0%B9%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F%E6%9E%84%E5%BB%BA%E5%AE%9E%E9%AA%8C%E5%8F%8A%E8%BF%87%E7%A8%8B%E8%AF%A6%E8%A7%A3/%E8%AF%BE%E4%BB%B6_2.19.%E6%A0%B9%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F%E6%9E%84%E5%BB%BA%E5%AE%9E%E9%AA%8C%E5%8F%8A%E8%BF%87%E7%A8%8B%E8%AF%A6%E8%A7%A3.txt)

 #### 4-3-2.[根文件系统构建](https://blog.csdn.net/weixin_47397155/article/details/120668438)

 #### 4-3-3.[嵌入式交叉编译器，`uboot`,`kernel`,根文件系统，`tslib`,qt编译配置](https://www.itdaan.com/blog/2015/08/05/df7043e150582735f2872ccdbe97eb2c.html)

 #### 4-3-4.`x210v3`开发板裸机教程.pdf

 #### 4-3-5.`x210v3` `linux`平台用户手册.pdf

 #### 4-3-6.[`linux` 查看动态库和可执行程序依赖库](https://blog.csdn.net/song240948380/article/details/124226816)

 #### 4-3-7.[根文件系统的原理](https://gitee.com/x210bv3s/v210-note_code/blob/master/%E8%AF%BE%E4%BB%B6/2.uboot%E5%92%8Clinux%E5%86%85%E6%A0%B8%E7%A7%BB%E6%A4%8D/2.18.%E6%A0%B9%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F%E7%9A%84%E5%8E%9F%E7%90%86/%E8%AF%BE%E4%BB%B6_2.18.%E6%A0%B9%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F%E7%9A%84%E5%8E%9F%E7%90%86.txt)

#### 4-3-8.[文件系统知识点-制作根文件系统](https://download.csdn.net/download/I_feige/87069326)

> 1.`inittab`表`action`说明； 
>
> 2.嵌入式相关文件系统：`autofs`、`cramfs`、`squashfs`、`JFFS2`、`YAFF2`、`ubifs`、`ramdisk`、`ramfs`、`tmpfs`、`procfs`、`sysfs`； 
>
> 3.`windows`文件系统：`fat`、`FAT32`、`NTFS`、`EXT3`、`EXT4`;

#### 4-3-9.[`s5pv210`根文件系统制作说明](https://download.csdn.net/download/I_feige/87069361)

### 4-4.简单驱动模块测试

#### 4-4-1.源码

```c
/*******************************************************************
 *   > File Name: module_test.c
 *   > Author: fly
 *   > Mail: 1358326274@qq.com
 *   > Create Time: 2022年11月19日 星期六 14时17分26秒
 ******************************************************************/

#include <linux/module.h>
#include <linux/init.h>

static int __init chrdev_init(void)
{
    printk(KERN_INFO"chardev_init hello world init.\n");
    return 0;
}

static void __exit chrdev_exit(void)
{
    printk(KERN_INFO"chrdev_exit hello world exit.\n");
}

module_init(chrdev_init);
module_exit(chrdev_exit);
 
MODULE_LICENSE("GPL");
MODULE_AUTHOR("flyer");
MODULE_DESCRIPTION("module test");
MODULE_ALIAS("alias xxx");
```

#### 4-4-2.Makefile

```makefile
#ubuntu的内核源码树，如果要编译在ubuntu中安装的模块就打开这2个
#KERN_VER = $(shell uname -r)
#KERN_DIR = /lib/modules/$(KERN_VER)/build	

		
# 开发板的linux内核的源码树目录
KERN_DIR = /home/fly/project/x210bv3s/qt_x210v3s_160307/kernel

obj-m	+= module_test.o

all:
	make -C $(KERN_DIR) M=`pwd` modules 

cp:
	cp *.ko ../../x210_rootfs/usr/driver_test

.PHONY: clean	
clean:
	make -C $(KERN_DIR) M=`pwd` modules clean
```

-------------

#### 4-4-3.编译

```bash
fly@fly-vm:simple$ make
make -C /home/fly/project/x210bv3s/qt_x210v3s_160307/kernel M=`pwd` modules
make[1]: Entering directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/kernel'
  CC [M]  /home/fly/project/x210bv3s/busybox-1.35.0/linux-drv/simple/module_test.o
  Building modules, stage 2.
  MODPOST 1 modules
  CC      /home/fly/project/x210bv3s/busybox-1.35.0/linux-drv/simple/module_test.mod.o
  LD [M]  /home/fly/project/x210bv3s/busybox-1.35.0/linux-drv/simple/module_test.ko
make[1]: Leaving directory '/home/fly/project/x210bv3s/qt_x210v3s_160307/kernel'
fly@fly-vm:simple$ vim Makefile
fly@fly-vm:simple$ make cp
cp *.ko ../../x210_rootfs/usr/driver_test
fly@fly-vm:simple$ ls
Makefile       Module.symvers  module_test.ko     module_test.mod.o
modules.order  module_test.c   module_test.mod.c  module_test.o
```

-------------

#### 4-4.4、安装、测试

```bash
[root@x210bv3s ]# cd /usr/driver_test/
[root@x210bv3s driver_test]# ls
module_test.ko
[root@x210bv3s driver_test]# insmod module_test.ko
[ 5265.453968] chardev_init hello world init.
[root@x210bv3s driver_test]# lsmod
Module                  Size  Used by    Not tainted
module_test              588  0
[root@x210bv3s driver_test]# modinfo module_test.ko
modinfo: can't open '/lib/modules/2.6.35.7+/modules.dep': No such file or directory
[root@x210bv3s driver_test]# rmmod module_test.ko
[ 5326.782614] chrdev_exit hello world exit.
[root@x210bv3s driver_test]# lsmod
Module                  Size  Used by    Not tainted
```

