## `S5PV210`  |  裸机按键检测、控制外设实验

-----

[TOC]



- ### 电路原理

<img src="https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/HW-KEY.PNG" alt="HW-KEY" style="zoom: 67%;" />

> **对应的`GPIO`口默认硬件上拉，有按键按下时对应的`IO`口变为低电平，将对应的`GPIO`口设置为输入，检测对应的电平即可检测按键的动作。**

> **将`LED`、`BEEP`、`KEY`相关的`GPIO`都初始化，然后进入`while（1）`死循环，不停的判断按键的动作，一旦有按键按下，执行对应的`if-else`语句，然后控制对应的`LED`、`BEEP`。**

- ### `DATASHEET`（寄存器详解）

- #### 2.2.56 `GPH0`控制寄存器端口组

> **`GPH0`控制寄存器组包含4个控制寄存器：`GPH0CON`, `GPH0DAT`, `GPH0PUD` 和 `GPH0DRV`。**

- ##### 2.2.56.1 Port Group GPH0 Control Register (GPH0CON, R/W, Address = 0xE020_0C00)  

![GPH0CON](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/GPH0CON.PNG)

- ##### 2.2.56.2 Port Group GPH0 Control Register (GPH0DAT, R/W, Address = 0xE020_0C04)  

![GPH0DAT](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/GPH0DAT.PNG)

- ##### 2.2.56.3 Port Group GPH0 Control Register (GPH0PUD, R/W, Address = 0xE020_0C08)  

![GPH0DRV](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/GPH0DRV.PNG)

- ##### 2.2.56.4 Port Group GPH0 Control Register (GPH0DRV, R/W, Address = 0xE020_0C0C)  

![GPH0PUD](https://gitee.com/x210bv3s/s5pv210-noos-dev/raw/master/md-doc/res/GPH0PUD.PNG)

- #### 2.2.58 PORT GROUP GPH2 CONTROL REGISTER  

>  GPH2寄存器组详解（此处省略，具体可查看文档S5PV210_UM_REV1.1.pdf 中相关部分）



- ### 代码编写

- #### demo源码工程目录详解

```bash
06-key/
├── Makefile
└── source
    ├── beep.c
    ├── include
    │   ├── beep.h
    │   ├── include_all.h
    │   ├── key.h
    │   └── led.h
    ├── key.c
    ├── led.c
    ├── main.c
    └── start.S

2 directories, 10 files
```
> 其他（led、beep及start.S等）部分代码请参考之前章节（裸机LED/BEEP）。

------

```c
/*****************************************************
 *   > File Name: key.c
 *   > Author: fly
 *   > Create Time: 2021-07-02  5/26  13:57:16 +0800
 *==================================================*/
#include <key.h>
#include <led.h>
#include <beep.h>

typedef struct{
    unsigned int GPH0CON;
    unsigned int GPH0DAT;
    unsigned int GPH0PUD;
    unsigned int GPH0DRV;
}gph0;
#define GPH0    (*(volatile gph0 *)0xE0200C00)

typedef struct{
    unsigned int GPH2CON;
    unsigned int GPH2DAT;
    unsigned int GPH2PUD;
    unsigned int GPH2DRV;
}gph2;
#define GPH2    (*(volatile gph2 *)0xE0200C40)

void key_init(void)
{
    /*
     * POWER -> EINT1   -> GPH0_1
     * LEFT  -> EINT2   -> GPH0_2
     * DOWN  -> EINT3   -> GPH0_3
     * UP    -> KP_COL0 -> GPH2_0
     * RIGHT -> KP_COL1 -> GPH2_1
     * BACK  -> KP_COL2 -> GPH2_2
     * MENU  -> KP_COL3 -> GPH2_3
     */
    GPH0.GPH0CON = (GPH0.GPH0CON & ~(0xf<<4)) | (0x0<<4);
    GPH0.GPH0CON = (GPH0.GPH0CON & ~(0xf<<8)) | (0x0<<8);
    GPH0.GPH0CON = (GPH0.GPH0CON & ~(0xf<<12)) | (0x0<<12);

    GPH2.GPH2CON = (GPH2.GPH2CON & ~(0xf<<0)) | (0x0<<0);
    GPH2.GPH2CON = (GPH2.GPH2CON & ~(0xf<<4)) | (0x0<<4);
    GPH2.GPH2CON = (GPH2.GPH2CON & ~(0xf<<8)) | (0x0<<8);
    GPH2.GPH2CON = (GPH2.GPH2CON & ~(0xf<<12)) | (0x0<<12);
}

unsigned int keyPower_value(void)
{
    return (GPH0.GPH0DAT & (0x1<<1));
}

unsigned int key1_value(void)
{
    return (GPH0.GPH0DAT & (0x1<<2));
}

unsigned int key2_value(void)
{
    return (GPH0.GPH0DAT & (0x1<<3));
}

unsigned int key3_value(void)
{
    return (GPH2.GPH2DAT & (0x1<<0));
}

unsigned int key4_value(void)
{
    return (GPH2.GPH2DAT & (0x1<<1));
}

unsigned int key5_value(void)
{
    return (GPH2.GPH2DAT & (0x1<<2));
}

unsigned int key6_value(void)
{
    return (GPH2.GPH2DAT & (0x1<<3));
}

void tester_key(void)
{
    key_init();
    led_init();
    beep_init();

    while(1){
        if(!key1_value()){
            led_set_led1(1);
        }else{
            led_set_led1(0);
        }

        if(!key2_value()){
            led_set_led2(1);
        }else{
            led_set_led2(0);
        }

        if(!key3_value()){
            led_set_led3(1);
        }else{
            led_set_led3(0);
        }

        if(!key4_value()){
            led_set_led4(1);
        }else{
            led_set_led4(0);
        }

        if(!key5_value()){
            led_set_all_led(1);
        }else{
            //led_set_all_led(0);
        }

        if(!key6_value()){
            beep_set(1);
        }else{
            beep_set(0);
        }
#if 0
        if(keyPower_value())
        {
            beep_set(0);
        }
#endif /* 测试NG */        
    }
}
```

------

```c
/***************************************************
 *   > File Name: main.c
 *   > Author: fly
 *   > Create Time: 2021-07-02  5/26  12:45:25 +0800
 *==================================================*/
#include <include_all.h>

int main(int argc, char* argv[])
{
    tester_key();
    return 0;
}
```


------

- ### Makefile文件：

```makefile
# 将所有的.o文件链接成.elf文件，“-Ttext 0x0”
# 表示程序的运行地址是0x0，由于目前编写的是位置
# 无关码，可以在任一地址运行
# 将elf文件抽取为可在开发板上运行的bin文件
# 将elf文件反汇编保存在dis文件中，调试程序会用
# 添加文件头
.PHONY: all clean tools

CROSS   ?= arm-linux-
NAME    := KEY

LD              := $(CROSS)ld
OC              := $(CROSS)objcopy
OD              := $(CROSS)objdump
CC              := $(CROSS)gcc
MK              := ../../tools/mk_image/mkv210_image
CFLAGS  := -nostdlib -Wall

INCDIR  := ./source/include
SRCDIR  := ./source
OBJS    := start.o \
           beep.o \
           led.o \
           key.o \
           main.o

OUTDIR  := output

all:$(NAME).bin

$(NAME).bin : $(OBJS)
        $(LD) -Ttext 0x0 -o $(NAME).elf $^
        $(OC) -O binary $(NAME).elf $(NAME).bin
        $(OD) -D $(NAME).elf > $(NAME)_elf.dis
        $(MK) $(NAME).bin

# 将当前目录下存在的汇编文件及C文件编译成.o文件
%.o : $(SRCDIR)/%.S
        $(CC) -o $@ $< -c $(CFLAGS) -I $(INCDIR)
%.o : $(SRCDIR)/%.c
        $(CC) -o $@ $< -c $(CFLAGS) -I $(INCDIR)

clean:
        $(RM) *.o *.elf *.bin *.dis *.sd

tools:
        make -C ../../tools/mk_image/
```

