## `S5PV210` | `Linux` `SDK`[开发及说明](https://www.cnblogs.com/feige1314/p/17586440.html)

----

**时间：**`2023-07-10`

[TOC]



## 1.参考

> 1.[S5PV210- - hello,123456 - 博客园 (cnblogs.com)](https://www.cnblogs.com/feige1314/collections/1265)
>
> 2.[5.ARM裸机_1234567890@world的博客-CSDN博客](https://blog.csdn.net/i_feige/category_6738192.html?spm=1001.2014.3001.5482)
>
> 3.[6.Linux驱动及内核开发_1234567890@world的博客-CSDN博客](https://blog.csdn.net/i_feige/category_6738193.html)
>
> 4.[x210bv3s: ARM Cortex-A8 （s5pv210）的开发与学习 硬件版本：（九鼎）X210BV3S 20160513 ](https://gitee.com/x210bv3s)
>
> 5.[x210bv3s · GitCode](https://gitcode.net/project1/x210bv3s)
>
> 6.[movi命令（do_movi函数的源码分析）_movi指令_天糊土的博客-CSDN博客](https://blog.csdn.net/oqqHuTu12345678/article/details/127728162)
>
> 7.[(4条消息) linux内核nfs挂载文件系统崩溃-20220112-服务器-CSDN问答](https://ask.csdn.net/questions/7629284)
>
> 8.[Windows下的Tftpd32(Tftpd64)软件下载和使用教程-集成了Tftp服务器、客户端_tftp工具_wkd_007的博客-CSDN博客](https://blog.csdn.net/wkd_007/article/details/129018831)
>
> 9.[v210-note_code: 课件和代码 (gitee.com)](https://gitee.com/x210bv3s/v210-note_code)
>
> 10.[项目管理 - PingCode](https://jmyl20230915231323500.pingcode.com/pjm/projects/S5PV210/kanban/6504ff0688f6abbf29a7510d)

## 2.代码编译

### `2-1`.`UBOOT`

#### 2-1-1.编译`UBOOT`

> **`SDK`**代码：`git@gitee.com:x210bv3s/qt_x210v3s_160307.git`
>
> 通过如下步骤编译`uboot`，然后通过`x210_Fusing_Tool`工具烧录bin文件到`SD`卡；使用`SD`卡中的`uboot`启动机器；

```bash
# 进入uboot源码目录
[fly@fly-vm qt_x210v3s_160307]$ cd uboot/
/home/fly/project/x210bv3s/qt_x210v3s_160307/uboot
# 清理uboot
 make clean
# 配置uboot
 make config
# 编译
 make -j8
# 添加校验头
make head
# 复制uboot到tftpboot目录
sudo cp u-boot.bin /tftpboot/
```

#### 2-1-2.`UBOOT`环境变量设置

> 使用`UBOOT`启动`SD`卡启动开发板，通过串口进入`uboot`命令行；

##### 打印环境变量

```bash
x210 # printenv
baudrate=115200
ethact=dm9000
ethaddr=00:40:5c:26:0a:5b
mtdpart=80000 400000 3000000
bootdelay=2
filesize=60000
fileaddr=C0008000
netmask=255.255.255.0
gatewayip=192.168.0.1
ipaddr=192.168.0.188
bootcmd=tftp 30008000 zImage;bootm 30008000
serverip=192.168.0.104
bootargs=root=/dev/nfs nfsroot=192.168.0.104:/opt/v210_rootfs ip=192.168.0.188:192.168.0.104:192.168.0.1:255.255.255.0::eth0:off init=/linuxrc console=ttySAC2,115200
```

---

##### 设置环境变量

###### 开发机网络配置

![image](https://img2023.cnblogs.com/blog/1069917/202307/1069917-20230711221220414-1727309455.png)
![image](https://img2023.cnblogs.com/blog/1069917/202307/1069917-20230711221302204-1099469159.png)

-----

```bash
# 设置网关
setenv gatewayip 192.168.1.1
# 设置开发板IP
setenv ipaddr 192.168.1.100
# 设置服务器IP
setenv serverip 192.168.1.12
# 设置启动参数
setenv bootargs 'root=/dev/nfs nfsroot=192.168.1.12:/opt/v210_rootfs ip=192.168.1.100:192.168.1.12:192.168.1.1:255.255.255.0::eth0:off  init=/linuxrc console=ttySAC2,115200'
# 保存环境变量
saveenv
Saving Environment to SMDK bootable device...
done
```

##### 通过`tftp`下载`uboot`

```bash
tftp 0x40008000 u-boot.bin
```

##### 烧录`UBOOT`

```bash
# 将`DDR`内存地址`0x40008000`处的内容写入到`uboot`分区  
movi write u-boot 0x40008000
```



### `2-2`.`Kernel`

#### 2-2-1.编译KERNEL

> 编译完的`kernel`位于`arch/arm/boot/zImage`目录中，复制到`TFTP`目录，可通过`TFTP`下载内核到开发板；

```bash
# 进入linux kernel源码目录
[fly@fly-vm qt_x210v3s_160307]$ cd kernel/
/home/fly/project/x210bv3s/qt_x210v3s_160307/kernel
# 清理
make distclean
# 配置
make v210_defconfig
# 编译
 make -j8
# 复制内核镜像
sudo cp arch/arm/boot/zImage /tftpboot
```

#### 2-2-2.烧录内核

```bash
 tftp 0x40008000 zImage
 movi write kernel 0x40008000
```

### `2-3`.`rootfs`

#### `2-3-1`.挂载根文件系统

> 1.[Linux开发 | 安装、配置、测试NFS和TFTP服务器 - hello,123456 - 博客园 (cnblogs.com)](https://www.cnblogs.com/feige1314/p/17586428.html)

```bash
# 工作路径
[fly@fly-vm busybox-1.35.0]$ pwd
/home/fly/project/x210bv3s/busybox-1.35.0
# 远程仓库
[fly@fly-vm busybox-1.35.0]$ git remote -v
origin	git@gitee.com:x210bv3s/busybox-1.35.0.git (fetch)
origin	git@gitee.com:x210bv3s/busybox-1.35.0.git (push)
# 安装
[fly@fly-vm busybox-1.35.0]$ make x210_install 
install rootfs
/home/fly/project/x210bv3s/busybox-1.35.0
sudo ln -s /home/fly/project/x210bv3s/busybox-1.35.0/x210_rootfs /opt/x210_rootfs
```

#### `2-3-2`.编译文件系统

##### 参考资料

> 1.[md-doc/10-S5PV210上进行Linux开发.md · x210bv3s/s5pv210-noos-dev - 码云 - 开源中国 ](https://gitee.com/x210bv3s/s5pv210-noos-dev/blob/master/md-doc/10-S5PV210上进行Linux开发.md)
>
> 2.[md-doc/14-s5pv210-使用BusyBox制作根文件系统.md · x210bv3s/s5pv210-noos-dev - 码云 - 开源中国](https://gitee.com/x210bv3s/s5pv210-noos-dev/blob/master/md-doc/14-s5pv210-使用BusyBox制作根文件系统.md)
>
> 3.[md-doc/15-根文件系统的制作与使用.md · x210bv3s/s5pv210-noos-dev - 码云 - 开源中国](https://gitee.com/x210bv3s/s5pv210-noos-dev/blob/master/md-doc/15-根文件系统的制作与使用.md)
>
> 4.[md-doc/16-根文件系统详情.md · x210bv3s/s5pv210-noos-dev - 码云 - 开源中国 ](https://gitee.com/x210bv3s/s5pv210-noos-dev/blob/master/md-doc/16-根文件系统详情.md)
>
> 5.[课件/2.uboot和linux内核移植/2.19.根文件系统构建实验及过程详解](https://gitee.com/x210bv3s/v210-note_code/tree/master/课件/2.uboot和linux内核移植/2.19.根文件系统构建实验及过程详解)
>
> 6.[课件/2.uboot和linux内核移植 · x210bv3s/v210-note_code - 码云 - 开源中国 (gitee.com)](https://gitee.com/x210bv3s/v210-note_code/tree/master/课件/2.uboot和linux内核移植)
>
> 7.[课件/2.uboot和linux内核移植/2.20.buildroot的引入和介绍/编译buildroot中的错误及解决方案记录.txt ](https://gitee.com/x210bv3s/v210-note_code/blob/master/课件/2.uboot和linux内核移植/2.20.buildroot的引入和介绍/编译buildroot中的错误及解决方案记录.txt)
>
> 8.[课件/2.uboot和linux内核移植/2.20.buildroot的引入和介绍/课件_2.20.buildroot的引入和介绍.txt ](https://gitee.com/x210bv3s/v210-note_code/blob/master/课件/2.uboot和linux内核移植/2.20.buildroot的引入和介绍/课件_2.20.buildroot的引入和介绍.txt)
>
> 9.[linux开发 | 编译buildroot出错及处理](https://blog.csdn.net/I_feige/article/details/119506425)
>
> 10.[S5PV210 | 文件系统制作过程问题解决-CSDN社区](https://bbs.csdn.net/topics/617564088)

##### 编译

```bash
[fly@752fac4b02e9 qt_x210v3s_160307]$ cd buildroot/
[fly@752fac4b02e9 buildroot]$ make x210_defconfig
[fly@752fac4b02e9 buildroot]$ make
```

```bash
[fly@752fac4b02e9 output]$ cd images/
[fly@752fac4b02e9 images]$ pwd
/home/fly/project/x210bv3s/qt_x210v3s_160307/buildroot/output/images
[fly@752fac4b02e9 images]$ ls
rootfs.tar
[fly@752fac4b02e9 images]$ cd ..
[fly@752fac4b02e9 output]$ ls
build  host  images  staging  stamps  target  toolchain
[fly@752fac4b02e9 output]$ cd target/
[fly@752fac4b02e9 target]$ ls
bin  dev  etc  home  lib  linuxrc  media  mnt  opt  proc  root  run  sbin  sys  THIS_IS_NOT_YOUR_ROOT_FILESYSTEM  tmp  usr  var
```

##### 制作镜像

```bash
[fly@752fac4b02e9 qt_x210v3s_160307]$ cp -v buildroot/output/images/rootfs.tar  release/
'buildroot/output/images/rootfs.tar' -> 'release/rootfs.tar'
[fly@464de60d2280 qt_x210v3s_160307]$ sudo ./mk -re
[sudo] password for fly:
making ext3 qt4.8 rootfs now,wait a moment...
262144+0 records in
262144+0 records out
268435456 bytes (268 MB, 256 MiB) copied, 0.392745 s, 683 MB/s
mke2fs 1.42.13 (17-May-2015)
Discarding device blocks: done
Creating filesystem with 262144 1k blocks and 65536 inodes
Filesystem UUID: 955e6bce-c05a-4d52-abd0-c87a79948846
Superblock backups stored on blocks:
        8193, 24577, 40961, 57345, 73729, 204801, 221185

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

^_^ make rootfs_qt4.ext3 successful!
[fly@752fac4b02e9 qt_x210v3s_160307]$ cd release/
[fly@752fac4b02e9 release]$ ls
rootfs  rootfs_img  rootfs_qt4.ext3  rootfs.tar
```

##### 升级文件系统

> 目前，测试通过`tftp`下载文件系统是失败的，文件下载（比较慢）完后，命令行无法操作；
>
> 选择通过`fastboot`烧录，需要克服驱动安装的难题（如下是fastboot烧录文件系统的操作）；
>
> 通过nfs挂载文件系统也比较麻烦，需要搭建服务器，还需要配置网络；

###### fastboot升级文件系统

 ![image](https://img2023.cnblogs.com/blog/1069917/202311/1069917-20231112173924880-1471973203.png)

------

![image](https://img2023.cnblogs.com/blog/1069917/202311/1069917-20231112173936125-441295865.png)

###### sd卡升级文件系统

> 参考：x210v3开发板SD卡烧写教程.pdf
>
> [知识星球 | 开发版光盘资料\X210V3S_A\UserManual](https://wx.zsxq.com/dweb2/index/topic_detail/411522541524118)

![image](https://img2023.cnblogs.com/blog/1069917/202311/1069917-20231113233506968-1984136053.png)


> `sdfuse flash system rootfs_qt4.ext3`
>
> 显示 `sdfuse` 相关信息：`sdfuse info`

![image](https://img2023.cnblogs.com/blog/1069917/202311/1069917-20231113233543022-440693493.png)




##### `uboot`设置启动参数

```bash
setenv bootcmd 'movi read kernel 30008000; movi read rootfs 30B00000 300000; bootm 30008000 30B00000'
setenv bootargs 'console=ttySAC2,115200 root=/dev/mmcblk0p2 rw init=/linuxrc rootfstype=ext3'
```
