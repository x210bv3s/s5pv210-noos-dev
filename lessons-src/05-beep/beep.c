/******************************************************
 *   > File Name: beep.c
 *   > Author: fly
 *   > Create Time: 2021-07-01  4/26  14:52:31 +0800
 *==================================================*/

typedef struct{
    unsigned int GPD0CON;
    unsigned int GPD0DAT;
    unsigned int GPD0PUD;
    unsigned int GPD0DRV;
    unsigned int GPD0CONPDN;
    unsigned int GPD0PUDPDN;
}gpd0;
#define GPD0 (*(volatile gpd0*)0xE02000A0) 

void delay(volatile unsigned int i)
{
    while(i--);
}

void beep_init(void)
{
    GPD0.GPD0CON = (GPD0.GPD0CON & ~(0xf<<8)) | (0x1<<8);
    GPD0.GPD0DAT = (GPD0.GPD0DAT & ~(0x1<<2)) | (0x0<<2);
}

void beep_set(unsigned int value)
{
    if (value){  
        GPD0.GPD0DAT = (GPD0.GPD0DAT & ~(0x1<<2)) | (0x1<<2);//ON
    }else{
        GPD0.GPD0DAT = (GPD0.GPD0DAT & ~(0x1<<2)) | (0x0<<2);//OFF  
    }
}

void tester_beep(void)
{
    beep_init();

    for(;;){
        beep_set(1);
        delay(900000);
        beep_set(0);
        delay(900000);   
    }
}

int main(int argc, char* argv[])
{
    tester_beep();
    return 0;
}
