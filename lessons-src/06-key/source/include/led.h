/***********************************************************
 *   > File Name: led.h
 *   > Author: fly
 *   > Create Time: 2021-07-03  6/26  14:36:57 +0800
 *========================================================*/
#ifndef __LED_H__
#define __LED_H__ 

extern void led_init(void);
extern void led_set_led1(unsigned int value);
extern void led_set_led2(unsigned int value);
extern void led_set_led3(unsigned int value);
extern void led_set_led4(unsigned int value);
extern void led_set_all_led(unsigned int value);

#endif /* __LED_H__ */
