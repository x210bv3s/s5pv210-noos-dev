/*****************************************************
 *   > File Name: key.h
 *   > Author: fly
 *   > Create Time: 2021-07-02  5/26  13:57:16 +0800
 *==================================================*/
#ifndef __KEY_H__
#define __KEY_H__

extern void key_init(void);
extern unsigned int keyPower_value(void);
extern unsigned int key1_value(void);
extern unsigned int key2_value(void);
extern unsigned int key3_value(void);
extern unsigned int key4_value(void);
extern unsigned int key5_value(void);
extern unsigned int key6_value(void);
extern void tester_key(void);

#endif /* __KEY_H__ */