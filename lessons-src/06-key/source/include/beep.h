/*****************************************************
 *   > File Name: beep.h
 *   > Author: fly
 *   > Create Time: 2021-07-02  5/26  12:47:45 +0800
 *==================================================*/
#ifndef __BEEP_H__
#define __BEEP_H__

extern void delay(volatile unsigned int i);
extern void beep_init(void);
extern void beep_set(unsigned int value);
extern void tester_beep(void);

#endif /* __BEEP_H__ */
