/*****************************************************
 *   > File Name: key.c
 *   > Author: fly
 *   > Create Time: 2021-07-02  5/26  13:57:16 +0800
 *==================================================*/
#include <key.h>
#include <led.h>
#include <beep.h>

typedef struct{
    unsigned int GPH0CON;
    unsigned int GPH0DAT;
    unsigned int GPH0PUD;
    unsigned int GPH0DRV;
}gph0;
#define GPH0    (*(volatile gph0 *)0xE0200C00)

typedef struct{
    unsigned int GPH2CON;
    unsigned int GPH2DAT;
    unsigned int GPH2PUD;
    unsigned int GPH2DRV;
}gph2;
#define GPH2    (*(volatile gph2 *)0xE0200C40)

void key_init(void)
{
    /*
     * POWER -> EINT1   -> GPH0_1
     * LEFT  -> EINT2   -> GPH0_2
     * DOWN  -> EINT3   -> GPH0_3
     * UP    -> KP_COL0 -> GPH2_0
     * RIGHT -> KP_COL1 -> GPH2_1
     * BACK  -> KP_COL2 -> GPH2_2
     * MENU  -> KP_COL3 -> GPH2_3
     */
    GPH0.GPH0CON = (GPH0.GPH0CON & ~(0xf<<4)) | (0x0<<4);
    GPH0.GPH0CON = (GPH0.GPH0CON & ~(0xf<<8)) | (0x0<<8); 
    GPH0.GPH0CON = (GPH0.GPH0CON & ~(0xf<<12)) | (0x0<<12); 

    GPH2.GPH2CON = (GPH2.GPH2CON & ~(0xf<<0)) | (0x0<<0); 
    GPH2.GPH2CON = (GPH2.GPH2CON & ~(0xf<<4)) | (0x0<<4);  
    GPH2.GPH2CON = (GPH2.GPH2CON & ~(0xf<<8)) | (0x0<<8); 
    GPH2.GPH2CON = (GPH2.GPH2CON & ~(0xf<<12)) | (0x0<<12);      
}

unsigned int keyPower_value(void)
{
    return (GPH0.GPH0DAT & (0x1<<1));
}

unsigned int key1_value(void)
{
    return (GPH0.GPH0DAT & (0x1<<2));
}

unsigned int key2_value(void)
{
    return (GPH0.GPH0DAT & (0x1<<3)); 
}

unsigned int key3_value(void)
{
    return (GPH2.GPH2DAT & (0x1<<0));   
}

unsigned int key4_value(void)
{
    return (GPH2.GPH2DAT & (0x1<<1));   
}

unsigned int key5_value(void)
{
    return (GPH2.GPH2DAT & (0x1<<2));   
}

unsigned int key6_value(void)
{
    return (GPH2.GPH2DAT & (0x1<<3));   
}

void tester_key(void)
{
    key_init();
    led_init();
    beep_init();

    while(1){
        if(!key1_value()){
            led_set_led1(1);
        }else{
            led_set_led1(0);
        }

        if(!key2_value()){
            led_set_led2(1);
        }else{
            led_set_led2(0);
        }

        if(!key3_value()){
            led_set_led3(1);
        }else{
            led_set_led3(0);
        }

        if(!key4_value()){
            led_set_led4(1);
        }else{
            led_set_led4(0);
        }

        if(!key5_value()){
            led_set_all_led(1);
        }else{
            //led_set_all_led(0);
        }

        if(!key6_value()){
            beep_set(1);
        }else{
            beep_set(0);
        }
        
#if 0   /* 测试NG */
        if(keyPower_value())
        {
            beep_set(0);
        }
#endif
    }   
}
