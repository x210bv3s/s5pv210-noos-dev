/**********************************************************
 *   > File Name: start.S
 *   > Author: fly
 *   > Create Time: 2020年07月17日 星期五 07时56分19秒
 *********************************************************/
#define PS_HOLD_CONTORL 0xE010E81C
#define WTCON           0xE2700000
#define SVC_STACK       0xD0037D80

//#define CONFIG_SYS_ICACHE_OFF   1

.global _start
_start:
    // 给5v电源置锁
    // LDR指令：从内存中将1个32位的字读取到目标寄存器中
    // STR指令：将1个32位的字数据写入到指令中指定的内存单元中
    // ORR指令：逻辑或
    // BIC指令：位清零
    ldr r0,=PS_HOLD_CONTORL     // r0 = 0xE010E81C
    ldr r1,[r0]                 // 将r0地址处的数据读出，保存到r1中（零偏移）
    orr r1,r1,#0x300            // 设置r1的第8、9位，其他位保持不变
    orr r1,r1,#0x1              // 设置r1的第1位，其他位保持不变
    str r1,[r0]                 // 将r1中的内容传输到r0中数指定的地址内存中去

    //关看门狗
    ldr r0, =WTCON			// r0 = 0xE2700000
    mov r1, #0				// r1 = 0
    str r1, [r0]			// 将r1中的内容传输到r0中数指定的地址内存中去

    //开/关iCache
    // MRC指令:从协处理器寄存器传数据到ARM寄存器
    // MCR指令:从ARM寄存器传数据到协处理器寄存器
    mrc p15, 0, r0, c1, c0, 0
    #ifdef CONFIG_SYS_ICACHE_OFF
    bic r0, r0, #0x00001000     @ clear bit 12 (I) I-Cache
    #else
    orr r0, r0, #0x00001000     @ set bit 12 (I) I-Cache
    #endif
    mcr p15, 0, r0, c1, c0, 0

    //设置栈，以便调用c函数
    ldr sp, =SVC_STACK  // sp = 0xD0037D80

    bl clock_init   //初始化时钟

    bl main

    b . /* 死循环 */
