/*********************************************************
 *   > File Name: led.c
 *   > Author: fly
 *   > Mail: xxxxxx@icode.net
 *   > Create Time: 2021-06-26  6/25  21:46:02 +0800
 *======================================================*/
#define GPD0CON         0xE02000A0
#define GPD0DAT         0xE02000A4
#define GPD0PUD         0xE02000A8

#define GPJ0CON         0xE0200240
#define GPJ0DAT         0xE0200244
#define GPJ0PUD         0xE0200248


void delay(volatile unsigned int i)
{
    while(i--);
}

unsigned int readl(unsigned int addr)
{
    return ( *((volatile unsigned int *)addr) );
}

void writel(unsigned int addr, unsigned int value)
{
    *((volatile unsigned int *)addr) = value;
}

void led_init(void)
{
    /* LED1 */
    writel(GPJ0CON, (readl(GPJ0CON) & ~(0xf<<12)) | (0x1<<12));
    writel(GPJ0DAT, (readl(GPJ0DAT) & ~(0x1<<3)) | (0x1<<3));

    /* LED2 */
    writel(GPJ0CON, (readl(GPJ0CON) & ~(0xf<<16)) | (0x1<<16));
    writel(GPJ0DAT, (readl(GPJ0DAT) & ~(0x1<<4)) | (0x1<<4));

    /* LED3 */
    writel(GPJ0CON, (readl(GPJ0CON) & ~(0xf<<20)) | (0x1<<20));
    writel(GPJ0DAT, (readl(GPJ0DAT) & ~(0x1<<5)) | (0x1<<5));

    /* LED4 */
    writel(GPD0CON, (readl(GPD0CON) & ~(0xf<<4)) | (0x1<<4));
    writel(GPD0DAT, (readl(GPD0DAT) & ~(0x1<<1)) | (0x1<<1));
}

void led_set_led1(unsigned int value)
{
    if (value){
        writel(GPJ0DAT, (readl(GPJ0DAT) & ~(0x1<<3)) | (0x0<<3));//ON
    }else{
        writel(GPJ0DAT, (readl(GPJ0DAT) & ~(0x1<<3)) | (0x1<<3));//OFF
    }
}

void led_set_led2(unsigned int value)
{
    if (value){
        writel(GPJ0DAT, (readl(GPJ0DAT) & ~(0x1<<4)) | (0x0<<4));//ON
    }else{
        writel(GPJ0DAT, (readl(GPJ0DAT) & ~(0x1<<4)) | (0x1<<4));//OFF
    }
}

void led_set_led3(unsigned int value)
{
    if (value){
        writel(GPJ0DAT, (readl(GPJ0DAT) & ~(0x1<<5)) | (0x0<<5));//ON
    }else{
        writel(GPJ0DAT, (readl(GPJ0DAT) & ~(0x1<<5)) | (0x1<<5));//OFF
    }
}

void led_set_led4(unsigned int value)
{
    if (value){
        writel(GPD0DAT, (readl(GPD0DAT) & ~(0x1<<1)) | (0x0<<1));//ON
    }else{
        writel(GPD0DAT, (readl(GPD0DAT) & ~(0x1<<1)) | (0x1<<1));//OFF
    }
}

void tester_led(void)
{
    led_init();

    while(1){
        led_set_led1(1);
        delay(900000);
        led_set_led1(0);
        led_set_led2(1);
        delay(900000);
        led_set_led2(0); 
        led_set_led3(1);
        delay(900000);
        led_set_led3(0); 
        led_set_led4(1);
        delay(900000);
        led_set_led1(1);
        led_set_led2(1);
        led_set_led3(1);
        led_set_led4(1);
        delay(900000);
        led_set_led1(0);
        led_set_led2(0);
        led_set_led3(0);
        led_set_led4(0);
        delay(900000);
    }
}

int main(int argc, char* argv[])
{
    tester_led();

    return 0;
}
