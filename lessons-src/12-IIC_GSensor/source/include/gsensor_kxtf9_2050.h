/***********************************************************
 *   > File Name: gsensor_kxtf9_2050.h
 *   > Author: fly
 *   > Create Time: 2021-07-22  4/29  20:24:21 +0800
 *========================================================*/
#ifndef __GSENSOR_KXTF9_2050_H__
#define __GSENSOR_KXTF9_2050_H__

#define WRITE_COMMAND_FOR_KXTF9         0x1e
#define READ_COMMAND_FOR_KXTF9          0x1f

#define CT_RESP         0x0C
#define WHO_AM_I        0x0F
#define TILT_POS_CUR    0x10
#define TILT_POS_PRE    0x11
#define XOUT            0x12
#define YOUT            0x13
#define ZOUT            0x14
#define INT_SRC_REG1    0x16
#define INT_SRC_REG2    0x17
#define STATUS_REG      0x18
#define INT_REL         0x1A
#define CTRL_REG1       0x1B
#define CTRL_REG2       0x1C
#define CTRL_REG3       0x1D
#define INT_CTRL_REG1   0x1E
#define INT_CTRL_REG2   0x1F
#define TILT_TIMER      0x28
#define WUF_TIMER       0x29
#define B2S_TIMER       0x2A
#define WUF_THRESH      0x5A
#define B2S_THRESH      0x5B
#define TILT_ANGLE      0x5C
#define HYST_SET        0x5F

#endif /* __GSENSOR_KXTF9_2050_H__ */
