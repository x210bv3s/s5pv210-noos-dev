/***********************************************************
 *   > File Name: debug.h
 *   > Author: fly
 *   > Create Time: 2021-07-22  4/29  22:28:01 +0800
 *========================================================*/
#ifndef __DEBUG_H__
#define __DEBUG_H__

#define __DBG_PRINT__   printf("[%s]<%s>(%d)\n", __FILE__, __func__, __LINE__)

#endif /* __DEBUG_H__ */
