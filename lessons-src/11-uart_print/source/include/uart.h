/*****************************************************
 *   > File Name: uart.h
 *   > Author: fly
 *   > Create Time: 2021-07-20  2/29  17:00:48 +0800
 *==================================================*/
#ifndef __UART_H__
#define __UART_H__

extern void uart0_init(void);
extern void uart0_putc(char c);
extern char uart0_getc(void);

extern void uart2_init(void);
extern void uart2_putc(char c);
extern char uart2_getc(void);

extern void putc(unsigned char c);
extern unsigned char getc(void);
extern void uart0_puts(const char *pstr);
extern void uart0_gets(char *p);

extern void tester_uart(void);

#endif /* __UART_H__ */
