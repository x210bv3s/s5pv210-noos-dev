.PHONY: test

test:
	@echo "This is test."
	@echo X_NAME:$(X_NAME)
	@echo X_INCDIRS:$(X_INCDIRS)
	@echo X_LIBDIRS:$(X_LIBDIRS)
	@echo X_SRCDIRS:$(X_SRCDIRS)
	@echo X_OBJDIRS:$(X_OBJDIRS)
	@echo X_LIBFILES:$(X_LIBFILES)
	@echo X_SFILES:$(X_SFILES)
	@echo X_CFILES:$(X_CFILES)
	@echo X_CPPFILES:$(X_CPPFILES)
	@echo X_SDEPS:$(X_SDEPS)
	@echo X_CDEPS:$(X_CDEPS)
	@echo X_CPPDEPS:$(X_CPPDEPS)
	@echo X_DEPS:$(X_DEPS)
	@echo X_SOBJS:$(X_SOBJS)
	@echo X_COBJS:$(X_COBJS)
	@echo X_CPPOBJS:$(X_CPPOBJS)
	@echo X_OBJS:$(X_OBJS)
	@echo VPATH:$(VPATH)
