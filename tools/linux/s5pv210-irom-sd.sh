###########################################################
  # File Name: s5pv210-irom-sd.sh
  # Author: fly
  # Mail: xxxxxx@icode.net
  # Created Time: 2021-06-27  0/25  14:51:59 +0800
###########################################################
#!/bin/bash

# s5pv210 irom sd boot fusing tool

# display usage message
USAGE()
{
    echo Usage: $(basename "$0") '<device> <bootloader>'
    echo '      device      = disk device name of for SD card.'
    echo '      bootloader  = /path/to/*.bin.sd'
    echo 'e.g. '$(basename "$0")' /dev/sdb boot.bin.sd'
}

[ `id -u` == 0 ] || { echo "you must be root user"; exit 1;}
[ -z "$1" -o -z "$2" ] && { USAGE; exit 1; }

dev="$1"
xboot="$2"

# validate parameters
[ -b "${dev}" ] || { echo "${dev} is not a valid block device"; exit 1; }
[ X"${dev}" = X"${dev%%[0-9]}" ] || { echo "${dev} is a partition, please use device, perhaps ${dev%%[0-9]}"; exit 1; }
[ -f ${xboot} ] || { echo "${xboot} is not a bootloader binary file."; exit 1; }

# copy the full bootloader image to block device
dd if="${xboot}" of="${dev}" bs=512 seek=1 conv=sync

sync;
sync;
sync;

echo "^_^ The image is fused successfully"
