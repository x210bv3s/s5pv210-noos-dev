.PHONY: all clean test

MKFILES		:= $(shell find -L ./ -name Makefile)
MKDIRS		:= ${patsubst %Makefile, %, $(MKFILES)}

all:
	@echo "Build all."

clean:$(MKDIRS)

$(MKDIRS):%:$(MKFILES)
	@echo "Clean all."
	make -C $@ clean

test:
	@echo "This is a test."
	@echo $(MKFILES)
	@echo $(MKDIRS)
